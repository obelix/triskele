#include <iostream>

#include "test.hpp"

string
getMsg () {
  cerr << "coucou" << endl;
  return "hello, world";
}

Mem::Mem (string msg)
  : Mem (msg, 0) {
}

Mem::Mem (string msg, int id)
  : msg (msg), id (id) {
}

Mem::~Mem () {
}

void
Mem::set (string msg) {
  this->msg = msg;
}

string
Mem::get () {
  return msg;
}
