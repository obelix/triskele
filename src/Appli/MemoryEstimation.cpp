////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "misc.hpp"
#include "Appli/MemoryEstimation.hpp"

using namespace obelix;
using namespace triskele;

// ================================================================================
MemoryEstimation::MemoryEstimation (const Size<2> &crop, const GDALDataType &pixelType,
				    const Connectivity &connectivity, const DimSortCeil &countingSortCeil,
				    const DimCore &tileNumber, const float &parentsRate, const bool &onFly)
  : crop (crop),
    pixelSize (GDALDataTypeSizeOf (pixelType)),
    neighborsNumber (getNeighborsNumber (connectivity)),
    //connectivity (connectivity),
    countingSortCeil (countingSortCeil),
    tileNumber (tileNumber),
    parentsRate (parentsRate),
    onFly (onFly),
    numberOfPixels (0),
    numberOfPixelsPerTile (0),
    numberOfParents (0),
    memChanel (0),
    memTree (0),
    memTreeSeqPeak (0),
    memTreeParPeak (0) {
  // XXX not 3d computation
  if (floor (std::log ((uint64_t) crop.side [1] * (uint64_t) crop.side [0])) / 256 > sizeof (DimImg)) {
    cerr
      << endl
      << "Error: image size too large !" << endl
      << "compile triskele with -GeoDimTAGE_40BITS option" << endl;
    throw invalid_argument ("image size too large");
  }
  // in the worst case, numberOfNodes = numberOfPixels (when ratioOfParents)
  numberOfPixels = (DimImg) crop.side [1] * (DimImg) crop.side [0]; //XXX * (DimImg) crop.length;
  numberOfPixelsPerTile = numberOfPixels / tileNumber;
  if (floor (std::log ((double)numberOfPixelsPerTile)) / 256 > sizeof (DimTile)) {
    cerr
      << endl
      << "Error: tile size too large !" << endl
      << "compile triskele with -GeoDimTAGE_40BITS option" << endl;
    throw invalid_argument ("tile size too large");
  }
  valuesSize = numberOfPixels * pixelSize;

  numberOfParents = DimNodePack (numberOfPixels * parentsRate);
}

// ================================================================================
void
MemoryEstimation::addTree (const size_t &img, const size_t &tile) {
  memTree =  max (memTree, img);
  memTreeSeqPeak = max (memTreeSeqPeak, tile);
  memTreeParPeak = max (memTreeParPeak, tileNumber*tile);
}

void
MemoryEstimation::addChanel (const size_t &mem) {
  if (onFly)
    memChanel = max (memChanel, mem);
  else
    memChanel += mem;
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const MemoryEstimation &memoryEstimation) {
  size_t base = memoryEstimation.memChanel+memoryEstimation.memTree;
  return out
    << "parallel  : " << readableBytes (base) << " (max " << readableBytes (base+memoryEstimation.memTreeParPeak) << ")"<< endl
    << "sequential: " << readableBytes (base) << " (max " << readableBytes (base+memoryEstimation.memTreeSeqPeak) << ")"<< endl;
}


// ================================================================================
