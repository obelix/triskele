////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/AreaAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "AreaAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
AreaAttributes<GeoDimT>::AreaAttributes (const Tree<GeoDimT> &tree)
  : CompAttribute<DimImgPack, GeoDimT> (tree) {
  DEF_LOG ("AreaAttributes::AreaAttributes", "");
  compute ();
}

template<int GeoDimT>
AreaAttributes<GeoDimT>::~AreaAttributes () {
  DEF_LOG ("AreaAttributes::~AreaAttributes", "");
}

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
void
AreaAttributes<GeoDimT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
			  const vector<DimImgPack> &thresholds) const {
  DEF_LOG ("AreaAttributes::cut", "");
  CA::cut (allBands, attributeProfiles, thresholds);
}

// ================================================================================
template<int GeoDimT>
void
AreaAttributes<GeoDimT>::compute () {
  DEF_LOG ("AreaAttributes::compute", "");
  auto start = high_resolution_clock::now ();

  CA::tree.forEachComp
    ([this] (const DimParent &compId) {
       CA::values[compId] = 0;
     });

  CA::tree.forEachLeaf
    ([this] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       ++CA::values [parentId];
     });
  CA::tree.forEachComp
    ([this] (const DimParent &compId) {
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       CA::values [parentId] += CA::values [compId];
     });
  TreeStats::global.addTime (areaStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
