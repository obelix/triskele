////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_OptRanges_hpp
#define _Obelix_OptRanges_hpp

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <boost/algorithm/string.hpp>

namespace obelix {
  using namespace std;
  using namespace boost;

  // ================================================================================
  class OptRanges {
  private:
    /*!
     * \brief set Vector qui correspond à l'ensemble
     */
    vector<int> set;
    /*!
     * \brief Indicateur si le max doit etre defini (à l'aide de la methode setMaxValue())
     */
    bool minFlag, maxFlag;
    int minInter, maxInter;

  public:
    /*!
     * \brief getSet Getter de l'ensemble
     * \return L'ensemble set
     */
    inline const vector<int> &getSet () const;
    /*!
     * \brief empty Vérifie si l'ensemble est vide ou non
     * \return true si l'ensemble est vide, false sinon
     */
    inline bool empty () const;
    /*!
     * \brief uncompleted indique si la définition de l'intervale est incomplète
     * \return true si l'intervale est incomplète
     */
    inline bool uncompleted () const;
    /*!
     * \brief size Retourne la taille de l'ensemble
     * \return La taille de l'ensemble
     */
    inline int size () const;
    /*!
     * \brief first Retourne la première valeur de l'ensemble
     * \return La première valeur de l'ensemble
     */
    inline int first () const;
    /*!
     * \brief last Retourne la dernière valeur de l'ensemble
     * \return La dernière valeur de l'ensemble
     */
    inline int last () const;

    /*!
     * \brief OptRanges Constructeur qui convertit la chaîne option en ensemble
     * \param option Chaîne de caractère à convertir
     */
    OptRanges (const string &option = "");
    /*!
     * \brief OptRanges Constructeur avec un interval
     */
    OptRanges (int first, int last);
    /*!
     * \brief OptRanges Constructeur de copie
     * \param optRanges Objet à copier
     */
    OptRanges (const OptRanges &optRanges);

    void reset ();
    void init (const string &option);

    void setLimits (int minOpt, int maxOpt);
    /*!
     * \brief toSet Conversion en ensemble (On retire les doublons et ordonne le vector)
     * \return L'objet OptRanges
     */
    OptRanges &toSet ();
    /*!
     * \brief contains Vérifie si la valeur value est contenue dans l'ensemble
     * \param value valeur à vérifier
     * \return true si la valeur est contenue dans l'ensemble, false sinon
     */
    bool contains (int value) const;
  };
  ostream &operator << (ostream &out, const OptRanges &optRanges);
  istream &operator >> (istream &in, OptRanges &optRanges);

  // ================================================================================
#include "OptRanges.tpp"
}//namespace obelix

#endif //_Obelix_OptRanges_hpp
