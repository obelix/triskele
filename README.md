# Triskele

Please read the [manual](https://obelix.gitlabpages.inria.fr/triskele-doc/triskele_manual.pdf)

and the [doxygene](https://obelix.gitlabpages.inria.fr/triskele/doc/index.html) ressources.

## Install docker environment

### Linux
Just get some package

```bash
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian bookworm stable"
sudo apt-get update
sudo apt-get install --fix-missing docker-ce docker-compose
```

### Windows
You need Linux environement and create a user

1) Launch PowerShell (admin privileges)

```bash
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

2) Reboot

3) Setup Linux

```bash
wsl.exe --install
wsl.exe --update
wsl --list --online
```

4) Install docker

See <https://docs.docker.com/desktop/install/windows-install/>

Then download <https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe>

And double click "Docker Desktop Installer.exe"

The docker commandes must be launch in a PowerShell.

## Use docker
```bash
docker run -v /myLocalDataBaseImages:/images -i -t registry.gitlab.inria.fr/obelix/triskele/master --help
```

## Compilation

```bash
sudo apt-get install --fix-missing build-essential cmake g++ libboost-system-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-filesystem-dev libboost-date-time-dev libboost-serialization-dev libboost-thread-dev libhdf5-dev gdal-bin libgdal-dev libtbb-dev libtinyxml-dev

git clone https://gitlab.inria.fr/obelix/triskele.git

# or for contributors :
# git clone git+ssh://git@gitlab.inria.fr/obelix/triskele.git

cd triskele ; git submodule init ; git submodule update

mkdir ../build ; cd ../build
cmake -DCMAKE_BUILD_TYPE=Release ../triskele

# or debug mode
# cmake -DCMAKE_BUILD_TYPE=Debug ../triskele

make -j `nproc`
```
