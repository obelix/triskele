////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/assert.hpp>

#include "misc.hpp"
#include "Border.hpp"
using namespace obelix;
#include "Border.tcpp"

// ================================================================================
template<int GeoDimT>
DimImg
Border<GeoDimT>::getMapLength (const DimImg &pixelsCount) {
  // XXX overflow if pixelsCount > DimImg_MAX - BorderBlockBits
  return (pixelsCount+BorderBlockBits-1)/BorderBlockBits;
}

// ================================================================================
template<int GeoDimT>
bool
Border<GeoDimT>::exists () const {
  return map.size ();
}

// ================================================================================
template<int GeoDimT>
void
Border<GeoDimT>::clearBorder (DimImg idx) {
  if (map.empty ())
    createMap ();
  BOOST_ASSERT (idx < pixelsCount);
  if (GeoDimT > 2)
    idx %= framePixelsCount;
  map[idx/BorderBlockBits] &= ~(1 << (idx%BorderBlockBits));
}

template<int GeoDimT>
void
Border<GeoDimT>::clearBorder (const Point<GeoDimT> &p) {
  clearBorder (point2idx (size, p));
}

// ================================================================================
template<int GeoDimT>
void
Border<GeoDimT>::setBorder (DimImg idx) {
  if (map.empty ())
    createMap ();
  BOOST_ASSERT (idx < pixelsCount);
  if (GeoDimT > 2)
    idx %= framePixelsCount;
  map[idx/BorderBlockBits] |= (1 << (idx%BorderBlockBits));
}

template<int GeoDimT>
void
Border<GeoDimT>::setBorder (const Point<GeoDimT> &p) {
  setBorder (point2idx (size, p));
}

// ================================================================================
template<int GeoDimT>
Border<GeoDimT>::Border ()
  : pixelsCount (0),
    framePixelsCount (0),
    mapLength (0),
    size (),
    map (),
    defaultVal (false) {
}

template<int GeoDimT>
Border<GeoDimT>::Border (const Border &border)
  : pixelsCount (border.pixelsCount),
    framePixelsCount (border.framePixelsCount),
    mapLength (border.mapLength),
    size (border.size),
    map (border.map),
    defaultVal (border.defaultVal) {
}

template<int GeoDimT>
Border<GeoDimT>::Border (const Size<GeoDimT> &size, bool defaultVal)
  : pixelsCount (size.getPixelsCount ()),
    framePixelsCount (size.getFramePixelsCount ()),
    mapLength (getMapLength (framePixelsCount)),
    size (size),
    map (),
    defaultVal (defaultVal) {
}

template<int GeoDimT>
Border<GeoDimT>::Border (const Border<GeoDimT> &border, const Rect<GeoDimT> &tile)
  : Border (tile.size, border.defaultVal) {
  if (border.map.empty ())
    return;
  // XXX todo
  // width >= 8 => for bytes
  // width < 8 => for bits
  BOOST_ASSERT (false);
}

template<int GeoDimT>
Border<GeoDimT>::~Border () {
}

// ================================================================================
template<int GeoDimT>
void
Border<GeoDimT>::reset (bool defaultVal) {
  this->defaultVal = defaultVal;
  map.clear ();
}

template<int GeoDimT>
void
Border<GeoDimT>::reduce () {
  DimImg count = borderCount ();
  if (!count)
    reset (false);
  else if (count == pixelsCount)
    reset (true);
}

template<int GeoDimT>
DimImg
Border<GeoDimT>::borderCount () const {
  if (map.empty ())
    return defaultVal ? pixelsCount : 0;
  DimImg result (0);
  for (DimImg i = 0; i < mapLength; ++i)
    result += ones64 (map[i]);
  return result;
}

template <int GeoDimT>
void
Border<GeoDimT>::printBorderDim (ostream &out) const {
  for (int i = 0; i < GeoDimT; ++i)
    if (size.side [i] > printMapMaxSide) {
      out << "/* map too big to print! */";
      return;
    }
  DimImg idx (0);
  printBorderDim (out, GeoDimT-1, idx);
}
template <int GeoDimT>
void
Border<GeoDimT>::printBorderDim (ostream &out, const int &rank, DimImg &idx) const {
  for (DimSideImg i = 0; i < size.side [rank]; ++i) {
    if (rank) {
      printBorderDim (out, rank-1, idx);
      out << endl;
    } else {
      out << (isBorder (idx) ? "  B " : "  - ");
      ++idx;
    }
  }
}

template<int GeoDimT>
void
Border<GeoDimT>::createMap () {
  map.assign (mapLength, defaultVal ? BorderBlockFull : 0);
  if (!(mapLength && defaultVal))
    return;
  map[mapLength-1U] &= BorderBlockFull >> (BorderBlockBits-(pixelsCount%BorderBlockBits))%BorderBlockBits;
}

template<int GeoDimT>
ostream &
obelix::operator << (ostream &out, const Border<GeoDimT> &border) {
  border.printBorderDim (out);
  return out;
}

// ================================================================================
