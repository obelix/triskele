////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <thread>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include <boost/thread.hpp>

#include "obelixDebug.hpp"
#include "misc.hpp"
#include "obelixThreads.hpp"
#include "TestCallOnSortedSets.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp>

using namespace std;
using namespace boost::chrono;
using namespace obelix;

static bool show (false); // log or not

const size_t nbItem = 256;
const size_t maxQueues = 12;
const size_t nbTest = 256;
const int minVal = -100;
const int maxVal = 100;

static size_t allValueCount (0), allGetCount (0), allCmpCount (0);

// ================================================================================
ostream &
obelix::operator << (ostream &out, const TestCallOnSortedSets &test) {
  return out
    << "A " << test.queuesSizes << " " << test.getCount << " " << test.cmpCount << " " << test.setCount 
    << endl << "B " << test.ref
    << endl << "C " << test.result << flush;
}

// ================================================================================
TestCallOnSortedSets::TestCallOnSortedSets (size_t queuesCount, size_t minQueueSize, size_t maxQueueSize, int minValue, int maxValue)
  : queuesCount (queuesCount),
    minQueueSize (minQueueSize),
    maxQueueSize (maxQueueSize),
    minValue (minValue),
    maxValue (maxValue),
    valueCount (0),
    getCount (0),
    cmpCount (0),
    setCount (0)
{
  BOOST_ASSERT (queuesCount >= 0);
  BOOST_ASSERT (minQueueSize >= 0);
  BOOST_ASSERT (maxQueueSize >= minQueueSize);
  BOOST_ASSERT (maxValue >= minValue);
}

// ================================================================================
void
TestCallOnSortedSets::init () {
  queues.resize (queuesCount);
  queuesSizes.resize (queuesCount);
  valueCount = 0;
  size_t deltaSize = maxQueueSize-minQueueSize;
  size_t deltaValue = maxValue-minValue;
  for (size_t i (0); i < queuesCount; ++i) {
    size_t size (rand () % deltaSize + minQueueSize);
    ref.reserve (valueCount+size);
    queuesSizes [i] = size;
    queues[i].resize (size);
    for (size_t j (0); j < size; ++j) {
      int val = rand () % deltaValue + minValue;
      queues[i][j] = val;
      ref.push_back (val);
    }
    sort (queues[i].begin (), queues[i].end ());
    valueCount += size;
  }
  sort (ref.begin (), ref.end ());
  allValueCount += valueCount;
}

// ================================================================================
void
TestCallOnSortedSets::call () {
  result.resize (0);
  result.reserve (valueCount);
  callOnSortedSets<size_t, int> (queuesSizes,
				 [this] (const size_t &vectId, const size_t &itemId) {++getCount; return queues[vectId][itemId]; },
				 [this] (const int &a, const int &b) { ++cmpCount; return a < b; },
				 [this] (const size_t &vectId, const size_t &itemId) {++setCount; result.push_back (queues[vectId][itemId]); }
				 );
  allGetCount += getCount;
  allCmpCount += cmpCount;
}

// ================================================================================
bool
TestCallOnSortedSets::checkSize () {
  return valueCount == result.size ();
}

// ================================================================================
bool
TestCallOnSortedSets::checkSet () {
  return setCount == valueCount;
}

// ================================================================================
bool
TestCallOnSortedSets::checkSort () {
  if (result.size () != ref.size ())
    return false;
  for (size_t i (0); i < result.size (); ++i)
    if (result [i] != ref [i])
      return false;
  return true;
}

// ================================================================================
TEST_CASE ("Testing Call...") {
  Log::debug = false;
  srand (time(NULL));
  for (size_t i (0); i < nbTest; ++i) {
    TestCallOnSortedSets test (rand () % maxQueues, 0, nbItem, -10, 10);
    test.init ();
    if (show)
      cerr << test << endl;
    test.call ();
    REQUIRE (test.checkSize ());
    REQUIRE (test.checkSet ());
    REQUIRE (test.checkSort ());
    if (show)
      cerr << test << endl;
  }
  if (allValueCount)
    cout << "Average get: " << (allGetCount / double (allValueCount)) << " cmp: " << (allCmpCount / double (allValueCount))  << endl;
}

// SECTION( "should pass interpretation" ) {
//   CHECK_NOTHROW(Compile(...));
//   CHECK_THROWS(Compile(...));
// }

// ================================================================================

