////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <boost/filesystem.hpp>
#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "Tree.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
//#include "AttributeProfiles.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "gdal.h"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
using namespace boost::filesystem;

static const int GeoDimT (2);

// ========================================
template<typename InPixelT>
bool
hdf5 (IImage<GeoDimT> &inputImage) {
  bool testValid(false);

  TreeType treeType = MAX;

  Raster<InPixelT, GeoDimT> raster;
  inputImage.readBand (raster, 0);

  Border<GeoDimT> border (inputImage.getSize (), false);
  GraphWalker<GeoDimT> graphWalker (border);
  ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> atb (raster, graphWalker, treeType, GeoDimT);
  const DimCore coreCount (boost::thread::hardware_concurrency ());
  Tree<GeoDimT> tree (coreCount);
  WeightAttributes<InPixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);

  //AttributeProfiles<OutPixelT> attributeProfiles (tree);
  AreaAttributes<GeoDimT> areaAttributes (tree);
  
  // Get the basename of the input image to use io as the HDF5 filename
  path inputPath(inputImage.getFileName());
  std::string basename = inputPath.stem().string();
  std::string hdf5Filename = basename + ".h5";

  cout << "Input file  : " << inputImage.getFileName() << endl;
  cout << "Output file : " << hdf5Filename << endl;

  // Save the tree and areaAttributes to an HDF5 file
  HDF5 hdf5;
  hdf5.open_write (hdf5Filename);
  tree.save (hdf5);
  areaAttributes.save (hdf5);
  hdf5.close_file ();

  // Reload the tree and areaAttributes from the HDF5 file
  Tree<GeoDimT> tree2 (coreCount);
  AreaAttributes<GeoDimT> areaAttributes2 (tree2);

  hdf5.open_read (hdf5Filename);
  tree2.load (hdf5);
  areaAttributes2.load (hdf5);
  hdf5.close_file ();

  // Check the compareTo function by comparing the tree to itself 
  // (also after h5 dump and reload)
  if (tree.compareTo(tree) && tree2.compareTo(tree)) { testValid = true; }
  return testValid;
}

// ========================================
TEST_CASE("Testing hdf5 read/write and compareTo function...") {
    // (!) Currently, only GDT_UInt16 case is tested

    // ----------------------------------------------------
    // Read an image from a file (GDT_UInt16 data type)
    // ---------------------------------------------------

    auto file = std::ifstream("data/arles.tif");
    if (!file) {
        FAIL("Failed to open input image file: data/arles.tif");
    }
    file.close();

    IImage<GeoDimT> inputImage("data/arles.tif"); // GDT_UInt16 type
    inputImage.readImage();

    // ----------------------------------------------------
    // Check hdf5 writing/reading and compareTo function
    // ----------------------------------------------------

    cout << GDALGetDataTypeName(inputImage.getDataType()) << endl; // quelle est l'erreur ?

    switch (inputImage.getDataType()) {
        case GDT_Byte:
            cout << "GDT_Byte\n";
            REQUIRE(hdf5<uint8_t>(inputImage) == true);
            break;
        case GDT_UInt16:
            cout << "GDT_UInt16\n";
            REQUIRE(hdf5<uint16_t>(inputImage) == true);
            break;
        case GDT_Int16:
            cout << "GDT_Int16\n";
            REQUIRE(hdf5<uint16_t>(inputImage) == true);
            break;
        case GDT_UInt32:
            cout << "GDT_UInt32\n";
            REQUIRE(hdf5<uint32_t>(inputImage) == true);
            break;
        case GDT_Int32:
            cout << "GDT_Int32\n";
            REQUIRE(hdf5<int32_t>(inputImage) == true);
            break;
        case GDT_Float32:
            cout << "GDT_Float32\n";
            REQUIRE(hdf5<float>(inputImage) == true);
            break;
        case GDT_Float64:
            cout << "GDT_Float64\n";
            REQUIRE(hdf5<double>(inputImage) == true);
            break;
        default:
            cerr << "unknown type!" << endl;
            FAIL("Unknown data type encountered in input image.");
    }
}