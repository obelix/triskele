////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Border_hpp
#define _Obelix_Border_hpp

#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/assert.hpp>
#include <limits>

#include "obelixGeo.hpp"
//#include "triskeleArrayTreeBase.hpp"


namespace obelix {
  using namespace std;

  typedef unsigned int BorderBlockType;
  static const size_t BorderBlockBits (sizeof (BorderBlockType)*8);
  static const size_t BorderBlockFull (numeric_limits<BorderBlockType>::max ());

  // ================================================================================
  /** Carte de pixels "no-data". La carte est a 2 dimensions même si 3 dimension sont fournies. La même propriété et reproduite sur toutes les couches. */
  template<int GeoDimT>
  class Border {
  public:
    /*! Retourne la taille que doit faire le tableau simplifié */
    static DimImg getMapLength (const DimImg &pixelsCount);

    /*! Indique qu'une carte de bordure est présente (donc potentiellement des pixels de bordures). */
    bool exists () const;

    /*! Vérifie si un index est sur la map ou une bordure, en se basant sur le bit de l'index idx de la map */
    inline bool isBorder (const DimImg &idx) const;

    /*! Vérifie si un point est sur la map ou une bordure en appelant la méthode précédente après avoir converti le point en index */
    inline bool isBorder (const Point<GeoDimT> &p) const;

    /*! Supprime toutes les occurences de bordure à l'index idx */
    void clearBorder (DimImg idx);

    /*! Supprime toutes les occurences de bordure en appelant la méthode précédente après avoir converti le point en index */
    void clearBorder (const Point<GeoDimT> &p);

    /*! Rajoute une occurence de bordure à l'index indiqué */
    void setBorder (DimImg idx);

    /*! Rajoute une occurence de bordure en appelant la méthode précédente après avoir converti le point en index */
    void setBorder (const Point<GeoDimT> &p);

    /*! Construit par défault sans aucune bordure */
    Border ();

    /*! copy */
    Border (const Border &border);

    /*! Construit Border, les valeurs de map dépendent de defaultVal */
    Border (const Size<GeoDimT> &size, bool defaultVal);
    // XXX a virer ?
    Border (const Border<GeoDimT> &border, const Rect<GeoDimT> &tile);
    ~Border ();

    void reset (bool defaultVal);

    void reduce ();

    inline const Size<GeoDimT> &getSize () const;

    /*! Compte le nombre de pixels considérés comme de la bordure */
    DimImg borderCount () const;

    void printBorderDim (ostream &out) const;
    void printBorderDim (ostream &out, const int &rank, DimImg &idx) const;

  private:
    /*! Nombre de pixels dans l'image */
    DimImg pixelsCount;

    DimImg framePixelsCount;

    /*! Taille du tableau "simplifié" des pixels */
    DimImg mapLength;

    /*! Dimensions de l'image */
    const Size<GeoDimT> size;

    /*! Tableau "simplifié" des pixels */
    vector<BorderBlockType> map;

    bool defaultVal;

    void createMap ();
  };
  template<int GeoDimT>
  ostream &operator << (ostream &out, const Border<GeoDimT> &border);

  // ================================================================================
#include "Border.tpp"

} // triskele

#endif // _Obelix_Border_hpp
