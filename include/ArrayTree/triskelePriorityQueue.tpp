////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_PriorityQueue_tpp
#define _Obelix_Triskele_PriorityQueue_tpp

// ================================================================================
template<typename WeightT>
inline void
PriorityQueue<WeightT>::Entry::_ctor (const DimImg &idxa, const DimImg &idxb) {
  // DEF_LOG ("PriorityQueue::Entry::_ctor", "idxa:" << idxa << " idxb:" << idxb);
  nextEntry = 0;
  this->idxa = idxa;
  this->idxb = idxb;
}

// ================================================================================
template<typename WeightT>
inline void
PriorityQueue<WeightT>::Queue::_ctor (const WeightT &level, const DimImg &previousQueue, const DimImg &nextQueue) {
  // DEF_LOG ("PriorityQueue::Queue::_ctor", "level:" << level << " previousQueue:" << previousQueue << " nextQueue:" << nextQueue);
  this->previousQueue = previousQueue;
  this->nextQueue = nextQueue;
  this->level = level;
  firstEntry = 0;	// dumyEntry
  lastEntry = 0;	// dumyEntry
}

template<typename WeightT>
inline void
PriorityQueue<WeightT>::Queue::pushEntry (PriorityQueue &priorityQueue, const DimImg &idxa, const DimImg &idxb) {
  // DEF_LOG ("PriorityQueue::Queue::pushEntry", "level:" << level << " idxa:" << idxa << " idxb:" << idxb);
  BOOST_ASSERT ((firstEntry && lastEntry) || (!firstEntry && !lastEntry));
  const DimImg newEntry = priorityQueue.getEntry (idxa, idxb);
  if (lastEntry) {
    BOOST_ASSERT (firstEntry); // ! dumyEntry
    lastEntry = priorityQueue.entries [lastEntry].nextEntry = newEntry;
  } else {
    BOOST_ASSERT (!firstEntry); // dumyEntry
    firstEntry = lastEntry = newEntry;
  }
  BOOST_ASSERT (firstEntry); // ! dumyEntry
  BOOST_ASSERT (lastEntry); // ! dumyEntry
}

template<typename WeightT>
inline DimImg
PriorityQueue<WeightT>::Queue::popEntry (PriorityQueue &priorityQueue) {
  // DEF_LOG ("PriorityQueue::Queue::pushEntry", "level:" << level);
  BOOST_ASSERT (firstEntry); // ! dumyEntry
  BOOST_ASSERT (lastEntry); // ! dumyEntry
  DimImg result (firstEntry);
  firstEntry = priorityQueue.entries [firstEntry].nextEntry;
  if (!firstEntry) {
    BOOST_ASSERT (result == lastEntry);
    lastEntry = 0; // dumyEntry
  }
  // LOG (result);
  return result;
}

// ================================================================================
// DimImg PriorityQueue<WeightT>::dumyEntry (0);
// DimImg PriorityQueue<WeightT>::headIdx (0);

template<typename WeightT>
inline DimImg
PriorityQueue<WeightT>::getEntry (const DimImg &idxa, const DimImg &idxb) {
  DEF_LOG ("PriorityQueue::getEntry", "idxa:" << idxa << " idxb:" << idxb);
  DimImg entryIdx (entries.get ());
  // LOG (entryIdx);
  BOOST_ASSERT (entryIdx); // ! dumyEntry
  entries [entryIdx]._ctor (idxa, idxb);
  return entryIdx;
}

template<typename WeightT>
inline DimImg
PriorityQueue<WeightT>::getQueue (const WeightT &level, const DimImg &previousQueue, const DimImg &nextQueue) {
  // DEF_LOG ("PriorityQueue::getEntry", "level:" << level << " previousQueue:" << previousQueue << " nextQueue:" << nextQueue);
  DimImg queueIdx (queues.get ());
  BOOST_ASSERT (queueIdx); // ! headIdx
  queues [queueIdx]._ctor (level, previousQueue, nextQueue);
  // LOG (queueIdx);
  return queueIdx;
}

template<typename WeightT>
inline bool
PriorityQueue<WeightT>::find (const WeightT &level) {
  // DEF_LOG ("PriorityQueue::find", "level:" << level);
  for (;;) {
    if (!currentIdx)
      return false;
    if (level == queues [currentIdx].level)
      return true;
    if (level < queues [currentIdx].level) {
      currentIdx = queues [currentIdx].previousQueue;
      continue;
    }
    if (!queues [currentIdx].nextQueue ||
	level < queues [queues [currentIdx].nextQueue].level)
      return false;
    currentIdx = queues [currentIdx].nextQueue;
  }
}

template<typename WeightT>
inline bool
PriorityQueue<WeightT>::empty () const {
  // DEF_LOG ("PriorityQueue::empty", !currentIdx);
  BOOST_ASSERT (!queues[0].level);
  return !currentIdx;
}

template<typename WeightT>
inline void
PriorityQueue<WeightT>::push (const WeightT &level, const DimImg &idxa, const DimImg &idxb) {
  // DEF_LOG ("PriorityQueue::push", "level:" << level << " idxa:" << idxa << " idxb:" << idxb);
  BOOST_ASSERT (!queues[0].level);
  if (!find (level)) {
    currentIdx = getQueue (level, currentIdx, queues[currentIdx].nextQueue);
    Queue &current (queues [currentIdx]);
    queues [current.previousQueue].nextQueue = currentIdx;
    queues [current.nextQueue].previousQueue = currentIdx;
  }
  BOOST_ASSERT (currentIdx); // ! headIdx
  queues [currentIdx].pushEntry (*this, idxa, idxb);
  BOOST_ASSERT (!queues[0].level);
}


template<typename WeightT>
inline void
PriorityQueue<WeightT>::pop (WeightT &level, DimImg &idxa, DimImg &idxb) {
  DEF_LOG ("PriorityQueue::push", "level:" << level);
  BOOST_ASSERT (currentIdx);
  BOOST_ASSERT (!queues[0].level);
  find (level);
  if (!currentIdx)
    currentIdx = queues [currentIdx].nextQueue;
  BOOST_ASSERT (currentIdx);
  level = queues [currentIdx].level;
  const DimImg entryIdx (queues [currentIdx].popEntry (*this));
  idxa = entries [entryIdx].idxa;
  idxb = entries [entryIdx].idxb;
  BOOST_ASSERT (entryIdx);
  entries.give (entryIdx);
  // LOG ("level:" << level << " idxa:" << idxa << " idxb:" << idxb);
  if (queues [currentIdx].firstEntry)
    return;
  BOOST_ASSERT (currentIdx);
  Queue &current (queues [currentIdx]);
  queues [current.previousQueue].nextQueue = current.nextQueue;
  queues [current.nextQueue].previousQueue = current.previousQueue;
  const DimImg oldQueue (currentIdx);
  currentIdx = queues [currentIdx].previousQueue;
  if (!currentIdx)
    currentIdx = queues [oldQueue].nextQueue;
  BOOST_ASSERT (oldQueue);
  queues.give (oldQueue);
  BOOST_ASSERT (!queues[0].level);
}

// ================================================================================

#endif // _Obelix_Triskele_PriorityQueue_tpp
