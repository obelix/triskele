////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>

using namespace boost::chrono;
#include "misc.hpp"

#include "ArrayTree/Weight.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "ArrayTreeBuilderBuildTemp.tcpp"

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::buildTree (Tree<GeoDimT> &tree, WeightAttributes<WeightT, GeoDimT> &weightAttributes, const bool &timeFlag) {
  DEF_LOG ("ArrayTreeBuilder::buildTree",  "size:" << graphWalker.getSize ().getPixelsCount ());
  TreeBuilder<GeoDimT>::buildTree (tree);
  TreeBuilder<GeoDimT>::setTreeSize (graphWalker.getSize ());
  parentCapacity = 0;

  if (!TreeBuilder<GeoDimT>::leafCount)
    return;
  SMART_LOG_EXPR (weightAttributes.resize (TreeBuilder<GeoDimT>::leafCount));
  pCompWeights = &weightAttributes;
  compWeights = pCompWeights->getValues ();
  SMART_LOG_EXPR (dealThreadFill_n (TreeBuilder<GeoDimT>::leafCount-1U, coreCount, compWeights, 0));

  auto start = high_resolution_clock::now ();
  switch (treeType) {
  case MIN:
    buildTreeType (tree, WeightFunction<PixelT, WeightT, GeoDimT, MIN> (raster.getPixels (), graphWalker), timeFlag);
    break;
  case MAX:
    buildTreeType (tree, WeightFunction<PixelT, WeightT, GeoDimT, MAX> (raster.getPixels (), graphWalker), timeFlag);
    break;
  case MED:
    buildTreeType (tree, WeightFunction<PixelT, WeightT, GeoDimT, MED> (raster.getPixels (), graphWalker), timeFlag);
    break;
  case TOS:
    buildTreeType (tree, WeightFunction<PixelT, WeightT, GeoDimT, TOS> (raster.getPixels (), graphWalker), timeFlag);
    break;
  case ALPHA:
    buildTreeType (tree, WeightFunction<PixelT, WeightT, GeoDimT, ALPHA> (raster.getPixels (), graphWalker), timeFlag);
    break;
  default:
    cerr << "*** unknown tree type: " << treeType << endl;
  }
  auto startChildren = high_resolution_clock::now ();

  if (needChildren)
    buildChildren ();
  childCount = nullptr;

  auto end = high_resolution_clock::now ();
  TreeStats::global.addTime (buildTreeStats, duration_cast<duration<double> > (startChildren-start).count ());
  if (needChildren)
    TreeStats::global.addTime (buildChildrenStats, duration_cast<duration<double> > (end-start).count ());
  TreeStats::global.addDim (treeType, TreeBuilder<GeoDimT>::leafCount, TreeBuilder<GeoDimT>::compCount);

  weightAttributes.setWeightBounds (tree);

  SMART_LOG (tree.printTree ());
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::connectLeaf (DimImg a, DimImg b, const WeightT &weight, DimParent &parCount, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  SMART_DEF_LOG ("ArrayTreeBuilder::connectLeaf", "a:" << a << " b:" << b << " weight:" << weight);
  BOOST_ASSERT (a < TreeBuilder<GeoDimT>::leafCount);
  BOOST_ASSERT (b < TreeBuilder<GeoDimT>::leafCount);
  BOOST_ASSERT (DimImg (TreeBuilder<GeoDimT>::leafParents[a]) < TreeBuilder<GeoDimT>::leafCount || TreeBuilder<GeoDimT>::leafParents[a] == DimParent_MAX);
  BOOST_ASSERT (DimImg (TreeBuilder<GeoDimT>::leafParents[b]) < TreeBuilder<GeoDimT>::leafCount || TreeBuilder<GeoDimT>::leafParents[b] == DimParent_MAX);
  DimParent parA = findTopComp (TreeBuilder<GeoDimT>::leafParents[a], weight, weightFunct);
  DimParent parB = findTopComp (TreeBuilder<GeoDimT>::leafParents[b], weight, weightFunct);
  SMART_LOG ("parA: " << printComp (parA) << " parB: " << printComp (parB));
  // upW0 : no parent(s)
  // Border case if parX == DimParent_MAX
  if (parA == parB) {
    if (parA == DimParent_MAX) {
      SMART_LOG ("upW0: no parents");
      createImgComp (parCount, weight, TreeBuilder<GeoDimT>::leafParents [a], TreeBuilder<GeoDimT>::leafParents [b]);
      SMART_LOG_EXPR (cerr << "CV P" << a << " N" << printComp (parCount-1U) << endl);
      SMART_LOG_EXPR (cerr << "CV P" << b << " N" << printComp (parCount-1U) << endl);
      SMART_LOG ("createImgComp: " << printComp (TreeBuilder<GeoDimT>::leafParents[a]));
    }
    SMART_LOG ("same parents for: " << a << ", " << b);
    return;
  }
  if (parB == DimParent_MAX) {
    // XXX 20180776 bug fixed parA => parB
    swap (a, b);
    swap (parA, parB);
    SMART_LOG ("swap: " << printComp (parA) << " " << printComp (parB));
  }
  // XXX parB =? DimParent_MAX
  if ((parA == DimParent_MAX || weightFunct.isWeightInf (weight, compWeights[parA])) &&
      weightFunct.isWeightInf (weight, compWeights[parB])) {
    // upW1 & upW2 : upper
    SMART_LOG ("upW1 | upW2: upper");
    unlinkParent (parA);
    unlinkParent (parB);
    SMART_LOG ("parA: " << printComp (parA) << " parB: " << printComp (parB));
    DimParent newComp = createImgComp (parCount, weight, TreeBuilder<GeoDimT>::leafParents [a], TreeBuilder<GeoDimT>::leafParents [b]);
    SMART_LOG_EXPR (cerr << "CV P" << a << " N" << printComp (parCount-1U) << endl);
    SMART_LOG_EXPR (cerr << "CV P" << b << " N" << printComp (parCount-1U) << endl);
    SMART_LOG ("createImgComp: " << printComp (newComp));
    connect3Comp (newComp, parA, parB, weightFunct);
    return;
  }
  if (parA != DimParent_MAX && weightFunct.isWeightInf (compWeights[parA], compWeights[parB])) {
    swap (a, b);
    swap (parA, parB);
    SMART_LOG ("swap: " << printComp (parA) << " " << printComp (parB));
  }
  if (weightFunct.isWeightInf (compWeights[parB], weight) &&
      (parA == DimParent_MAX ||
       weightFunct.isWeightInf (compWeights[parA], weight) ||
       weightFunct.isWeightInf (weight, compWeights[parA]))) {
    // loW0 | loW1 | loW2 : lower
    SMART_LOG ("loW0 | loW1 | loW2");
    if (parA != DimParent_MAX && weightFunct.isWeightInf (compWeights[parA], weight)) {
      swap (a, b);
      swap (parA, parB);
      SMART_LOG ("swap: " << printComp (parA) << " " << printComp (parB));
    }
    DimParent grandParB = findTopComp (TreeBuilder<GeoDimT>::compParents[parB], weightFunct);
    unlinkParent (grandParB);
    SMART_LOG ("grandParB: " << printComp (grandParB));
    if (parA == DimParent_MAX || weightFunct.isWeightInf (weight, compWeights[parA])) {
      // loW1 | loW2
      SMART_LOG ("loW1 | loW2: lower");
      unlinkParent (parA);
      SMART_LOG ("parA: " << printComp (parA));
      DimParent newComp = createImgComp (parCount, weight, TreeBuilder<GeoDimT>::leafParents [a], TreeBuilder<GeoDimT>::compParents [parB]);
      SMART_LOG_EXPR (cerr << "CV P" << a << " N" << printComp (parCount-1U) << endl);
      SMART_LOG_EXPR (cerr << "CV N" << parB << " N" << printComp (parCount-1U) << endl);
      SMART_LOG ("createImgComp: " << printComp (newComp));
      connect3Comp (newComp, parA, grandParB, weightFunct);
      return;
    }
    // loW0
    SMART_LOG ("loW0: lower");
    DimParent grandParA = findTopComp (TreeBuilder<GeoDimT>::compParents[parA], weightFunct);
    unlinkParent (grandParA);
    SMART_LOG ("grandParA: " << printComp (grandParA));
    DimParent newComp = createImgComp (parCount, weight, TreeBuilder<GeoDimT>::compParents [parA], TreeBuilder<GeoDimT>::compParents [parB]);
    SMART_LOG_EXPR (cerr << "CV N" << parA << " N" << printComp (parCount-1U) << endl);
    SMART_LOG_EXPR (cerr << "CV N" << parB << " N" << printComp (parCount-1U) << endl);
    SMART_LOG ("createImgComp: " << printComp (newComp));
    connect3Comp (newComp, grandParA, grandParB, weightFunct);
    return;
  }
  // eqW0 | eqW1 | eqW2 : no creation
  if (parA == DimParent_MAX || weightFunct.isWeightInf (weight, compWeights[parA])) {
    // eqW1 | eqW2
    SMART_LOG ("eqW1 | eqW2: incr leaf");
    unlinkParent (parA);
    ++childCount[parB];
    TreeBuilder<GeoDimT>::leafParents[a] = parB;
    SMART_LOG ("link a: " << a << " parB: " << printComp (parB));
  }
  // eqW0 | eqW1 | eqW2
  SMART_LOG ("eqW0 | eqW1 | eqW2: connect");
  connectComp (parA, parB, weightFunct);
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::connect3Comp (DimParent newComp, DimParent topA, DimParent topB, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  SMART_DEF_LOG ("ArrayTreeBuilder::connect3Comp", "newComp:" << newComp << " topA:" << topA << " topB:" << topB);
  BOOST_ASSERT (newComp != DimParent_MAX);

  if (topB == DimParent_MAX)
    swap (topA, topB);
  if (topB == DimParent_MAX)
    return;
  BOOST_ASSERT (topB != DimParent_MAX);
  if (topA != DimParent_MAX && weightFunct.isWeightInf (compWeights[topA], compWeights[topB]))
    swap (topA, topB);
  BOOST_ASSERT (findTopComp (topB, weightFunct) == topB);
  BOOST_ASSERT (weightFunct.isWeightInf (compWeights[newComp], compWeights[topB]));
  addChild (topB, TreeBuilder<GeoDimT>::compParents[newComp]);
  SMART_LOG_EXPR (cerr << "CV N" << newComp << " N" << printComp (topB) << endl);
  SMART_LOG ("topB: " << printComp (topB));
  connectComp (topA, topB, weightFunct);
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::connectComp (DimParent topA, DimParent topB, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  SMART_DEF_LOG ("ArrayTreeBuilder::connectComp", "topA:" << topA << " topB:" << topB);

  for (;;) {
    if (topA == DimParent_MAX || topB == DimParent_MAX)
      return;
    BOOST_ASSERT (findTopComp (topA, weightFunct) == topA);
    BOOST_ASSERT (findTopComp (topB, weightFunct) == topB);
    if (weightFunct.isWeightInf (compWeights[topA], compWeights[topB]))
      swap (topA, topB);
    topB = findTopComp (topB, compWeights[topA], weightFunct);
    if (topA == topB)
      return;
    if (compWeights[topA] == compWeights[topB] &&
	childCount[topA] < childCount[topB])
      swap (topA, topB);
    DimParent grandParB = findTopComp (TreeBuilder<GeoDimT>::compParents[topB], weightFunct);
    if (compWeights[topA] == compWeights[topB]) {
      childCount[topA] += childCount[topB];
      childCount[topB] = 0;
    } else
      ++childCount[topA];
    TreeBuilder<GeoDimT>::compParents[topB] = topA;
    SMART_LOG ("topA: " << printComp (topA) << " topB: " << printComp (topB));
    if (grandParB == DimParent_MAX)
      return;
    BOOST_ASSERT (childCount [grandParB]);
    --childCount[grandParB];
    SMART_LOG ("grandParB: " << printComp (grandParB));
    topB = topA;
    topA = findTopComp (grandParB, compWeights[topA], weightFunct);
  }
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::updateNewId (DimParentPack *newCompId, const vector<DimParent> &compBases, const vector<DimParent> &compTops,
							 const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  DEF_LOG ("ArrayTreeBuilder::updateNewId", "");
  DimParent compCount = compBases[0];
  vector<DimParent> sizes (compBases.size ());
  for (DimParent i = 0; i < sizes.size (); ++i)
    sizes [i] = compTops [i] - compBases [i];
  // XXX non parallèle
  callOnSortedSets<DimParent, WeightT> (sizes,
					[this, &compBases, &newCompId] (const DimParent &vectId, const DimParent &itemId) {
					  return compWeights[compBases[vectId]+itemId]; },
					weightFunct.isWeightInf,
					[this, &newCompId, &compBases, &compCount] (const DimParent &vectId, const DimParent &itemId) {
					  updateNewId (newCompId, compBases[vectId]+itemId, compCount); });
  LOG ("after call B");
  return compCount;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::findTopComp (const DimParent &comp, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) const {
  if (comp == DimParent_MAX)
    return DimParent_MAX;
  DimParent result = findTopComp (comp, compWeights[comp], weightFunct);
  BOOST_ASSERT (result < TreeBuilder<GeoDimT>::leafCount);
  // XXX pas pour MED ! BOOST_ASSERT (compWeights[result] == compWeights[comp]);
  return result;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::findTopComp (DimParent comp, const WeightT &weight, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) const {
  if (comp == DimParent_MAX)
    return DimParent_MAX;
  if (weightFunct.isWeightInf (weight, compWeights [comp]))
    return findTopComp (comp, compWeights [comp], weightFunct);
  DimParent last = comp;
  for (;;) {
    if (comp == DimParent_MAX)
      return last;
    if (weightFunct.isWeightInf (weight, compWeights [comp]))
      return last;
    last = comp;
    comp = TreeBuilder<GeoDimT>::compParents[comp];
  }
  // XXX never execute
}

// ================================================================================
