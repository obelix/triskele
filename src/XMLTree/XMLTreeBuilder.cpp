////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "XMLTree/XMLTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;

template<int GeoDimT>
void
XMLTreeBuilder<GeoDimT>::buildTree (Tree<GeoDimT> &tree) {
  TiXmlDocument doc (fileName.c_str ());
  if (!doc.LoadFile ())
    return;

  const TiXmlElement *treeNode = doc.FirstChild ("Tree")->ToElement ();
  int width, height;
  if (treeNode->QueryIntAttribute ("width", &width) == TIXML_SUCCESS &&
      treeNode->QueryIntAttribute ("height", &height) == TIXML_SUCCESS)
    setTreeSize (Size<GeoDimT> (width, height, 1));
  else {
    if (treeNode->QueryIntAttribute ("leafCount", &width) != TIXML_SUCCESS)
      return;
    setTreeSize (Size<GeoDimT> (width, 1, 1));
  }

  DimImg nodeCount = getNodeCount (treeNode);
  TreeBuilder<GeoDimT>::setCompCount (nodeCount);

  DimNode rootParent = nodeCount;
  TreeBuilder<GeoDimT>::compParents[--rootParent] = nodeCount;
  readNodeChildren (treeNode->FirstChild ()->ToElement (), rootParent);

  std::partial_sum (TreeBuilder<GeoDimT>::childrenStart, TreeBuilder<GeoDimT>::childrenStart+nodeCount+2, TreeBuilder<GeoDimT>::childrenStart);
  for (DimImg i = 0; i < tree.getLeafCount (); ++i) {
    DimParent idP = TreeBuilder<GeoDimT>::leafParents[i];
    TreeBuilder<GeoDimT>::children[size_t (TreeBuilder<GeoDimT>::childrenStart[size_t (idP)+1]++)] = i;
  }
  for (DimImg i = tree.getLeafCount (); i < tree.getLeafCount ()+nodeCount-1; ++i) {
    DimParent idP = TreeBuilder<GeoDimT>::compParents[i-tree.getLeafCount ()];
    TreeBuilder<GeoDimT>::children[size_t (TreeBuilder<GeoDimT>::childrenStart[size_t (idP)+1]++)] = i;
  }
}

template<int GeoDimT>
DimImg
XMLTreeBuilder<GeoDimT>::getNodeCount (const TiXmlElement *node) {
  DimImg nodeCount = 0;
  for (const TiXmlNode *child = node->FirstChild ("Node"); child; child = child->NextSibling ("Node")) {
    nodeCount++;
    nodeCount += getNodeCount (child->ToElement ());
  }
  return nodeCount;
}

template<int GeoDimT>
void
XMLTreeBuilder<GeoDimT>::readNodeChildren (const TiXmlElement *node, DimNode &id) {
  DimNode idP = id;
  for (const TiXmlNode *child = node->FirstChild (); child; child = child->NextSibling ()) {
    if (std::string (child->Value ()) == "Node") {
      TreeBuilder<GeoDimT>::compParents[--id] = idP;
      std::cout << id << std::endl;
      readNodeChildren (child->ToElement (), id);
      TreeBuilder<GeoDimT>::childrenStart[idP+2]++;
    } else if (child->Value () == std::string ("Leaf")) {
      int leafId;
      child->ToElement ()->QueryIntAttribute ("id", &leafId);
      TreeBuilder<GeoDimT>::leafParents[leafId] = idP;
      TreeBuilder<GeoDimT>::childrenStart[idP+2]++;
    }
  }
}

template<int GeoDimT>
void
XMLTreeBuilder<GeoDimT>::exportToFile (const Tree<GeoDimT> &tree, const std::string &fileName) {
  TiXmlDocument doc;
  TiXmlDeclaration *decl = new TiXmlDeclaration ( "1.0", "", "" );
  doc.LinkEndChild(decl);

  TiXmlElement *treeNode = new TiXmlElement ("Tree");
  doc.LinkEndChild (treeNode);
  treeNode->SetAttribute ("leafCount", tree.getLeafCount ());

  if (tree.getSize ().side [0] != 0)
    treeNode->SetAttribute ("width", tree.getSize ().side [0]);
  if (tree.getSize().side [1] != 0)
    treeNode->SetAttribute ("height", tree.getSize ().side [1]);

  // Construct the tree
  writeNodeChildren (tree, tree.getRootId (), treeNode);
  doc.SaveFile (fileName.c_str ());
}

template<int GeoDimT>
void
XMLTreeBuilder<GeoDimT>::writeNodeChildren (const Tree<GeoDimT> &tree, const DimNode &id, TiXmlElement *node) {
  if (id < tree.getLeafCount ()) {
    TiXmlElement *leafNode = new TiXmlElement ("Leaf");
    node->LinkEndChild (leafNode);
    leafNode->SetAttribute ("id", id);
    return;
  }
  TiXmlElement *nodeNode = new TiXmlElement ("Node");
  node->LinkEndChild (nodeNode);

  tree.forEachChild (id - tree.getLeafCount (),
  		     [&tree, &nodeNode] (const DimNode &childId) {
  		       writeNodeChildren (tree, childId, nodeNode);
  		     });
}
