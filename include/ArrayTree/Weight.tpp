////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Weight_tpp
#define _Obelix_Triskele_Weight_tpp

// ================================================================================
template<typename WeightT>
inline ostream&
printWeight (ostream &out, const WeightT &val) {
  return out << val;
}

template<>
inline ostream&
printWeight (ostream &out, const uint8_t &val) {
  return out << (int) val;
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT *
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getPixels () const {
  return pixels;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const Size<GeoDimT> &
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getSize () const {
  return size;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT &
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getValue (const DimImg &idx) const {
  return pixels[idx];
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getDecr () const {
  return false;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT &
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getMaxPixel () const {
  return maxPixel;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT &
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getHalfPixel () const {
  return halfPixel;
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunctionBase<PixelT, WeightT, GeoDimT>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a < b;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunctionBase<PixelT, WeightT, GeoDimT>::isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b) {
  return isWeightInf (a.getWeight(), b.getWeight());
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunctionBase<PixelT, WeightT, GeoDimT>::isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::getWeight (const DimImg &idx) const {
  return WB::getValue (idx);
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::getWeight (const DimImg &a, const DimImg &b) const {
  return std::max (getWeight (a), getWeight (b));
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::getDecr () const {
  return true;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a > b;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b) {
  return isWeightInf (a.getWeight(), b.getWeight());
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::getWeight (const DimImg &idx) const {
  return WB::getValue (idx);
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::getWeight (const DimImg &a, const DimImg &b) const {
  return std::min (getWeight (a), getWeight (b));
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getDecr () const {
  return true;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MED>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a > b;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MED>::isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b) {
  return isWeightInf (a.weight, b.weight);
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, MED>::isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT &
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getMedian () const {
  return median;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const PixelT &
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getThresholdPixel () const {
  return thresholdPixel;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline const WeightT &
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getThresholdWeight () const {
  return thresholdWeight;
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MED>::value2weight (const PixelT &val) const {
  if (median < WB::halfPixel) {
    if (val >= thresholdPixel)
      return val;
    return val < median ? (median-val)*2 - 1 : (val-median)*2;
  }
  if (val < thresholdPixel)
    return WB::maxPixel - val;
  return val < median ? (median-val)*2 - 1 : (val-median)*2;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline PixelT
WeightFunction<PixelT, WeightT, GeoDimT, MED>::weight2value (const WeightT &weight) const {
  if (median < WB::halfPixel) {
    if (weight >= thresholdWeight)
      return weight;
    return int (weight) % 2 ? median - 1U - PixelT (weight)/2U : median + PixelT(weight)/2U;
  }
  if (weight > thresholdWeight)
    return WB::maxPixel - PixelT (weight);
  return int (weight) % 2 ? median - PixelT (weight)/2U : median + PixelT (weight)/2U;
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getWeight (const DimImg &idx) const {
  return value2weight (WB::getValue (idx));
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getWeight (const DimImg &a, const DimImg &b) const {
  return std::min (getWeight (a), getWeight (b));
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::getWeight (const DimImg &idx) const {
  return 0;
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::getWeight (const DimImg &a, const DimImg &b) const {
  PixelT va = WB::getValue (a), vb = WB::getValue (b);
  return std::max (va, vb) - std::min (va, vb);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
inline bool
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a > b;
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::value2weight (const PixelT &val) const {
  if (median < WB::halfPixel) {
    if (val >= thresholdPixel)
      return val;
    return val < median ? (median-val)*2 - 1 : (val-median)*2;
  }
  if (val < thresholdPixel)
    return WB::maxPixel - val;
  return val < median ? (median-val)*2 - 1 : (val-median)*2;
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::getWeight (const DimImg &idx) const {
  return value2weight (WB::getValue (idx));
}

template <typename PixelT, typename WeightT, int GeoDimT>
inline WeightT
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::getWeight (const DimImg &a, const DimImg &b) const {
  return std::min (getWeight (a), getWeight (b));
}

// ================================================================================

#endif // _Obelix_Triskele_Weight_tpp
