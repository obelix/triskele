////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_ArrayTreeBuilder_hpp
#define _Obelix_Triskele_ArrayTreeBuilder_hpp

#ifdef ENABLE_LOG
#include <iostream>
#endif // ENABLE_LOG

#include <vector>

#include "obelixGeo.hpp"
#include "triskeleSort.hpp"
#include "obelixThreads.hpp"
#include "Appli/MemoryEstimation.hpp"
#include "TreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "IImage.hpp"
#include "GraphWalker.hpp"
#include "Weight.hpp"
#include "Leader.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    template <typename PixelT, typename WeightT, int GeoDimT>
    class ArrayTreeBuilder : public TreeBuilder<GeoDimT> {

      // ----------------------------------------
      class ArrayTreeTile {
      public:
	const GraphWalker<GeoDimT>	&graphWalker;
	Rect<GeoDimT>		crop;
	DimTile			leafCount;
	Leader			leaders;
	DimTile			tileTop;
	vector<DimTile>		tParents;
	DimTile			*leafParents;
	DimTile			*compParents;
	vector<WeightT>		tWeights;
	WeightT			*weights;
	vector<DimTile>		tChildCount;
	DimTile			*childCount;
	double			getEdgesTime;
	double			memPeak;

	/**/	ArrayTreeTile (const GraphWalker<GeoDimT> &graphWalker);
	void	init (const Rect<GeoDimT> &crop);
	void	dynTileAlloc ();
	void	free ();
	DimTile	findRoot (DimTile comp) const;
	void	createTileComp (const WeightT &weight, DimTile &parentChildA, DimTile &parentChildB);
	void	addChild (const DimTile &parent, DimTile &child);
	void	addChildren (const DimTile &parent, const DimTile &sibling);
	void	move (const DimParent &offset, DimParentPack *iParents, DimImgPack *iChildCount, WeightT *iWeights) const;

	template<TreeType TreeTypeT>
	void	buildParents (const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

	// nice ostream
	struct CPrintTileComp {
	  const ArrayTreeTile &att;
	  const DimTile &compId;
	  CPrintTileComp (const ArrayTreeTile &att, const DimTile &compId);
	  ostream &print (ostream &out) const;
	};
	inline CPrintTileComp printTileComp (const DimTile &compId) const { return CPrintTileComp (*this, compId); }
	friend inline ostream &operator << (ostream &out, const CPrintTileComp &lc) { return lc.print (out); }
      };
      // ----------------------------------------

    protected:
      DimCore					coreCount, threadPerImage, tilePerThread;
      bool					seqTile;
      const bool				autoThreadFlag;
      const Raster<PixelT, GeoDimT>		&raster;
      const GraphWalker<GeoDimT>		&graphWalker;
      const TreeType				treeType;
      const bool				needChildren;

      // transcient
      DimImgPack				*childCount;
      WeightAttributes<WeightT, GeoDimT>	*pCompWeights;
      WeightT					*compWeights;
      DimParent					parentCapacity;
    public:
      ArrayTreeBuilder (const Raster<PixelT, GeoDimT> &raster, const GraphWalker<GeoDimT> &graphWalker,
			const TreeType &treeType,
			const bool &autoThreadFlag = false,
			const bool &needChildren = false);

      ArrayTreeBuilder (const Raster<PixelT, GeoDimT> &raster, const GraphWalker<GeoDimT> &graphWalker,
			const TreeType &treeType, const DimCore &threadPerImage, const DimCore &tilePerThread,
			const bool &seqTile = false, const bool &autoThreadFlag = false,
			const bool &needChildren = false);
      ~ArrayTreeBuilder ();

      void	buildTree (Tree<GeoDimT> &tree, WeightAttributes<WeightT, GeoDimT> &weightAttributes, const bool &timeFlag = false);
      void	setAttributProfiles (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles);
    protected:
      template<TreeType TreeTypeT>
      void	buildTreeType (Tree<GeoDimT> &tree, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct, const bool &timeFlag = false);

      template<TreeType TreeTypeT>
      void setAttributProfiles (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

      template<TreeType TreeTypeT>
      void	connectLeaf (DimImg a, DimImg b, const WeightT &weight, DimParent &parCount, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

      void	unlinkParent (const DimParent &par);

      template<TreeType TreeTypeT>
      void	connect3Comp (DimParent newComp, DimParent topA, DimParent topB, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

      template<TreeType TreeTypeT>
      void	connectComp (DimParent topA, DimParent topB, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

      template<TreeType TreeTypeT>
      DimParent	updateNewId (DimParentPack *newCompId, const vector<DimParent> &compBases, const vector<DimParent> &compTops, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct);

      void	updateNewId (DimParentPack *newCompId, const DimParent curComp, DimParent &compCount);

      void	compress (DimParentPack *newCompId, const DimParent &compTop);

      void	dynImgAlloc (DimParent currentMax);
      DimParent	createImgComp (DimParent &topParent, const WeightT &weight, DimParentPack &childA, DimParentPack &childB);

      void	addChild (const DimParent &parent, DimParentPack &child);

      DimParent	findRoot (DimParent comp) const;

      template<TreeType TreeTypeT>
      DimParent	findTopComp (const DimParent &comp, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) const;

      template<TreeType TreeTypeT>
      DimParent
      findTopComp (DimParent comp, const WeightT &weight, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) const;

      DimParent	findNotLonelyTop (DimParent comp) const;
      DimParent	findMultiChildrenTop (DimParentPack *newCompId, DimParent comp) const;

      void	buildChildren ();

      // nice ostream
      struct CPrintComp {
	const ArrayTreeBuilder &atb;
	const DimParent &compId;
	CPrintComp (const ArrayTreeBuilder &atb, const DimParent &compId);
	ostream &print (ostream &out) const;
      };

      inline CPrintComp printComp (const DimParent &compId) const { return CPrintComp (*this, compId); }
      friend inline ostream &operator << (ostream &out, const CPrintComp &lc) { return lc.print (out); }
    };

    void ArrayTreeBuilderMemoryImpact (MemoryEstimation &memoryEstimation);

    // ================================================================================
  } // triskele
} // obelix

#endif //  _Obelix_Triskele_ArrayTreeBuilder_hpp
