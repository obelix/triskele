////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_HDF5_hpp
#define _Obelix_Triskele_HDF5_hpp

/**
 * F. Raimbault
 * november 2019
 *
 * Utility functions to write and read scalars and arrays on HDF5
 */

#include <vector>
#include <string>
#include "H5Cpp.h"

namespace obelix {
  namespace triskele {
    using namespace std;
    using namespace H5;

    class HDF5 {
    public:

      int open_write (string filename);
      int open_read_write (string filename);
      int open_read (string filename);
      int set_compression_level (unsigned int level = 5);
      int set_cache_size (size_t size = 1024);
      int close_file ();

      vector<string> list () const;

      template<typename T>
      int write_array (const vector<T> &arrayvect, const string &arrayname, const string &arraygroup);
      template<typename T>
      int read_array (vector<T> &arrayvect, const string &arrayname, const string &arraygroup) const;

      int write_string (const string &msg, const string &msgname, const string &msggroup);
      int read_string (string &msg, const string &msgname, const string &msggroup) const;

    private:
      H5std_string h5_filename; // filename of the HDF5 file
      H5::H5File h5_file; // the current HDF5 file descriptor
      const int h5_rank = 1; // rank of arrays inside dataset
      unsigned int h5_compression_level = 0; // [0..9] Lower compression levels are faster but result in less compression.
      size_t h5_cache_size = 1024;
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_HDF5_hpp
