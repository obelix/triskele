#! /usr/bin/python3

from libtest import getMsgPy, MemPy

print (getMsgPy ())

word = MemPy ('A')
word.id = 1;
print (f"m:{word.getPy ()} id:{word.idPy}")
word.setPy ('B')
print (f"m:{word.getPy ()} id:{word.idPy}")

word2 = MemPy ('C', 1)
print (f"m:{word2.getPy ()} id:{word2.idPy}")

word2.msgPy = word.msgPy+"2"
print (word2.msgPy)
