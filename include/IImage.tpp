////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_IImage_tpp
#define _Obelix_IImage_tpp

// ================================================================================
template<typename PixelT, int GeoDimT>
inline const Size<GeoDimT> &
Raster<PixelT, GeoDimT>::getSize () const {
  return size;
}

template<typename PixelT, int GeoDimT>
inline const PixelT *
Raster<PixelT, GeoDimT>::getPixels () const {
  return pixels.data ();
}

template<typename PixelT, int GeoDimT>
inline PixelT *
Raster<PixelT, GeoDimT>::getPixels () {
  return pixels.data ();
}

template<typename PixelT, int GeoDimT>
inline const PixelT *
Raster<PixelT, GeoDimT>::getFrame (const DimChannel &frameId) const {
  return &pixels[size.getOffsetFrame (frameId)];
}

template<typename PixelT, int GeoDimT>
inline PixelT *
Raster<PixelT, GeoDimT>::getFrame (const DimChannel &frameId) {
  return &pixels[size.getOffsetFrame (frameId)];
}

template<typename PixelT, int GeoDimT>
inline vector<PixelT> &
Raster<PixelT, GeoDimT>::getPixelsVector () {
  return pixels;
}

template<typename PixelT, int GeoDimT>
inline const vector<PixelT> &
Raster<PixelT, GeoDimT>::getPixelsVector () const {
  return pixels;
}

template<typename PixelT, int GeoDimT>
inline DimImg
Raster<PixelT, GeoDimT>::pointIdx (const Point<GeoDimT> &p) const {
  return point2idx (size, p);
}

template<typename PixelT, int GeoDimT>
inline PixelT
Raster<PixelT, GeoDimT>::getValue (const DimImgPack &idx) const {
  return pixels[idx];
}

template<typename PixelT, int GeoDimT>
inline PixelT
Raster<PixelT, GeoDimT>::getValue (const Point<GeoDimT> &point) const {
  return pixels [pointIdx (point)];
}

template<typename PixelT, int GeoDimT>
inline PixelT&
Raster<PixelT, GeoDimT>::getValue (const Point<GeoDimT> &point) {
  return pixels [pointIdx (point)];
}


// ================================================================================
template<int GeoDimT>
inline const string &
IImage<GeoDimT>::getFileName () const {
  return fileName;
}

template<int GeoDimT>
inline const Size<GeoDimT> &
IImage<GeoDimT>::getSize () const {
  return size;
}

template<int GeoDimT>
inline const DimChannel &
IImage<GeoDimT>::getBandCount () const {
  return bandCount;
}

template<int GeoDimT>
inline GDALDataType
IImage<GeoDimT>::getDataType () const {
  return dataType;
}

template<int GeoDimT>
inline const bool
IImage<GeoDimT>::isRead () const {
  return read;
}

template<int GeoDimT>
inline const bool
IImage<GeoDimT>::isEmpty () const {
  return size.isNull ();
}

// ================================================================================
#endif // _Obelix_IImage_tpp
