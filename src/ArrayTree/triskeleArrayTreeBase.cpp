////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <sstream>
#include <string>

#include "ArrayTree/triskeleArrayTreeBase.hpp"

using namespace std;
using namespace obelix::triskele;

// ========================================
const string
obelix::triskele::tileShapeLabels[] = {
				       "Volume", "Horizontal", "Vertical", "Plane"
};
const map<string, TileShape>
obelix::triskele::tileShapeMap = boost::assign::map_list_of
  ("volume", Volume)
  ("horizontal", Horizontal)
  ("vertical", Vertical)
  ("Plane", Plane)
  ;

// ========================================
ostream &
obelix::triskele::operator << (ostream &out, const Connectivity &c) {
  BOOST_ASSERT (c >= C1 && c <= (C8|CT|CTP|CTN));
  vector<string> result;
  if (C8 & (c == C8))
    result.push_back ("C8");
  else {
    if (C4 & c)
      result.push_back ("C4");
    if (C6P & c)
      result.push_back ("C6P");
    else if (C6N & c)
      result.push_back ("C6N");
  }
  if (CT & c)
    result.push_back ("CT");
  if (CTP & c)
    result.push_back ("CTP");
  if (CTN & c)
    result.push_back ("CTN");
  return out << boost::algorithm::join (result, "|");
}

istream &
obelix::triskele::operator >> (istream &in, Connectivity &c) {
  string token;
  in >> token;
  std::istringstream buffer (token);
  c = C1;
  for (std::string item; std::getline (buffer, item, '|'); ) {
    if (item == "C4")
      c = Connectivity (c|C4);
    else if (item == "C6P")
      c = Connectivity (c|C6P);
    else if (item == "C6N")
      c = Connectivity (c|C6N);
    else if (item == "C8")
      c = Connectivity (c|C8);
    else if (item == "CT")
      c = Connectivity (c|CT);
    else if (item == "CTP")
      c = Connectivity (c|CTP);
    else if (item == "CTN")
      c = Connectivity (c|CTN);
    else
      in.setstate (ios_base::failbit);
  }
  return in;
}

ostream &
obelix::triskele::operator << (ostream &out, const TileShape &tileShape) {
  BOOST_ASSERT (tileShape >= Volume && tileShape <= Plane);
  return out << tileShapeLabels[tileShape];
}

istream &
obelix::triskele::operator >> (istream &in, TileShape &tileShape) {
  string token;
  in >> token;
  auto pos = tileShapeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == tileShapeMap.end ())
    in.setstate (ios_base::failbit);
  else
    tileShape = pos->second;
  return in;
}

// ========================================
