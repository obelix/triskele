////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
// découpe fichier TestThread.cpp
// calcul alignement
// lecture seq => faire somme
// lecture // => faire somme

// écriture seq => écrire idx
// écriture // => écrire idx

// lecture/écriture seq => écrire x/2
// lecture/écriture // => écrire x/2

#include <cstddef>
#include <iostream>
#include <vector>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>


#define TEST_NUM 0

namespace obelix {
  namespace triskele {
    using namespace std;
    namespace ba = boost::accumulators;
    typedef ba::accumulator_set<double,
				ba::stats<ba::tag::sum,
					  ba::tag::count,
					  ba::tag::mean,
					  ba::tag::min,
					  ba::tag::max> > AlgoStat;
    enum TimeType
      {
       directDealStats,
       threadTTBDealStats,
       threadBoostDealStats,
       inThreadDealStats,

       initStats,
       seqReadStats,
       parReadStats,
       seqWriteStats,
       parWriteStats,
       seqRWStats,
       parRWStats,

       TimeTypeCard
      };
    typedef uint32_t DimImgPack;
    typedef uint32_t DimImg;

    class TestThread {
    public:

      static const unsigned int maxCoreCount;
      static const DimImg nbItem;
      static const std::size_t nbTest;

      AlgoStat timeStats[TimeTypeCard];
      // XXX vérifier l'alignement vector
      //vector<DimImgPack> global;

      TestThread ();
      // XXX en commun
      static inline string ns2string (double delta);
      inline void addTime (const TimeType &timeType, const double &duration) {
	timeStats[timeType] (duration);
      }
      ostream &print (ostream &out, const AlgoStat stats[]);
      template <typename T>
      void fillVector (vector<T> &vect);

      void testDeal (const unsigned int &coreCount, const DimImg &nbItem);
    };
  }
}
