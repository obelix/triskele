////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_OptChannel_hpp
#define _Obelix_Triskele_OptChannel_hpp

#include <iostream>
#include <map>
#include <gdal_priv.h>
#include <tinyxml.h>

#include "obelixGeo.hpp"
#include "Tree.hpp"
#include "Appli/MemoryEstimation.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptSizes.hpp"
#include "CompAttribute.hpp"
#include "misc.hpp"

namespace obelix {
  namespace triskele {
    using namespace std;

    // ================================================================================
    enum ApOrigPos
      {
       apOrigNoPos, apOrigPosBegin, apOrigPosEnd, apOrigPosBoth, apOrigPosEverywhere
      };
    extern const string apOrigPosLabels[];
    extern const map<string, ApOrigPos> ApOrigPosMap;
    ostream &operator << (ostream &out, const ApOrigPos &pos);
    istream &operator >> (istream &in, ApOrigPos &pos);


    // ================================================================================
    enum ChannelType
      {
       Original, Ndvi, Pantex, Sobel, AP, DAP, Haar, Stat
      };
    extern const string channelTypeLabels[];
    extern const map<string, ChannelType> channelTypeMap;
    ostream &operator << (ostream &out, const ChannelType &channelType);
    istream &operator >> (istream &in, ChannelType &channelType);

    enum FeatureOutputType
      {
       PixelFeature, DimFeature, ChannelFeature, DoubleFeature, FeatureOutputTypeCount
      };
    extern const string featureOutputTypeLabels[];
    ostream &operator << (ostream &out, const FeatureOutputType &feature);

    // ================================================================================
    class InputChannel {
    public:
      vector<string> localComments;
      ChannelType channelType;
      DimChannel inputId;

      /* size => Original:0, Ndvi:2, Pantex:1, Sobel:1, AP: 1, DAP: 1, Haar:1, Stat:1, *:1 */
      vector<InputChannel*> generateBy;
      vector<InputChannel*> produce;

      vector<Size<2> > winSizes;
      TreeType treeType;
      FeatureType featureType;
      AttributeType attributeType;
      vector<double> thresholds;
      bool dapWeight;
      ApOrigPos apOrigPos;

      /** produce output */
      bool doWrite;
      /** need to be compute */
      bool prodWrite;
      bool prodNewImage, prodInetgralImage, prodDoubleInetgralImage, prodDap;
      DimChannel copyRank, typeRank, origDapRank;
      DimChannel integralImageRank, doubleIntegralImageRank;
      DimChannel outputRank;

      bool isNdvi () const { return channelType == Ndvi; }
      bool isPantex () const { return channelType == Pantex; }
      bool isSobel () const { return channelType == Sobel; }
      bool updateProdWrite ();

      InputChannel (const ChannelType &ChannelType, const DimChannel &inputId, const bool &doWrite = false);
      InputChannel (const ChannelType &channelType, const DimChannel &inputId, const bool &doWrite, const vector<InputChannel *> &generateBy);
      void resetTransient ();

      vector<DimChannel>	getRelIdx () const;
      void writeXml (TiXmlElement *cfgNode) const;
    };
    ostream &operator << (ostream &out, const InputChannel &inputChannel);

    // ================================================================================
    class OptChannel {
      string name, created, modified;
      GDALDataType inputType;
      vector<vector <InputChannel *> > channels;
      vector<InputChannel *> selectedSet;
      TreeType selectedTreeType;
      FeatureType selectedFeatureType;
      AttributeType selectedAttributeType;
      map<DimChannel, InputChannel *> outputOrder, apOrder, dapOrder, haarOrder, statOrder;
      vector<string> outComments, comments;

      DimChannel nextChannelUniqId;
      DimChannel copyCount, originalUsedCount, origDapUsedCount, ndviUsedCount, pantexUsedCount, sobelUsedCount;
      DimChannel integralImageCount, doubleIntegralImageCount;
      DimChannel apCount, dapCount, haarCount, statCount, outputCount;
      void resetTransient ();

    public:
      DimChannel	spectralDepth;
      bool		mixedBandFlag;
      int		dim;
      static const string chExt;

      DimChannel getMaxInputIdCount () const { return nextChannelUniqId; }
      DimChannel getSize (const ChannelType &channelType) const;
      DimChannel getCopyCount () const { return copyCount; }
      DimChannel getOrigDapCount () const { return origDapUsedCount; }
      DimChannel getNdviCount () const { return ndviUsedCount; }
      DimChannel getPantexCount () const { return pantexUsedCount; }
      DimChannel getSobelCount () const { return sobelUsedCount; }
      DimChannel getApCount () const { return apCount; }
      DimChannel getDapCount () const { return dapCount; }
      DimChannel getIntegralImageCount () const { return integralImageCount; }
      DimChannel getDoubleIntegralImageCount () const { return doubleIntegralImageCount; }
      DimChannel getHaarCount () const { return haarCount; }
      DimChannel getStatCount () const { return statCount; }
      DimChannel getOutputCount () const { return outputCount; }
      const vector<InputChannel*> getOriginal () const { return channels [Original];}
      const map<DimChannel, InputChannel *> getApOrder () const { return apOrder;}
      const map<DimChannel, InputChannel *> getDapOrder () const { return dapOrder;}
      const map<DimChannel, InputChannel *> getHaarOrder () const { return haarOrder;}
      const map<DimChannel, InputChannel *> getStatOrder () const { return statOrder;}

      OptChannel ();
      ~OptChannel ();

      bool operator== (const OptChannel &rhs) const;
      bool isEmpty () const;
      bool inputMatches (const GDALDataType &inputType, const DimChannel &inputBandsCount) const;

      void setType (const GDALDataType &inputType, const DimChannel &inputBandsCount);
      void setName (const string &name);

      void loadFile (const string &fileName);
      void saveFile (const string &fileName) const;

      InputChannel *findChannel (const DimChannel &channelId);
      void updateChannelUniqId ();
      void updateChannels ();
      vector<InputChannel*> printableOrder (vector<InputChannel*> &circularDefinition) const;
      vector<InputChannel*> getActiveOrder () const;

      FeatureOutputType getFeatureOutputType (const InputChannel &inputChannel) const;
      set<FeatureOutputType> getFeatureOutputType () const;
      GDALDataType getOutputImageType (const GDALDataType &inputType) const;

      void selectInputs (const vector<int> &channelsId);
      void selectTreeType (const TreeType &treeType);
      void selectFeatureType (const FeatureType &featureType);
      void selectAttributeType (const AttributeType &attributeType);
      vector <InputChannel *> selectedAP ();

      void remove (const vector<InputChannel *> &toRemove);
      void remove (const ChannelType &channelType);
      void removeCopy ();
      void removeThresholds ();

      void addNdvi (const DimChannel &first, const DimChannel &last);
      void addNdvi (const DimChannel &id, const DimChannel &first, const DimChannel &last, const bool &doWrite);
      void addPantex (const DimChannel &channel);
      void addPantex (const DimChannel &id, const DimChannel &channel, const bool &doWrite);
      void addSobel (const DimChannel &channel);
      void addSobel (const DimChannel &id, const DimChannel &channel, const bool &doWrite);
      void addCopy (const DimChannel &channel);

      void addThresholds (const string &thresholds);
      void addThresholds (const vector<double> &thresholds);
      void addThresholds (const DimChannel &id, const DimChannel &from, const bool &doWrite, const vector<string> &localComments,
			  const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds);
      void addThresholds (const DimChannel &id, const bool &doWrite,
			  const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds,
			  InputChannel *inputChannel, const vector<string> &localComments);

      void addDap (const bool &dapWeight, const ApOrigPos &apOrigPos);
      void addDap (const DimChannel &id, const DimChannel &from, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos);
      void addDap (const DimChannel &id, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos, InputChannel *inputChannel);

      void addHaar (const vector<Size<2> > &winSizes);
      void addStat (const vector<Size<2> > &winSizes);
      void addTexture (const ChannelType &channelType, const vector<Size<2> > &winSizes);
      void addTexture (const ChannelType &channelType, const DimChannel &id, const DimChannel &from, const bool &doWrite, const vector<string> &localComments, const vector<Size<2> > &winSizes);
      void addTexture (const ChannelType &channelType, const DimChannel &id, const bool &doWrite, const vector<Size<2> > &winSizes, InputChannel *inputChannel, const vector<string> &localComments);

      void memoryImpact (MemoryEstimation &memoryEstimation);

      ostream &printOutput (ostream &out) const;
      ostream &printState (ostream &out) const;
      friend ostream &operator << (ostream &out, const OptChannel &optChannel);
    };
    ostream &operator << (ostream &out, const OptChannel &optChannel);

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptChannel_hpp
