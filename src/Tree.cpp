////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>
#include <boost/algorithm/string.hpp>

#include "obelixGeo.hpp"
#include "Tree.hpp"
#include "Border.hpp"
#include "ArrayTree/GraphWalker.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "Tree.tcpp"

// ================================================================================
const string
obelix::triskele::treeTypeLabels[] =
  {
   "Min", "Max", "Med", "Alpha", "ToS"
  };
const map<string, TreeType>
obelix::triskele::treeTypeMap = boost::assign::map_list_of
  ("min", MIN)
  ("max", MAX)
  ("med", MED)
  ("alpha", ALPHA)
  ("tos", TOS)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const TreeType &treeType) {
  BOOST_ASSERT (treeType >= MIN && treeType <= ALPHA);
  return out << treeTypeLabels[treeType];
}
istream &
obelix::triskele::operator >> (istream &in, TreeType &treeType) {
  string token;
  in >> token;
  auto pos = treeTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == treeTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    treeType = pos->second;
  return in;
}

// ================================================================================
// template<int GeoDimT>
// Tree<GeoDimT>::Tree (const Size<GeoDimT>> &size, const DimCore &coreCount)
//   : Tree (coreCount) {
//   resize (size);
// }

template<int GeoDimT>
Tree<GeoDimT>::Tree (const DimCore &coreCount)
  : coreCount (coreCount),
    size (),
    leafCount (0),
    nodeCount (0),
    leafParents (),
    compParents (nullptr),
    childrenStart (),
    children (),
    state (State::Void) {
  clear ();
}

template<int GeoDimT>
Tree<GeoDimT>::~Tree () {
  free ();
}

// ================================================================================
template<int GeoDimT>
void
Tree<GeoDimT>::save (HDF5 &hdf5) const {
  DEF_LOG ("Tree::save", "");
#if defined IMAGE_40BITS || defined IMAGE_LARGE
  BOOST_ASSERT (false);
#else
  hdf5.write_array ((GeoDimT == 2 ?
		     vector<DimSideImg> {size.side [0], size.side [1]} :
		     vector<DimSideImg> {size.side [0], size.side [1], size.side [2]}),
		    "imgSize","/");
  hdf5.write_array (leafParents,"parents","/");
#endif // IMAGE_40BITS || IMAGE_LARGE
}

template<int GeoDimT>
void
Tree<GeoDimT>::load (HDF5 &hdf5) {
  DEF_LOG ("Tree::load", "");
  vector<DimSideImg> sides (GeoDimT);
  hdf5.read_array (sides,"imgSize","/");
#if defined IMAGE_40BITS || defined IMAGE_LARGE
  BOOST_ASSERT (false);
#else
  if (GeoDimT < 3) {
    resize (Size<GeoDimT> (sides [0], sides[1]));
  } else {
    resize (Size<GeoDimT> (sides [0], sides[1], sides[2]));
  }
  hdf5.read_array (leafParents,"parents","/");

  compParents = leafParents.data ()+leafCount;
  setCompCount (leafParents.size ()-leafCount);
  childrenStart.clear ();
  childrenStart.resize (compCount+2);
  childrenStart[0] = childrenStart[1] = 0;
  DimNodePack *childCount = &childrenStart[2];
  forEachNode ([this, &childCount] (const DimNode &nodeId) {
		 if (leafParents [nodeId] == DimParent_MAX)
		   return;
		 ++childCount [size_t (leafParents [nodeId])];
	       });
  partial_sum (childCount, childCount+compCount, childCount);
#endif // IMAGE_40BITS || IMAGE_LARGE
}

// ================================================================================
template<int GeoDimT>
void
Tree<GeoDimT>::clear () {
  nodeCount = leafCount;
  leafParents.resize (0);
  weightBounds.resize (0);
  childrenStart.resize (2);
  childrenStart[0] = childrenStart[1] = 0;
}

template<int GeoDimT>
void
Tree<GeoDimT>::resize (const Size<GeoDimT> &size) {
  this->size = size;
  leafCount = size.getPixelsCount ();
  clear ();
  // XXX book (size.getPixelsCount ());
}

template<int GeoDimT>
void
Tree<GeoDimT>::resizeParents (const DimParent &maxParentCount) {
  leafParents.resize (leafCount+maxParentCount, DimParent_MAX);
  compParents = leafParents.data ()+leafCount;
  childrenStart.resize (maxParentCount+2);
}

template<int GeoDimT>
void
// XXX sans argument (
Tree<GeoDimT>::bookChildren () {
#ifdef USE_SMART_LOG
  children.assign (nodeCount-1U, 0);
#endif
  children.resize (nodeCount-1U);
}

template<int GeoDimT>
void
Tree<GeoDimT>::free () {
  leafParents = vector<DimParentPack> ();
  compParents = nullptr;
  children = vector<DimNodePack> ();
  childrenStart = vector<DimNodePack> ();
  weightBounds = vector<DimParentPack> ();
}

template<int GeoDimT>
DimParent
Tree<GeoDimT>::ancestor (DimParent pa, DimParent pb) const {
  BOOST_ASSERT (pa <= rootId && pb <= rootId);
  if (pa >= rootId || pb >= rootId)
    return rootId;
  for (;;) {
    if (pa == pb)
      return pa;
    for (; pa < pb; pa = compParents [pa])
      ;
    for (; pb < pa; pb = compParents [pb])
      ;
  }
}

// template<int GeoDimT>
// void
// Tree<GeoDimT>::book (const DimImg &leafCount) {
//   this->leafCount = leafCount;
//   clear ();
//   compParents = &leafParents[leafCount];
//   children.resize ((leafCount-1)*2);
//   childrenStart.resize (leafCount+1);
// }

// ================================================================================
/*!
  Les nœuds de l'arbre sont ranger en fonction de la mesure de distance choisie.
  Cependant l'ordre pour un même niveau dépend du nombre de tuiles étudiées en parallèle.
  Dans ce cas, nous devons disposer d'une méthode permettant de déterminer la similitude de deux arabres.
  C'est à dire que leur structure est identique même si le rang des nœuds peuvent différer.

  Ce méthode procède en plusieurs étapes :<ul>
  <li>vérification de la géométrie générale (hauteur, largeur, nombre de nœuds)</li>
  <li>vérification des zonnes "no-data" de la matrice de pixels</li>
  <li>construction d'un vecteur de correspondance entre le rang de chaque nœud d'un arbre à l'autre.</li>
  </ul>
*/

template<int GeoDimT>
bool
Tree<GeoDimT>::compareTo (const Tree<GeoDimT> &tree, bool testChildren) const {
  const DimImg leafCount = getLeafCount ();
  const DimParent compCount = getCompCount ();
  DEF_LOG ("Tree::compareTo", "leafCount: " << leafCount << " compCount: " << compCount);

  if (leafCount != tree.getLeafCount ()) {
    LOG ("not same image (" << leafCount << " != " << tree.getLeafCount () << ")");
    return false;
  }
  if (compCount != tree.getCompCount ()) {
    LOG ("not same structure (" << compCount << " != " << tree.getCompCount () << ")");
    return false;
  }

  vector<DimParentPack> pearComps (compCount, DimParent_MAX);

  // test no-data and check nodes level 0
  for (DimImg lId = 0; lId < leafCount; ++lId) {
    const DimParent parentId = getLeafParent (lId);
    const DimParent pearParentId = tree.getLeafParent (lId);
    if (parentId == DimParent_MAX && pearParentId == DimParent_MAX)
      continue;
    if (parentId == DimParent_MAX || pearParentId == DimParent_MAX) {
      LOG ("no-data at " << lId << (parentId == DimParent_MAX ? " pear: " : " this: ") << (parentId == DimParent_MAX ? pearParentId : parentId));
      return false;
    }
    if (pearComps [parentId] == DimParent_MAX) {
      // first child
      pearComps [parentId] = pearParentId;
      continue;
    }
    if (pearComps [parentId] == pearParentId)
      // an other child
      continue;
    DimImg lId2 = 0;
    for (; lId2 < lId; ++lId2)
      if (parentId == getLeafParent (lId2) && pearComps [parentId] == tree.getLeafParent (lId2))
	break;
    LOG ("not same parent in " << lId2 << "(" << parentId << "<=>" << pearComps [parentId] << ") and " << lId << "(" << parentId << "<=>" << pearParentId << ")");
    return false;
  }

  // test parents
  for (DimParent compId = 0; compId < compCount; ++compId) {
    const DimParent pearCompId = pearComps[compId];

    const DimParent parentId = getCompParent (compId);
    const DimParent pearParentId = tree.getCompParent (compId);

    if (parentId == DimParent_MAX && pearParentId == DimParent_MAX)
      // same root
      continue;
    if (parentId == DimParent_MAX || pearParentId == DimParent_MAX) {
      LOG ("not same root at " << compId << " (" << pearCompId << ")" <<  (parentId == DimParent_MAX ? " pear: " : " this: ") << (parentId == DimParent_MAX ? pearParentId : parentId));
      return false;
    }
    if (pearComps [parentId] == DimParent_MAX) {
      // first child
      pearComps [parentId] = pearParentId;
      continue;
    }
    if (pearComps [parentId] == pearParentId)
      // an other child
      continue;
    DimParent compId2 = 0;
    for (; compId2 < compId; ++compId2)
      if (parentId == getCompParent (compId2) && pearComps [parentId] == tree.getCompParent (compId2))
	break;
    LOG ("not same parent in " << compId2 << "(" << parentId << "<=>" << pearComps [parentId] << ") and " << compId << "(" << parentId << "<=>" << pearParentId << ")");
    return false;
  }

  if (testChildren) {
    // test children
    for (DimParent compId = 0; compId < compCount; ++compId) {
      const DimParent pearCompId = pearComps[compId];
      const DimImg childrenCount = getChildrenCount (compId);
      if (childrenCount != tree.getChildrenCount (pearCompId)) {
	LOG ("not same childrenCount " << childrenCount << " (" << compId << ") != " << tree.getChildrenCount (pearCompId) << " (" << pearCompId << ")");
	return false;
      }

      for (DimImg childId = 0; childId < childrenCount; ++childId) {
	DimNode nodeId = tree.getChild (pearCompId, childId);
	if (pearCompId != tree.isLeaf (nodeId) ?
	    tree.getLeafParent (tree.getLeafId (nodeId)) :
	    tree.getCompParent (tree.getCompId (nodeId))) {
	  LOG ("not same child for comp: " << pearCompId << " child: " << childId);
	  return false;
	}
      }
    }
  }
  return true;
}

// ================================================================================
template<int GeoDimT>
void
Tree<GeoDimT>::checkSpare (const Border<GeoDimT> &border) const {
  GraphWalker<GeoDimT> graphWalker (border);
  vector<Rect<GeoDimT> > tiles;
  vector<Rect<GeoDimT> > boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (coreCount, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);

  vector<DimImg> vertexMaxBounds;
  vector<DimImg> edgesMaxBounds;
  graphWalker.setMaxBounds (tiles, vertexMaxBounds, edgesMaxBounds);
  DimCore tileCount = tiles.size ();
  vector<DimParent> maxParents (tileCount);

  // check parents
  for (DimCore i = 0; i < tileCount; ++i) {
    DimImg base = vertexMaxBounds [i], top = vertexMaxBounds [i+1];
    DimParent &maxParent = maxParents [i];
    graphWalker.forEachVertex (tiles[i],
			       [this, &base, &top, &maxParent] (DimImg leafId, const BorderFlagType &lowBorder) {
				 BOOST_ASSERT (leafParents[leafId] >= base);
				 BOOST_ASSERT (leafParents[leafId] < top);
				 if (maxParent < DimParent (leafParents[leafId]))
				   maxParent = leafParents[leafId];
			       });
  }
  // XXX childrenStart+2 if needChildren
  // XXX childrenStart+1 if !needChildren
  // DimImgPack *childCount = (DimImgPack*)&childrenStart[2]; // Only used for the assert (so put to comment to prevent warnings)
  for (DimCore i = 0; i < tileCount; ++i) {
    DimImg base = vertexMaxBounds [i];
    DimParent maxParent = maxParents [i];
    for (DimParent compId = base; compId < maxParent; ++compId) {
      BOOST_ASSERT (compParents[compId] > base);
      BOOST_ASSERT (compParents[compId] != compId);
      BOOST_ASSERT (compParents[compId] < maxParent);
      if (compParents[compId] < compId)
	BOOST_ASSERT (childrenStart[2 + DimParent (compParents[compId])] > childrenStart[2 + compId]);
      //BOOST_ASSERT (childCount[compParents[compId]] > childCount[compId]); // Edited line to prevent the "Unused variable" warning
    }
    BOOST_ASSERT (compParents[maxParent] == DimParent_MAX);
  }
  {
    vector<bool> parentsMap (vertexMaxBounds[tileCount], false);
    for (DimCore i = 0; i < tileCount; ++i) {
      graphWalker.forEachVertex (tiles[i],
				 [this, &parentsMap] (DimImg leafId, const BorderFlagType &lowBorder) {
				   parentsMap [leafParents [leafId]] = true;
				 });
      DimImg base = vertexMaxBounds [i];
      DimParent maxParent = maxParents [i];
      for (DimParent compId = base; compId < maxParent; ++compId)
	parentsMap [compParents [compId]] = true;
      for (DimParent compId = base; compId < maxParent; ++compId)
	BOOST_ASSERT (parentsMap [compId]);
    }
  }
  // check weight
  // XXX monotone

  // check childrenStart

}

// ================================================================================
template<int GeoDimT>
void
Tree<GeoDimT>::check (const Border<GeoDimT> &border) const {
  GraphWalker<GeoDimT> graphWalker (border);
  DimParent compCount = getCompCount ();
  BOOST_ASSERT (compCount < leafCount);
  // check parents
  graphWalker.forEachVertexSimple ([this, &compCount] (const DimImg &leafId) {
				     // XXX si border => leafParents [leafId] == DimParent_MAX
				     BOOST_ASSERT (leafParents[leafId] < compCount);
				   });
  forEachComp ([this, &compCount] (const DimParent &compId) {
		 if (compId == compCount-1)
		   BOOST_ASSERT (compParents[compId] == DimParent_MAX);
		 else {
		   BOOST_ASSERT (compParents[compId] > compId);
		   BOOST_ASSERT (compParents[compId] < compCount);
		 }
	       });
  {
    vector<bool> parentsMap (compCount, false);
    //parentsMap.assign (compCount, false);
    forEachNode ([this, &parentsMap] (const DimNode &nodeId) {
		   if (leafParents[nodeId] == DimParent_MAX)
		     return;
		   parentsMap [leafParents [nodeId]] = true;
		 });
    for (DimParent compId = 0; compId < compCount; ++compId)
      BOOST_ASSERT (parentsMap [compId]);
  }
  // check weight

  // check weightBounds

  // check childrenStart
  if (children.size ()) {
    // XXX only if (needChildren)
    vector<DimNodePack> childrenStart2 (compCount, 0);
    forEachLeaf ([this, &childrenStart2] (const DimImg &leafId) {
		   if (leafParents[leafId] == DimParent_MAX)
		     // boder
		     return;
		   ++childrenStart2 [leafParents [leafId]];
		 });
    forEachComp ([this, &childrenStart2, &compCount] (const DimParent &compId) {
		   if (compId == compCount-1)
		     return;
		   ++childrenStart2 [compParents [compId]];
		 });
    for (DimParent compId = 0; compId < compCount; ++compId) {
      // check count
      BOOST_ASSERT ((DimNode)childrenStart2 [compId] == (DimNode)childrenStart [compId+1] - (DimNode)childrenStart[compId]);
      // at least 2 children
      BOOST_ASSERT ((DimNode)childrenStart2 [compId] > 1U);
    }
  }

  // check children
  if (children.size ()) {
    vector<DimNodePack> childrenMap (nodeCount, DimNode_MAX);
    forEachComp ([this, &childrenMap] (const DimParent &compId) {
		   DimNode minChild = childrenStart[compId], maxChild = childrenStart[compId+1];
		   for (DimNode childId = minChild; childId < maxChild; ++childId) {
		     DimNode child = children[childId];
		     BOOST_ASSERT (leafParents [child] == compId);
		     BOOST_ASSERT (childrenMap [child] == DimNode_MAX);
		     childrenMap [child] = compId;
		   }
		   for (DimNode childId = minChild+1U; childId < maxChild; ++childId) {
		     BOOST_ASSERT (children[childId-1] < children[childId]);
		   }
		 });
    for (DimNode child = 0; child < nodeCount-1; ++child)
      BOOST_ASSERT (childrenMap [child] != DimNode_MAX || (child < leafCount && border.isBorder (child)));
  }
}

// ================================================================================
template<int GeoDimT>
Tree<GeoDimT>::CPrintTree::CPrintTree (const Tree<GeoDimT> &tree, const int &onRecord)
  : tree (tree),
    onRecord (onRecord),
    capacity (tree.leafParents.size ()) {
}

template<int GeoDimT>
ostream &
Tree<GeoDimT>::CPrintTree::print (ostream &out) const {
  Size<GeoDimT> doubleSize (tree.size.getDoubleHeight ());
  out << "Tree::printTree: leafCount:" << tree.leafCount
      << " nodeCount:" << (onRecord ? "~" : "") << tree.nodeCount << " compCount:" << tree.nodeCount - tree.leafCount << endl
      << "parent"
      << (onRecord ? " count" : "")
      << (tree.children.size () ? " bounds children" : "") << endl
      << printMap (tree.leafParents.data (), doubleSize, capacity) << endl << endl;
  if (onRecord)
    out << printMap (&tree.childrenStart[onRecord], tree.size, capacity - tree.leafCount);
  if (tree.children.size ())
    out << printMap (&tree.childrenStart[0], tree.size, tree.compCount+1);
  out << endl << endl;
  if (tree.children.size ())
    out << printMap (tree.children.data (), doubleSize, tree.nodeCount-1) << endl << endl;
  if (!tree.weightBounds.size ())
    return out << "no weightBounds" << endl << endl;
  return out << "weightBounds: " << endl
	     << printMap (tree.weightBounds.data (), tree.size, tree.weightBounds.size ()) << endl << endl;
}

// ================================================================================
