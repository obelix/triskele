////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Tree_hpp
#define _Obelix_Triskele_Tree_hpp

#include <vector>
#include <map>
#include <string>
#include <numeric>
#include <boost/thread.hpp>

#include "obelixThreads.hpp"
#include "obelixGeo.hpp"
#include "Border.hpp"
#include "HDF5.hpp"

namespace obelix {
  namespace triskele {

    enum State
      {	Void = 0,
	Initialized = 1 << 0,
	Constructed = 1 << 1
    };

    /*! Définit le type d'arbre à construire */
    enum TreeType { MIN, MAX, MED, TOS, ALPHA, TreeTypeCard};

    /*! Définit les noms des arbres (utile pour le debug) */
    extern const string treeTypeLabels[];
    extern const map<string, TreeType> treeTypeMap;
    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const TreeType &treeType);
    istream &operator >> (istream &in, TreeType &treeType);

    // ================================================================================
    template<int GeoDimT>
    class Tree {
      template<int GeoDimT2> friend class TreeBuilder;
    private:
      inline DimNodePack *getChildSum ();

    protected:
      /*! nb core for build and compute attributes */
      const DimCore coreCount;

      /*! Info about the picture */
      Size<GeoDimT> size;

      /*! Number of pixels / leaves */
      DimImg leafCount;
      /*! Number of componants */
      DimParent compCount;
      /*! last componant */
      DimParent rootId;
      /*! number of node */
      DimNode nodeCount; /* nodeCount = leafCount+compCount */

      /*! Pointers on the parents of each leafs / nodes */
      vector<DimParentPack> leafParents;
      DimParentPack *compParents;

      /*! Pointers on the children and count how many children a parents have */
      vector<DimNodePack> childrenStart;
      vector<DimNodePack> children;

      /*! Pointers of same weight in parents (+1 : last is root)*/
      vector<DimParentPack> weightBounds;

      /*! State of the tree */
      State state;

      // /*! Allocate the size according to the size previously defined */
      // void book (const DimImg &leafCount);

      /*! Free all the memory and set all pointers to nullptr */
      void free ();

    public:
      // Constructors, destructor and resize method (does the same as the constructors)

      // XXX test sans defaut treeCoreCount
      Tree (const DimCore &coreCount = boost::thread::hardware_concurrency ());
      ~Tree ();

      void save (HDF5 &hdf5) const;
      void load (HDF5 &hdf5);

      /*! clear values according to the size defined */
      void clear ();
      void resize (const Size<GeoDimT> &size);
      void resizeParents (const DimParent &maxParentCount);
      void bookChildren ();


      // Setter for nodeCount and size
      inline void setCompCount (const DimParent &newCompCount);
      inline void setSize (const Size<GeoDimT> &newSize);

      inline DimCore getCoreCount () const;
      inline DimCore getThreadPerImage () const;
      inline DimCore getTilePerThread () const;

      // Get the tree state and the size
      inline State getState () const;
      inline Size<GeoDimT> getSize () const;

      // Getters for tree structure
      inline const DimImg		&getLeafCount () const;
      inline const DimNode		&getNodeCount () const;
      inline DimParent			getCompCount () const;
      inline DimParent			getRootId () const;
      // inline DimNode			getNodeRootId () const;
      inline bool			isLeaf (const DimNode &nodeId) const;
      inline DimImg			getLeafId (const DimNode &nodeId) const;
      inline DimParent			getCompId (const DimNode &nodeId) const;
      inline DimNode			getChild (const DimParent &compId, const DimImg &childId) const;

      inline const DimParent		getParent (const DimNode &nodeId) const;
      inline const DimParent		getLeafParent (const DimImg &leafId) const;
      inline const DimParent		getCompParent (const DimParent &compId) const;
      inline const DimParentPack	*getLeafParents () const;

      inline const DimImg		getChildrenCount (const DimParent &CompId) const;
      inline const DimNode		getChildren (const DimNode &childId) const;

      inline const vector<DimParentPack> &getWeightBounds () const;
      inline vector<DimParentPack>	 &getWeightBounds ();


      DimParent	ancestor (DimParent pa, DimParent pb) const;

      // Functions to apply to specific entities
      template<typename FuncToApply>
      void forEachLeaf (const FuncToApply &f /* f (DimImg leafId) */) const;

      template<typename FuncToApply>
      inline void forEachComp (const FuncToApply &f /* f (DimParent compId) */) const;
      template<typename FuncToApply>
      inline void forEachCompFromRoot (const FuncToApply &f /* f (DimParent compId) */) const;

      template<typename FuncToApply>
      inline void forEachNode (const FuncToApply &f /* f (DimNode nodeId) */) const;

      template<typename FuncToApply>
      inline void forEachChild (const DimParent &parentId, const FuncToApply &f /* f (DimNode childId) */) const;
      template<typename FuncToApply>
      inline void forEachChildTI (const DimParent &parentId, const FuncToApply &f /* f (bool isLeaf, DimImg|DimParent childId) */) const;


      bool compareTo (const Tree &tree, bool testChildren = false) const;

      void checkSpare (const Border<GeoDimT> &border) const;
      void check (const Border<GeoDimT> &border) const;
      // XXX void checkWeightCurve (bool incr) const;

      // nice ostream
      struct CPrintTree {
	const Tree<GeoDimT> &tree;
	const int &onRecord;
	const DimNode capacity;
	CPrintTree (const Tree<GeoDimT> &tree, const int &onRecord);
	ostream &print (ostream &out) const;
      };
      CPrintTree printTree (const int &onRecord = 0) const { return CPrintTree (*this, onRecord); }
      friend ostream &operator << (ostream &out, const CPrintTree &cpt) { return cpt.print (out); }
    };

    // ================================================================================
#include "Tree.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Tree_hpp
