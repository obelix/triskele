////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_PriorityQueue_hpp
#define _Obelix_Triskele_PriorityQueue_hpp

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "obelixLowAlloc.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    
    template<typename WeightT>
    class PriorityQueue {
      struct Entry {
	DimImg nextEntry;
	DimImg idxa;
	DimImg idxb;

	Entry ();
	inline void _ctor (const DimImg &idxa, const DimImg &idxb);
      };
      struct Queue {
	DimImg previousQueue;
	DimImg nextQueue;
	WeightT level;
	DimImg firstEntry;
	DimImg lastEntry;

	Queue ();
	inline void _ctor (const WeightT &level, const DimImg &previousQueue, const DimImg &nextQueue);

	inline void pushEntry (PriorityQueue &priorityQueue, const DimImg &idxa, const DimImg &idxb);
	inline DimImg popEntry (PriorityQueue &priorityQueue);

	DimImg entriesCount (PriorityQueue &priorityQueue) const;
      };

      LowAlloc<Entry>	entries;
      LowAlloc<Queue>	queues;
      // static inline const DimImg dumyEntry = 0;
      // static inline const DimImg headIdx = 0;
      DimImg currentIdx;
      
      inline DimImg getEntry (const DimImg &idxa, const DimImg &idxb);
      inline DimImg getQueue (const WeightT &level, const DimImg &previousQueue, const DimImg &nextQueue);
      inline bool find (const WeightT &level);

    public:
      PriorityQueue (const DimImg &pixelsCount);
      ~PriorityQueue ();

      inline bool empty () const;
      inline void push (const WeightT &level, const DimImg &idxa, const DimImg &idxb);
      inline void pop (WeightT &level, DimImg &idxa, DimImg &idxb);

      DimImg queuesCount () const;
      DimImg queuesCountBack () const;
    };

    // ================================================================================
#include "triskelePriorityQueue.tpp"
  } // triskele
} // obelix


#endif // _Obelix_Triskele_PriorityQueue_hpp
