////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Filter_hpp
#define _Obelix_Triskele_Filter_hpp

#include "IImage.hpp"
#include "TextWin.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Appli/OptFilter.hpp"
#include "Attributes/AttributesCache.hpp"

namespace obelix {
  namespace triskele {
    using namespace obelix;

    // ================================================================================
    template <typename PixelT, int GeoDimT>
    class TriskeleFilter {
    public:
      TriskeleFilter (IImage<GeoDimT> &inputImage, IImage<GeoDimT> &outputImage,
		      const Point<2> &topLeft, const Size<GeoDimT> &size, const OptFilter &optFilter);
      ~TriskeleFilter ();

      void parseInput ();

    private:
      IImage<GeoDimT> &inputImage;
      IImage<GeoDimT> &outputImage;
      const OptFilter &optFilter;
      Point<2> topLeft;
      Size<GeoDimT> size;
      Border<GeoDimT> border;
      GraphWalker<GeoDimT> graphWalker;
      DimImg pixelCount;
      Raster<PixelT, GeoDimT> inputRaster;
      Raster<PixelT, GeoDimT> outputRaster;

      vector<vector<double> > gTmp;
      vector<Raster<PixelT, GeoDimT> > ndviRaster;
      vector<Raster<PixelT, GeoDimT> > pantexRasters;
      vector<Raster<PixelT, GeoDimT> > sobelRasters;
      vector<double> integralImage, doubleIntegralImage;
      Tree<GeoDimT> tree;
      map<const InputChannel *, map<TreeType, map<AttributeType, const InputChannel *> > > waitingAP;

      Raster<PixelT, GeoDimT> &getProducerRaster (const InputChannel &inputChannel);
      void readInput (const DimChannel &channel);
      void updateBorder ();
      void updateIntegralImage (const InputChannel &inputChannel);
      void updateNdvi (const InputChannel &inputChannel, const InputChannel &ndviChannel);
      void updatePantex (const InputChannel &inputChannel, const InputChannel &pantexChannel);
      void updateSobel (const InputChannel &inputChannel, const InputChannel &sobelChannel);
      void writeHaar (const InputChannel &inputChannel, const InputChannel &haarChannel);
      void writeStat (const InputChannel &inputChannel, const InputChannel &statChannel);
      void writeSelectedFP (const InputChannel &inputChannel, const InputChannel &apChannel);

      template <typename OutPixelT, typename APFunct>
      void writeFilteredAttr (ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb, const Raster<PixelT, GeoDimT> &raster,
			      AttributesCache<PixelT, GeoDimT> &attributesCache, const APFunct &setAP, const map <AttributeType, const InputChannel*> &attrProd);
      template <typename OutPixelT>
      void writeDAP (const vector<const OutPixelT*> &dapSrcIdx, const InputChannel &dapChannel);

      void getOneBandImage (IImage<2> &oneBandImage, const GDALDataType &dataType, const DimChannel &band) const;
      void dumpInput (const InputChannel &inputChannel);
      template <typename FeatureT>
      void writeBand (FeatureT *featureT, const DimChannel &channel);

      // deprecated
      // void writePixelBand (PixelT *pixels, const DimChannel &channel);
      // void writeDoubleBand (double *pixels, const DimChannel &channel);
      // void writeDimImgBand (DimImgPack *pixels, const DimChannel &channel);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Filter_hpp
