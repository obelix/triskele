////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <algorithm>    // std::sort
#include "obelixDebug.hpp"
#include "misc.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/GraphWalker.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "GraphWalker.tcpp"

//template<> ostream &obelix::operator << <Neighbor>(ostream &out, const vector<Neighbor> &v);

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const BorderFlagType &a) {
  BOOST_ASSERT (a >= NO && a <= ALL);
  vector<string> result;
  if (X1 & a)
    result.push_back ("X1");
  if (Y1 & a)
    result.push_back ("Y1");
  if (Z1 & a)
    result.push_back ("Z1");
  if (X2 & a)
    result.push_back ("X2");
  if (Y2 & a)
    result.push_back ("Y2");
  if (Z2 & a)
    result.push_back ("Z2");
  if (result.empty ())
    return out << "NO";
  return out << boost::algorithm::join (result, "|");
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const Neighbor &a) {
  return out << "[" << a.off << " " << a.border << "]";
}

// ================================================================================
template<int GeoDimT>
const vector<Neighbor>
GraphWalker<GeoDimT>::determineDelta (const Size<GeoDimT> &size, const Connectivity &connectivity,
				  const TileShape &tileShape, bool first) {
  DEF_LOG ("GraphWalker::determineDelta", "size:" << size << " connectivity:" << connectivity
	   << " tileShape:" << tileShape << " first:" << first);
  vector<Neighbor> result;
  if (size.isNull ())
    return result;
  DimImg slice (size.getFramePixelsCount ());
  switch (tileShape) {

  case Volume: {
    if (connectivity & Connectivity::C4) {
      result.push_back (Neighbor (X1, 1U));
      result.push_back (Neighbor (Y1, size.side [0]));
    }
    if (connectivity & Connectivity::C6N)
      result.push_back (Neighbor (Y1|X2, size.side [0] - 1U));
    if (connectivity & Connectivity::C6P)
      result.push_back (Neighbor (X1|Y1, size.side [0] + 1U));

    if (GeoDimT > 2 && size.side [2] > 1U) {
      if (connectivity & Connectivity::CT)
	result.push_back (Neighbor (Z1, slice));
      if (connectivity & Connectivity::CTP) {
	result.push_back (Neighbor (X1|Z1, slice + 1U));
	result.push_back (Neighbor (Y1|Z1, slice + size.side [0]));
      }
      if (connectivity & Connectivity::CTN) {
	result.push_back (Neighbor (X1|Y1|Z1, slice + size.side [0] + 1U));
	result.push_back (Neighbor (Y1|Z1|X2, slice + size.side [0] - 1U));
      }
    }
    if (first)
      for (Neighbor &n : result)
    	n.border = BorderFlagType ((n.border << 3) | (n.border >> 3));
  }
    break;

  case Horizontal: {
    if (first) {
      if (GeoDimT > 2 && size.side [2] > 1U) {
  	if (connectivity & Connectivity::CTP)
  	  result.push_back (Neighbor (Z1|Y2, slice - size.side [0]));
  	if (connectivity & Connectivity::CTN) {
  	  result.push_back (Neighbor (Z1|X2|Y2, slice - size.side [0] - 1U));
  	  result.push_back (Neighbor (X1|Z1|Y2, slice - size.side [0] + 1U));
  	}
      }
    } else {
      if (connectivity & Connectivity::C6N)
  	result.push_back (Neighbor (Y1|X2, size.side [0] - 1U));
      if (connectivity & Connectivity::C4)
  	result.push_back (Neighbor (Y1, size.side [0]));
      if (connectivity & Connectivity::C6P)
  	result.push_back (Neighbor (X1|Y1, size.side [0] + 1U));
      if (GeoDimT > 2 && size.side [2] > 1U) {
  	if (connectivity & Connectivity::CTP)
  	  result.push_back (Neighbor (Y1|Z1, slice + size.side [0]));
  	if (connectivity & Connectivity::CTN) {
  	  result.push_back (Neighbor (Y1|Z1|X2, slice + size.side [0] - 1U));
  	  result.push_back (Neighbor (X1|Y1|Z1, slice + size.side [0] + 1U));
  	}
      }
    }
  }
    break;
  case Vertical: {
    if (first) {
      if (connectivity & Connectivity::C6N)
  	result.push_back (Neighbor (Y1|X2, size.side [0] - 1U));
      if (GeoDimT > 2 && size.side [2] > 1U) {
  	if (connectivity & Connectivity::CTP)
  	  result.push_back (Neighbor (Z1|X2, slice - 1U));
  	if (connectivity & Connectivity::CTN) {
  	  result.push_back (Neighbor (Z1|X2|Y2, slice - size.side [0] - 1U));
  	  result.push_back (Neighbor (Y1|Z1|X2, slice + size.side [0] - 1U));
  	}
      }
    } else {
      if (connectivity & Connectivity::C4)
  	result.push_back (Neighbor (X1, 1U));
      if (connectivity & Connectivity::C6P)
  	result.push_back (Neighbor (X1|Y1, size.side [0] + 1U));
      if (GeoDimT > 2 && size.side [2] > 1U) {
  	if (connectivity & Connectivity::CTP)
  	  result.push_back (Neighbor (X1|Z1, slice + 1U));
  	if (connectivity & Connectivity::CTN) {
  	  result.push_back (Neighbor (X1|Z1|Y2, slice - size.side [0] + 1U));
  	  result.push_back (Neighbor (X1|Y1|Z1, slice + size.side [0] + 1U));
  	}
      }
    }
  }
    break;

  case Plane: {
    if (GeoDimT <= 2 || size.side [2] <= 1U)
      return result;
    if (connectivity & Connectivity::CT)
      result.push_back (Neighbor (Z1, slice));
    if (connectivity & Connectivity::CTP) {
      result.push_back (Neighbor (Z1|Y2, slice - size.side [0]));
      result.push_back (Neighbor (Z1|X2, slice - 1U));
      result.push_back (Neighbor (Y1|Z1, slice + size.side [0]));
      result.push_back (Neighbor (X1|Z1, slice + 1U));
    }
    if (connectivity & Connectivity::CTN) {
      result.push_back (Neighbor (Z1|X2|Y2, slice - size.side [0] - 1U));
      result.push_back (Neighbor (Y1|Z1|X2, slice + size.side [0] - 1U));
      result.push_back (Neighbor (X1|Z1|Y2, slice - size.side [0] + 1U));
      result.push_back (Neighbor (X1|Y1|Z1, slice + size.side [0] + 1U));
    }
  }
    break;
  default:
    BOOST_ASSERT (false);
  }
  sort (result.begin (), result.end ());

  // using namespace obelix::triskele;
  LOG ("result:" << result.size ());
  return result;
}

// ================================================================================
template<int GeoDimT>
BorderFlagType
GraphWalker<GeoDimT>::getBorderFlag (const Size<GeoDimT> &size, const DimImg &idx) {
  BorderFlagType result (NO);
  const DimImg yz (idx / size.side [0]);
  const DimImg x  (idx % size.side [0]);
  const DimImg y  (yz % size.side [1]);
  const DimImg z  (yz / size.side [1]);
  if (!x)
    result |= X1;
  if (x == size.side [0]-1)
    result |= X2;
  if (!y)
    result |= Y1;
  if (y == size.side [1]-1)
    result |= Y2;
  if (!z)
    result |= Z1;
  if (z == size.side [2]-1)
    result |= Z2;
  return result;
}

// ================================================================================
template<int GeoDimT>
GraphWalker<GeoDimT>::GraphWalker (const Border<GeoDimT> &border, const Connectivity &connectivity, const DimSortCeil &countingSortCeil)
  : border (border),
    connectivity (connectivity),
    countingSortCeil (countingSortCeil),
    volDelta1 (determineDelta (border.getSize (), connectivity, Volume, true)),
    volDelta2 (determineDelta (border.getSize (), connectivity, Volume, false)),
    horDelta1 (determineDelta (border.getSize (), connectivity, Horizontal, true)),
    horDelta2 (determineDelta (border.getSize (), connectivity, Horizontal, false)),
    verDelta1 (determineDelta (border.getSize (), connectivity, Vertical, true)),
    verDelta2 (determineDelta (border.getSize (), connectivity, Vertical, false)),
    plaDelta (determineDelta (border.getSize (), connectivity, Plane)) {
  DEF_LOG ("GraphWalker::GraphWalker", "size: " << border.getSize () << " connectivity: " << connectivity);
  BOOST_ASSERT (!border.getSize ().isNull ());
}

// ================================================================================
template<int GeoDimT>
DimEdge
GraphWalker<GeoDimT>::edgeMaxCount () const {
  return edgeMaxCount (border.getSize ());
}

// ================================================================================
template<int GeoDimT>
DimEdge
GraphWalker<GeoDimT>::edgeMaxCount (const Size<GeoDimT> &tileSize) const {
  if (tileSize.isNull ())
    return 0;
  const DimImg wt (tileSize.side [0]);
  const DimImg wp (tileSize.side [0]-1);
  const DimImg ht (tileSize.side [1]);
  const DimImg hp (tileSize.side [1]-1);
  const DimImg lt (tileSize.side [2]);
  const DimImg lp (tileSize.side [2]-1);
  DimEdge plan (0);
  if (connectivity & Connectivity::C4)
    plan += wt*hp + wp*ht;
  if (connectivity & Connectivity::C6P)
    plan += wp*hp;
  if (connectivity & Connectivity::C6N)
    plan += wp*hp;
  plan *= lt;

  DimEdge deep (0);
  if (connectivity & Connectivity::CT)
    deep += wt*ht;
  if (connectivity & Connectivity::CTP)
    deep += 2*(wt*hp + wp*ht);
  if (connectivity & Connectivity::CTN)
    deep += 4*wp*hp;
  deep *= lp;
  return plan+deep;
}

template<int GeoDimT>
DimEdge
GraphWalker<GeoDimT>::edgeMaxCount (const Size<GeoDimT> &size, const TileShape &boundaryAxe) const {
  DEF_LOG ("GraphWalker::edgeMaxCount", "boundaryAxe:" << boundaryAxe << " size:" << size);
  DimEdge result (0);
  if (size.isNull ()) {
    LOG ("null: " << size);
    return result;
  }
  switch (boundaryAxe) {
  case Volume:
    result = edgeMaxCount (size);
    break;
  case Vertical:
    if (connectivity & Connectivity::C4)
      result += size.side [1] * size.side [2];
    if (connectivity & Connectivity::C6P)
      result += (size.side [1]-1) * size.side [2];
    if (connectivity & Connectivity::C6N)
      result += (size.side [1]-1) * size.side [2];
    if (connectivity & Connectivity::CTP)
      result += 2 * size.side [1] * (size.side [2]-1);
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.side [1]-1) * (size.side [2]-1);
    break;

  case Horizontal:
    if (connectivity & Connectivity::C4)
      result += size.side [0] * size.side [2];
    if (connectivity & Connectivity::C6P)
      result += (size.side [0]-1) * size.side [2];
    if (connectivity & Connectivity::C6N)
      result += (size.side [0]-1) * size.side [2];
    if (connectivity & Connectivity::CTP)
      result += 2 * size.side [0] * (size.side [2]-1);
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.side [0]-1) * (size.side [2]-1);
    break;

  case Plane:
    if (connectivity & Connectivity::CT)
      result += size.getFramePixelsCount ();
    if (connectivity & Connectivity::CTP)
      result +=
	2 * ((size.side [0]-1) * size.side [1] +
	     size.side [0] * (size.side [1]-1));
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.side [0]-1) * (size.side [1]-1);
    break;

  default:
    BOOST_ASSERT (false);
  }
  LOG ("result: " << result);
  return result;
}

// ================================================================================
template<int GeoDimT>
void
GraphWalker<GeoDimT>::setTiles (const DimCore &coreCount, vector<Rect<GeoDimT> > &tiles, vector<Rect<GeoDimT> > &boundaries, vector<TileShape> &boundaryAxes) const {
  setTiles (coreCount, Rect<GeoDimT> (Point<GeoDimT> (), border.getSize ()), tiles, boundaries, boundaryAxes);
}

template<int GeoDimT>
void
GraphWalker<GeoDimT>::setTiles (const DimCore &coreCount, const Rect<GeoDimT> &rect, vector<Rect<GeoDimT> > &tiles, vector<Rect<GeoDimT> > &boundaries, vector<TileShape> &boundaryAxes) const {
  //
  BOOST_ASSERT (coreCount);
  DEF_LOG ("GraphWalker::setTiles", "coreCount:" << coreCount << " rect:" << rect);
  DimSideImg maxSide = max (rect.size.side [0], rect.size.side [1]);
  if (GeoDimT != 2)
    maxSide = max (maxSide, rect.size.side [2]);
  if (coreCount < 2 ||
      maxSide < 4 ||
      rect.size.getPixelsCount () < 4) {
    LOG ("rect:" << rect);
    tiles.push_back (rect);
    return;
  }
  TileShape cutAxe = Vertical;
  if (maxSide == rect.size.side [0])
    cutAxe = Vertical;
  else if (maxSide == rect.size.side [1])
    cutAxe = Horizontal;
  else if (GeoDimT != 2 && maxSide == rect.size.side [2])
    cutAxe = Plane;
  bool odd = coreCount & 0x1;
  DimImg thin = maxSide / (odd ? coreCount : 2);
  Rect<GeoDimT> tileA (rect);
  Rect<GeoDimT> tileB (rect);
  switch (cutAxe) {
  case Vertical:
    tileA.size.side [0] = thin;
    tileB.point.coord [0] += thin;
    tileB.size.side [0] -= thin;
    break;
  case Horizontal:
    tileA.size.side [1] = thin;
    tileB.point.coord [1] += thin;
    tileB.size.side [1] -= thin;
    break;
  case Plane:
    if (GeoDimT != 2) {
      tileA.size.side [2] = thin;
      tileB.point.coord [2] += thin;
      tileB.size.side [2] -= thin;
    }
    break;
  default:
    BOOST_ASSERT (false);
  }
  setTiles ((odd ? 1 : coreCount/2), tileA, tiles, boundaries, boundaryAxes);
  boundaries.push_back (tileB);
  boundaryAxes.push_back (cutAxe);
  setTiles ((odd ? coreCount-1 : coreCount/2), tileB, tiles, boundaries, boundaryAxes);
}

// ================================================================================
template<int GeoDimT>
void
GraphWalker<GeoDimT>::setMaxBounds (const vector<Rect<GeoDimT> > &tiles, vector<DimImg> &vertexMaxBounds, vector<DimImg> &edgesMaxBounds) const {
  DimCore tileCount = tiles.size ();
  vertexMaxBounds.resize (tileCount+1U);
  edgesMaxBounds.resize (tileCount+1U);
  DimImg vertexBase = 0, edgesBase = 0;
  vertexMaxBounds [0] = edgesMaxBounds [0] = 0;
  for (DimCore i = 0; i < tileCount; ++i) {
    Size<GeoDimT> zoneSize (tiles[i].size);
    vertexMaxBounds [i+1U] = vertexBase += zoneSize.getPixelsCount (); /* -1, but prety LOG */
    edgesMaxBounds [i+1U] = edgesBase += edgeMaxCount (zoneSize);
  }
}

// ================================================================================
template<int GeoDimT>
template <typename PixelT, typename WeightT, TreeType TreeTypeT>
DimEdge
GraphWalker<GeoDimT>::getSortedTileEdges (const Rect<GeoDimT> &rect, vector <TileEdge<WeightT> > &tileEdges,
					  const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef.getWeight (const DimImg &a, const DimImg &b) ef->getDecr ()*/) const {
  SMART_DEF_LOG ("GraphWalker::getCountingSortedTileEdges", "rect:" << rect);
  BOOST_ASSERT (!rect.isNull ());
  if (TreeTypeT == TOS) {
    cerr << endl << "*** GraphWalker::getCountingSortedTileEdges: not allowed with TOS ***" << endl;
    return 0;
  }
  if (sizeof (WeightT) > countingSortCeil)
    return getQSortedTileEdges (rect, tileEdges, ef);
  double maxVal (numeric_limits<WeightT>::max ());  // not const to avoid Warning overflow
  double minVal (numeric_limits<WeightT>::min ());  // not const to avoid Warning overflow
  const DimTile dim (maxVal - minVal + 1);
  SMART_LOG ("dim:" << dim);
  vector<DimTile> counters (dim+1, 0);
  DimTile * indices = counters.data ();
  DimTile * const histogramBase = indices+1;
  DimTile * const histogramRelative = histogramBase-int (minVal);
  DimTile sum = 0;
  forEachEdgeIdx (rect, Volume,
		  [&histogramRelative, &ef] (const DimImg &a, const DimImg &b) {
		    ++histogramRelative[int (ef->getWeight (a, b))];
		    SMART_LOG_EXPR (cerr << "a:" << a << " b:" << b << " v:" << int (ef->getWeight (a, b)) << endl);
		  });

  // get indices by prefix sum
  partial_sum (histogramBase, histogramBase+dim, histogramBase);
  sum = indices[dim];
  tileEdges.resize (sum);
  SMART_LOG ("sum:" << sum << " decr:" << ef->getDecr ());
  if (ef->getDecr ()) {
    for (size_t i = 0; i < dim; ++i)
      histogramBase[i] = sum - DimTile (histogramBase[i]);
    indices++;
  } else
    indices[0] = 0;
  // extract edges
  TileEdge<WeightT> *tileEdgesP (tileEdges.data ());
  indices -= int (minVal);
  const Size<GeoDimT> &imageSize (border.getSize ());
  forEachEdgeIdx (rect, Volume,
		  [&ef, &tileEdgesP, &indices, &rect, &imageSize] (const DimImg &a, const DimImg &b) {
		    WeightT weight = ef->getWeight (a, b);
		    TileEdge<WeightT> &tileEdge = tileEdgesP[indices[int (weight)]++];
		    tileEdge.indexes[0] = rect.relIdx (a, imageSize);
		    tileEdge.indexes[1] = rect.relIdx (b, imageSize);
		    tileEdge.weight = weight;
		    SMART_LOG_EXPR (cerr << printTileEdge (tileEdge, imageSize, rect) << endl);
		  });
  SMART_LOG (endl << printTileEdges (tileEdgesP, imageSize, rect, sum));
  return sum;
}

template<int GeoDimT>
template <typename PixelT, typename WeightT, TreeType TreeTypeT>
DimEdge
GraphWalker<GeoDimT>::getQSortedTileEdges (const Rect<GeoDimT> &rect, vector <TileEdge<WeightT> > &tileEdges,
					   const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b) ef->sort ()*/) const {
  SMART_DEF_LOG ("GraphWalker::getSortedTileEdges", "");
  if (TreeTypeT == TOS) {
    cerr << endl << "*** GraphWalker::getCountingSortedTileEdges: not allowed with TOS ***" << endl;
    return 0;
  }

  tileEdges.resize (edgeMaxCount (rect.size, Volume));
  const Size<GeoDimT> &imageSize (border.getSize ());
  DimEdge tileEdgeCount (0);
  TileEdge<WeightT> *tileEdgesP (tileEdges.data ());
  forEachEdgeIdx (rect, Volume,
		  [&tileEdgesP, &tileEdgeCount, &rect, &imageSize, &ef] (const DimImg &a, const DimImg &b) {
		    TileEdge<WeightT> &tileEdge = tileEdgesP[tileEdgeCount++];
		    tileEdge.indexes[0] = rect.relIdx (a, imageSize);
		    tileEdge.indexes[1] = rect.relIdx (b, imageSize);
		    tileEdge.weight = ef->getWeight (a, b);
		  });
  ef->sort (tileEdgesP, tileEdgeCount);
  SMART_LOG (endl << printTileEdges (tileEdgesP, border.getSize (), rect, tileEdgeCount));
  return tileEdgeCount;
}

// ================================================================================
template<int GeoDimT>
template <typename PixelT, typename WeightT, TreeType TreeTypeT>
// template DimImg
DimEdge
GraphWalker<GeoDimT>::getSortedEdges (const Rect<GeoDimT> &rect, TileShape tileShape, vector <Edge<WeightT, GeoDimT> > &edges,
				      const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b)*/) const {
  SMART_DEF_LOG ("GraphWalker::getCountingSortedEdges", "");
  BOOST_ASSERT (!rect.isNull ());
  if (TreeTypeT == TOS) {
    cerr << endl << "*** GraphWalker::getCountingSortedTileEdges: not allowed with TOS ***" << endl;
    return 0;
  }
  if (sizeof (WeightT) > countingSortCeil)
    return getQSortedEdges (rect, tileShape, edges, ef);
  double maxVal (numeric_limits<WeightT>::max ()); // not const to avoid Warning overflow
  double minVal (numeric_limits<WeightT>::min ()); // not const to avoid Warning overflow
  const DimTile dim (maxVal - minVal + 1);
  SMART_LOG ("dim:" << dim);
  vector<DimEdge> counters (dim+1, 0);
  DimEdge *indices = counters.data ();
  DimEdge * const histogramBase = indices+1;
  DimEdge * const histogramRelative = histogramBase-int (minVal);
  DimEdge sum (0);
  forEachEdgeIdx (rect, tileShape,
		  [&histogramRelative, &ef] (const DimImg &a, const DimImg &b) {
		    ++histogramRelative[int (ef->getWeight (a, b))];
		  });

  // get indices by prefix sum
  partial_sum (histogramBase, histogramBase+dim, histogramBase);
  sum = indices[dim];
  edges.resize (sum);
  SMART_LOG ("sum:" << sum << " decr:" << ef->getDecr ());
  if (ef->getDecr ()) {
    for (size_t i = 0; i < dim; ++i)
      histogramBase[i] = sum - DimImg (histogramBase[i]);
    ++indices;
  } else
    indices[0] = 0;
  // extract edges
  Edge<WeightT, GeoDimT> *edgesP (edges.data ());
  indices -= int (minVal);
  const Size<GeoDimT> imageSize (border.getSize ());
  forEachEdgeIdx (rect, tileShape,
		  [&ef, &edgesP, &indices, &minVal, &imageSize] (const DimImg &a, const DimImg &b) {
		    WeightT weight = ef->getWeight (a, b);
		    Edge<WeightT, GeoDimT> &edge = edgesP[indices[int (weight)]++]; // à vérifier
		    edge.setAB (imageSize, a, b);
		    edge.setWeight (weight);
		    SMART_LOG_EXPR (cerr << printEdge (edge, imageSize) << endl);
		  });
  SMART_LOG (endl << printEdges (edgesP, imageSize, sum));
  return sum;
}

template<int GeoDimT>
template <typename PixelT, typename WeightT, TreeType TreeTypeT>
DimEdge
GraphWalker<GeoDimT>::getQSortedEdges (const Rect<GeoDimT> &rect, TileShape tileShape, vector <Edge<WeightT, GeoDimT> > &edges,
				       const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b) ef->sort ()*/) const {
  SMART_DEF_LOG ("GraphWalker::getSortedEdges", "");
  if (TreeTypeT == TOS) {
    cerr << endl << "*** GraphWalker::getCountingSortedTileEdges: not allowed with TOS ***" << endl;
    return 0;
  }

  edges.resize (edgeMaxCount (rect.size, tileShape));
  const Size<GeoDimT> imageSize (border.getSize ());
  DimEdge edgeCount (0);
  Edge<WeightT, GeoDimT> *edgesP (edges.data ());
  forEachEdgeIdx (rect, tileShape,
		  [&edgesP, &edgeCount, &imageSize, &ef] (const DimImg &a, const DimImg &b) {
		    Edge<WeightT, GeoDimT> &edge = edgesP[edgeCount];
		    ++edgeCount;
		    edge.setAB (imageSize, a, b);
		    edge.setWeight (ef->getWeight (a, b));
		  });
  // SMART_LOG (endl << printEdges (edges, border.getSize (), edgeCount));
  ef->sort (edgesP, edgeCount);
  SMART_LOG (endl << printEdges (edgesP, border.getSize (), edgeCount));
  return edgeCount;
}

// ================================================================================
// GeoDimT 2
// ================================================================================
template<>
BorderFlagType
GraphWalker<2>::getBorderFlag (const Size<2> &size, const DimImg &idx) {
  BorderFlagType result (NO);
  const DimImg x (idx % size.side [0]);
  const DimImg y (idx / size.side [0]);
  if (!x)
    result |= X1;
  if (x == size.side [0]-1)
    result |= X2;
  if (!y)
    result |= Y1;
  if (y == size.side [1]-1)
    result |= Y2;
  return result;
}

template<>
DimEdge
GraphWalker<2>::edgeMaxCount (const Size<2> &tileSize) const {
  if (tileSize.isNull ())
    return 0;
  const DimImg wt (tileSize.side [0]);
  const DimImg wp (tileSize.side [0]-1);
  const DimImg ht (tileSize.side [1]);
  const DimImg hp (tileSize.side [1]-1);
  DimEdge plan (0);
  if (connectivity & Connectivity::C4)
    plan += wt*hp + wp*ht;
  if (connectivity & Connectivity::C6P)
    plan += wp*hp;
  if (connectivity & Connectivity::C6N)
    plan += wp*hp;
  return plan;
}

template<>
DimEdge
GraphWalker<2>::edgeMaxCount (const Size<2> &size, const TileShape &boundaryAxe) const {
  DEF_LOG ("GraphWalker::edgeMaxCount", "boundaryAxe:" << boundaryAxe << " size:" << size);
  DimEdge result (0);
  if (size.isNull ()) {
    LOG ("null: " << size);
    return result;
  }
  switch (boundaryAxe) {
  case Volume:
    result = edgeMaxCount (size);
    break;
  case Vertical:
    if (connectivity & Connectivity::C4)
      result += size.side [1];
    if (connectivity & Connectivity::C6P)
      result += size.side [1]-1;
    if (connectivity & Connectivity::C6N)
      result += size.side [1]-1;
    break;

  case Horizontal:
    if (connectivity & Connectivity::C4)
      result += size.side [0];
    if (connectivity & Connectivity::C6P)
      result += size.side [0]-1;
    if (connectivity & Connectivity::C6N)
      result += size.side [0]-1;
    break;

  case Plane:
    break;

  default:
    BOOST_ASSERT (false);
  }
  LOG ("result: " << result);
  return result;
}

// ================================================================================
