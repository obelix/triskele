////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2022							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

// is_any_of
#include <boost/algorithm/string.hpp>

// pixels type
#include "gdal.h"
// log functions
#include "obelixDebug.hpp"
// Point and Size (2D or 3D)
#include "obelixGeo.hpp"
// read/write images
#include "IImage.hpp"
// nodata pixels definition in images
#include "Border.hpp"

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "AttributeProfiles.hpp"

using namespace std;
using namespace boost;
using namespace obelix;
using namespace obelix::triskele;

static const int GeoDimT (2);
static const TreeType treeType = MAX;

template<typename PixelT> void filter (const IImage<GeoDimT> &inputImage, IImage<GeoDimT> &outputImage, const vector <DimImgPack> &thresholds);

// ========================================
int
main (int argc, char** argv, char** envp) {
  // get parameters
  if (argc != 4) {
    cout << "Usage: " << argv[0] << " input-filename outpout-filename thresholds,..." << endl;
    exit (1);
  }
  // set thresholds vetcor
  vector <string> values;
  split (values, argv[3], is_any_of (","));
  vector <DimImgPack> thresholds (values.size ());
  transform (values.begin (), values.end (), thresholds.begin (), [] (const string &val) { return stoi (val); });

  IImage<GeoDimT> inputImage (argv[1]);
  // get metadata
  inputImage.readImage ();
  // get image size (width x height)
  const Size<GeoDimT> size (inputImage.getSize ());

  IImage<GeoDimT> outputImage (argv[2]);
  // set metadata
  outputImage.createImage (size, inputImage.getDataType (), thresholds.size (),
			   inputImage, NullPoint2D);

  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    filter<uint8_t> (inputImage, outputImage, thresholds); break;
  case GDT_UInt16:
    filter<uint16_t> (inputImage, outputImage, thresholds); break;
  case GDT_Int16:
    filter<int16_t> (inputImage, outputImage, thresholds); break;
  case GDT_UInt32:
    filter<uint32_t> (inputImage, outputImage, thresholds); break;
  case GDT_Int32:
    filter<int32_t> (inputImage, outputImage, thresholds); break;
  case GDT_Float32:
    filter<float> (inputImage, outputImage, thresholds); break;
  case GDT_Float64:
    filter<double> (inputImage, outputImage, thresholds); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}
 
// ========================================
template<typename PixelT>
void
filter (const IImage<GeoDimT> &inputImage, IImage<GeoDimT> &outputImage, const vector <DimImgPack> &thresholds) {

  // switch on debug mode
  // Log::debug = true;

  // empty raster
  Raster<PixelT, GeoDimT> raster;
  // image size (width x height)
  const Size<GeoDimT> size (inputImage.getSize ());
  // read first band (0) in 2D mode from origine [0,0] to end [width, height].
  inputImage.readBand (raster, 0, NullPoint2D, size);

  // no border (i.e. all pixels are take in account)
  const Border<GeoDimT> border (size, false);
  // neighbors take in account (default connectivity C4)
  const GraphWalker<GeoDimT> graphWalker (border);
  // tree builder base on raster, connectivity and type of tree
  ArrayTreeBuilder<PixelT, PixelT, GeoDimT> atb (raster, graphWalker, treeType);
  // get the number of core
  const DimCore coreCount (boost::thread::hardware_concurrency ());
  // declare empty tree and set the number of thread for all algorithms
  Tree<GeoDimT> tree (coreCount);
  // reserve nodes weight attributes
  WeightAttributes<PixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));

  atb.buildTree (tree, weightAttributes);
  // create area attribute
  const AreaAttributes<GeoDimT> areaAttributes (tree);

  // output channels count
  const DimChannel outputChannelCount (thresholds.size ());
  // number of pixels (width x height)
  const DimImg pixelsCount (size.getPixelsCount ());

  // write node grayscale as attribute profiles
  AttributeProfiles<PixelT, GeoDimT> attributeProfiles (tree);
  // copy weightAttributes to attributeProfiles
  atb.setAttributProfiles (attributeProfiles);

  // result allocation memory
  vector <vector <PixelT> > allBands (outputChannelCount, vector<PixelT> (pixelsCount, 0));
  // do all cuts
  areaAttributes.cut (allBands, attributeProfiles, thresholds);

  // for all cuts
  for (DimChannel channel (0); channel < outputChannelCount; ++channel)
    // write output raster
    outputImage.writeBand (allBands[channel].data (), channel);
}
