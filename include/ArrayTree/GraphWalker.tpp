////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_GraphWalker_tpp
#define _Obelix_Triskele_GraphWalker_tpp

const DimSideImg DimImgCacheSize = 1024*16/4; // pageSize * nbPage(=cache/connectivity) / maxPixelSize
//const DimSideImg DimImgCacheSize = 3; // testGraphwalker only

// ================================================================================
inline BorderFlagType operator~ (const BorderFlagType &a) { return (BorderFlagType)~(uint)a; }
inline BorderFlagType operator| (const BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType)((uint)a | (uint)b); }
inline BorderFlagType operator& (const BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType)((uint)a & (uint)b); }
inline BorderFlagType operator^ (const BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType)((uint)a ^ (uint)b); }
inline BorderFlagType& operator|= (BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType&)((uint&)a |= (uint)b); }
inline BorderFlagType& operator&= (BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType&)((uint&)a &= (uint)b); }
// inline BorderFlagType& operator^= (BorderFlagType &a, const BorderFlagType &b) { return (BorderFlagType&)((uint&)a ^= (uint)b); }

// ================================================================================
template<int GeoDimT>
inline const Size<GeoDimT> &
GraphWalker<GeoDimT>::getSize () const {
  return border.getSize ();
}

// ================================================================================
template<int GeoDimT>
template<typename Funct>
inline DimImg
GraphWalker<GeoDimT>::forEachVertexSimple (const Funct &lambda /*(const DimImg &idx)*/) const {
  DimImg maxCount = border.getSize ().getPixelsCount ();
  DimImg vertexCount = 0;
  if (border.exists ())
    for (DimImg idx = 0; idx < maxCount; ++idx) {
      if (!border.isBorder (idx)) {
	lambda (idx);
	++vertexCount;
      }
    }
  else
    for (DimImg idx = 0; idx < maxCount; ++idx) {
      lambda (idx);
      ++vertexCount;
    }
  return vertexCount;
}

template<int GeoDimT>
template<typename Funct>
inline DimImg
GraphWalker<GeoDimT>::forEachVertex (const Rect<GeoDimT> &rect,
				     const Funct &lambda /*(const DimImg &idx, const BorderFlagType &lowBorder)*/) const {
  const Size<GeoDimT> &size (border.getSize ());
  DimImg vertexCount (0);

  const DimImg leftWidth (size.side [0]-rect.size.side [0]);
  const DimImg leftFrame ((size.side [1]-rect.size.side [1])*size.side [0]);

  const DimSideImg firstX (rect.point.coord [0]);
  const DimSideImg firstY (rect.point.coord [1]);
  const DimChannel firstZ (rect.point.coord [2]);

  const DimSideImg lastX (rect.point.coord [0]+rect.size.side [0]-1);
  const DimSideImg lastY (rect.point.coord [1]+rect.size.side [1]-1);
  const DimChannel lastZ (rect.point.coord [2]+rect.size.side [2]-1);

  DimImg idx (point2idx (size, rect.point));
  BorderFlagType lowBorder (Z1);
  if (border.exists ())
    for (DimChannel z = firstZ; ; ) {
      if (z == lastZ)
	lowBorder |= Z2;
      lowBorder |= Y1;
      lowBorder &= ~Y2;
      for (DimSideImg y = firstY; ; ) {
	if (y == lastY)
	  lowBorder |= Y2;
	lowBorder |= X1;
	lowBorder &= ~X2;
	for (DimSideImg x = firstX; ; ) {
	  if (x == lastX)
	    lowBorder |= X2;
	  if (!border.isBorder (idx)) {
	    lambda (idx, lowBorder);
	    ++vertexCount;
	  }
	  lowBorder &= ~X1;
	  ++idx;
	  if (++x > lastX)
	    break;
	}
	lowBorder &= ~Y1;
	if (++y > lastY)
	  break;
	idx += leftWidth;
      }
      lowBorder &= ~Z1;
      if (++z > lastZ)
	break;
      idx += leftWidth;
      idx += leftFrame;
    }
  else
    for (DimChannel z = firstZ; ; ) {
      if (z == lastZ)
	lowBorder |= Z2;
      lowBorder |= Y1;
      lowBorder &= ~Y2;
      for (DimSideImg y = firstY; ; ) {
	if (y == lastY)
	  lowBorder |= Y2;
	lowBorder |= X1;
	lowBorder &= ~X2;
	for (DimSideImg x = firstX; ; ) {
	  if (x == lastX)
	    lowBorder |= X2;
	  lambda (idx, lowBorder);
	  ++vertexCount;
	  lowBorder &= ~X1;
	  ++idx;
	  if (++x > lastX)
	    break;
	}
	lowBorder &= ~Y1;
	if (++y > lastY)
	  break;
	idx += leftWidth;
      }
      lowBorder &= ~Z1;
      if (++z > lastZ)
	break;
      idx += leftWidth;
      idx += leftFrame;
    }

  return vertexCount;
}

// ================================================================================
template<int GeoDimT>
template<typename Funct>
inline DimImg
GraphWalker<GeoDimT>::forEachVertexYX (const Rect<GeoDimT> &rect,
				       const Funct &lambda /*(const DimImg &idx, const BorderFlagType &borderFlags)*/) const {
  const Size<GeoDimT> &size (border.getSize ());
  DimImg vertexCount = 0;

  const DimImg leftWidth (size.side [0]-rect.size.side [0]);
  const DimImg leftFrame ((size.side [1]-rect.size.side [1])*size.side [0]);

  const DimSideImg firstX (rect.point.coord [0]);
  const DimSideImg firstY (rect.point.coord [1]);
  const DimChannel firstZ (rect.point.coord [2]);

  const DimSideImg lastX (rect.point.coord [0]+rect.size.side [0]-1);
  const DimSideImg lastY (rect.point.coord [1]+rect.size.side [1]-1);
  const DimChannel lastZ (rect.point.coord [2]+rect.size.side [2]-1);

  DimImg idx = point2idx (size, rect.point)-firstX;
  BorderFlagType borderFlags (Z1);
  if (border.exists ())
    for (DimChannel z = firstZ; ; ) {
      if (z == lastZ)
	borderFlags |= Z2;

      DimImg fOrig (idx);
      for (DimSideImg sx = firstX, colId = firstX/DimImgCacheSize+1U; ; ++colId) {
	const DimSideImg xLimit (min (DimImgCacheSize*colId -1U, lastX));

	borderFlags |= Y1;
	borderFlags &= ~Y2;
	DimImg lOrig (0);
	for (DimSideImg y = firstY; ; lOrig += size.side [0]) {
	  if (y == lastY)

	    borderFlags |= Y2;
	  // reset idx
	  idx = fOrig+lOrig+sx;

	  if (sx == firstX)
	    borderFlags |= X1;
	  borderFlags &= ~X2;
	  for (DimSideImg x = sx; ;) {
	    if (x == lastX)
	      borderFlags |= X2;
	    if (!border.isBorder (idx)) {
	      lambda (idx, borderFlags); //, Point<GeoDimT> (x, y, z));
	      ++vertexCount;
	    }
	    borderFlags &= ~X1;
	    ++idx;
	    if (++x > xLimit)
	      break;
	  }
	  borderFlags &= ~Y1;
	  if (++y > lastY)
	    break;
	  idx += leftWidth;
	}
	sx = xLimit;
	if (++sx > lastX)
	  break;

      }
      borderFlags &= ~Z1;
      if (++z > lastZ)
	break;
      idx += leftWidth;
      idx += leftFrame;
    }
  else
    for (DimChannel z = firstZ; ; ) {
      if (z == lastZ)
	borderFlags |= Z2;

      DimImg fOrig (idx);
      for (DimSideImg sx = firstX, colId = firstX/DimImgCacheSize+1U; ; ++colId) {
	const DimSideImg xLimit (min (DimImgCacheSize*colId -1U, lastX));

	borderFlags |= Y1;
	borderFlags &= ~Y2;
	DimImg lOrig (0);
	for (DimSideImg y = firstY; ; lOrig += size.side [0]) {
	  if (y == lastY)

	    borderFlags |= Y2;
	  // reset idx
	  idx = fOrig+lOrig+sx;

	  if (sx == firstX)
	    borderFlags |= X1;
	  borderFlags &= ~X2;
	  for (DimSideImg x = sx; ;) {
	    if (x == lastX)
	      borderFlags |= X2;
	    lambda (idx, borderFlags); //, Point<GeoDimT> (x, y, z));
	    ++vertexCount;
	    borderFlags &= ~X1;
	    ++idx;
	    if (++x > xLimit)
	      break;
	  }
	  borderFlags &= ~Y1;
	  if (++y > lastY)
	    break;
	  idx += leftWidth;
	}
	sx = xLimit;
	if (++sx > lastX)
	  break;

      }
      borderFlags &= ~Z1;
      if (++z > lastZ)
	break;
      idx += leftWidth;
      idx += leftFrame;
    }

  return vertexCount;
}

// ================================================================================
template<int GeoDimT>
template<typename Funct>
inline DimEdge
GraphWalker<GeoDimT>::forEachPrevEdge (const Rect<GeoDimT> &edgeRect, const vector<Neighbor> prevDelta, const BorderFlagType &mask,
				       const Funct &lambda /*(const DimImg &a, const DimImg &b)*/) const {
  DimImg edgeCount = 0;
  if (edgeRect.size.isNull () || prevDelta.empty ())
    return edgeCount;
  const Size<GeoDimT> size (border.getSize ());
  forEachVertex (edgeRect,
		 [this, &edgeCount, &lambda, &prevDelta, &mask] (const DimImg &a, const BorderFlagType &borderFlags) {
		   if (border.exists ())
		     for (Neighbor delta : prevDelta) {
		       if (delta.border & borderFlags & mask)
			 continue;
		       const DimImg b (a-delta.off);
		       if (border.isBorder (a) || border.isBorder (b))
			 continue;
		       lambda (a, b);
		       ++edgeCount;
		     }
		   else
		     for (Neighbor delta : prevDelta) {
		       if (delta.border & borderFlags & mask)
			 continue;
		       const DimImg b (a-delta.off);
		       lambda (a, b);
		       ++edgeCount;
		     }
		 });
  return edgeCount;
}

// ================================================================================
template<int GeoDimT>
template<typename Funct>
inline DimEdge
GraphWalker<GeoDimT>::forEachEdgeIdx (const Rect<GeoDimT> &rect, TileShape tileShape,
				      const Funct &lambda /*(const DimImg &a, const DimImg &b)*/) const {
  DimImg edgeCount = 0;
  if (rect.isNull ())
    return edgeCount;

  const DimImg x (rect.point.coord [0]);
  const DimImg y (rect.point.coord [1]);
  const DimChannel z (rect.point.coord [2]);
  const DimImg w (rect.size.side [0]);
  const DimImg h (rect.size.side [1]);
  const DimChannel l (rect.size.side [2]);

  switch (tileShape) {

  case Volume:
    return forEachPrevEdge (rect, volDelta2, ALL, lambda);

  case Horizontal: {
    return
      forEachPrevEdge (Rect<GeoDimT> (Point<GeoDimT> (x, y-1U, z), Size<GeoDimT> (1U, h, l)), horDelta1, ALL^Y2, lambda) +
      forEachPrevEdge (Rect<GeoDimT> (rect.point, Size<GeoDimT> (w, 1U, l)), horDelta2, ALL^Y1, lambda);
  }
    break;

  case Vertical: {
    return
      forEachPrevEdge (Rect<GeoDimT> (Point<GeoDimT> (x-1U, y, z), Size<GeoDimT> (1U, h, l)), verDelta1, ALL^X2, lambda) +
      forEachPrevEdge (Rect<GeoDimT> (rect.point, Size<GeoDimT> (1U, h, l)), verDelta2, ALL^X1, lambda);
  }
    break;

  case Plane: {
    return
      forEachPrevEdge (Rect<GeoDimT> (rect.point, Size<GeoDimT> (w, h, 1U)), plaDelta, ALL^Z1, lambda);
  }
    break;

  default:
    BOOST_ASSERT (false);
  }
  return edgeCount;
}

// ================================================================================
template<int GeoDimT>
template<typename Funct>
inline void
GraphWalker<GeoDimT>::forEachNeighbor (const DimImg &a, const BorderFlagType &borderFlags, const Funct &lambda /*(const DimImg &b)*/) const {
  for (Neighbor delta : volDelta1) {
    if (delta.border & borderFlags)
      continue;
    lambda (a+delta.off);
  }
  for (Neighbor delta : volDelta2) {
    if (delta.border & borderFlags)
      continue;
    lambda (a-delta.off);
  }
}

// ================================================================================
// GeoDimT 2
// ================================================================================
template<>
template<typename Funct>
inline DimImg
GraphWalker<2>::forEachVertex (const Rect<2> &rect,
			       const Funct &lambda /*(const DimImg &idx, const BorderFlagType &lowBorder)*/) const {
  const Size<2> &size (border.getSize ());
  DimImg vertexCount (0);

  const DimImg leftWidth (size.side [0]-rect.size.side [0]);

  const DimSideImg firstX (rect.point.coord [0]);
  const DimSideImg firstY (rect.point.coord [1]);

  const DimSideImg lastX (rect.point.coord [0]+rect.size.side [0]-1);
  const DimSideImg lastY (rect.point.coord [1]+rect.size.side [1]-1);

  DimImg idx (point2idx (size, rect.point));
  BorderFlagType lowBorder (Y1);
  if (border.exists ())
    for (DimSideImg y = firstY; ; ) {
      if (y == lastY)
	lowBorder |= Y2;
      lowBorder |= X1;
      lowBorder &= ~X2;
      for (DimSideImg x = firstX; ; ) {
	if (x == lastX)
	  lowBorder |= X2;
	if (!border.isBorder (idx)) {
	  lambda (idx, lowBorder);
	  ++vertexCount;
	}
	lowBorder &= ~X1;
	++idx;
	if (++x > lastX)
	  break;
      }
      lowBorder &= ~Y1;
      if (++y > lastY)
	break;
      idx += leftWidth;
    }
  else
    for (DimSideImg y = firstY; ; ) {
      if (y == lastY)
	lowBorder |= Y2;
      lowBorder |= X1;
      lowBorder &= ~X2;
      for (DimSideImg x = firstX; ; ) {
	if (x == lastX)
	  lowBorder |= X2;
	lambda (idx, lowBorder);
	++vertexCount;
	lowBorder &= ~X1;
	++idx;
	if (++x > lastX)
	  break;
      }
      lowBorder &= ~Y1;
      if (++y > lastY)
	break;
      idx += leftWidth;
    }
  return vertexCount;
}

// ================================================================================
template<>
template<typename Funct>
inline DimImg
GraphWalker<2>::forEachVertexYX (const Rect<2> &rect,
				 const Funct &lambda /*(const DimImg &idx, const BorderFlagType &borderFlags)*/) const {
  const Size<2> &size (border.getSize ());
  DimImg vertexCount = 0;

  const DimImg leftWidth (size.side [0]-rect.size.side [0]);

  const DimSideImg firstX (rect.point.coord [0]);
  const DimSideImg firstY (rect.point.coord [1]);

  const DimSideImg lastX (rect.point.coord [0]+rect.size.side [0]-1);
  const DimSideImg lastY (rect.point.coord [1]+rect.size.side [1]-1);

  DimImg idx = point2idx (size, rect.point)-firstX;
  BorderFlagType borderFlags (NO);

  DimImg fOrig (idx);
  if (border.exists ())
    for (DimSideImg sx = firstX, colId = firstX/DimImgCacheSize+1U; ; ++colId) {
      const DimSideImg xLimit (min (DimImgCacheSize*colId -1U, lastX));

      borderFlags |= Y1;
      borderFlags &= ~Y2;
      DimImg lOrig (0);
      for (DimSideImg y = firstY; ; lOrig += size.side [0]) {
	if (y == lastY)

	  borderFlags |= Y2;
	// reset idx
	idx = fOrig+lOrig+sx;

	if (sx == firstX)
	  borderFlags |= X1;
	borderFlags &= ~X2;
	for (DimSideImg x = sx; ;) {
	  if (x == lastX)
	    borderFlags |= X2;
	  if (!border.isBorder (idx)) {
	    lambda (idx, borderFlags); //, Point<2> (x, y));
	    ++vertexCount;
	  }
	  borderFlags &= ~X1;
	  ++idx;
	  if (++x > xLimit)
	    break;
	}
	borderFlags &= ~Y1;
	if (++y > lastY)
	  break;
	idx += leftWidth;
      }
      sx = xLimit;
      if (++sx > lastX)
	break;
    }
  else
    for (DimSideImg sx = firstX, colId = firstX/DimImgCacheSize+1U; ; ++colId) {
      const DimSideImg xLimit (min (DimImgCacheSize*colId -1U, lastX));

      borderFlags |= Y1;
      borderFlags &= ~Y2;
      DimImg lOrig (0);
      for (DimSideImg y = firstY; ; lOrig += size.side [0]) {
	if (y == lastY)

	  borderFlags |= Y2;
	// reset idx
	idx = fOrig+lOrig+sx;

	if (sx == firstX)
	  borderFlags |= X1;
	borderFlags &= ~X2;
	for (DimSideImg x = sx; ;) {
	  if (x == lastX)
	    borderFlags |= X2;
	  lambda (idx, borderFlags); //, Point<2> (x, y));
	  ++vertexCount;
	  borderFlags &= ~X1;
	  ++idx;
	  if (++x > xLimit)
	    break;
	}
	borderFlags &= ~Y1;
	if (++y > lastY)
	  break;
	idx += leftWidth;
      }
      sx = xLimit;
      if (++sx > lastX)
	break;

    }

  return vertexCount;
}

// ================================================================================
template<>
template<typename Funct>
inline DimEdge
GraphWalker<2>::forEachEdgeIdx (const Rect<2> &rect, TileShape tileShape,
				const Funct &lambda /*(const DimImg &a, const DimImg &b)*/) const {
  DimImg edgeCount = 0;
  if (rect.isNull ())
    return edgeCount;

  const DimImg x (rect.point.coord [0]);
  const DimImg y (rect.point.coord [1]);
  const DimImg w (rect.size.side [0]);
  const DimImg h (rect.size.side [1]);

  switch (tileShape) {

  case Volume:
    return forEachPrevEdge (rect, volDelta2, ALL, lambda);

  case Horizontal: {
    return
      forEachPrevEdge (Rect<2> (Point<2> (x, y-1U), Size<2> (1U, h)), horDelta1, ALL^Y2, lambda) +
      forEachPrevEdge (Rect<2> (rect.point, Size<2> (w, 1U)), horDelta2, ALL^Y1, lambda);
  }
    break;

  case Vertical: {
    return
      forEachPrevEdge (Rect<2> (Point<2> (x-1U, y), Size<2> (1U, h)), verDelta1, ALL^X2, lambda) +
      forEachPrevEdge (Rect<2> (rect.point, Size<2> (1U, h)), verDelta2, ALL^X1, lambda);
  }
    break;

  default:
    BOOST_ASSERT (false);
  }
  return edgeCount;
}


// ================================================================================
#endif // _Obelix_Triskele_GraphWalker_tpp
