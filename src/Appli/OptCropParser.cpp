////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <boost/program_options.hpp>

#include "obelixDebug.hpp"
#include "misc.hpp"

#include "Appli/OptCropParser.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix;

namespace po = boost::program_options;

// ================================================================================

OptCropParser::OptCropParser ()
  : left ("-1"),
    top ("-1"),
    width ("-1"),
    height ("-1"),
    description ("Crop options", getCols ()) {
  DEF_LOG ("OptCropParser::OptCropParser", "");

  description.add_options ()
    ("left,x", po::value<OptRate> (&left)->default_value (left), "left crop (-1 = center)")
    ("top,y", po::value<OptRate> (&top)->default_value (top), "top crop (-1 = middle)")
    ("width,w", po::value<OptRate> (&width)->default_value (width), "width crop (-1 = input width)")
    ("height,h", po::value<OptRate> (&height)->default_value (height), "height crop (-1 input height)")
    ;
}

// ================================================================================
void
OptCropParser::parse (OptCrop &optCrop, const Size<2> &size, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptCropParser::parse", "");

  long
    x (left.getValue ()),
    y (top.getValue ()),
    w (width.getValue ()),
    h (height.getValue ());

  if (size.isNull ()) {
    if (left.isPercent () || top.isPercent () || width.isPercent () || height.isPercent () ||
	x < 0 || y < 0 || w < 0 || h < 0)
      throw invalid_argument ("empty input size");
  } else {
    if (x >= 0)
      x = left.getRate (size.side [0]);
    if (y >= 0)
      y = top.getRate (size.side [1]);
    if (w >= 0)
      w = width.getRate (size.side [0]);
    if (h >= 0)
      h = height.getRate (size.side [1]);

    if (w < 0 || w > (long) size.side [0])
      w = x <= 0 ? (long) size.side [0] : (x < size.side [0] ? (long) size.side [0]-x : 1L);
    if (h < 0 || h > (long) size.side [1])
      h = y <= 0 ? (long) size.side [1] : (y < size.side [1] ? (long) size.side [1]-y : 1L);

    if (x < 0)
      x = ((long) size.side [0] - w)/2;
    if (y < 0)
      y = ((long) size.side [1] - h)/2;

    if (x > (long) size.side [0] - w)
      x = (long) size.side [0] - w;
    if (y > (long) size.side [1] - h)
      y = (long) size.side [1] - h;
  }
  optCrop.topLeft = Point<2> ((DimSideImg) x, (DimSideImg) y);
  optCrop.size = Size<2> ((DimSideImg) w, (DimSideImg) h);
}


// ================================================================================
