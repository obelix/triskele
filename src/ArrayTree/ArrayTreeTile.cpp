////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>

using namespace boost::chrono;
#include "misc.hpp"

#include "ArrayTree/Weight.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"


using namespace obelix;
using namespace obelix::triskele;
#include "ArrayTreeTile.tcpp"

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::ArrayTreeTile (const GraphWalker<GeoDimT> &graphWalker)
  : graphWalker (graphWalker),
    crop (),
    leafCount (),
    leaders (),
    tileTop (0),
    tParents (),
    leafParents (nullptr),
    compParents (nullptr),
    tWeights (),
    weights (nullptr),
    tChildCount (),
    childCount (nullptr),
    getEdgesTime (0),
    memPeak (0) {
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::ArrayTreeTile",
	   "crop: " << crop << " leafCount: " << leafCount << " tileTop: " << tileTop <<
	   " tParents.size: " << tParents.size () << " tWeights.size: " << tWeights.size () << " tChildCount: " << tChildCount.size ());
}

template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::init (const Rect<GeoDimT> &crop) {
  leafCount = crop.size.getPixelsCount ();
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::init", "crop: " << crop << " leafCount: " << leafCount);
  this->crop = crop;
  leaders.book (leafCount);
}

template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::dynTileAlloc () {
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::dynTileAlloc", "");
  DimTile estimatedCount = leafCount;
  for (DimTile i = 0; i < 10; ++i) {
    DimTile threshold = leafCount / DimTile (10U) * i;
    // XXX min (img, max (10% img, 10% pixels))
    if (tileTop < threshold) {
      estimatedCount = threshold;
      break;
    }
  }
  LOG ("estimatedCount:" << estimatedCount << " leafCount:" << leafCount);
  if (estimatedCount > tChildCount.size ()) {
    tParents.resize (leafCount+estimatedCount, DimTile_MAX);
    tWeights.resize (estimatedCount);
    tChildCount.resize (estimatedCount, 0U);
  }
  leafParents = tParents.data ();
  compParents = tParents.data ()+leafCount;
  weights = tWeights.data ();
  childCount = tChildCount.data ();
}

template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::free () {
  leaders.free ();
  // vector<DimTile> ().swap (tParents);
  // vector<WeightT> ().swap (tWeights);
  // vector<DimTile> ().swap (tChildCount);
  tParents = vector<DimTile> (0);
  tWeights = vector<WeightT> ();
  tChildCount = vector<DimTile> ();
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
DimTile
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::findRoot (DimTile comp) const {
  if (comp == DimTile_MAX)
    return comp;
  for (;;) {
    DimTile p = compParents [comp];
    if (p == DimTile_MAX)
      return comp;
    comp = p;
  }
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::createTileComp (const WeightT &weight, DimTile &parentChildA, DimTile &parentChildB) {
  if (tileTop >= tChildCount.size ()) {
    DimNode idxA (&parentChildA - leafParents);
    DimNode idxB (&parentChildB - leafParents);
    dynTileAlloc ();
    leafParents[idxA] = leafParents [idxB] = tileTop;
  } else
    parentChildA = parentChildB = tileTop;
  childCount [tileTop] = 2U;
  weights [tileTop] = weight;
  ++tileTop;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::addChild (const DimTile &parent, DimTile &child) {
  // DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::addChild", "parent:" << parent << " child:" << child);
  ++childCount [parent];
  child = parent;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::addChildren (const DimTile &parent, const DimTile &sibling) {
  // DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::addChildren", "parent:" << parent << " sibling:" << sibling);
  childCount [parent] += childCount [sibling];
  childCount [sibling] = 0;
  compParents [sibling] = parent;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::move (const DimParent &offset, DimParentPack *iParents, DimImgPack *iChildCount, WeightT *iWeights) const {
  // DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::move",  "offset: " << offset);
  const Size<GeoDimT> imgSize (graphWalker.getSize ());
  for (DimTile leafIdx = 0U; leafIdx < leafCount; ++leafIdx) {
    // LOG ("point:" << crop.absPt (leafIdx) << " : " << idx2point (crop.size, leafIdx) << " / " << crop.absIdx (leafIdx, imgSize) << " : " << leafIdx << " crop: " << crop);
    iParents [crop.absIdx (leafIdx, imgSize)] = (leafParents[leafIdx] == DimTile_MAX) ? DimParent_MAX : (leafParents[leafIdx] + offset);
  }
  DimTile *srcParent = compParents;
  DimTile *srcCount = childCount;
  WeightT *srcWeight = weights;
  DimParentPack *dstParent = iParents+imgSize.getPixelsCount ()+offset;
  DimImgPack *dstCount = iChildCount+offset;
  WeightT *dstWeight = iWeights+offset;
  for (DimTile curComp = 0U; curComp < tileTop; ++curComp, ++srcParent, ++dstParent, ++srcCount, ++dstCount, ++srcWeight, ++dstWeight) {
    *dstCount = *srcCount;
    *dstWeight = *srcWeight;
    *dstParent = (*srcParent == DimTile_MAX) ? DimParent_MAX : (*srcParent + offset);
  }
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::ArrayTreeTile::buildParents (const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::buildParents",  "");

  auto start = high_resolution_clock::now ();
  // vector<TileEdge<WeightT> > tileEdges (graphWalker.edgeMaxCount ());
  vector<TileEdge<WeightT> > tileEdges;
  const DimEdge edgeCount = weightFunct.getEdges (crop, tileEdges, graphWalker);
  getEdgesTime += duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ();

  LOG ("edgeCount: " << edgeCount);

  TileEdge<WeightT> *tileEdgesEnd = tileEdges.data ()+edgeCount;
  for (TileEdge<WeightT> *curEdgeP = tileEdges.data (); curEdgeP < tileEdgesEnd; ++curEdgeP) {
    const TileEdge<WeightT> &curEdge = *curEdgeP;
    DimTile pa = curEdge.indexes[0];
    DimTile pb = curEdge.indexes[1];
    DimTile la = leaders.find (pa);
    DimTile lb = leaders.find (pb);
    DimTile ra = findRoot (leafParents [la]); // for alphaTree
    DimTile rb = findRoot (leafParents [lb]); // for alphaTree

    BOOST_ASSERT (pa < leafCount);
    BOOST_ASSERT (pb < leafCount);
    BOOST_ASSERT (la < leafCount);
    BOOST_ASSERT (lb < leafCount);
    BOOST_ASSERT (ra < leafCount || ra == DimTile_MAX);
    BOOST_ASSERT (rb < leafCount || rb == DimTile_MAX);
    BOOST_ASSERT (ra == DimTile_MAX || compParents [ra] == DimTile_MAX);
    BOOST_ASSERT (rb == DimTile_MAX || compParents [rb] == DimTile_MAX);
    SMART_LOG (" e:" << printTileEdge (curEdge, graphWalker.getSize (), crop));
    SMART_LOG ("pa:" << pa << " pb:" << pb << " la:" << la << " lb:" << lb);
    SMART_LOG ("ra:" << printTileComp (ra) << " rb:" << printTileComp (rb));

    if (la == lb) {
      leaders.link (pa, la);
      leaders.link (pb, la);
      SMART_LOG ("la=lb");
      continue;
    }
    if (ra == DimTile_MAX) {
      swap (la, lb);
      swap (ra, rb);
    }
    DimTile leader = DimTile_MAX;
    if (ra == DimTile_MAX) {
      // ra = rb = DimTile_MAX
      createTileComp (curEdge.weight, leafParents [la], leafParents [lb]);
      SMART_LOG_EXPR (cerr << "CV P" << la << " N" << printTileComp (tileTop-1U) << endl);
      SMART_LOG_EXPR (cerr << "CV P" << lb << " N" << printTileComp (tileTop-1U) << endl);
      if (curEdge.weight != weightFunct.getWeight (la))
    	swap (la, lb);
      leader = la;
    } else if (rb == DimTile_MAX) {
      if (curEdge.weight == weights[ra]) {
    	// rb.weight <= curEdge.weight = ra.weight
    	addChild (ra, leafParents [lb]);
    	SMART_LOG_EXPR (cerr << "CV N" << lb << " N" << printTileComp (ra) << endl);
    	leader = la;
      } else {
    	// ra.weight < curEdge.weight = rb.weight
    	createTileComp (curEdge.weight, compParents [ra], leafParents [lb]);
    	SMART_LOG_EXPR (cerr << "CV N" << ra << " N" << printTileComp (tileTop-1U) << endl);
    	SMART_LOG_EXPR (cerr << "CV P" << lb << " N" << printTileComp (tileTop-1U) << endl);
    	leader = lb;
      }
    } else if (ra == rb) {
      SMART_LOG ("ra=rb        ****  XXXX   ****");
      BOOST_ASSERT (false);
      leader = la;
    } else if (weights[ra] == weights [rb]) {
      // ra.weight = rb.weight // XXX ?= curEdge.weight
      if (childCount [ra] < childCount [rb]) {
    	swap (la, lb);
    	swap (ra, rb);
      }
      addChildren (ra, rb);
      SMART_LOG_EXPR (cerr << "CV N" << rb << " N" << printTileComp (ra) << endl);
      leader = la;
    } else {
      // XXX YYY test weight ra ou rb
      if (weights[ra] != curEdge.weight) {
    	swap (la, lb);
    	swap (ra, rb);
      }
      leader = la;
      if (weights[ra] == curEdge.weight) {
    	// rb.weight <= ra.weight = curEdge.weight
    	addChild (ra, compParents [rb]);
    	SMART_LOG_EXPR (cerr << "CV N" << rb << " N" << printTileComp (ra) << endl);
      } else {
    	// ra.weight & rb.weight < curEdge.weight
    	createTileComp (curEdge.weight, compParents [ra], compParents [rb]);
    	SMART_LOG_EXPR (cerr << "CV N" << ra << " N" << printTileComp (tileTop-1U) << endl);
    	SMART_LOG_EXPR (cerr << "CV N" << rb << " N" << printTileComp (tileTop-1U) << endl);
      }
    }
    BOOST_ASSERT (leader != DimTile_MAX);
    leaders.link (pa, leader);
    leaders.link (pb, leader);

    SMART_LOG (" leader:" << leader << " ra:" << printTileComp (ra) << " rb:" << printTileComp (rb) << " nr:" << printTileComp (findRoot (leafParents [la])));
  }
  SMART_LOG ("tileTop:" << tileTop);
  SMART_LOG ("leaders:" << endl
	     << printMap (leaders.getLeaders (), crop.size, 0) << endl << endl);
  vector<double> memMonitor;
  getMemSize (memMonitor);
  memPeak = memMonitor [9]+memMonitor [3];
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::CPrintTileComp::CPrintTileComp (const ArrayTreeTile &att, const DimTile &compId)
  : att (att),
    compId (compId) {
}

template<typename PixelT, typename WeightT, int GeoDimT>
ostream &
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeTile::CPrintTileComp::print (ostream &out) const {
  if (compId == DimTile_MAX)
    return out << "M(-/-)";
  out << compId << "(" << att.childCount[compId] << "/";
  printWeight (out, att.weights [compId]);
  return out << ")";
}

// ================================================================================
