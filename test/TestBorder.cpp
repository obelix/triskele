////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>

#include "misc.hpp"
#include "Border.hpp"
#include <chrono>
using namespace std::chrono;

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace obelix;

static const int GeoDimT (2);

static bool show (false); // log or not

void
testBorder () {
  const Size<GeoDimT> size (10240, 10240);
  const DimImg pixelsCount = size.getPixelsCount ();
  Border<GeoDimT> border (size, false);

  auto start = high_resolution_clock::now ();
  for (DimImg i = 0; i < pixelsCount; ++i)
    border.setBorder (i);

  REQUIRE (border.borderCount () == pixelsCount);

  for (DimImg i = 0; i < pixelsCount; ++i)
    border.clearBorder (i);

  REQUIRE (border.borderCount () == 0);

  if (show)
    cerr << ns2string (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ()) << endl;
}

TEST_CASE ("Testing Border...") {
  testBorder ();
}
