////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>	// std::fill
#include <climits>	// std::numeric_limits<PixelT>::min
#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Tree.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/MaxAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "MaxAttributes.tcpp"

// ================================================================================
template<typename PixelT, int GeoDimT>
MaxAttributes<PixelT, GeoDimT>::MaxAttributes (const Tree<GeoDimT> &tree, const Raster<PixelT, GeoDimT> &raster)
  : CompAttribute<PixelT, GeoDimT> (tree) {
  DEF_LOG ("MaxAttributes::MaxAttributes", "");
  pRaster = &raster;
  compute ();
}

template<typename PixelT, int GeoDimT>
MaxAttributes<PixelT, GeoDimT>::~MaxAttributes () {
  DEF_LOG ("MaxAttributes::~MaxAttributes", "");
}

// ================================================================================
template<typename PixelT, int GeoDimT>
void
MaxAttributes<PixelT, GeoDimT>::compute () {
  DEF_LOG ("MaxAttributes::compute", "");
  auto start = high_resolution_clock::now ();

  fill (CA::values.begin (), CA::values.end (), numeric_limits<PixelT>::min ());

  const PixelT *pixels = pRaster->getPixels ();
  CA::tree.forEachLeaf
    ([this, &pixels] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       if (CA::values [parentId] < pixels[leafId])
	 CA::values [parentId] = pixels[leafId];
     });

  CA::tree.forEachComp
    ([this] (const DimParent &compId) {
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       if (CA::values [parentId] < CA::values [compId])
	 CA::values [parentId] = CA::values [compId];
     });

  TreeStats::global.addTime (maxStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
