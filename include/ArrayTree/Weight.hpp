////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Weight_hpp
#define _Obelix_Triskele_Weight_hpp

#include <iostream>
#include <typeinfo>

namespace obelix {
  namespace triskele {
    using namespace std;

    template<typename WeightT>
    inline ostream &printWeight (ostream &out, const WeightT &val);

  } // triskele
} // obelix

#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "triskeleArrayTreeBase.hpp"
#include "GraphWalker.hpp"
#include "Tree.hpp"

namespace obelix {
  namespace triskele {

    template<int GeoDimT>
    class GraphWalker;

    // ================================================================================
    /*! Structure définissant la base d'un poids entre les pixels */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunctionBase {
    protected:
      const PixelT *pixels;
      Size<GeoDimT> size;
      PixelT halfPixel, maxPixel;
      void reset (const PixelT *pixels, const Size<GeoDimT> &size);
    public:
      inline const PixelT *getPixels () const;
      inline const Size<GeoDimT> &getSize () const;
      inline const PixelT &getValue (const DimImg &idx) const;
      inline bool getDecr () const;
      inline const PixelT &getMaxPixel () const;
      inline const PixelT &getHalfPixel () const;

      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b);
      static inline bool isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b);

      WeightFunctionBase (const PixelT *pixels, const Size<GeoDimT> &size);

      static void sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount);
      static void sort (TileEdge<WeightT> *tileEdgesP, const DimEdge &edgeCount);
      PixelT getMedian (const GraphWalker<GeoDimT> &graphWalker) const;

      void copyPixelsBound (PixelT *leafAPTree,
			    const DimImg &minVal, const DimImg &maxVal) const;
      void weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
			      const DimParent &minVal, const DimParent &maxVal) const;
    };

    template <typename PixelT, typename WeightT, int GeoDimT, TreeType TreeTypeT> struct WeightFunction {};

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un MinTree car la fonction calcul le poids maximum. */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunction<PixelT, WeightT, GeoDimT, MIN>
      : public WeightFunctionBase<PixelT, WeightT, GeoDimT> {
      typedef WeightFunctionBase<PixelT, WeightT, GeoDimT> WB;

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const DimImg &a, const DimImg &b) const;

      WeightFunction ();
      WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker);
      WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MIN> &model, const PixelT *pixels, const Size<GeoDimT> &size);

      DimEdge getEdges (const Rect<GeoDimT> &rect,
			vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const;
      DimEdge getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
			vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un MaxTree car la fonction calcul le poids minimum */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunction<PixelT, WeightT, GeoDimT, MAX>
      : public WeightFunctionBase<PixelT, WeightT, GeoDimT> {
      typedef WeightFunctionBase<PixelT, WeightT, GeoDimT> WB;

      inline bool getDecr () const;
      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b);
      static inline bool isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b);

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const DimImg &a, const DimImg &b) const;

      WeightFunction ();
      WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker);
      WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MAX> &model, const PixelT *pixels, const Size<GeoDimT> &size);

      static void sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount);
      static void sort (TileEdge<WeightT> *tileEdgesP, const DimEdge &edgeCount);

      DimEdge getEdges (const Rect<GeoDimT> &rect,
			vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const;
      DimEdge getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
			vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const;

    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un MédianTree car la fonction calcul la distance à la médiane (distance maximum dans les feuilles donc comme MaxTree donc comme MinWeight) */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunction<PixelT, WeightT, GeoDimT, MED>
      : public WeightFunctionBase<PixelT, WeightT, GeoDimT> {
      typedef WeightFunctionBase<PixelT, WeightT, GeoDimT> WB;
    protected:
      const PixelT median, thresholdPixel;
      const WeightT thresholdWeight;

    public:
      inline bool getDecr () const;
      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b);
      static inline bool isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b);

      inline const PixelT &getMedian () const;
      inline const PixelT &getThresholdPixel () const;
      inline const WeightT &getThresholdWeight () const;

      inline WeightT value2weight (const PixelT &val) const;
      inline PixelT weight2value (const WeightT &weight) const;

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const DimImg &a, const DimImg &b) const;

      WeightFunction ();
      WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker);
      WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MED> &model, const PixelT *pixels, const Size<GeoDimT> &size);

      static void sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount);
      static void sort (TileEdge<WeightT> *tileEdgesP, const DimEdge &edgeCount);

      DimEdge getEdges (const Rect<GeoDimT> &rect,
			vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const;
      DimEdge getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
			vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const;

      void weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
			      const DimParent &minVal, const DimParent &maxVal) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un AlphaTree car la fonction calcul la distance minimum (0 dans les feuilles comme MinTree donc comme MaxWeight) */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>
      : public WeightFunctionBase<PixelT, WeightT, GeoDimT> {
      typedef WeightFunctionBase<PixelT, WeightT, GeoDimT> WB;

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const DimImg &a, const DimImg &b) const;

      WeightFunction ();
      WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker);
      WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, ALPHA> &model, const PixelT *pixels, const Size<GeoDimT> &size);

      DimEdge getEdges (const Rect<GeoDimT> &rect,
			vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const;
      DimEdge getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
			vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un TreeOfShape car la fonction calcul la distance utilise une file de proirité. */
    template <typename PixelT, typename WeightT, int GeoDimT> struct WeightFunction<PixelT, WeightT, GeoDimT, TOS>
      : public WeightFunctionBase<PixelT, WeightT, GeoDimT> {
      typedef WeightFunctionBase<PixelT, WeightT, GeoDimT> WB;
    protected:
      const PixelT median, thresholdPixel;
    public:
      // inline bool getDecr () const;
      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      // static inline bool isEdgeInf (const Edge<WeightT, GeoDimT> &a, const Edge<WeightT, GeoDimT> &b);
      // static inline bool isTileEdgeInf (const TileEdge<WeightT> &a, const TileEdge<WeightT> &b);

      inline WeightT value2weight (const PixelT &val) const;

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const DimImg &a, const DimImg &b) const;

      WeightFunction ();
      WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker);
      WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, TOS> &model, const PixelT *pixels, const Size<GeoDimT> &size);

      DimEdge getEdges (const Rect<GeoDimT> &rect,
			vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const;
      DimEdge getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
			vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const;

      // inline void weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
      // 				     const DimParent &minVal, const DimParent &maxVal) const;
    };

    // ================================================================================
  } // triskele
} // obelix

#include "GraphWalker.hpp"

namespace obelix {
  namespace triskele {
#include "Weight.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Weight_hpp
