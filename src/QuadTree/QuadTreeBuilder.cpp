////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "QuadTree/QuadTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
void
QuadTreeBuilder::buildTree (Tree<2> &tree) {
  // Building the tree by setting leafs's and components's parents
  setTreeSize (Size<2> (width, height));
  DimParent parentCount = getStepCount (0, 0, width, height);
  DimParent parentUsed = parentCount;

  // Set the root
  compParents[parentCount-1U] = parentCount;
  tree.setCompCount (parentCount-(DimNode (width)*height));
  setParents (parentCount, 0, 0, width, height, width, height);

  // Building children array
  // #if defined IMAGE_40BITS || defined IMAGE_LARGE // partial_sum
  std::partial_sum (childrenStart, childrenStart+parentUsed+2, childrenStart);
  for (DimImg i = 0; i < tree.getLeafCount (); ++i) {
    DimParent idP = leafParents[i];
    children[DimNode (childrenStart[idP+1]++)] = i;
  }

  for (DimImg i = tree.getLeafCount (); i < tree.getLeafCount ()+parentUsed-1; ++i) {
    DimParent idP = compParents[i-tree.getLeafCount ()];
    children[DimNode (childrenStart[idP+1]++)] = i;
  }
}

// ================================================================================
DimParent
QuadTreeBuilder::getStepCount (const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height) const {
  DimParent nbSteps = 1;
  if (width <= 2 && height <= 2)
    return nbSteps;

  DimSideImg dw1 = width >> 1;
  DimSideImg dh1 = height >> 1;
  DimSideImg dw2 = (width >> 1) + (width & 1);
  DimSideImg dh2 = (height >> 1) + (height & 1);
  DimSideImg coords [4][4] = {
			      {x      , y      , dw1, dh1},
			      {x + dw1, y      , dw2, dh1},
			      {x      , y + dh1, dw1, dh2},
			      {x + dw1, y + dh1, dw2, dh2}
  };

  for (int i = 0; i < 4; i++)
    nbSteps += getStepCount (coords[i][0], coords[i][1], coords[i][2], coords[i][3]);
  return nbSteps;
}

DimParent
QuadTreeBuilder::setParents (DimParent &parentId,
			     const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height,
			     const DimSideImg &imgWidth, const DimSideImg &imgHeight, DimParent level) const {
  DEF_LOG ("setParents", "parentId: " << parentId << " x: " << x << " y: " << y << " w: " << width << " h: " << height);
  DimParent localId = --parentId;

  if (width <= 2 && height <= 2) {
    for (DimSideImg i = x; i < width + x; i++)
      for (DimSideImg j = y; j < height + y; j++) {
	DimSideImg id = i + j * imgWidth;
	leafParents[id] = localId;
	childrenStart[localId+2]++;
      }
    return localId;
  }
  DimSideImg dw1 = width >> 1;
  DimSideImg dh1 = height >> 1;
  DimSideImg dw2 = (width >> 1) + (width & 1);
  DimSideImg dh2 = (height >> 1) + (height & 1);
  DimSideImg coords [4][4] = {
			      {x      , y      , dw1, dh1},
			      {x + dw1, y      , dw2, dh1},
			      {x      , y + dh1, dw1, dh2},
			      {x + dw1, y + dh1, dw2, dh2}
  };
  childrenStart[localId+2] = 4;
  for (int i = 0; i < 4; i++)
    compParents[setParents (parentId, coords[i][0], coords[i][1], coords[i][2], coords[i][3], imgWidth, imgHeight, level+1)] = localId;
  return localId;
}

// ================================================================================
