////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Tree_tpp
#define _Obelix_Triskele_Tree_tpp

// ================================================================================
template void Tree<2>::setCompCount (const DimParent &newCompCount);
template void Tree<3>::setCompCount (const DimParent &newCompCount);


// ================================================================================
template<int GeoDimT>
inline DimNodePack *
Tree<GeoDimT>::getChildSum () {
  return childrenStart.data ();
}

template<int GeoDimT>
inline void
Tree<GeoDimT>::setCompCount (const DimParent &newCompCount) {
  compCount = newCompCount;
  rootId = compCount-1U;
  nodeCount = DimNode (leafCount)+DimNode (compCount);
  leafParents.resize (nodeCount);
}
template<int GeoDimT>
inline void
Tree<GeoDimT>::setSize (const Size<GeoDimT> &newSize) {
  size = newSize;
}

template<int GeoDimT>
inline DimCore
Tree<GeoDimT>::getCoreCount () const {
  return coreCount;
}

template<int GeoDimT>
inline State
Tree<GeoDimT>::getState () const {
  return state;
}

template<int GeoDimT>
inline Size<GeoDimT>
Tree<GeoDimT>::getSize () const {
  return size;
}

template<int GeoDimT>
inline const DimImg &
Tree<GeoDimT>::getLeafCount () const {
  return leafCount;
}
template<int GeoDimT>
inline const DimNode &
Tree<GeoDimT>::getNodeCount () const {
  return nodeCount;
}
template<int GeoDimT>
inline DimParent
Tree<GeoDimT>::getCompCount () const {
  return nodeCount-leafCount;
}
template<int GeoDimT>
inline DimParent
Tree<GeoDimT>::getRootId () const {
  return rootId;
}
// inline DimNode
// Tree::getNodeRootId () const {
//   return rootId+leafCount;
// }
template<int GeoDimT>
inline bool
Tree<GeoDimT>::isLeaf (const DimNode &nodeId) const {
  return nodeId < leafCount;
}
template<int GeoDimT>
inline DimImg
Tree<GeoDimT>::getLeafId (const DimNode &nodeId) const {
  return nodeId;
}
template<int GeoDimT>
inline DimParent
Tree<GeoDimT>::getCompId (const DimNode &nodeId) const {
  return nodeId-leafCount;
}
template<int GeoDimT>
inline DimNode
Tree<GeoDimT>::getChild (const DimParent &compId, const DimImg &childId) const {
  return children [DimNode (childrenStart [compId]+childId)];
}

template<int GeoDimT>
inline const DimParent
Tree<GeoDimT>::getParent (const DimNode &nodeId) const {
  return DimImg (leafParents[nodeId]);
}
template<int GeoDimT>
inline const DimParent
Tree<GeoDimT>::getLeafParent (const DimImg &leafId) const {
  return leafParents[leafId];
}
template<int GeoDimT>
inline const DimParent
Tree<GeoDimT>::getCompParent (const DimParent &compId) const {
  return compParents[compId];
}

template<int GeoDimT>
inline const DimParentPack *
Tree<GeoDimT>::getLeafParents () const {
  return leafParents.data ();
}

template<int GeoDimT>
inline const DimImg
Tree<GeoDimT>::getChildrenCount (const DimParent &compId) const {
  return childrenStart[compId];
}
template<int GeoDimT>
inline const DimNode
Tree<GeoDimT>::getChildren (const DimNode &childId) const {
  return children[childId];
}

template<int GeoDimT>
inline const vector<DimParentPack> &
Tree<GeoDimT>::getWeightBounds () const {
  return weightBounds;
}
template<int GeoDimT>
inline vector<DimParentPack> &
Tree<GeoDimT>::getWeightBounds () {
  return weightBounds;
}

// ================================================================================
template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachLeaf (const FuncToApply &f /* f (DimImg leafId) */) const {
  for (DimImg leafId = 0; leafId < leafCount; ++leafId)
    f (leafId);
}

template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachComp (const FuncToApply &f /* f (DimParent compId) */) const {
  const DimParent compCount = getCompCount ();
  for (DimParent compId = 0; compId < compCount; ++compId)
    f (compId);
}

template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachCompFromRoot (const FuncToApply &f /* f (DimParent compId) */) const {
  const DimParent compCount = getCompCount ();
  if (!compCount)
    return;
  for (DimParent compId = compCount-1; ; --compId) {
    f (compId);
    if (!compId)
      return;
  }
}

template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachNode (const FuncToApply &f /* f (DimNode nodeId) */) const {
  for (DimNode nodeId = 0; nodeId < nodeCount; ++nodeId)
    f (nodeId);
}

template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachChild (const DimParent &parentId, const FuncToApply &f /* f (DimNode childId) */) const {
  DimNode minChild = childrenStart[parentId], maxChild = childrenStart[parentId+1U];
  for (DimNode childId = minChild; childId < maxChild; ++childId)
    f (children[childId]);
}
template<int GeoDimT>
template<typename FuncToApply>
inline void
Tree<GeoDimT>::forEachChildTI (const DimParent &parentId, const FuncToApply &f /* f (bool isLeaf, DimImg|DimParent childId) */) const {
  DimNode minChild = childrenStart[parentId], maxChild = childrenStart[parentId+1U];
  for (DimNode childId = minChild; childId < maxChild; ++childId) {
    const DimNode &child (getChildren (childId));
    const bool isLeaf = child < leafCount;
    if (child >= getNodeCount ()) {
      cerr << "coucou Tree::forEachChildTI parentId:" << parentId << " compCount:" << getCompCount () << " minChild:" << minChild << " maxChild:" << maxChild << " childId:" << childId << " child:" << child << endl;
    }
    BOOST_ASSERT (child < getNodeCount ());
    f (isLeaf, isLeaf ? child : (child-leafCount));
  }
}

// ================================================================================
#endif // _Obelix_Triskele_Tree_tpp
