////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_OptChannelParser_hpp
#define _Obelix_Triskele_OptChannelParser_hpp

#include <boost/program_options.hpp>

#include "OptChannel.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    enum OptionNames {
		      ONInvalidOption,
		      ONNdviBands,
		      ONPantexBands,
		      ONSobelBands,
		      ONCopyBands,
		      ONWithBands,
		      ONFeatureType,
		      ONTreeType,
		      ONAttributeType,
		      ONThresholds,
		      ONDapWeight,
		      ONDapPos,
		      ONHaarSizes,
		      ONStatSizes
    };

    // ================================================================================
    class OptChannelParser {
      static const map<string, OptionNames> OptionNamesLabels;
      static OptionNames resolveOption (const string &input);

      map<string, int> optRank;
      int getOptRank (const string &optName);

      bool noMixedBand, noNdviFlag, noPantexFlag, noSobelFlag, noCopyFlag, noHaarFlag, noStatFlag, showChannel;
      vector<OptRanges> ndviBands;
      OptRanges pantexBands;
      OptRanges sobelBands;

      vector<FeatureType> featureTypes;
      vector<TreeType> treeTypes;
      vector<AttributeType> attributeType;
      vector<string> thresholdsValues;
      vector<ApOrigPos> apOrigPos;
      bool dapWeightFlags;

      OptRanges copyBands;
      vector<OptRanges> selectedBands;
      vector<OptSizes> haarSizes, statSizes;

      string cfgName, loadName, saveName;

    public:
      DimChannel spectralDepth;
      bool mixedBandFlag;
      boost::program_options::options_description description;

      OptChannelParser ();

      void parse (OptChannel &optChannel, const string &inputFileNameImage, boost::program_options::basic_parsed_options<char> &parsed);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptChannelParser_hpp
