////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>

using namespace boost::chrono;
#include "misc.hpp"

#include "ArrayTree/Weight.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "ArrayTreeBuilderBuildTypeTemp.tcpp"

#define TILE_FACT 25.
#define MEM_FACT 17.

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::buildTreeType (Tree<GeoDimT> &tree, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct, const bool &timeFlag) {
  DEF_LOG ("ArrayTreeBuilder::buildTree", "");
  coreCount = tree.getCoreCount ();
  DimCore threadPerImage (this->threadPerImage);
  DimCore tilePerThread (this->tilePerThread);

  if (!threadPerImage)
    threadPerImage = coreCount;
  if (!tilePerThread)
    tilePerThread = 1;

  // buildParents
  auto start = high_resolution_clock::now ();
  // set tiles
  vector<Rect<GeoDimT> > tiles, boundaries;
  vector<TileShape> boundaryAxes;
  const Size<GeoDimT> &size (graphWalker.getSize ());
  if (autoThreadFlag) {
    const DimImg pixelsCount (size.getPixelsCount ());
    vector<double> memMonitor;
    getMemSize (memMonitor);
    double ratio = max (floor ((memMonitor [1]-MEM_FACT*pixelsCount)/(TILE_FACT*pixelsCount)), 1.);

    LOG ("free:" << memMonitor [1] << " " << TILE_FACT << "*pixelsCount:" << (TILE_FACT*pixelsCount) << " ratio:" << ratio);
    tilePerThread = 1;
    if (ratio > 1) {
      threadPerImage = DimCore (min (double (coreCount), ratio));
      seqTile = false;
    } else {
      seqTile = true;
      threadPerImage = DimCore (max (1., ceil ((TILE_FACT*2*pixelsCount)/abs(memMonitor [1]-(MEM_FACT*pixelsCount)))));
    }
  }
  if (TreeTypeT == TOS)
    threadPerImage = tilePerThread = 1;
  if (timeFlag)
    cout << "threadPerImage:" << threadPerImage << " tilePerThread:" << tilePerThread << " seqTile:" << seqTile << endl;
  graphWalker.setTiles (threadPerImage*tilePerThread, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);
  DimCore tileCount = tiles.size ();
  LOG ("tileCount:" << tileCount);

  vector<ArrayTreeTile> tilebuilders (tileCount, graphWalker);
  for (DimCore i = 0; i < tileCount; ++i)
    tilebuilders[i].init (tiles[i]);

  auto startTileParent = high_resolution_clock::now ();
  vector<DimParent> compBases (tileCount);
  vector<DimParent> compTops (tileCount);
  DimParent topSum = DimParent (0U);
  vector<double> memPeak (threadPerImage, 0);
  double edgeTime (0);

  if (seqTile) {
    for (DimTile tileId = 0; tileId < tileCount; ++tileId) {
      tilebuilders [tileId].dynTileAlloc ();
      tilebuilders [tileId].buildParents (weightFunct);
      compBases [tileId] = topSum;
      compTops [tileId] = topSum += tilebuilders [tileId].tileTop;
      LOG ("base:" << compBases [tileId]  << " top:" << compTops [tileId] << " (" << tilebuilders [tileId].tileTop << ")");
      dynImgAlloc (topSum);
      tilebuilders [tileId].move (compBases [tileId], TreeBuilder<GeoDimT>::leafParents, childCount, compWeights);
      memPeak[0] = max (memPeak [0], tilebuilders [tileId].memPeak);
      tilebuilders [tileId].free ();
      edgeTime += tilebuilders [tileId].getEdgesTime;
    }
  } else {
    for (DimCore tb = 0; tb < tileCount; ) {
      DimCore step = min (threadPerImage, tileCount - tb);
      for (DimCore tileId = 0; tileId < step; ++tileId)
	tilebuilders [tb+tileId].dynTileAlloc ();
      dealThread (step, threadPerImage,
		  [this, &tilebuilders, &weightFunct, &tb, &memPeak] (const DimCore &threadId, const DimTile &minVal, const DimTile &maxVal) {
		    for (DimTile tileId = minVal; tileId < maxVal; ++tileId) {
		      tilebuilders [tb+tileId].buildParents (weightFunct);
		      memPeak [threadId] = max (memPeak [threadId], tilebuilders [tb+tileId].memPeak);
		    }
		  });
      double maxEdgeTime (0);
      for (DimCore tileId = 0; tileId < step; ++tileId) {
	compBases [tb+tileId] = topSum;
	compTops [tb+tileId] = topSum += tilebuilders [tb+tileId].tileTop;
	LOG ("base:" << compBases [tb+tileId]  << " top:" << compTops [tb+tileId] << " (" << tilebuilders [tb+tileId].tileTop << ")");
	maxEdgeTime = max (maxEdgeTime, tilebuilders [tileId].getEdgesTime);
      }
      edgeTime += maxEdgeTime;
      dynImgAlloc (topSum);
      dealThread (step, threadPerImage,
		  [this, &tb, &tilebuilders, &compBases] (const DimCore &threadId, const DimTile &minVal, const DimTile &maxVal) {
		    for (DimTile tileId = minVal; tileId < maxVal; ++tileId) {
		      tilebuilders [tb+tileId].move (compBases [tb+tileId], TreeBuilder<GeoDimT>::leafParents, childCount, compWeights);
		      tilebuilders [tb+tileId].free ();
		    }
		  });
      tb += step;
    }
  }
  tilebuilders.clear ();
  SMART_LOG ("compWeights:" << endl
	     << printMap (compWeights, size, topSum) << endl << endl
	     << tree.printTree (1+needChildren));

  // merge sub-tree
  auto startMerge = high_resolution_clock::now ();
  DimParent extendedComp (topSum);
  DimCore boundCount = boundaries.size ();
  if (boundCount) {
    vector<vector<Edge<WeightT, GeoDimT> > > boundariesEdges (boundCount);
    vector<DimEdge> edgesCounts (boundCount);
    auto start = high_resolution_clock::now ();
    dealThread (boundCount, coreCount,
		[this, &boundariesEdges, &boundaryAxes, &boundaries, &edgesCounts, &weightFunct] (const DimCore &threadId, const DimEdge &minVal, const DimEdge &maxVal) {
		  for (DimCore id = minVal; id < maxVal; ++id) {
		    // boundariesEdges[id].resize (graphWalker.edgeMaxCount (boundaries[id].size, boundaryAxes[id]));
		    edgesCounts [id] = weightFunct.getEdges (boundaries [id], boundaryAxes [id], boundariesEdges [id], graphWalker);
		  }
		});
    edgeTime += duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ();
    LOG ("edgeBounds: done");

    callOnSortedSets<DimEdge, WeightT> (edgesCounts,
					[&boundariesEdges] (const DimCore &vectId, const DimTile &itemId) {
					  return boundariesEdges [vectId][itemId].getWeight();
					},
					weightFunct.isWeightInf,
					[this, &boundariesEdges, &size,  &extendedComp, &weightFunct] (const DimCore &vectId, const DimTile &itemId) {
					  Edge<WeightT, GeoDimT> *edge = &boundariesEdges [vectId][itemId];
					  connectLeaf (edge->getA (), edge->getB (size), edge->getWeight(), extendedComp, weightFunct);
					});
  }
  LOG ("after call A");

  // XXX YYYY a refaire
  if (topSum != extendedComp) {
    compBases.push_back (topSum);
    compTops.push_back (extendedComp);

#ifndef DEBUG
    if (TreeTypeT != TOS) {
      // XXX test croisance weight pour compBases[n-1] => compTops[n-1]
      WeightT prev (compWeights [topSum]);
      for (DimParent compId = topSum+1; compId < extendedComp; ++compId)
	if (weightFunct.isWeightInf (compWeights [compId], prev))
	  cerr << " *** bad extended order " << compId << endl;
    }
#endif
  }


  // SMART_LOG ("compWeights:" << endl
  // 	     << treeprintMap (compWeights, size, extendedComp) << endl << endl
  // 	     << tree.printTree (1+needChildren));
  auto startForest = high_resolution_clock::now ();
  if (graphWalker.border.exists () || tileCount > 1) {
    // merge comp forest
    DimParent rootId = 0;
    for (DimCore tileId = 0; tileId < tileCount; tileId++)
      if (compTops [tileId] != compBases [tileId]) {
	rootId = findRoot (compTops [tileId] - 1U);
	break;
      }

    LOG ("merge forest: " << printComp (rootId));
    for (DimCore tileId = 0; tileId < tileCount; ++tileId)
      for (DimParent compId = compBases [tileId]; compId < compTops [tileId]; ++compId)
	if (TreeBuilder<GeoDimT>::compParents [compId] == DimParent_MAX) {
	  connectComp (compId, rootId, weightFunct);
	  rootId = findRoot (rootId);
	  LOG ("merge top: compId:" << printComp (compId) << " nr:" << printComp (rootId));
	}

    // merge pixels forest
    vector<DimImg> lonelyPixelsCount (tileCount, 0);
    vector<WeightT> topsWeight (tileCount, compWeights [rootId]);
    dealThread (tileCount, coreCount,
		[this, &tiles, &rootId, &weightFunct, &lonelyPixelsCount, &topsWeight] (const DimCore &threadId, const DimCore &minVal, const DimCore &maxVal) {
		  for (DimCore tileId = minVal; tileId < maxVal; ++tileId) {
		    WeightT &topWeight (topsWeight [tileId]);
		    DimImg &lonelyPixelCount (lonelyPixelsCount [tileId]);
		    graphWalker.forEachVertex
		      (tiles [tileId],
		       [this, &rootId, &lonelyPixelCount, &topWeight, &weightFunct] (const DimImg &leafId, const BorderFlagType &lowBorder) {
			 if (TreeBuilder<GeoDimT>::leafParents [leafId] == DimParent_MAX) {
			   WeightT pixelWeight (weightFunct.getWeight (leafId));
			   TreeBuilder<GeoDimT>::leafParents [leafId] = rootId;
			   lonelyPixelCount++;
			   if (weightFunct.isWeightInf (topWeight, pixelWeight))
			     topWeight = pixelWeight;
			 }
		       });
		  }
		});
    for (DimCore tileId = 0; tileId < tileCount; ++tileId) {
      childCount [rootId] += lonelyPixelsCount [tileId];
      if (weightFunct.isWeightInf (compWeights [rootId], topsWeight [tileId]))
	compWeights [rootId] = topsWeight [tileId];
      // XXX on ne reprend pas la fraterie inférieur car plus d'appel à findMultiChildrenTop
      LOG ("merge pixels: tileId:" << tileId << " lonely:" << lonelyPixelsCount [tileId] << " root:" << printComp (rootId));
    }
  }

  SMART_LOG ("compWeights:" << endl
	     << printMap (compWeights, size, extendedComp) << endl << endl
	     << tree.printTree (1+needChildren));
  // compress
  auto startIndex = high_resolution_clock::now ();

  DimParentPack empty (DimParent_MAX);
  vector<DimParentPack> newCompId (extendedComp, empty);

  SMART_LOG ("newCompId:" << endl
	     << printMap (newCompId.data (), size, extendedComp) << endl << endl);
  DimParent maxTop = updateNewId (newCompId.data (), compBases, compTops, weightFunct);

  SMART_LOG ("updateNewId:" << endl
	     << printMap (newCompId.data (), size, extendedComp) << endl << endl);

  auto startCompress = high_resolution_clock::now ();
  compress (newCompId.data (), extendedComp);
  SMART_LOG ("compress:" << endl
	     << printMap (newCompId.data (), size, extendedComp) << endl << endl);
  while (maxTop > 1U && DimParent (childCount[maxTop-1U]) == DimParent (1U)) {
    --maxTop;
    TreeBuilder<GeoDimT>::compParents [DimParent (maxTop)] = DimParent_MAX;
    SMART_LOG ("reduce lonely root:" << printComp (maxTop) << endl);
  }
  TreeBuilder<GeoDimT>::setCompCount (maxTop);
  LOG ("nodeCount:" << tree.getNodeCount());
  // DimEdge rootId = maxTop-1;
  // compParents[rootId] = rootId;

  SMART_LOG ("compWeights:" << endl
	     << printMap (compWeights, size, extendedComp) << endl << endl);
  auto end = high_resolution_clock::now ();
  TreeStats::global.addTime (buildSetupStats, duration_cast<duration<double> > ( startTileParent-start).count ());
  TreeStats::global.addTime (buildEdgesStats, edgeTime);
  TreeStats::global.addTime (buildTileParentsStats, duration_cast<duration<double> > (startMerge-startTileParent).count ()-edgeTime);
  //TreeStats::global.addTime (buildImgParentsStats, duration_cast<duration<double> > (startMerge-startImgParent).count ());
  TreeStats::global.addTime (buildMergeStats, duration_cast<duration<double> > (startForest-startMerge).count ());
  TreeStats::global.addTime (buildForestStats, duration_cast<duration<double> > (startIndex-startForest).count ());
  TreeStats::global.addTime (buildIndexStats, duration_cast<duration<double> > (startCompress-startIndex).count ());
  TreeStats::global.addTime (buildCompressStats, duration_cast<duration<double> > (end-startCompress).count ());

  vector<double> memMonitor;
  getMemSize (memMonitor);
  if (timeFlag)
    cout << "memory peak: " << readableBytes (*max_element (memPeak.begin (), memPeak.end ())) << endl
	 << "proc: " << readableBytes (memMonitor[9]+memMonitor [3])
	 << " (used: " << readableBytes (memMonitor [6])
	 << " / max:" << readableBytes (memMonitor [8]) << ")" << endl;
}

