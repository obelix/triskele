////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <map>
#include <boost/program_options.hpp>

#include "IImage.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptChannelParser.hpp"
#include "misc.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
const map<string, OptionNames>
OptChannelParser::OptionNamesLabels { {
    "ndviBands", ONNdviBands }, {
    "pantexBands", ONPantexBands }, {
    "sobelBands", ONSobelBands }, {
    "copyBands", ONCopyBands }, {
    "withBands", ONWithBands }, {
    "featureType", ONFeatureType }, {
    "attributeType", ONAttributeType }, {
    "treeType", ONTreeType }, {
    "thresholds", ONThresholds }, {
    "dapWeight", ONDapWeight }, {
    "dapPos", ONDapPos }, {
    "haarSizes", ONHaarSizes }, {
    "statSizes", ONStatSizes }
};

OptionNames
OptChannelParser::resolveOption (const string &input) {
  auto itr = OptionNamesLabels.find (input);
  if (itr != OptionNamesLabels.end ())
    return itr->second;
  return ONInvalidOption;
}

int
OptChannelParser::getOptRank (const string &optName) {
  int val = 0;
  auto it = optRank.find (optName);
  if (it != optRank.end ())
    val = it->second+1;
  optRank[optName] = val;
  return val;
}

// ================================================================================
class MultiSwitch {
public:
  MultiSwitch () {}
};
void
validate (boost::any &, vector<string> const &, MultiSwitch*, long) {
}

// ================================================================================
OptChannelParser::OptChannelParser ()
  : noMixedBand (false),
    noNdviFlag (false),
    noPantexFlag (false),
    noSobelFlag (false),
    noCopyFlag (false),
    noHaarFlag (false),
    noStatFlag (false),
    showChannel (false),
    dapWeightFlags (false),
    spectralDepth (0),
    mixedBandFlag (false),
    description ("Channel config options", getCols ()) {
  DEF_LOG ("OptChannelParser::OptChannelParser", "");

  description.add_options ()
    ("cfgName", po::value<string> (&cfgName), "config name")

    ("spectralDepth,d", po::value<DimChannel> (&spectralDepth)->default_value (spectralDepth), "spectral band count (0 all bands are spectral = no volume)")
    ("mixedBandFlag", po::bool_switch (&mixedBandFlag)->default_value (mixedBandFlag), "if bands are mixed in frames")

    ("noMixedBand", bool_switch (&noMixedBand), "remove mixed band flag")
    ("noNdvi", bool_switch (&noNdviFlag), "remove ndvi from config")
    ("noPantex", bool_switch (&noPantexFlag), "remove pantex from config")
    ("noSobel", bool_switch (&noSobelFlag), "remove sobel from config")
    ("noCopy", bool_switch (&noCopyFlag), "remove copy from config")
    ("noHaar", bool_switch (&noHaarFlag), "remove haar from config")
    ("noStat", bool_switch (&noStatFlag), "remove stat from config")

    ("ndviBands,n", po::value<vector <OptRanges> > (&ndviBands)->multitoken (), "band1,band2 : produce ndvi band (first band is 0)")
    ("pantexBands,p", po::value<OptRanges> (&pantexBands)->default_value (pantexBands), "band,... : produce pantex band (first band is 0)")
    ("sobelBands,s", po::value<OptRanges> (&sobelBands)->default_value (sobelBands), "band,... : produce sobel band (first band is 0)")
    ("copyBands,c", po::value<OptRanges> (&copyBands)->multitoken (), "copy input band (included ndvi, pantex and sobel)")
    ("withBands,b", po::value<vector <OptRanges> > (&selectedBands)->multitoken (), "with input bands do...")

    ("featureType,f", po::value<vector <FeatureType> > (&featureTypes), "feature profiles produced (AP (default attribute profiles), Area, Perimeter, ZLength, STS, Compactness, Complexity, Simplicity, Rectangularity, Min, Max, Mean, SD, SDW, MoI)") // , SDA

    ("treeType,t", po::value<vector <TreeType> > (&treeTypes), "select tree type (Min, Max, Med, Alpha)")
    ("attributeType,a", po::value<vector <AttributeType> > (&attributeType)->multitoken (), "select attribute type (Area, Perimeter, ZLength, STS, Compactness, Complexity, Simplicity, Rectangularity, Weight, SD, SDW, MoI)") // ,SDA
    ("thresholds", po::value<vector <string> > (&thresholdsValues)->multitoken (), "t1,t2,... : thresholds")

    ("dapWeight", po::value<MultiSwitch>()->zero_tokens (), "use origin weight for DAP")
    ("dapPos", po::value<vector <ApOrigPos> > (&apOrigPos), "position of origin on DAP (apOrigNoPos, apOrigPosBegin, apOrigPosEnd, apOrigPosBoth, apOrigPosEverywhere)")

    ("haarSizes,H", po::value<vector <OptSizes> > (&haarSizes)->multitoken (), "size,... : produce haar for window sizes (size : HxW)")
    ("statSizes,T", po::value<vector <OptSizes> > (&statSizes)->multitoken (), "size,... : produce stat for window sizes (size : HxW)")

    ("loadChannel", po::value<string> (&loadName), "XML file name (config base)")
    ("saveChannel", po::value<string> (&saveName), "XML file name")

    ("showChannel", bool_switch (&showChannel), "Show output channels")
    ;
}

// ================================================================================
void
OptChannelParser::parse (OptChannel &optChannel, const string &inputFileNameImage, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptChannelParser::parse", "");
  if (loadName.size ()) {
    loadName = addExtensionIfNone (loadName, OptChannel::chExt);
    optChannel.loadFile (loadName);
  }

  if (mixedBandFlag && noMixedBand)
    throw invalid_argument ("Can't used mixedBandFlag and noMixedBand in same time");
  if (noMixedBand)
    mixedBandFlag = optChannel.mixedBandFlag = false;
  if (mixedBandFlag)
    optChannel.mixedBandFlag = true;
  optChannel.spectralDepth = spectralDepth;


  IImage<2> inputImage (inputFileNameImage);
  inputImage.readImage ();
  // if (inputImage.isEmpty () && optChannel.isEmpty ())
  //   usage ("No valid input image");
  if (!inputImage.isEmpty ()) {
    GDALDataType inputType = inputImage.getDataType ();
    DimChannel inputBandsCount = inputImage.getBandCount ();
    if (spectralDepth && (inputBandsCount % spectralDepth))
      throw invalid_argument (string ("Inconsistent depth (" +
				      boost::lexical_cast<std::string> (inputBandsCount) +
				      " != x * " +
				      boost::lexical_cast<std::string> (spectralDepth)+")").c_str ());
    optChannel.dim = (!spectralDepth || spectralDepth == inputBandsCount) ? 2 : 3;
    if (optChannel.dim == 3)
      inputBandsCount = spectralDepth;
    if (optChannel.isEmpty ()) {
      LOG ("setType");
      optChannel.setType (inputType, inputBandsCount);
    } else if (!optChannel.inputMatches (inputType, inputBandsCount))
      // XXX
      throw invalid_argument ("Can't parse unknown or inconsistant bands");
  }

  if (cfgName.size ())
    optChannel.setName (cfgName);

  if (noNdviFlag)
    optChannel.remove (Ndvi);
  if (noPantexFlag)
    optChannel.remove (Pantex);
  if (noSobelFlag)
    optChannel.remove (Sobel);
  if (noCopyFlag)
    optChannel.removeCopy ();
  if (noHaarFlag)
    optChannel.remove (Haar);
  if (noStatFlag)
    optChannel.remove (Stat);

  for (auto ndvi : ndviBands) {
    if (ndvi.size () != 2)
      throw invalid_argument ("ndvi must define by 2 channels");
    optChannel.addNdvi (ndvi.first (), ndvi.last ());
  }

  pantexBands.setLimits (0, optChannel.getSize (Original)+optChannel.getSize (Ndvi)-1);
  pantexBands.toSet ();
  for (auto pantex : pantexBands.getSet ())
    optChannel.addPantex (pantex);

  sobelBands.setLimits (0, optChannel.getSize (Original)+optChannel.getSize (Ndvi)+optChannel.getSize (Pantex)-1);
  sobelBands.toSet ();
  for (auto sobel : sobelBands.getSet ())
    optChannel.addSobel (sobel);

  int maxInput = optChannel.getSize (Original)+optChannel.getSize (Ndvi)+optChannel.getSize (Pantex)+optChannel.getSize (Sobel)-1;
  copyBands.setLimits (0, maxInput);
  copyBands.toSet ();
  for (auto copy : copyBands.getSet ())
    optChannel.addCopy (copy);

  bool weightFlag (false);
  for (auto &x : parsed.options) {
    string optName = x.string_key;
    int optRank = getOptRank (optName);
    LOG ("optName: " << optName << " optRank: " << optRank << " switch: " << resolveOption (optName));
    switch (resolveOption (optName)) {
    case ONWithBands:
      selectedBands[optRank].setLimits (0, maxInput);
      selectedBands[optRank].toSet ();
      optChannel.selectInputs (selectedBands [optRank].getSet ());
      break;
    case ONFeatureType:
      optChannel.selectFeatureType (featureTypes [optRank]);
      break;
    case ONTreeType:
      optChannel.selectTreeType (treeTypes [optRank]);
      break;
    case ONAttributeType:
      optChannel.selectAttributeType (attributeType [optRank]);
      break;
    case ONThresholds:
      optChannel.addThresholds (thresholdsValues [optRank]);
      break;

    case ONDapWeight:
      weightFlag = true;
      break;
    case ONDapPos:
      optChannel.addDap (weightFlag, apOrigPos [optRank]);
      weightFlag = false;
      break;

    case ONHaarSizes:
      optChannel.addHaar (haarSizes [optRank].getSizes ());
      break;
    case ONStatSizes:
      optChannel.addStat (statSizes [optRank].getSizes ());
      break;
    default:
      ; //std::cout << "This OptionName value is not handled (" << optName << ")" << endl; //exit(1);
    }
  }

  optChannel.updateChannels ();
  if (saveName.size ()) {
    saveName = addExtensionIfNone (saveName, OptChannel::chExt);
    optChannel.saveFile (saveName);
  }
  if (showChannel)
    optChannel.printOutput (cout);
}

// ================================================================================
