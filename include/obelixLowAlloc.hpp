////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_LowAlloc_hpp
#define _Obelix_LowAlloc_hpp

#include <vector>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"

namespace obelix {

  using namespace std;

  // ================================================================================
  template<typename Type>
  class LowAlloc {
    /*! sizeof Type */
    size_t sizeType;
    /*! block allocation size */
    const DimImg	blockSize;
    /*! number of items allocated */
    DimImg		capacity;
    /*! number of max items used in same time */
    DimImg		maxUsed;
#ifdef DEBUG
    /*! for tuning */
    DimImg		peaskIndex;
#endif // DEBUG
    /*! number of items allocated but not actually used */
    DimImg		available;
    /*! items used or available */
    vector<char>	container;
    /*! available index in container */
    vector<DimImg>	garbage;

  public:
    LowAlloc (const DimImg &blockSize);
    LowAlloc (const size_t sizeType, const DimImg &blockSize);
    DimImg	get ();
    void	give (const DimImg &idx);
    Type	&operator[] (const DimImg &idx);
    const Type	&operator[] (const DimImg &idx) const;

    DimImg	getCapacity () const;
    DimImg	getMaxUsed () const;
    DimImg	getAvailable () const;

    DimImg	getPeakUse () const;
  };

  // ================================================================================

#include "obelixLowAlloc.tpp"
} // namespace obelix

#endif // _Obelix_LowAlloc_hpp
