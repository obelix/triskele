////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <algorithm>
#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "TreeStats.hpp"
#include "IntegralImage.hpp"
#include "obelixNdvi.hpp"
#include "obelixPantex.hpp"
#include "obelixSobel.hpp"

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"

#include "Attributes/AttributesCache.hpp"

#include "Appli/OptChannel.hpp"
#include "Appli/TriskeleFilter.hpp"

using namespace std;
using namespace boost::chrono;
using namespace obelix;
using namespace obelix::triskele;
#include "TriskeleFilter.tcpp"

// ================================================================================
template <typename PixelT, int GeoDimT>
TriskeleFilter<PixelT, GeoDimT>::TriskeleFilter (IImage<GeoDimT> &inputImage, IImage<GeoDimT> &outputImage,
						 const Point<2> &topLeft, const Size<GeoDimT> &size, const OptFilter &optFilter)
  : inputImage (inputImage),
    outputImage (outputImage),
    optFilter (optFilter),
    topLeft (topLeft),
    size (size),
    border (size, !optFilter.includeNoDataFlag),
    graphWalker (border, optFilter.connectivity, optFilter.countingSortCeil),
    pixelCount (size.getPixelsCount ()),
    inputRaster (size),
    outputRaster (size),
    ndviRaster (optFilter.optChannel.getNdviCount (), Raster<PixelT, GeoDimT> (size)),
    pantexRasters (optFilter.optChannel.getPantexCount (), Raster<PixelT, GeoDimT> (size)),
    sobelRasters (optFilter.optChannel.getSobelCount (), Raster<PixelT, GeoDimT> (size)),
    integralImage (),
    doubleIntegralImage (),
    tree (optFilter.coreCount) {
  DEF_LOG ("TriskeleFilter::TriskeleFilter", "size: " << size << " pixelCount: " << pixelCount);
  if (optFilter.noWriteFlag)
    return;
  if (optFilter.oneBandFlag)
    return;
  outputImage.createImage (size, optFilter.optChannel.getOutputImageType (getGDALType (PixelT (0))),
			   optFilter.optChannel.getOutputCount (), inputImage, topLeft);
}

// ================================================================================
template <typename PixelT, int GeoDimT>
TriskeleFilter<PixelT, GeoDimT>::~TriskeleFilter () {
  DEF_LOG ("TriskeleFilter::~TriskeleFilter", "");
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::parseInput () {
  DEF_LOG ("TriskeleFilter::parseInput", "");
  updateBorder ();

  vector<InputChannel *> activeOrderedChannels = optFilter.optChannel.getActiveOrder ();
  waitingAP.clear ();
  for (InputChannel *inputChannelP : activeOrderedChannels)
    if (inputChannelP->channelType == AP)
      waitingAP [inputChannelP->generateBy[0]] [inputChannelP->treeType] [inputChannelP->attributeType] = inputChannelP;

  for (InputChannel *inputChannelP : activeOrderedChannels) {
    InputChannel &inputChannel (*inputChannelP);
    if (inputChannel.channelType == Original)
      readInput (inputChannel.inputId);
    if (inputChannel.doWrite)
      dumpInput (inputChannel);

    updateIntegralImage (inputChannel);

    for (InputChannel *childP : inputChannel.produce) {
      if (!childP->prodWrite)
	continue;
      updateNdvi (inputChannel, *childP);
      updatePantex (inputChannel, *childP);
      updateSobel (inputChannel, *childP);

      writeHaar (inputChannel, *childP);
      writeStat (inputChannel, *childP);
      writeSelectedFP (inputChannel, *childP);
    }
    vector<double> ().swap (integralImage);
    vector<double> ().swap (doubleIntegralImage);
  }
  outputImage.close ();
  gTmp = vector<vector<double> > ();
}

// ================================================================================
template <typename PixelT, int GeoDimT>
Raster<PixelT, GeoDimT> &
TriskeleFilter<PixelT, GeoDimT>::getProducerRaster (const InputChannel &inputChannel) {
  DEF_LOG ("TriskeleFilter::getProducerRaster", "");
  switch (inputChannel.channelType) {
  case Original:
    LOG ("inputRaster");
    return inputRaster;
  case Ndvi:
    LOG ("ndviRaster:" << inputChannel.typeRank);
    return ndviRaster [inputChannel.typeRank];
  case Pantex:
    LOG ("pantexRaster:" << inputChannel.typeRank);
    return pantexRasters [inputChannel.typeRank];
  case Sobel:
    LOG ("sobelRaster:" << inputChannel.typeRank);
    return sobelRasters [inputChannel.typeRank];
  default:
    break;
  }
  throw invalid_argument ("getProducerRaster not input channel");
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::readInput (const DimChannel &channel) {
  DEF_LOG ("TriskeleFilter::readInput", "channel: " << channel);
  auto start = high_resolution_clock::now ();
  inputImage.readBand (inputRaster, channel, topLeft, size);
  TreeStats::global.addTime (readStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::updateBorder () {
  DEF_LOG ("TriskeleFilter::updateBorder", "");
  PixelT *inputPixels = inputRaster.getPixels ();
  border.reset (!optFilter.includeNoDataFlag);
  if (!optFilter.includeNoDataFlag) {
    for (const InputChannel *inputChannelP : optFilter.optChannel.getOriginal ()) {
      const double doubleValue (isnan (optFilter.noDataValue) ?
				inputImage.getNoDataValue (inputChannelP->inputId) :
				optFilter.noDataValue);
      if (isnan (doubleValue))
	continue;
      const PixelT noDataValue (doubleValue);
      readInput (inputChannelP->inputId);

      auto start = high_resolution_clock::now ();
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
	if (inputPixels [pixelId] != noDataValue)
	  border.clearBorder (pixelId);
      TreeStats::global.addTime (borderStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    }
    border.reduce ();
  }
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::updateIntegralImage (const InputChannel &inputChannel) {
  DEF_LOG ("TriskeleFilter::updateIntegralImage", "inputChannel: " << inputChannel);
  if (inputChannel.prodInetgralImage) {
    LOG ("produce arithmetic bands");
    auto start = high_resolution_clock::now ();
    integralImage.resize (pixelCount);
    computeIntegralImage (size, getProducerRaster (inputChannel).getPixels (), integralImage.data ());
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }
  if (inputChannel.prodDoubleInetgralImage) {
    auto start = high_resolution_clock::now ();
    doubleIntegralImage.resize (pixelCount);
    doubleMatrix (size, getProducerRaster (inputChannel).getPixels (), doubleIntegralImage.data ());
    computeIntegralImage (size, doubleIntegralImage.data (), doubleIntegralImage.data ());
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::updateNdvi (const InputChannel &inputChannel, const InputChannel &ndviChannel) {
  if (ndviChannel.channelType != Ndvi)
    return;
  DEF_LOG ("TriskeleFilter::updateNdvi", "ndviChannel: " << ndviChannel);
  auto start = high_resolution_clock::now ();
  bool reverse (ndviChannel.generateBy [0]->inputId > ndviChannel.generateBy [1]->inputId);
  bool min (&inputChannel == ndviChannel.generateBy [reverse ? 1 : 0]);
  computeNdvi (getProducerRaster (inputChannel), ndviRaster[ndviChannel.typeRank], min, reverse);
  TreeStats::global.addTime (ndviStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::updatePantex (const InputChannel &inputChannel, const InputChannel &pantexChannel) {
  if (pantexChannel.channelType != Pantex)
    return;
  DEF_LOG ("TriskeleFilter::updatePantex", "pantexChannel: " << pantexChannel);
  auto start = high_resolution_clock::now ();
  computePantex (getProducerRaster (inputChannel), pantexRasters[pantexChannel.typeRank], optFilter.coreCount, optFilter.pantexWindowSide);
  TreeStats::global.addTime (pantexStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::updateSobel (const InputChannel &inputChannel, const InputChannel &sobelChannel) {
  if (sobelChannel.channelType != Sobel)
    return;
  DEF_LOG ("TriskeleFilter::updateSobel", "sobelChannel: " << sobelChannel);
  auto start = high_resolution_clock::now ();
  computeSobel (getProducerRaster (inputChannel), sobelRasters[sobelChannel.typeRank], gTmp, optFilter.coreCount);
  TreeStats::global.addTime (sobelStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::writeHaar (const InputChannel &inputChannel, const InputChannel &haarChannel) {
  if (haarChannel.channelType != Haar)
    return;
  DEF_LOG ("TriskeleFilter::writeHaar", "haarChannel: " << haarChannel);
  if (!haarChannel.doWrite)
    return;
  vector<Rect<GeoDimT> > tiles, boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (optFilter.coreCount, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);

  for (const Size<2> pattern : haarChannel.winSizes) {
    auto start = high_resolution_clock::now ();
    TextWin<GeoDimT> haar (size, pattern);
    vector<vector<double> > haarOutput (4, vector<double> (pixelCount));
    dealThread (tiles.size (), optFilter.coreCount,
		[this, &tiles, &haar, &haarOutput] (const DimCore &threadId, const DimCore &minVal, const DimCore &maxVal) {
		  for (DimCore tileId = minVal; tileId < maxVal; ++tileId)
		    graphWalker.forEachVertex (tiles [tileId],
					       [this, &haar, &haarOutput] (const DimImg &pixelId, const BorderFlagType &) {
						 // XXX uniquement en 2D
						 Point<GeoDimT> p = idx2point (size, pixelId);
						 double hx (haar.getHx (integralImage.data (), p.coord [0], p.coord [1], pixelId));
						 double hy (haar.getHy (integralImage.data (), p.coord [0], p.coord [1], pixelId));
						 haarOutput [0] [pixelId] = hx;		// hx+M/2 => pixel
						 haarOutput [1] [pixelId] = hx+hy;	// (hx+hy)/2+M/2 => pixel
						 haarOutput [2] [pixelId] = hy;		// hx+M/2 => pixel
						 haarOutput [3] [pixelId] = hy-hx;	// (hx+hy)/2+M/2 => pixel
					       });
		});
    TreeStats::global.addTime (haarStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    for (DimChannel localChannel (0), channel (haarChannel.outputRank); localChannel < 4; ++localChannel, ++channel)
      writeBand (haarOutput[localChannel].data (), channel);
  }
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::writeStat (const InputChannel &inputChannel, const InputChannel &statChannel) {
  if (statChannel.channelType != Stat)
    return;
  DEF_LOG ("TriskeleFilter::writeStat", "statChannel: " << statChannel);
  if (!statChannel.doWrite)
    return;
  vector<Rect<GeoDimT> > tiles, boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (optFilter.coreCount, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);

  for (const Size<2> pattern : statChannel.winSizes) {
    auto start = high_resolution_clock::now ();
    TextWin<GeoDimT> stat (size, pattern);
    vector<vector<double> > statOutput (3, vector<double> (pixelCount));
    // XXX uniquement en 2D
    dealThread (tiles.size (), optFilter.coreCount,
		[this, &tiles, &stat, &statOutput] (const DimCore &threadId, const DimCore &minVal, const DimCore &maxVal) {
		  for (DimCore tileId = minVal; tileId < maxVal; ++tileId)
		    graphWalker.forEachVertex (tiles [tileId],
					       [this, &stat, &statOutput] (const DimImg &pixelId, const BorderFlagType &) {
						 Point<GeoDimT> p = idx2point (size, pixelId);
						 double mean, std, ent;
						 stat.getMeanStdEnt (mean, std, ent, integralImage.data (), doubleIntegralImage.data (), p.coord [0], p.coord [1], pixelId);
						 statOutput [0][pixelId] = mean;	// => pixel
						 statOutput [1][pixelId] = std;		// => double
						 statOutput [2][pixelId] = ent;		// => double
					       });
		});
    TreeStats::global.addTime (statStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    for (DimChannel localChannel (0), channel (statChannel.outputRank); localChannel < 3; ++localChannel, ++channel)
      writeBand (statOutput[localChannel].data (), channel);
  }
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::writeSelectedFP (const InputChannel &inputChannel, const InputChannel &apChannel) {
  if (apChannel.channelType != AP)
    return;
  DEF_LOG ("TriskeleFilter:writeSelectedFP", "apChannel: " << apChannel);
  if (!apChannel.prodWrite)
    return;

  if (waitingAP[&inputChannel].empty ())
    return;
  Raster<PixelT, GeoDimT> &inputRaster (getProducerRaster (inputChannel));
  for (const pair<TreeType, map <AttributeType, const InputChannel*> > &item : waitingAP[&inputChannel]) {
    const TreeType &treeType = item.first;
    LOG ("treeType: " << treeType);

    ArrayTreeBuilder<PixelT, PixelT, GeoDimT> atb (inputRaster, graphWalker, treeType, optFilter.threadPerImage, optFilter.tilePerThread, optFilter.seqTileFlag, optFilter.autoThreadFlag, false);
    WeightAttributes<PixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (apChannel.treeType));
    atb.buildTree (tree, weightAttributes, optFilter.timeFlag);
    AttributesCache<PixelT, GeoDimT> attributesCache (tree, graphWalker, inputRaster, weightAttributes);

    switch (apChannel.featureType) {
    case FAP:
      writeFilteredAttr<PixelT> (atb, inputRaster, attributesCache,
				 [] (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   atb.setAttributProfiles (attributeProfiles);
				 }, item.second);
      break;
    case FArea:
      writeFilteredAttr<DimImgPack> (atb, inputRaster, attributesCache,
				     [&attributesCache] (AttributeProfiles<DimImgPack, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				       attributeProfiles.setValues (1, attributesCache.getArea ().getValues ());
				     }, item.second);
      break;
    case FPerimeter:
      writeFilteredAttr<DimImgPack> (atb, inputRaster, attributesCache,
				     [&attributesCache] (AttributeProfiles<DimImgPack, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				       attributeProfiles.setValues (1., attributesCache.getPerimeter ().getValues ());
				     }, item.second);
      break;
    case FZLength:
      writeFilteredAttr<DimChannel> (atb, inputRaster, attributesCache,
				     [&attributesCache] (AttributeProfiles<DimChannel, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				       attributeProfiles.setValues (1., attributesCache.getZLength ().getValues ());
				     }, item.second);
      break;

    case FSTS:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (0., attributesCache.getSTS ().getValues ());
				 }, item.second);
      break;

    case FCompactness:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (1., attributesCache.getCompactness ().getValues ());
				 }, item.second);
      break;
    case FComplexity:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (1., attributesCache.getComplexity ().getValues ());
				 }, item.second);
      break;
    case FSimplicity:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (1., attributesCache.getSimplicity ().getValues ());
				 }, item.second);
      break;
    case FRectangularity:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (1., attributesCache.getRectangularity ().getValues ());
				 }, item.second);
      break;

    case FMin:
      writeFilteredAttr<PixelT> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMin ().getValues ());
				 }, item.second);
      break;
    case FMax:
      writeFilteredAttr<PixelT> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMax ().getValues ());
				 }, item.second);
      break;
    case FMean:
      writeFilteredAttr<PixelT> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMean ().getValues ());
				 }, item.second);
      break;

    case FSD:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (0., attributesCache.getSD ().getValues ());
				 }, item.second);
      break;

    case FSDW:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (0., attributesCache.getSDW ().getValues ());
				 }, item.second);
      break;
      // case FSDA:
      //   writeFilteredAttr<double> (atb, inputRaster, attributesCache,
      // 				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
      // 				   attributeProfiles.setValues (0., attributesCache.getSDA ().getValues ());
      // 				 }, item.second);
      //   break;
    case FMoI:
      writeFilteredAttr<double> (atb, inputRaster, attributesCache,
				 [&attributesCache] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
				   attributeProfiles.setValues (0., attributesCache.getMoI ().getValues ());
				 }, item.second);
      break;
    default:
      cerr << "*** Warning: Feature Profile (" << apChannel.featureType << ") no implemented!" << endl;
      BOOST_ASSERT (false);
    }
  }
  waitingAP[&inputChannel].clear ();
}

// ================================================================================
template <typename PixelT, int GeoDimT>
template <typename OutPixelT, typename APFunct>
void
TriskeleFilter<PixelT, GeoDimT>::writeFilteredAttr (ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb, const Raster<PixelT, GeoDimT> &srcRaster, AttributesCache<PixelT, GeoDimT> &attributesCache,
						    const APFunct &setAP, const map <AttributeType, const InputChannel*> &attrProd) {
  DEF_LOG ("TriskeleFilter::writeFilteredAttr", "");
  AttributeProfiles<OutPixelT, GeoDimT> attributeProfiles (tree);
  setAP (attributeProfiles, atb);

  for (const pair<AttributeType, const InputChannel*> &item2 : attrProd) {
    const InputChannel &srcChannel (* item2.second);
    LOG ("apChannel: " << srcChannel);
    const AttributeType &attributeType (srcChannel.attributeType);
    LOG ("attributeType: " << attributeType);
    vector<vector<OutPixelT> > apOutput (srcChannel.thresholds.size (), vector<OutPixelT> (pixelCount));

    switch (attributeType) {
    case AArea: {
      vector<DimImgPack> thresholds (attributesCache.getArea ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getArea ().cut (apOutput, attributeProfiles, thresholds);
    } break;
    case APerimeter: {
      vector<DimImgPack> thresholds (attributesCache.getPerimeter ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getPerimeter ().cut (apOutput, attributeProfiles, thresholds, Max);
    } break;
    case AZLength: {
      vector<DimChannel> thresholds (attributesCache.getZLength ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getZLength ().cut (apOutput, attributeProfiles, thresholds);
    } break;
    case ASTS: {
      attributesCache.getSTS ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case ACompactness: {
      attributesCache.getCompactness ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case AComplexity: {
      attributesCache.getComplexity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case ASimplicity: {
      attributesCache.getSimplicity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case ARectangularity: {
      attributesCache.getRectangularity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case AWeight: {
      vector<PixelT> thresholds (attributesCache.getWeight ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getWeight ().cut (apOutput, attributeProfiles, thresholds);
    } break;
    case ASD: {
      attributesCache.getSD ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
    case ASDW: {
      attributesCache.getSDW ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
      // case ASDA: {
      //   attributesCache.getSDA ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
      // } break;
    case AMoI: {
      attributesCache.getMoI ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
    default:
      cerr << "*** Warning: cut (" << attributeType << ") no implemented!" << endl;
      BOOST_ASSERT (false);
    }
    if (srcChannel.doWrite)
      for (DimChannel localChannel (0), channel (srcChannel.outputRank); localChannel < apOutput.size (); ++localChannel, ++channel)
	writeBand (apOutput[localChannel].data (), channel);

    if (srcChannel.produce.empty ())
      continue;
    vector<const OutPixelT*> dapSrcIdx (apOutput.size ()+1, nullptr);
    vector<OutPixelT> convertSrc;
    if (typeid (PixelT) == typeid (OutPixelT))
      dapSrcIdx[0] = (OutPixelT*) srcRaster.getPixels ();
    else {
      convertSrc.assign (srcRaster.getPixelsVector ().begin (), srcRaster.getPixelsVector ().end ());
      dapSrcIdx[0] = convertSrc.data ();
    }
    for (DimChannel i = 1; i < dapSrcIdx.size (); ++i)
      dapSrcIdx[i] = apOutput [i-1].data ();

    for (InputChannel *grandchildP : srcChannel.produce)
      writeDAP (dapSrcIdx, *grandchildP);
  }
}

// ================================================================================
template <typename PixelT, int GeoDimT>
template <typename OutPixelT>
void
TriskeleFilter<PixelT, GeoDimT>::writeDAP (const vector<const OutPixelT*> &dapSrcIdx, const InputChannel &dapChannel) {
  DEF_LOG ("TriskeleFilter::writeDAP", "dapChannel: " << dapChannel);
  BOOST_ASSERT (dapChannel.channelType == DAP);
  if (!dapChannel.doWrite)
    return;
  vector<DimChannel> dapChannelIdx = dapChannel.getRelIdx ();
  vector<vector<double> > dapOutput (dapChannelIdx.size ()/2, vector<double> (pixelCount));
  const bool dapWeight (dapChannel.dapWeight);
  // graphWalker.forEachVertex (Rect<GeoDimT> (Point<GeoDimT> (), size),
  // 			     [this, &dapOutput, &dapChannelIdx, &dapSrcIdx, &dapChannel, &dapWeight] (const DimImg &pixelId, const BorderFlagType &borderFlags) {
  for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
    for (DimChannel localChannel = 0; localChannel < dapOutput.size (); ++localChannel) {
      double dapValue = dapSrcIdx [dapChannelIdx [localChannel*2]][pixelId] - dapSrcIdx [dapChannelIdx [localChannel*2+1]][pixelId];
      if (dapWeight)
	dapValue *= double (dapSrcIdx [0][pixelId]);
      if (dapValue < 0)
	dapValue = - dapValue;
      dapOutput [localChannel][pixelId] = dapValue;
    }
  for (DimChannel localChannel (0), channel (dapChannel.outputRank); localChannel < dapOutput.size (); ++localChannel, ++channel)
    writeBand (dapOutput[localChannel].data (), channel);
  // });
}			   

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::getOneBandImage (IImage<2> &oneBandImage, const GDALDataType &dataType, const DimChannel &band) const {
  string outputBaseName  = boost::filesystem::path (outputImage.getFileName ()).replace_extension ("").string ();
  string outputExtension  = boost::filesystem::extension (outputImage.getFileName ());

  ostringstream fileNameStream;
  fileNameStream << outputBaseName << "-" << std::setfill ('0') << std::setw (3) << (band) << outputExtension;
  oneBandImage.setFileName (fileNameStream.str ());
  oneBandImage.createImage (optFilter.optCrop.size, dataType, 1);
  string projectionRef;
  vector<double> geoTransform;
  inputImage.getGeo (projectionRef, geoTransform);
  oneBandImage.setGeo (projectionRef, geoTransform, optFilter.optCrop.topLeft);
}

// ================================================================================
template <typename PixelT, int GeoDimT>
void
TriskeleFilter<PixelT, GeoDimT>::dumpInput (const InputChannel &inputChannel) {
  PixelT *pixels = nullptr;
  switch (inputChannel.channelType) {
  case Original: pixels = inputRaster.getPixels (); break;
  case Ndvi:     pixels = ndviRaster [inputChannel.typeRank].getPixels (); break;
  case Pantex:   pixels = pantexRasters [inputChannel.typeRank].getPixels (); break;
  case Sobel:    pixels = sobelRasters [inputChannel.typeRank].getPixels (); break;
  default:
    break;
  }
  if (!pixels)
    return;
  DEF_LOG ("TriskeleFilter::dumpInput", "inputChannel: " << inputChannel);
  writeBand (pixels, inputChannel.outputRank);
}

// ================================================================================
template<typename PixelT, int GeoDimT>
template<typename FeatureT>
void
TriskeleFilter<PixelT, GeoDimT>::writeBand (FeatureT *featureT, const DimChannel &channel) {
  if (optFilter.noWriteFlag)
    return;
  DEF_LOG ("TriskeleFilter::writeBand", "channel: " << channel);
  auto start = high_resolution_clock::now ();
  if (optFilter.oneBandFlag) {
    BOOST_ASSERT (GeoDimT != 3); // XXX
    IImage<2> oneBandImage;
    getOneBandImage (oneBandImage, getGDALType (featureT[0]), channel);
    oneBandImage.writeBand (featureT, 0);
  } else
    outputImage.writeBand (featureT, channel);
  TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
// Deprecated
// ================================================================================
// template<typename PixelT, GeoDimT>
// void
// TriskeleFilter<PixelT, GeoDimT>::writePixelBand (PixelT *pixels, const DimChannel &channel) {
//   if (optFilter.noWriteFlag)
//     return;
//   DEF_LOG ("TriskeleFilter::writePixelBand", "channel: " << channel);
//   auto start = high_resolution_clock::now ();
//   if (optFilter.oneBandFlag) {
//     IImage<GeoDimT> oneBandImage;
//     getOneBandImage (oneBandImage, outputImage.getDataType (), channel);
//     oneBandImage.writeBand (pixels, 0);
//   } else
//     outputImage.writeBand (pixels, channel);
//   TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
// }

// // ================================================================================
// template<typename PixelT, GeoDimT>
// void
// TriskeleFilter<PixelT, GeoDimT>::writeDoubleBand (double *pixels, const DimChannel &channel) {
//   if (optFilter.noWriteFlag)
//     return;
//   DEF_LOG ("TriskeleFilter::writeDoubleBand", "channel: " << channel);
//   auto start = high_resolution_clock::now ();
//   if (optFilter.oneBandFlag) {
//     IImage<GeoDimT> oneBandImage;
//     getOneBandImage (oneBandImage, GDT_Float64, channel);
//     oneBandImage.writeBand (pixels, 0);
//   } else
//     outputImage.writeBand (pixels, channel);
//   TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
// }

// // ================================================================================
// template<typename PixelT, GeoDimT>
// void
// TriskeleFilter<PixelT, GeoDimT>::writeDimImgBand (DimImgPack *pixels, const DimChannel &channel) {
//   if (optFilter.noWriteFlag)
//     return;
//   DEF_LOG ("TriskeleFilter::writeDimImgBand", "channel: " << channel);
//   auto start = high_resolution_clock::now ();
//   if (optFilter.oneBandFlag) {
//     IImage<GeoDimT> oneBandImage;
//     getOneBandImage (oneBandImage, GDT_UInt32, channel);
//     oneBandImage.writeBand (pixels, 0);
//   } else
//     outputImage.writeDimImgBand (pixels, channel);
//   TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
// }

// // ================================================================================
