////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "Appli/OptFilter.hpp"
#include "Appli/TriskeleFilter.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template<typename PixelT, int GeoDimT>
void
filterImage (OptFilter &optFilter) {
  IImage<GeoDimT> inputImage (optFilter.inputFilename);
  inputImage.readImage (optFilter.optChannel.spectralDepth, optFilter.optChannel.mixedBandFlag);
  IImage<GeoDimT> outputImage (optFilter.outputFilename);
  Size<GeoDimT> size (inputImage.getSize ());
  for (int i = 0; i < 2; ++i)
    size.side [i] = optFilter.optCrop.size.side [i];
  TriskeleFilter<PixelT, GeoDimT> triskeleFilter (inputImage, outputImage, optFilter.optCrop.topLeft, size, optFilter);
  triskeleFilter.parseInput ();
  inputImage.close ();
  if (optFilter.countFlag)
    cout << TreeStats::global.printDim ();
  if (optFilter.timeFlag)
    cout << TreeStats::global.printTime ();
}

// ================================================================================
int
main (int argc, char** argv, char** envp) {
  // Log::debug = true; // for debug option purpose
  OptFilter optFilter (argc, argv);
  DEF_LOG ("main", "");

  if (optFilter.optChannel.dim == 2)
    switch (optFilter.inputDataType) {
    case GDT_Byte:
      filterImage<uint8_t, 2> (optFilter); break;
    case GDT_UInt16:
      filterImage<uint16_t, 2> (optFilter); break;
    case GDT_Int16:
      filterImage<int16_t, 2> (optFilter); break;
    case GDT_UInt32:
      filterImage<uint32_t, 2> (optFilter); break;
    case GDT_Int32:
      filterImage<int32_t, 2> (optFilter); break;
    case GDT_Float32:
      filterImage<float, 2> (optFilter); break;
    case GDT_Float64:
      filterImage<double, 2> (optFilter); break;

    default :
      cerr << "unknown type!" << endl; break;
      return 1;
    }
  else
    switch (optFilter.inputDataType) {
    case GDT_Byte:
      filterImage<uint8_t, 3> (optFilter); break;
    case GDT_UInt16:
      filterImage<uint16_t, 3> (optFilter); break;
    case GDT_Int16:
      filterImage<int16_t, 3> (optFilter); break;
    case GDT_UInt32:
      filterImage<uint32_t, 3> (optFilter); break;
    case GDT_Int32:
      filterImage<int32_t, 3> (optFilter); break;
    case GDT_Float32:
      filterImage<float, 3> (optFilter); break;
    case GDT_Float64:
      filterImage<double, 3> (optFilter); break;

    default :
      cerr << "unknown type!" << endl; break;
      return 1;
    }

  return 0;
}

// ================================================================================
