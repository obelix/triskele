#include<string>

using namespace std;

string getMsg ();


class Mem {
private:
  string msg;

public:
  int id;

  Mem (string msg);
  Mem (string msg, int id);
  virtual ~Mem ();

  void set (string msg);
  string get ();
};
