////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Pantex_hpp
#define _Obelix_Pantex_hpp

#include "obelixThreads.hpp"
#include "IImage.hpp"

namespace obelix {

  using namespace std;
  using namespace triskele;

  // XXX only 2D

  // ================================================================================
  typedef uint8_t DimPantexGS; // 0-127
  typedef int8_t DimPantexSide; // 35 (not unsigned because of delta)
  typedef uint16_t DimPantexOcc; // 35*35  DimPantexSide*DimPantexSide
  typedef uint16_t DimPantexIdx; // 127*127  DimPantexGS*DimPantexGS
  typedef uint32_t DimPantexSum; // 35*35 * 127*127  DimPantexSide*DimPantexSide * DimPantexGS*DimPantexGS


  template<typename PixelT, int GeoDimT>
  void	computePantex (const Raster<PixelT, GeoDimT> &inputRaster, Raster<PixelT, GeoDimT> &pantexScaledRaster,
		       const DimCore &coreCount, const DimPantexOcc &pantexWindowSide = 35);

  template<typename PixelT, int GeoDimT>
  void	pantexLbUb (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer,
		    const float &lbPercent, const float &ubPercent,
		    PixelT &lb, PixelT &ub);
  template<typename PixelT, int GeoDimT>
  void	pantexQLbUb (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer,
		     const float &lbPercent, const float &ubPercent,
		     PixelT &lb, PixelT &ub);

  template<typename PixelT, int GeoDimT>
  void	pantexRescale (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer, const PixelT &lb, const PixelT &ub, Raster<DimPantexGS, 2> &rescaledRaster);

  DimPantexIdx pantexOccMat (const Raster<DimPantexGS, 2> &inputByteRaster,
			     const DimSideImg &ox, const DimSideImg &oy,
			     const DimPantexSide &width, const DimPantexSide &height,
			     const DimPantexSide &dx, const DimPantexSide &dy,
			     DimPantexSum &gOcc,
			     const bool &removeOldY = false, const bool &addNewY = false);
  void	computePantexShort (const Raster<DimPantexGS, 2> &inputRaster, Raster<DimPantexIdx, 2> &pantexShortRaster,
			    const DimCore &coreCount, const DimPantexOcc &pantexWindowSide);

  // ================================================================================
} // obelix

#endif // _Obelix_Pantex_hpp
