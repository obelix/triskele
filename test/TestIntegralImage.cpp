////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include <chrono>
#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IntegralImage.hpp"
#include "IImage.hpp"
#include "TreeStats.hpp"

using namespace std;
using namespace std::chrono;
using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template <typename PixelT>
void
createII (IImage<3> &inputImage, IImage<3> &outputImage) {
  const Size<3> size (inputImage.getSize ());
  const DimImg pixelsCount = size.getPixelsCount ();
  Raster<PixelT, 3> raster (size);
  inputImage.readBand<PixelT> (raster, 0);

  {
    vector<double> integralImage (pixelsCount);
    auto start = high_resolution_clock::now ();
    computeIntegralImage (size, raster.getPixels (), integralImage.data ());
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    outputImage.writeBand (integralImage.data (), 0);
  }

  {
    vector<double> doubleIntegralImage (pixelsCount);
    auto start = high_resolution_clock::now ();
    doubleMatrix (size, raster.getPixels (), doubleIntegralImage.data ());
    computeIntegralImage (size, doubleIntegralImage.data (), doubleIntegralImage.data ());
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    outputImage.writeBand (doubleIntegralImage.data (), 1);
  }

  cerr << TreeStats::global.printTime ();
}

// ================================================================================
int
main (int argc, char **argv, char **envp) {
  if (argc != 3) {
    cerr << "Usage: " << argv[0] << ": inputImage outputImage" << endl;
    exit (1);
  }
  IImage<3> inputImage (argv[1]);
  IImage<3> outputImage (argv[2]);
  inputImage.readImage ();
  outputImage.createImage (inputImage.getSize (), GDT_Float64, 2);
  
  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    createII<uint8_t> (inputImage, outputImage); break;
  case GDT_UInt16:
    createII<uint16_t> (inputImage, outputImage); break;
  case GDT_Int16:
    createII<int16_t> (inputImage, outputImage); break;
  case GDT_UInt32:
    createII<uint32_t> (inputImage, outputImage); break;
  case GDT_Int32:
    createII<int32_t> (inputImage, outputImage); break;
  case GDT_Float32:
    createII<float> (inputImage, outputImage); break;
  case GDT_Float64:
    createII<double> (inputImage, outputImage); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}
