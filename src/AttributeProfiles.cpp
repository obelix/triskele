////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "obelixThreads.hpp"
#include "obelixGeo.hpp"
#include "AttributeProfiles.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "AttributeProfiles.tcpp"

// ================================================================================
template<typename AttrT, int GeoDimT>
AttributeProfiles<AttrT, GeoDimT>::AttributeProfiles (const Tree<GeoDimT> &tree)
  : tree (tree),
    leafCount (0) {
  DEF_LOG ("AttributeProfiles::AttributeProfiles", "");
  updateTranscient ();
}

template<typename AttrT, int GeoDimT>
AttributeProfiles<AttrT, GeoDimT>::~AttributeProfiles () {
  DEF_LOG ("AttributeProfiles::~AttributeProfiles", "");
  free ();
}

template<typename AttrT, int GeoDimT>
void
AttributeProfiles<AttrT, GeoDimT>::updateTranscient () {
  DEF_LOG ("AttributeProfiles::updateTranscient", "");
  book (tree.getLeafCount ());
}

template<typename AttrT, int GeoDimT>
AttrT *
AttributeProfiles<AttrT, GeoDimT>::getValues () {
  return values.data ();
}

template<typename AttrT, int GeoDimT>
const AttrT *
AttributeProfiles<AttrT, GeoDimT>::getValues () const {
  return values.data ();
}

// ================================================================================
template<typename AttrT, int GeoDimT>
template<typename WeightT>
void
AttributeProfiles<AttrT, GeoDimT>::setValues (const AttrT defaultValue, const WeightT *weights) {
  updateTranscient ();
  fill_n (values.data (), tree.getLeafCount (), defaultValue);
  AttrT *compAP = &values[tree.getLeafCount ()];
  dealThread (tree.getCompCount (), tree.getCoreCount (), [&compAP, &weights] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
							    for (DimParent compIdx = minVal; compIdx < maxVal; ++compIdx)
							      compAP[compIdx] = weights[compIdx];
							  });
}

template<typename AttrT, int GeoDimT>
template<typename WeightT>
void
AttributeProfiles<AttrT, GeoDimT>::setValues (const AttrT *pixels, const WeightT *weights) {
  updateTranscient ();
  AttrT *leafAP = values.data ();
  dealThread (tree.getLeafCount (), tree.getCoreCount (), [&leafAP, &pixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
							    for (DimImg i = minVal; i < maxVal; ++i)
							      leafAP[i] = pixels [i];
							  });
  AttrT *compAP = leafAP+tree.getLeafCount ();
  dealThread (tree.getCompCount (), tree.getCoreCount (), [&compAP, &weights] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
							    for (DimParent compIdx = minVal; compIdx < maxVal; ++compIdx)
							      compAP[compIdx] = weights[compIdx];
							  });
}

// ================================================================================
template<typename AttrT, int GeoDimT>
void
AttributeProfiles<AttrT, GeoDimT>::free () {
  DEF_LOG ("AttributeProfiles::free", "");
  values = vector<AttrT> ();
}

template<typename AttrT, int GeoDimT>
void
AttributeProfiles<AttrT, GeoDimT>::book (const DimImg &leafCount) {
  DEF_LOG ("AttributeProfiles::book", "");
  this->leafCount = leafCount;
  // XXX max : leafCount*2-1
  values.resize (leafCount*2);
}

// ================================================================================
template<typename AttrT, int GeoDimT>
ostream &
AttributeProfiles<AttrT, GeoDimT>::print (ostream &out) const {
  out << "AP" << endl
      << printMap (values.data (), tree.getSize ().getDoubleHeight (), tree.getNodeCount ()) << endl << endl;
  return out;
}
// ================================================================================
