////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/WeightAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "WeightAttributes.tcpp"

// ================================================================================
template<typename WeightT, int GeoDimT>
WeightAttributes<WeightT, GeoDimT>::WeightAttributes (const Tree<GeoDimT> &tree, const bool &decr)
  : CompAttribute<WeightT, GeoDimT> (tree, decr) {
  DEF_LOG ("WeightAttributes::WeightAttributes", "");
}

template<typename WeightT, int GeoDimT>
WeightAttributes<WeightT, GeoDimT>::~WeightAttributes () {
  DEF_LOG ("WeightAttributes::~WeightAttributes", "");
}

// ================================================================================
template<typename WeightT, int GeoDimT>
void
WeightAttributes<WeightT, GeoDimT>::resetDecr (bool decr) {
  DEF_LOG ("WeightAttributes::resetDecr", "decr: " << decr);
  CA::decr = decr;
  CA::updateTranscient ();
}

// ================================================================================
template<typename WeightT, int GeoDimT>
void
WeightAttributes<WeightT, GeoDimT>::setWeightBounds (Tree<GeoDimT> &tree) {
  DEF_LOG ("WeightAttributes::setWeightBounds", "");
  DimParent rootId = tree.getRootId ();

  DimParent stepsCard = 0;
  WeightT curLevel = CA::values [0];
  for (DimParent compId = 1U; compId <= rootId; ++compId) {
    if (CA::values [compId] == curLevel)
      continue;
    ++stepsCard;
    curLevel = CA::values [compId];
  }
  vector<DimParentPack> &weightBounds (tree.getWeightBounds ());
  weightBounds.clear ();
  weightBounds.reserve (stepsCard+1U);
  weightBounds.push_back (0);
  curLevel = CA::values [0];
  for (DimParent compId = 1U; compId <= rootId; ++compId) {
    if (CA::values [compId] == curLevel)
      continue;
    weightBounds.push_back (compId);
    curLevel = CA::values [compId];
  }
  weightBounds.push_back (rootId+1);
}


// ================================================================================
template<typename WeightT, int GeoDimT>
template<typename PixelT>
void
WeightAttributes<WeightT, GeoDimT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
				     const vector<WeightT> &thresholds, const PruningStrategy &pruningStrategy) const {
  DEF_LOG ("WeightAttributes::cut", "");
  vector<WeightT> orderedThresholds (thresholds);
  if (CA::decr)
    reverse (orderedThresholds.begin (), orderedThresholds.end ());
  CA::cut (allBands, attributeProfiles, orderedThresholds, pruningStrategy);
}

// ================================================================================
