////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Geo2D_tpp
#define _Obelix_Geo2D_tpp

// ================================================================================
template<>
inline
Point<2>::Point () {
  coord [0] = coord [1] = 0;
}
template<>
inline
Point<2>::Point (const DimSideImg &x, const DimSideImg &y) {
  coord [0] = x;
  coord [1] = y;
}
template<>
inline
Point<2>::Point (const DimSideImg &x, const DimSideImg &y, const DimSideImg &z) {
  BOOST_ASSERT (false);
  coord [0] = x;
  coord [1] = y;
}
template<>
inline
Point<2>::Point (const DimSideImg _coord[]) {
  coord [0] = _coord [0];
  coord [1] = _coord [1];
}

// ================================================================================
template<>
inline const Point<2> &
Point<2>::getNull () {
  return NullPoint2D;
}

template<>
inline bool
obelix::operator == (const Point<2> &a, const Point<2> &b) {
  return
    a.coord [0] == b.coord [0] &&
    a.coord [1] == b.coord [1];
}

// inline template bool &obelix::operator != <2>(const Point<2> &a, const Point<2> &b);

// ================================================================================
template<>
inline DimImg
obelix::point2idx (const Size<2> &size, const Point<2> &p) {
  return
    DimImg (p.coord[0]) +
    DimImg (size.side [0])*DimImg (p.coord [1]);
}

template<>
inline Point<2>
obelix::idx2point (const Size<2> &size, const DimImg &idx) {
  return Point<2> (idx % size.side [0], idx / size.side [0]);
}

// ================================================================================
template<>
inline
DeltaPoint<2>::DeltaPoint (const Size<2> &size, const DimImg &idx1, const DimImg &idx2) {
  const DimImg x1 (idx1 % size.side [0]);
  const DimImg x2 (idx2 % size.side [0]);
  BOOST_ASSERT (x1 <= x2 || x1 == x2+1);
  BOOST_ASSERT (x2 <= x1 || x2 == x1+1);

  const DimImg y1 (idx1 / size.side [0]);
  const DimImg y2 (idx2 / size.side [0]);
  BOOST_ASSERT (y1 <= y2 || y1 == y2+1);
  BOOST_ASSERT (y2 <= y1 || y2 == y1+1);

  delta.x = ((x1 > x2) << 1) + (x1 != x2);
  delta.y = ((y1 > y2) << 1) + (y1 != y2);
  //deltaZ = 0;
}

template<>
inline DimImg
DeltaPoint<2>::addIdx (const Size<2> &size, const DimImg &idx1) const {
  // BOOST_ASSERT (!(deltaX & 1) || idx1 < size.width-1);
  // BOOST_ASSERT (!(deltaX & 2) || idx1 > 0);
  // ...
  return idx1
    + (delta.x & 1)
    - (delta.x & 2)
    + (delta.y & 1)*size.side [0]
    - (delta.y & 2)*size.side [0];
}

// ================================================================================
template<>
inline
Size<2>::Size () {
  side [0] = side [1] = 0;
}
template<>
inline
Size<2>::Size (const DimSideImg &w, const DimSideImg &h) {
  side [0] = w;
  side [1] = h;
}
template<>
inline
Size<2>::Size (const DimSideImg &w, const DimSideImg &h, const DimSideImg &l) {
  BOOST_ASSERT (false);
  side [0] = w;
  side [1] = h;
}
template<>
inline
Size<2>::Size (const DimSideImg _side[]) {
  side [0] = _side [0];
  side [1] = _side [1];
}

template<>
inline bool
Size<2>::isNull () const {
  return !side [0] || !side [1];
}

template<>
inline bool
operator== (const Size<2> &a, const Size<2> &b) {
  return
    a.side [0] == b.side [0] &&
    a.side [1] == b.side [1];
}

// ================================================================================
template<>
inline DimImg
Rect<2>::relIdx (const DimImg &absIdx, const Size<2> &fullSize) const {
  return
    DimImg (absIdx % fullSize.side [0] - point.coord [0]) +
    DimImg (size.side [0])*(absIdx / fullSize.side [0] - point.coord [1]);
}
template<>
inline DimImg
Rect<2>::absIdx (const DimTile &relIdx, const Size<2> &fullSize) const {
  return
    (relIdx % size.side [0] + point.coord [0]) +
    (relIdx / size.side [0] + point.coord [1])*fullSize.side [0];
}
template<>
inline Point<2>
Rect<2>::absPt (const DimTile &relIdx) const {
  return Point<2> (DimSideImg (relIdx % size.side [0]) + point.coord [0],
		   DimSideImg (relIdx / size.side [0]) + point.coord [1]);
}

template <>
inline bool
Rect<2>::isNull () const {
  return size.isNull ();
}

// ================================================================================
// template<typename T>
// inline void
// Rect<2>::cpToTile (const Size<2> &size, const T* src, vector<T> &dst) const;

// template<typename T>
// inline void
// Rect<2>::cpFromTile (const Size<2> &size, const vector<T> &src, T *dst) const;

// template<typename T>
// inline void
// Rect<2>::cpFromTileMove (const Size<3> &size, const vector<T> &src, T *dst, const T &move) const;

// ================================================================================
#endif // _Obelix_Geo2D_tpp
