////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Thread_tpp
#define _Obelix_Thread_tpp

using namespace std;

// ================================================================================
template<typename DimImgT, typename FunctThreadMinMax>
inline void
dealThread (const DimImgT &maxId, DimCore coreCount, const FunctThreadMinMax &functThreadMinMax/* functThreadMinMax (threadId, minVal, maxVal) */) {
  //DEF_LOG ("dealThreadBound", "coreCount:" << coreCount << " maxId:" << maxId);
  if (!maxId || !coreCount)
    return;
  vector<DimImgT> maxIds = getDealThreadBounds (maxId, coreCount);
  coreCount = maxIds.size ()-1;
  if (coreCount == 1) {
    functThreadMinMax (0, 0, maxId);
    return;
  }

#ifdef THREAD_DISABLE
  for (DimCore idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
  }
#elif INTEL_TBB_THREAD
  using namespace tbb;
#pragma warning(disable: 588)
  parallel_for (size_t (0), coreCount, [&maxIds, &functThreadMinMax] (size_t idCopyValInThread) {
					 functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
				       });
#else /* BOOST thread */
  vector<boost::thread> tasks;
  for (DimCore idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    tasks.push_back (boost::thread ([/*no ref!!!*/idCopyValInThread, &maxIds, &functThreadMinMax] () {
				      functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
				    }));
  }
  for (DimCore i = 0; i < coreCount; ++i)
    tasks[i].join ();
#endif
}

// ================================================================================
template <typename DimImgT, class OutputIterator, class T>
inline void dealThreadFill_n (const DimImgT &maxId, DimCore coreCount, OutputIterator first, const T &val) {
  dealThread (maxId, coreCount, [&first, &val] (const DimCore &threadId, const DimImgT &minVal, const DimImgT &maxVal) {
				  fill_n (first+minVal, maxVal-minVal, val);
				});
}

// ================================================================================
template<typename DimImgT, typename WeightT, typename WeightFunct, typename CmpFunct, typename CallFunct>
inline void
callOnSortedSets (const vector<DimImgT> &sizes,
		  const WeightFunct &getWeight/* getWeight (vectId, itemId) */,
		  CmpFunct isWeightInf/* isWeightInf (w1, w2) */,
		  const CallFunct &callIdId/* callIdId (vectId, itemId) */) {
  DEF_LOG ("callOnSortedSets", "size:" << sizes.size ());
  struct Queue {
    DimCore rank;
    DimImgT done, left;
    Queue () : rank (0), done (0), left (0) {}
    Queue (const DimCore &rank, const DimImgT &size)
      : rank (rank), done (0), left (size) {}
  };

  vector<Queue> queues;
  queues.reserve (sizes.size ());
  for (DimCore rank = 0; rank < sizes.size (); ++rank)
    if (sizes [rank])
      queues.push_back (Queue (rank, sizes [rank]));
  if (!queues.size ())
    return;

  if (queues.size () == 1) {
    // copy
    DimImgT size (queues[0].left);
    for (DimImgT i (0); i < size; ++i)
      callIdId (0, i);
    return;
  }

  // get first
  bool firstNaN (true);
  DimImgT firstQueueId (0);
  WeightT firstWeight (0);
  for (DimCore queueId (0); queueId < queues.size (); ++queueId) {
    WeightT tmpWeight (getWeight (queues[queueId].rank, 0));
    if (! (firstNaN || isWeightInf (tmpWeight, firstWeight)))
      continue;
    firstQueueId = queueId;
    firstWeight = tmpWeight;
    firstNaN = false;
  }
  LOG ("firstNaN:" << firstNaN << " firstQueueId:" << firstQueueId << " rank:" << queues[firstQueueId].rank << " firstWeight:" << firstWeight);

  // loop
  for (; ; ) {
    bool nextNaN (true);
    DimImgT nextQueueId (0);
    WeightT nextWeight (0);

    // loop same weight and find next
    for (DimCore queueId (firstQueueId); ; ) {
      Queue &queue (queues[queueId]);
      WeightT tmpWeight = getWeight (queue.rank, queue.done);
      if (tmpWeight == firstWeight) {
	callIdId (queue.rank, queue.done);
	++queue.done;
	--queue.left;
	if (queue.left)
	  continue;
      }

      if (!queue.left) {
	queues [queueId] = queues [queues.size ()-1];
	queues.resize (queues.size ()-1);
	queueId %= queues.size (); // if remove last => ++queueId
	if (nextQueueId == queues.size ())
	  nextQueueId = queueId; // if nextQueueId move, update index
	firstQueueId %= queues.size (); // if firstQueueId remove, must left in range
	if (queues.size () < 2)
	  break;
	continue;
      }
      if (nextNaN || isWeightInf (tmpWeight, nextWeight)) {
	// min next weight != firstWeight
	nextQueueId = queueId;
	nextWeight = tmpWeight;
	nextNaN = false;
      }
      queueId = (queueId+1U) % queues.size ();
      if (queueId == firstQueueId)
	// all firstWeight done
	break;
    }

    if (queues.size () < 2)
      break;
    BOOST_ASSERT (!nextNaN);
    firstQueueId = nextQueueId;
    firstWeight = nextWeight;
  }

  BOOST_ASSERT (queues.size () == 1);
  if (!queues.size ())
    return;

  // drain
  for (Queue &lastQueue (queues[0]); lastQueue.left; ++lastQueue.done, --lastQueue.left)
    callIdId (lastQueue.rank, lastQueue.done);
}

// ================================================================================
#endif // _Obelix_Thread_tpp
