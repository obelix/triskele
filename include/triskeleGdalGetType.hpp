////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Gdal_hpp
#define _Obelix_Gdal_hpp

#include <iostream>

#ifdef NO_OTB
#include <gdal_priv.h>
#else
#include <otbGdalDataTypeBridge.h>
#endif


namespace obelix {
  namespace triskele {
    using namespace std;

    enum DataType {
		   Unknown = 0,
		   Byte    = 1 << 0,
		   UInt16  = 1 << 1,
		   Int16   = 1 << 2,
		   UInt32  = 1 << 3,
		   Int32   = 1 << 4,
		   Float   = 1 << 5,
		   Double  = 1 << 6
    };

    // ================================================================================
    template<typename Type>
    inline DataType
    getTriskeleType (const Type &v) { return Unknown; }

    template<>
    inline DataType
    getTriskeleType<uint8_t> (const uint8_t &v) { return Byte; }

    template<>
    inline DataType
    getTriskeleType<uint16_t> (const uint16_t &v) { return UInt16; }

    template<>
    inline DataType
    getTriskeleType<int16_t> (const int16_t &v) { return Int16; }

    template<>
    inline DataType
    getTriskeleType<uint32_t> (const uint32_t &v) { return UInt32; }

    template<>
    inline DataType
    getTriskeleType<int32_t> (const int32_t &v) { return Int32; }

    template<>
    inline DataType
    getTriskeleType<float> (const float &v) { return Float; }

    template<>
    inline DataType
    getTriskeleType<double> (const double &v) { return Double; }

    // ================================================================================
    inline GDALDataType
    toGDALType (const DataType &type) {
      switch (type) {
      case Byte:	return GDT_Byte;
      case UInt16:	return GDT_UInt16;
      case Int16:	return GDT_Int16;
      case UInt32:	return GDT_UInt32;
      case Int32:	return GDT_Int32;
      case Float:	return GDT_Float32;
      case Double:	return GDT_Float64;
      default:		return GDT_Unknown;
      }
    }
    template<typename Type>
    inline GDALDataType
    getGDALType (const Type &v) { return toGDALType (getTriskeleType (v)); }

    inline size_t
    GDALDataTypeSizeOf (const GDALDataType &dataType) {
      switch (dataType) {
      case GDT_Byte:
	return 1;
      case GDT_UInt16:
      case GDT_Int16:
	return 2;
      case GDT_UInt32:
      case GDT_Int32:
      case GDT_Float32:
	return 4;
      case GDT_Float64:
	return 8;
      default :
	cerr << "GDALDataTypeSizeOf unknown type!" << endl;
	return 0;
      }
    }

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Gdal_hpp
