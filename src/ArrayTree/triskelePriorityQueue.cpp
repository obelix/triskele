////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <vector>

#include "obelixDebug.hpp"
#include "ArrayTree/triskelePriorityQueue.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "triskelePriorityQueue.tcpp"

// ================================================================================
template<typename WeightT>
PriorityQueue<WeightT>::Entry::Entry ()
  : nextEntry (0),	// dumyEntry
    idxa (0),		// unknown
    idxb (0) {		// unknown
}

// ================================================================================
template<typename WeightT>
PriorityQueue<WeightT>::Queue::Queue ()
  : previousQueue (0),	// headIdx
    nextQueue (0),	// headIdx
    level (0),		// unknown
    firstEntry (0),	// dumyEntry
    lastEntry (0) {	// dumyEntry
}

template<typename WeightT>
DimImg
PriorityQueue<WeightT>::Queue::entriesCount (PriorityQueue &priorityQueue) const {
  DimImg result (0);
  for (DimImg entry (firstEntry); entry; entry = priorityQueue.entries [entry].nextEntry)
    ++result;
  return result;
}

// ================================================================================
template<typename WeightT>
PriorityQueue<WeightT>::PriorityQueue (const DimImg &pixelsCount)
  : entries (max (pixelsCount / 8, DimImg (1))),
    queues (sizeof (WeightT) == 1 ? DimImg (256) : DimImg (1024)),
    currentIdx (0) { // headIdx
  // DEF_LOG ("PriorityQueue::PriorityQueue", "pixelsCount:" << pixelsCount);
  // getEntry (0, 0); not allow because assert
  entries.get (); // dumyEntry
  entries [0]._ctor (0, 0); // unknown, unknown
  // getQueue (0, 0, 0);  not allow because assert
  queues.get (); // headIdx
  queues [0]._ctor (0, 0, 0); // unknown, headIdx, headIdx
}

template<typename WeightT>
PriorityQueue<WeightT>::~PriorityQueue () {
  cerr << "PriorityQueue::~PriorityQueue entries: peakUse:" << entries.getPeakUse () << endl; // XXX coucou
  cerr << "PriorityQueue::~PriorityQueue queues: peakUse:" << queues.getPeakUse () << endl;  // XXX coucou
}

template<typename WeightT>
DimImg
PriorityQueue<WeightT>::queuesCount () const {
  DimImg result (0);
  for (DimImg queue (0); ; ) {
    DimImg nextQueue (queues [queue].nextQueue);
    if (!nextQueue)
      break;
    ++result;
    queue = nextQueue;
  }
  return result;
}

template<typename WeightT>
DimImg
PriorityQueue<WeightT>::queuesCountBack () const {
  DimImg result (0);
  for (DimImg queue (0); ; ) {
    DimImg previousQueue (queues [queue].previousQueue);
    if (!previousQueue)
      break;
    ++result;
    queue = previousQueue;
  }
  return result;
}

// ================================================================================
