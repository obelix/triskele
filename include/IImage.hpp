////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_IImage_hpp
#define _Obelix_IImage_hpp

/**
 * \file IImage.hpp
 * \brief Interface Image, i.e. general image type independently of the libraries chosen (GDAL ...)
 * \author Francois Merciol, comments by Jerome More
 */

#include <string>
#include <iostream>
#include <boost/assert.hpp>

#include <gdal_priv.h>

#include "obelixGeo.hpp"
#include "triskeleGdalGetType.hpp"

namespace obelix {
  using namespace std;

  // ================================================================================
  /* raster of pixels, i.e. set of values stored in memory */
  template<typename PixelT, int GeoDimT>
  class Raster {
  private:
    /* size (width,height[,length]) of the raster */
    Size<GeoDimT>	size;
    /* set of pixels (stored in a 1D vector) */
    vector<PixelT>	pixels;

  public:
    /* set the size (width,height[,length]) of a raster */
    void			setSize (const Size<GeoDimT> &size);
    /* get the size (width,height[,length]) of a raster */
    inline const Size<GeoDimT>	&getSize () const;
    /* get the set of pixel values of the entire image */
    inline const PixelT		*getPixels () const;
    inline PixelT		*getPixels ();
    /* get the set of pixel values of one given image band */
    inline const PixelT		*getFrame (const DimChannel &frameId) const;
    inline PixelT		*getFrame (const DimChannel &frameId);
    /* get the set of pixel values of the entire image as a 1D vector */
    inline vector<PixelT>	&getPixelsVector ();
    inline const vector<PixelT> &getPixelsVector () const;
    /* get the 1D-index corresponding to a given 2D/3D-point */
    inline DimImg		pointIdx (const Point<GeoDimT> &p) const;
    /* get the pixel value corresponding to a given 1D-index */
    inline PixelT		getValue (const DimImgPack &idx) const;
    /* get the pixel value corresponding to a given 2D/3D-point */
    inline PixelT		getValue (const Point<GeoDimT> &point) const;
    inline PixelT		&getValue (const Point<GeoDimT> &point);

    /* constructor */
    Raster (const Size<GeoDimT> &size = Size<GeoDimT> ());
    /* constructor */
    Raster (const Size<GeoDimT> &size, const PixelT &initValue);
    /* copy constructor */
    Raster (const Raster<PixelT, GeoDimT> &raster, const Rect<GeoDimT> &tile);
    /* destructor */
    ~Raster ();
  };

  // ================================================================================
  /** Interface Image */
  template<int GeoDimT>
  class IImage {
  public:
    /* set the filename of an image */
    void			setFileName (string fileName);
    /* get the georeferencing information (map projection, coordinate systems) of an image */
    void			getGeo (string &projectionRef, vector<double> &geoTransform, const Point<2> &topLeft = Point<2> ()) const;
    /* set the georeferencing information (map projection, coordinate systems) of an image */
    void			setGeo (const string &projectionRef, const vector<double> &geoTransform, const Point<2> &topLeft = Point<2> ());
    /* translat georeferencing information coordinate systems of an image */
    void			moveGeo (const vector<double> &src, vector<double> &dst, const Point<2> &topLeft) const;
    /* get the filename of an image */
    inline const string		&getFileName () const;
    /* get the size (width,height[,length]) of an image */
    inline const Size<GeoDimT>	&getSize () const;
    /* get the number of bands of an image */
    inline const DimChannel	&getBandCount () const;
    /* get the data type of the pixels in the image */
    inline GDALDataType		getDataType () const;
    /* tells whether the image is open for reading or not (TBC?) */
    inline const bool		isRead () const;
    /* tells whether the image is empty not */
    inline const bool		isEmpty () const;

    /* constructor */
    IImage (const string &imageFileName = "");
    /* destructor */
    ~IImage ();

    /* read an image from a file */
    void	readImage (const DimChannel &spectralDepth = 0, const bool mixedBandFlag = false);
    void	init (const DimChannel &spectralDepth, const bool mixedBandFlag);
    void	createImage (const Size<GeoDimT> &size, const GDALDataType &dataType, const DimChannel &nbOutputBands);
    void	createImage (const Size<GeoDimT> &size, const GDALDataType &dataType, const DimChannel &nbOutputBands,
			     const IImage &inputImage, const Point<2> &topLeft);
    void	close ();

    double		getNoDataValue (const DimChannel &band) const;

    template<typename PixelT>
    void		readBand (Raster<PixelT, GeoDimT> &raster, const DimChannel &band) const;
    template<typename PixelT>
    void		readBand (Raster<PixelT, GeoDimT> &raster, DimChannel band, const Point<2> &cropOrig, const Size<GeoDimT> &cropSize) const;
    template<typename PixelT>
    void		readFrame (PixelT *pixels, DimChannel band, const Point<2> &cropOrig, const Size<2> &cropSize) const;

    template<typename PixelT>
    void		writeBand (const PixelT *pixels, DimChannel band) const;
    template<typename PixelT>
    void		writeFrame (const PixelT *pixels, DimChannel band) const;
    // void		writeDoubleBand (double *pixels, DimChannel band) const;
    // void		writeDoubleFrame (double *pixels, DimChannel band) const;
    // void		writeDimImgBand (DimImgPack *pixels, DimChannel band) const;
    // void		writeDimImgFrame (DimImgPack *pixels, DimChannel band) const;

  private:
    IImage (const IImage &o) = delete;
    IImage &operator= (const IImage&) = delete;

  private:
    static size_t	gdalCount;
    string		projectionRef;
    vector<double>	geoTransform;
    string		fileName;
    Size<GeoDimT>	 	size;
    DimChannel		bandCount;
    bool		mixedBandFlag;
    GDALDataType	dataType;
    GDALDataset		*gdalInputDataset;
    GDALDataset		*gdalOutputDataset;
    bool		read;
  };

  template<int GeoDimT>
  ostream &operator << (ostream &out, const IImage<GeoDimT> &iImage);

  // ================================================================================
#include "IImage.tpp"

} // obelix

#endif // _Obelix_IImage_hpp
