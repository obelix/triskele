////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
#include <iomanip>

#include "misc.hpp"
#include "TreeStats.hpp"

using namespace obelix;
using namespace obelix::triskele;
using namespace std;

// ================================================================================
TreeStats TreeStats::global;

const string TreeStats::timeTypeLabels [] =
  {
   "border",
   "NDVI",
   "pantex",
   "sobel",
   "IntegralImages",
   "Haar",
   "stat",

   "build tree",
   "  setup  ",
   "  to tiles  ",
   "  sort edges  ",
   "  tileParents  ",
   "  imgParents  ",
   "  from tiles  ",
   "  merge  ",
   "  forest mgt. ",
   "  index  ",
   "  compress  ",
   "  children  ",

   "set AP",

   "area",
   "perimeter",
   "zLength",
   "sts",
   "compactness",
   "complexity",
   "simplicity",
   "rectangularity",

   "min",
   "max",
   "mean",

   "bb",
   "centroid",
   "sd",
   "sdw",
   // "sda",
   "moi",

   "filtering",
   "copy image",
   "GDAL read",
   "GDAL write",
   "HDF5 read",
   "HDF5 write",
   "sum",

   "Learning",
   "clean Fg/Bg",
   "Check Acc.",
   "Prediction",

   "*** OUT OF TimeType ***"
  };

// ================================================================================
TreeStats::TreeStats ()
  : leavesStats (TreeTypeCard),
    compStats (TreeTypeCard),
    timeStats (TimeTypeCard) {
  reset ();
}

void
TreeStats::reset () {
  for (unsigned int i = 0; i < TreeTypeCard; ++i) {
    leavesStats[i] = TreeStatsDim ();
    compStats[i] = TreeStatsDim ();
  }
  for (unsigned int i = 0; i < TimeTypeCard; ++i)
    timeStats[i] = TreeStatsDouble ();
}

// ================================================================================
TreeStats::CPrintDim::CPrintDim (const TreeStats &treeStats)
  : treeStats (treeStats) {
}

ostream &
TreeStats::CPrintDim::print (ostream &out) const {
  bool empty = true;
  for (unsigned int i = 0; i < TreeTypeCard; ++i)
    if (!(empty = !ba::count (treeStats.leavesStats[i])))
      break;
  if (empty)
    return out;
  print (out, treeStats.leavesStats, "Leaf");
  return print (out, treeStats.compStats, "Comp");
}

ostream &
TreeStats::CPrintDim::print (ostream &out, const vector<TreeStatsDim> stats, const string &msg) const {
  out << endl
      << setw (11) << left << msg  << "\t"
      << setw (3)  << left << "Count" << "\t"
      << setw (9) << left << "Mean"  << "\t"
      << setw (9) << left << "Min"  << "\t"
      << setw (9) << left << "Max"  << endl;
  for (unsigned int i = 0; i < TreeTypeCard; ++i) {
    if (!ba::count (stats[i]))
      continue;
    out << setw (11) << right << treeTypeLabels [i] << "\t"
	<< setw (3) << ba::count (stats[i]) << "\t"
	<< setw (9) << DimImg (ba::mean (stats[i])) << "\t"
	<< setw (9) << ba::min (stats[i])  << "\t"
	<< setw (9) << ba::max (stats[i])
	<< endl << flush;
  }
  return out;
}

TreeStats::CPrintDim
TreeStats::printDim () const {
  return CPrintDim (*this);
}
// ostream&
// operator << (ostream &out, const TreeStats::CPrintDim &cpd) {
//   return cpd.print (out);
// }


// ================================================================================
TreeStats::CPrintTime::CPrintTime (const TreeStats &treeStats)
  : treeStats (treeStats) {
}
ostream &
TreeStats::CPrintTime::print (ostream &out) const {
  bool empty = true;
  for (unsigned int i = 0; i < TimeTypeCard; ++i)
    if (!(empty = !ba::count (treeStats.timeStats[i])))
      break;
  if (empty)
    return out;
  out << endl
      << setw (16) << left << "Time"  << "\t"
      << setw (15) << left << "Sum"   << "\t"
      << setw (3)  << left << "Count" << "\t"
      << setw (15) << left << "Mean"  << "\t"
      << setw (15) << left << "Min"  << "\t"
      << setw (15) << left << "Max"  << endl;
  for (unsigned int i = 0; i < TimeTypeCard; ++i) {
    if (!ba::count (treeStats.timeStats[i]))
      continue;
    out << setw (16) << right << timeTypeLabels[i] << "\t"
	<< ns2string (ba::sum (treeStats.timeStats[i]))  << "\t"
	<< setw (3) << ba::count (treeStats.timeStats[i]) << "\t"
	<< ns2string (ba::mean (treeStats.timeStats[i])) << "\t"
	<< ns2string (ba::min (treeStats.timeStats[i]))  << "\t"
	<< ns2string (ba::max (treeStats.timeStats[i]))  << "\t"
	<< endl << flush;
  }
  return out;
}

TreeStats::CPrintTime
TreeStats::printTime () const {
  return CPrintTime (*this);
}
// ostream&
// operator << (ostream &out, const TreeStats::CPrintTime &cpt) {
//   return cpt.print (out);
// }

// ================================================================================
