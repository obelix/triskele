////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_InteGralImage_tpp
#define _Obelix_InteGralImage_tpp

namespace obelix {

  // ================================================================================
  template<typename PixelT>
  inline void
  computeIntegralImage (const Size<2> &size, const PixelT *inputPixels, double *iiValues) {
    const DimImg line (size.side [0]);
    const DimImg frame (line*size.side [1]);
    const DimImg firstX (1);
    const DimImg lastX (line);
    const DimImg firstY (line);
    const DimImg lastY (frame);

    double value = 0;
    // [0][0]
    // iiValues [0 + 0] = double (inputPixels [0 + 0]);
    for (DimImg x = 0; x < lastX; ++x)
      // [x][0]
      iiValues [x + 0] = value += double (inputPixels [x + 0]);
    value = double (inputPixels [0 + 0]);
    for (DimImg y = firstY; y < lastY; y += line)
      // [0][y]
      iiValues [0 + y] = value += double (inputPixels [0 + y]);
    for (DimImg prevY = 0, y = firstY; y < lastY; prevY = y, y += line) {
      for (DimImg prevX = 0, x = firstX; x < lastX; prevX = x, ++x)
	// [x][y]
	iiValues [x + y] =
	  double (inputPixels [x + y])
	  + iiValues [x + prevY]
	  + iiValues [prevX + y]
	  - iiValues [prevX + prevY];
    }
  }

  template<typename PixelT>
  inline void
  computeIntegralImage (const Size<3> &size, const PixelT *inputPixels, double *iiValues) {
    const DimImg line (size.side [0]);
    const DimImg frame (line*size.side [1]);
    const DimImg cube (frame*size.side [2]);    
    const DimImg firstX (1);
    const DimImg lastX (line);
    const DimImg firstY (line);
    const DimImg lastY (frame);
    const DimImg firstZ (frame);
    const DimImg lastZ (cube);
    
    double value = 0;
    // [0][0][0]
    // iiValues [0 + 0] = double (inputPixels [0 + 0]);
    for (DimImg x = 0; x < lastX; ++x)
      // [x][0][0]
      iiValues [x + 0 + 0] = value += double (inputPixels [x + 0 + 0]);
    value = double (inputPixels [0 + 0 + 0]);
    for (DimImg y = firstY; y < lastY; y += line)
      // [0][y][0]
      iiValues [0 + y + 0] = value += double (inputPixels [0 + y + 0]);
    value = double (inputPixels [0 + 0 + 0]);
    for (DimImg z = firstZ; z < lastZ; z += frame)
      // [0][0][z]
      iiValues [0 + 0 + z] = value += double (inputPixels [0 + 0 + z]);

    for (DimImg prevY = 0, y = firstY; y < lastY; prevY = y, y += line) {
      for (DimImg prevX = 0, x = firstX; x < lastX; prevX = x, ++x)
	// [x][y]
	iiValues [x + y + 0] =
	  double (inputPixels [x + y + 0])
	  + iiValues [x + prevY + 0]
	  + iiValues [prevX + y + 0]
	  - iiValues [prevX + prevY + 0];
    }

    for (DimImg prevZ = 0, z = firstZ; z < lastZ; prevZ = z, z += frame)
      for (DimImg prevY = 0, y = firstY; y < lastY; prevY = y, y += line)
	for (DimImg prevX = 0, x = firstX; x < lastX; prevX = x, ++x)
	  // [x][y]
	  iiValues [x + y + z] =
	    double (inputPixels [x + y + z])
	    + iiValues [x + y + prevZ]
	    + iiValues [x + prevY + z]
	    + iiValues [prevX + y + z]
	    - iiValues [prevX + prevY + z]
	    - iiValues [prevX + y + prevZ]
	    - iiValues [x + prevY + prevZ]
	    + iiValues [prevX + prevY + prevZ];
  }

  // ================================================================================
  template<typename PixelT, int GeoDimT>
  inline void
  doubleMatrix (const Size<GeoDimT> &size, const PixelT *inputPixels, double *doubleValues) {
    const DimImg pixelCount = size.getPixelsCount ();
    for (DimImg i = 0; i < pixelCount; ++i, ++inputPixels, ++doubleValues)
      *doubleValues = double (*inputPixels) * double (*inputPixels);
  }

  // ================================================================================
} // obelix

#endif // _Obelix_InteGralImage_tpp
