////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>

#include "obelixExtendInt.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
//#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace std;
using namespace obelix;

bool
test_ExtendInt () {
  bool testValid (true);

  uint40_t u;
  // cout << "coucou : " << uint64_t (u) << endl;
  //++u;

  // for (uint16_t i = 0; i < 256; ++i) {
  //   uint8_t byte = i;
  //   if (!++byte)
  //     cout << "incr :" << i << " true " << (uint16_t) byte << endl;
  //   else
  //     cout << "incr :" << i << " false " << (uint16_t) byte << endl;
  // }

  // for (uint16_t i = 0; i < 256; ++i) {
  //   uint8_t byte = i;
  //   if (!byte--)
  //     cout << "decr :" << i << " true " << (uint16_t) byte << endl;
  //   else
  //     cout << "decr :" << i << " false " << (uint16_t) byte << endl;
  // }

  return testValid;
}

int main () { cout << "Test result : " << test_ExtendInt () << endl; }

// TEST_CASE("Testing 40 bits int...") {
//     REQUIRE (test_integralImage () == true);
// }
