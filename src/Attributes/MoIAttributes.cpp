////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>    // std::fill
#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/MoIAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "MoIAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
MoIAttributes<GeoDimT>::MoIAttributes (const Tree<GeoDimT> &tree, const AreaAttributes<GeoDimT> &areaAttributes, const Centroid<GeoDimT> &centroid)
  : CompAttribute<double, GeoDimT> (tree) {
  DEF_LOG ("MoIAttributes::MoIAttributes", "");
  compute (areaAttributes, centroid);
}

template<int GeoDimT>
MoIAttributes<GeoDimT>::~MoIAttributes () {
  DEF_LOG ("MoIAttributes::~MoIAttributes", "");
}

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
void
MoIAttributes<GeoDimT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
			 const vector<double> &thresholds, const PruningStrategy &pruningStrategy) const {
  DEF_LOG ("MoIAttributes::cut", "thresholds:" << thresholds.size ());
  if (thresholds.empty ())
    return;
  double maxValue = CA::getMaxValue ();
  cerr << "moi max value:" << maxValue << endl;
  CA::cut (allBands, attributeProfiles, CA::getScaledThresholds (thresholds, maxValue), pruningStrategy);
}

// ================================================================================
template<int GeoDimT>
void
MoIAttributes<GeoDimT>::compute (const AreaAttributes<GeoDimT> &areaAttributes, const Centroid<GeoDimT> &centroid) {
  DEF_LOG ("MoIAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CA::tree.getSize ().side [0];
  DimImg const height = CA::tree.getSize ().side [1];
  const AverageCoord<GeoDimT> *averageCoord (centroid.getValues ());

  fill (CA::values.begin(), CA::values.end (), 0.);

  CA::tree.forEachLeaf
    ([this, &width, &height, &averageCoord] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimImg yz = leafId / width;
       const DimSideImg y = yz % height;
       const DimChannel z = yz / height;

       const AverageCoord<GeoDimT> &averageCoordP (averageCoord[parentId]);
       const double dx = averageCoordP.coord [0] - x;
       const double dy = averageCoordP.coord [1] - y;
       const double dz = averageCoordP.coord [2] - z;
       CA::values [parentId] += dx*dx+dy*dy+dz*dz;
     });

  const DimImgPack *areas = areaAttributes.getValues ();
  CA::tree.forEachComp
    ([this, &areas, &averageCoord] (const DimParent &compId) {
       const double card = areas[compId];
       CA::values [compId] /= card*card;
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       const AverageCoord<GeoDimT> &averageCoordC (averageCoord[compId]);
       const AverageCoord<GeoDimT> &averageCoordP (averageCoord[parentId]);
       const double dx = averageCoordP.coord [0] - averageCoordC.coord [0];
       const double dy = averageCoordP.coord [1] - averageCoordC.coord [1];
       const double dz = averageCoordP.coord [2] - averageCoordC.coord [2];
       CA::values [parentId] += (dx*dx+dy*dy+dz*dz)*card;
     });

  TreeStats::global.addTime (moiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<>
void
MoIAttributes<2>::compute (const AreaAttributes<2> &areaAttributes, const Centroid<2> &centroid) {
  DEF_LOG ("MoIAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CompAttribute<double, 2>::tree.getSize ().side [0];
  const AverageCoord<2> *averageCoord (centroid.getValues ());

  fill (CompAttribute<double, 2>::values.begin(), CompAttribute<double, 2>::values.end (), 0.);

  CompAttribute<double, 2>::tree.forEachLeaf
    ([this, &width, &averageCoord] (const DimImg &leafId) {
       const DimParent parentId (CompAttribute<double, 2>::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimSideImg y = leafId / width;

       const AverageCoord<2> &averageCoordP (averageCoord[parentId]);
       const double dx = averageCoordP.coord [0] - x;
       const double dy = averageCoordP.coord [1] - y;
       CompAttribute<double, 2>::values [parentId] += dx*dx+dy*dy;
     });

  const DimImgPack *areas = areaAttributes.getValues ();
  CompAttribute<double, 2>::tree.forEachComp
    ([this, &areas, &averageCoord] (const DimParent &compId) {
       const double card = areas[compId];
       CompAttribute<double, 2>::values [compId] /= card*card;
       const DimParent parentId (CompAttribute<double, 2>::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       const AverageCoord<2> &averageCoordC (averageCoord[compId]);
       const AverageCoord<2> &averageCoordP (averageCoord[parentId]);
       const double dx = averageCoordP.coord [0] - averageCoordC.coord [0];
       const double dy = averageCoordP.coord [1] - averageCoordC.coord [1];
       CompAttribute<double, 2>::values [parentId] += (dx*dx+dy*dy)*card;
     });

  TreeStats::global.addTime (moiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
