////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/BoundingBoxAttributes.hpp"
using namespace obelix::triskele;
#include "BoundingBoxAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
BoundingBoxAttributes<GeoDimT>::BoundingBoxAttributes (const Tree<GeoDimT> &tree)
  : CompAttribute<BoundingBox<GeoDimT>, GeoDimT> (tree) {
  DEF_LOG ("BoundingBoxAttributes::BoundingBoxAttributes", "");
  compute ();
}

template<int GeoDimT>
BoundingBoxAttributes<GeoDimT>::~BoundingBoxAttributes () {
  DEF_LOG ("BoundingBoxAttributes::~BoundingBoxAttributes", "");
}

// ================================================================================
template<int GeoDimT>
void
BoundingBoxAttributes<GeoDimT>::compute () {
  DEF_LOG ("BoundingBoxAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CA::tree.getSize ().side [0];
  DimImg const height = CA::tree.getSize ().side [1];
  CA::tree.forEachLeaf
    ([this, &width, &height] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimImg yz = leafId / width;
       const DimSideImg y = yz % height;
       const DimChannel z = yz / height;
       BoundingBox<GeoDimT> &bb (CA::values[parentId]);
       if (bb.min [0] > x) bb.min [0] = x;
       if (bb.min [1] > y) bb.min [1] = y;
       if (bb.min [2] > z) bb.min [2] = z;
       if (bb.max [0] < x) bb.max [0] = x;
       if (bb.max [1] < y) bb.max [1] = y;
       if (bb.max [2] < z) bb.max [2] = z;
     });
  CA::tree.forEachComp
    ([this] (const DimParent &compId) {
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       const BoundingBox<GeoDimT> &bbc (CA::values[compId]);
       BoundingBox<GeoDimT> &bbp (CA::values[parentId]);
       if (bbp.min [0] > bbc.min [0]) bbp.min [0] = bbc.min [0];
       if (bbp.min [1] > bbc.min [1]) bbp.min [1] = bbc.min [1];
       if (bbp.min [2] > bbc.min [2]) bbp.min [2] = bbc.min [2];
       if (bbp.max [0] < bbc.max [0]) bbp.max [0] = bbc.max [0];
       if (bbp.max [1] < bbc.max [1]) bbp.max [1] = bbc.max [1];
       if (bbp.max [2] < bbc.max [2]) bbp.max [2] = bbc.max [2];
     });
  TreeStats::global.addTime (bbStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<>
void
BoundingBoxAttributes<2>::compute () {
  DEF_LOG ("BoundingBoxAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CompAttribute<BoundingBox<2>, 2>::tree.getSize ().side [0];
  CompAttribute<BoundingBox<2>, 2>::tree.forEachLeaf
    ([this, &width] (const DimImg &leafId) {
       const DimParent parentId (CompAttribute<BoundingBox<2>, 2>::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimSideImg y = leafId / width;
       BoundingBox<2> &bb (CompAttribute<BoundingBox<2>, 2>::values[parentId]);
       if (bb.min [0] > x) bb.min [0] = x;
       if (bb.min [1] > y) bb.min [1] = y;
       if (bb.max [0] < x) bb.max [0] = x;
       if (bb.max [1] < y) bb.max [1] = y;
     });
  CompAttribute<BoundingBox<2>, 2>::tree.forEachComp
    ([this] (const DimParent &compId) {
       const DimParent parentId (CompAttribute<BoundingBox<2>, 2>::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       const BoundingBox<2> &bbc (CompAttribute<BoundingBox<2>, 2>::values[compId]);
       BoundingBox<2> &bbp (CompAttribute<BoundingBox<2>, 2>::values[parentId]);
       if (bbp.min [0] > bbc.min [0]) bbp.min [0] = bbc.min [0];
       if (bbp.min [1] > bbc.min [1]) bbp.min [1] = bbc.min [1];
       if (bbp.max [0] < bbc.max [0]) bbp.max [0] = bbc.max [0];
       if (bbp.max [1] < bbc.max [1]) bbp.max [1] = bbc.max [1];
     });
  TreeStats::global.addTime (bbStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
