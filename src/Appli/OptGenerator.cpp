////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#define LAST_VERSION "2.5 2021-11-11 ("+getOsName ()+")"

#include <iostream>
#include <string.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "misc.hpp"
#include "IImage.hpp"
#include "Appli/OptGenerator.hpp"
#include "Appli/OptCropParser.hpp"

#ifndef TRISKELE_ERROR
#define TRISKELE_ERROR(expr) std::cerr << expr << std::endl << std::flush, std::exit (1)
#endif

using namespace std;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template<typename T>
vector<T>
readThresholds (string fileName) {
  vector<T> thresholds;
  if (fileName.empty ()) {
    return thresholds;
  }
  std::ifstream ifs;
  ifs.open (fileName);
  string line;
  while (getline (ifs, line)) {
    stringstream ss (line);
    T value;
    ss >> value;
    if (ss.fail ())
      continue;
    thresholds.push_back (value);
  }
  if (thresholds.empty ())
    cerr << "*** readThresholds: empty file: " << fileName << endl;
  sort (thresholds.begin (), thresholds.end ());
  return thresholds;
}
#if defined IMAGE_40BITS || defined IMAGE_LARGE
template<>
vector<DimImgPack>
readThresholds (string fileName) {
  vector<uint64_t> tmp (readThresholds<uint64_t>(fileName));
  vector<DimImgPack> thresholds (tmp.size  ());
  for (DimChannel i = 0; i < tmp.size (); ++i)
    thresholds [i] = tmp [i];
  return thresholds;
}
#endif // defined IMAGE_40BITS || defined IMAGE_LARGE

// ================================================================================
OptGenerator::OptGenerator ()
  : debugFlag (false),
    spectralDepth (0),
    mixedBandFlag (false),
    countingSortCeil (2),
    coreCount (boost::thread::hardware_concurrency ()),
    maxTreeFlag (false),
    minTreeFlag (false),
    medTreeFlag (false),
    alphaTreeFlag (false),
    border (false),
    oneBandFlag (false),
    connectivity (Connectivity (Connectivity::C4|Connectivity::CT)),
    featureType (FeatureType::FAP) {
}

OptGenerator::OptGenerator (int argc, char** argv)
  : OptGenerator () {
  parse (argc, argv);
}

// ================================================================================
namespace po = boost::program_options;
static OptCropParser optCropParser;
static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
OptGenerator::usage (string msg, bool hidden) {
  if (msg.size ())
    cout << msg << endl;
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [-i] inputFileName [-o] outputFileName [options]" << endl << endl
       << endl << mainDescription
       << endl << optCropParser.description
       << endl;
  if (hidden)
    cout << hide << endl;
  exit (1);
}

void
version () {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  GDAL  : read and write image (http://www.gdal.org/)" << endl
       << "  cct   : inspired by jirihavel library to build trees (https://github.com/jirihavel/libcct)" << endl
       << "  Boost : C++ libraries (http://www.boost.org/)" << endl
       << endl << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptGenerator::parse (int argc, char** argv) {
  prog = argv [0];
  bool helpFlag = false, versionFlag = false, useTheForceLuke = false;
  string inputFileName, outputFileName;
  string areaThresholdsName, levelThresholdsName, sdThresholdsName, sdwThresholdsName, moiThresholdsName; // , sdaThresholdsName
  try {
    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("version", po::bool_switch (&versionFlag), "display version information")
      ("debug", po::bool_switch (&debugFlag), "debug mode")

      ("spectralDepth,d", po::value<DimChannel> (&spectralDepth)->default_value (spectralDepth), "spectral band count (0 all bands are spectral = no volume)")
      ("mixedBandFlag", po::bool_switch (&mixedBandFlag)->default_value (mixedBandFlag), "if bands are mixed in frames")

      ("band,b", po::value<OptRanges> (&selectedBand)->default_value (selectedBand), "select input band (first band is 0)")

      ("border", po::bool_switch (&border), "build tree without border pixels (no-data)")
      ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
      ("input,i", po::value<string> (&inputFileName), "input file name image")
      ("output,o", po::value<string> (&outputFileName), "output file name hyperbands image (contains attributs profiles)")

      ("maxTree", po::bool_switch (&maxTreeFlag), "build max-tree")
      ("minTree", po::bool_switch (&minTreeFlag), "build min-tree")
      ("medTree", po::bool_switch (&medTreeFlag), "build tree-of-shape")
      ("alphaTree", po::bool_switch (&alphaTreeFlag), "build alpha-tree")

      ("featureType", po::value<FeatureType> (&featureType)->default_value (FAP), "feature profiles produced (AP = attribut profiles)")

      ("area,A", po::value<string> (&areaThresholdsName), "cut according area attributs")
      ("weight,W", po::value<string> (&levelThresholdsName), "cut according level attributs")
      ("SD,S", po::value<string> (&sdThresholdsName), "cut according standard deviation attributs on value")
      ("SDW", po::value<string> (&sdwThresholdsName), "cut according standard deviation attributs on level")
      // ("SDA", po::value<string> (&sdaThresholdsName), "cut according standard deviation attributs on area")
      ("MoI,M", po::value<string> (&moiThresholdsName), "cut according moment of inertia attributs")
      ;

    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used to build tree")
      ("oneBandFlag", po::bool_switch (&oneBandFlag), "split all bands, one band per image")

      ("countingSortCeil", po::value<DimSortCeil> (&countingSortCeil)->default_value (countingSortCeil), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")

      // ("no-border", po::bool_switch (&optGenerator.noBorder), "build tree with all pixels (included no-data)")
      // ("dap", po::bool_switch (&optGenerator.dapFlag), "produce DAP rather than AP")
      // ("use-all-orig", po::bool_switch (&optGenerator.useAllOrigFlag), "force use all original band")
      // ("dap-no-orig", po::bool_switch (&optGenerator.dapNoOrigFlag), "force don't use original AP band")
      // ("dap-orig-pos", po::value<unsigned int> (&apOrigPos), (string ("position of origin on DAP (default ")+
      // 							      boost::lexical_cast<std::string> (apOrigPos)+
      // 							      " = "+apOrigPosName [apOrigPos]+")").c_str ())
      // ("dap-orig-weight", po::bool_switch (&optGenerator.dapOrigWeightFlag), "use origin weight for dap")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optCropParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input thresholds output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (useTheForceLuke)
      usage ("", true);
    if (versionFlag)
      version ();
    if (helpFlag)
      usage ();

    int required = 2;
    if (vm.count ("input"))
      required--;
    if (vm.count ("output"))
      required--;

    int nbArgs = 0;
    if (vm.count (inputFileC))
      nbArgs = vm[inputFileC].as<vector<string> > ().size ();
    if (required-nbArgs != 0)
      usage ("Need one input and one output");
    if (required) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      if (outputFileName.empty ())
	outputFileName = var [--required];
      if (inputFileName.empty ())
	inputFileName = var [--required];
    }

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available. You must compile without: -DENABLE_LOG" << endl;
#endif
      Log::debug = true;
    }

    areaThresholds = readThresholds<DimImgPack> (areaThresholdsName);
    levelThresholds = readThresholds<double> (levelThresholdsName);
    sdThresholds = readThresholds<double> (sdThresholdsName);
    sdwThresholds = readThresholds<double> (sdwThresholdsName);
    // sdaThresholds = readThresholds<double> (sdaThresholdsName);
    moiThresholds = readThresholds<double> (moiThresholdsName);

    inputImage.setFileName (inputFileName);
    outputImage.setFileName (outputFileName);
    inputImage.readImage (spectralDepth, mixedBandFlag);

    GDALDataType inputType = inputImage.getDataType ();

    DimChannel bandInputCount = inputImage.getBandCount ();
    if (selectedBand.empty ())
      selectedBand = OptRanges (0, int (bandInputCount-1));
    selectedBand.setLimits (0, int (bandInputCount-1));
    selectedBand.toSet ();

    optCropParser.parse (optCrop, inputImage.getSize (), parsed);

    cout
      << "Input:" << inputFileName << " (channels of " << GDALGetDataTypeName (inputType) << ")" << endl
      << "Crop:" << optCrop << " band:" << selectedBand << endl
      << "Output:" << outputFileName << endl
      << "core count:" << coreCount << endl;

    if (selectedBand.size () &&
	(selectedBand.first () < 0 ||
	 selectedBand.last () >= int (bandInputCount)))
      TRISKELE_ERROR ("Band out of image (0 <= " << selectedBand.first () << " " << selectedBand.last () << " " << selectedBand << " < " << bandInputCount << ")");

  } catch (...) {
    usage ("Bad options");
  }
}

// ================================================================================
