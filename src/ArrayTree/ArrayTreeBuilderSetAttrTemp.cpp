////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "ArrayTree/ArrayTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "ArrayTreeBuilderSetAttrTemp.tcpp"

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::setAttributProfiles (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles) {
  DEF_LOG ("ArrayTreeBuilder::setAttributProfiles", "");
  attributeProfiles.updateTranscient ();
    switch (treeType) {
  case MIN:
    setAttributProfiles (attributeProfiles, WeightFunction<PixelT, WeightT, GeoDimT, MIN> (raster.getPixels (), graphWalker));
    break;
  case MAX:
    setAttributProfiles (attributeProfiles, WeightFunction<PixelT, WeightT, GeoDimT, MAX> (raster.getPixels (), graphWalker));
    break;
  case MED:
    setAttributProfiles (attributeProfiles, WeightFunction<PixelT, WeightT, GeoDimT, MED> (raster.getPixels (), graphWalker));
    break;
  case TOS:
    setAttributProfiles (attributeProfiles, WeightFunction<PixelT, WeightT, GeoDimT, TOS> (raster.getPixels (), graphWalker));
    break;
  case ALPHA:
    setAttributProfiles (attributeProfiles, WeightFunction<PixelT, WeightT, GeoDimT, ALPHA> (raster.getPixels (), graphWalker));
    break;
  default:
    cerr << "*** unknown tree type: " << treeType << endl;
  }

}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
template<TreeType TreeTypeT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::setAttributProfiles (AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> &weightFunct) {
  PixelT *leafAP = attributeProfiles.getValues ();
  dealThread (TreeBuilder<GeoDimT>::leafCount, coreCount, [&weightFunct, &leafAP] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
							weightFunct.copyPixelsBound (leafAP, minVal, maxVal);
						      });
  PixelT *compAP = leafAP+TreeBuilder<GeoDimT>::leafCount;
  dealThread (TreeBuilder<GeoDimT>::getCompCount (), coreCount, [this, &weightFunct, &compAP] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
							      weightFunct.weight2valueBound (compAP, compWeights, minVal, maxVal);
							    });
}

// ================================================================================
