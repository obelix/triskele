////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_MaxAttributes_hpp
#define _Obelix_Triskele_MaxAttributes_hpp

#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    template<typename PixelT, int GeoDimT>
    class MaxAttributes : public CompAttribute<PixelT, GeoDimT> {
    public:
      typedef CompAttribute<PixelT, GeoDimT> CA;

      MaxAttributes (const Tree<GeoDimT> &tree, const Raster<PixelT, GeoDimT> &raster);
      virtual ~MaxAttributes ();

      virtual inline void save (HDF5 &hdf5) const { CA::save (hdf5, "max"); }
      virtual inline void load (HDF5 &hdf5) { CA::load (hdf5, "max"); }

      // void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, const vector<PixelT> &thresholds, const PruningStrategy &pruningStrategy = Direct) const;
      virtual inline ostream &print (ostream &out) const { CA::print (out, "max"); return out; }
      virtual inline const PixelT getPixelValue (const DimImg &leafId) const { return pRaster->getValue (leafId); }
    protected:
      const Raster<PixelT, GeoDimT> *pRaster;
      void compute ();
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_MaxAttributes_hpp
