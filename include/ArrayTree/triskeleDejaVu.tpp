////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_DejaVu_tpp
#define _Obelix_Triskele_DejaVu_tpp

// ================================================================================
template DejaVu<2>::DejaVu (const Size<2> &size);
template bool DejaVu<2>::testAndSet (const DimImg &idxa, const DimImg &idxb);

template<int GeoDimT>
inline
DejaVu<GeoDimT>::DejaVu (const Size<GeoDimT> &size)
  : size (size),
    neighborMap (size.getPixelsCount ()) {
}

template<int GeoDimT>
inline bool
DejaVu<GeoDimT>::testAndSet (const DimImg &idxa, const DimImg &idxb) {
  if (idxb < idxa)
    return testAndSet (idxb, idxa);
  BOOST_ASSERT (DeltaPoint<GeoDimT> (size, idxa, idxb).set & ~0x0FU);
  uint16_t mask = 1U << DeltaPoint<GeoDimT> (size, idxa, idxb).set;
  if (neighborMap [idxa] & mask)
    return true;
  neighborMap [idxa] |= mask;
  return false;
}

inline
DejaVu<3>::DejaVu (const Size<3> &size)
  : size (size),
    neighborMap (size.getPixelsCount ()) {
}

inline bool
DejaVu<3>::testAndSet (const DimImg &idxa, const DimImg &idxb) {
  if (idxb < idxa)
    return testAndSet (idxb, idxa);
  BOOST_ASSERT (DeltaPoint<3> (size, idxa, idxb).set & ~0x3FUL);
  uint64_t mask = 1UL << DeltaPoint<3> (size, idxa, idxb).set;
  if (neighborMap [idxa] & mask)
    return true;
  neighborMap [idxa] |= mask;
  return false;
}

// ================================================================================

#endif // _Obelix_Triskele_DejaVu_tpp
