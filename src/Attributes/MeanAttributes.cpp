////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>    // std::fill
#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Tree.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/MeanAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "MeanAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
MeanAttributes<GeoDimT>::MeanAttributes (const Tree<GeoDimT> &tree, const Raster<PixelT, GeoDimT> &raster, const AreaAttributes<GeoDimT> &areaAttributes)
  : CompAttribute<double, GeoDimT> (tree) {
  DEF_LOG ("MeanAttributes::MeanAttributes", "");
  compute (raster, areaAttributes);
}

template<int GeoDimT>
MeanAttributes<GeoDimT>::~MeanAttributes () {
  DEF_LOG ("MeanAttributes::~MeanAttributes", "");
}

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
void
MeanAttributes<GeoDimT>::compute (const Raster<PixelT, GeoDimT> &raster, const AreaAttributes<GeoDimT> &areaAttributes) {
  DEF_LOG ("MeanAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  this->raster.setSize (raster.getSize ());
  double *copyPixels = this->raster.getPixels ();

  fill (CA::values.begin(), CA::values.end (), 0.);

  const PixelT *pixels = raster.getPixels ();
  CA::tree.forEachLeaf
    ([this, &copyPixels, &pixels] (const DimImg &leafId) {
       copyPixels [leafId] = double (pixels [leafId]);
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       CA::values [parentId] += double (pixels[leafId]);
     });

  const DimImgPack *areas = areaAttributes.getValues ();
  CA::tree.forEachComp
    ([this, &areas] (const DimParent &compId) {
       double &avc (CA::values [compId]);
       avc /= DimImg (areas[compId]);
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       CA::values [parentId] += avc;
     });
  TreeStats::global.addTime (meanStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
