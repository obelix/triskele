////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "obelixDebug.hpp"
#include "IImage.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "obelixSobel.hpp"
#include "obelixSobel.tcpp"

using namespace std;

// ================================================================================
template<typename PixelT, int GeoDimT>
void
obelix::computeSobel (const Raster<PixelT, GeoDimT> &inputRaster, Raster<PixelT, GeoDimT> &sobelRaster, vector<vector<double> > gTmp, const DimCore &coreCount) {
  DEF_LOG ("obelix::computeSobel", "");
  sobelRaster.setSize (inputRaster.getSize ());
  Size<GeoDimT> size = inputRaster.getSize ();
  DimImg framePixelsCount (size.getFramePixelsCount ());
  gTmp.resize (3);
  for (int i = 0; i < 3; ++i)
    gTmp[i].resize (framePixelsCount);
  double *gTmp1 (gTmp[0].data ());
  double *gTmp2 (gTmp[1].data ());
  double *gTmp3 (gTmp[2].data ());

  for (DimChannel layer = 0; ; ) {
    const PixelT *inputPixels (inputRaster.getFrame (layer));
    PixelT *sobelPixels (sobelRaster.getFrame (layer));

    // tmp1 = img*[-1 0 1]
    dealThread (size.side [1], coreCount,
		[&size, &gTmp1, &inputPixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  for (DimImg line = minVal; line < maxVal; ++line) {
		    DimImg y = line*size.side [0];
		    for (DimImg prevX = 0, x = 1; x < size.side [0]; ++prevX, ++x)
		      gTmp1 [prevX+y] = inputPixels [x+y];
		    for (DimImg prevX = 0, x = 1; x < size.side [0]; ++prevX, ++x)
		      gTmp1 [x+y] -= double (inputPixels [prevX+y]);
		  }
		});
    //             [1]
    // tmp2 = tmp1*[2]
    //             [1]
    dealThread (size.side [0], coreCount,
		[&size, &gTmp2, &gTmp1] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  DimImg pixelCount = size.getFramePixelsCount ();
		  for (DimImg x = minVal; x < maxVal; ++x) {
		    for (DimImg y = 0; y < pixelCount; y += size.side [0])
		      gTmp2 [x+y] = 2*gTmp1 [x+y];
		    for (DimImg prevY = 0, y = size.side [0]; y < pixelCount; prevY = y, y += size.side [0]) {
		      gTmp2 [x+prevY] += gTmp1 [x+y];
		      gTmp2 [x+y] += gTmp1 [x+prevY];
		    }
		  }
		});
    //            [-1]
    // tmp1 = img*[ 0]
    //            [ 1]
    dealThread (size.side [0], coreCount,
		[&size, &gTmp1, &inputPixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  DimImg pixelCount = size.getFramePixelsCount ();
		  for (DimImg x = minVal; x < maxVal; ++x) {
		    for (DimImg prevY = 0, y = size.side [0]; y < pixelCount; prevY = y, y += size.side [0])
		      gTmp1 [x+prevY] = double (inputPixels [x+y]);
		    for (DimImg prevY = 0, y = size.side [0]; y < pixelCount; prevY = y, y += size.side [0])
		      gTmp1 [x+y] -= double (inputPixels [x+prevY]);
		  }
		});
    // tmp3 = tmp1*[1 2 1]
    dealThread (size.side [1], coreCount,
		[&size, &gTmp3, &gTmp1] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  for (DimImg line = minVal; line < maxVal; ++line) {
		    DimImg y = line*size.side [0];
		    for (DimImg x = 0; x < size.side [0]; ++x)
		      gTmp3 [x+y] = 2*gTmp1 [x+y];
		    for (DimImg prevX = 0, x = 1; x < size.side [0]; ++prevX, ++x) {
		      gTmp3 [prevX+y] += gTmp1 [x+y];
		      gTmp3 [x+y] += gTmp1 [prevX+y];
		    }
		  }
		});
    // g = abs(tmp2)+abs(tmp3)/8
    dealThread (size.side [0], coreCount,
		[&size, &sobelPixels, &gTmp2, gTmp3] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  DimImg pixelCount = size.getFramePixelsCount ();
		  for (DimImg x = minVal; x < maxVal; ++x) {
		    for (DimImg y = 0; y < pixelCount; y += size.side [0])
		      sobelPixels [x+y] = PixelT ((std::abs (gTmp2 [x+y]) + std::abs (gTmp3 [x+y]))/8U);
		  }
		});
    if (GeoDimT == 2)
      return;
    // BOOST_ASSERT (GeoDimT == 2);
    // XXX 2D only
    if (layer == 0)
      cerr << "*** Warning: sobel processed only by layer!" << endl;
    if (++layer >= size.side [2])
      break;
  }
}

// ================================================================================
