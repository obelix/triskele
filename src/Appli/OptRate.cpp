////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <boost/lexical_cast.hpp>

#include "Appli/OptRate.hpp"

using namespace obelix;
using namespace std;

// ================================================================================
bool
OptRate::isPercent () const {
  return percent;
}

double
OptRate::getValue () const {
  return rate;
}

string
OptRate::getRate () const {
  return initOptRate;
}

OptRate::OptRate (const string &option)
  : initOptRate (""),
    percent (false),
    rate (0.) {
  init (option);
}

// ================================================================================
void
OptRate::init (const string &option) {
  try {
    char* pEnd;
    rate = strtod (option.c_str (), &pEnd);
    percent = *pEnd == '%';
    if (percent)
      ++pEnd;
    if (*pEnd != '\0')
      throw invalid_argument ("Bad rate");
    initOptRate = boost::lexical_cast<string> (rate);
    if (percent) {
      rate /= 100.;
      initOptRate += "%";
    }
  } catch (...) {
    throw invalid_argument ("Bad rate option: "+option);
  }
}

// ================================================================================
void
OptRate::check (const string &optionName, const bool &less100percent) const {
  if (rate < 0)
    throw invalid_argument (optionName+" must be > 0 ("+boost::lexical_cast<std::string> (rate)+")");
  if (less100percent && percent && rate > 1)
    throw invalid_argument (optionName+" must be < 100% ("+boost::lexical_cast<std::string> (rate)+")");
}

// ================================================================================
ostream &
obelix::operator << (ostream &out, const OptRate &optRate) {
  return out << optRate.getRate ();
}

istream &
obelix::operator >> (istream &in, OptRate &optRate) {
  string token;
  in >> token;
  try {
    optRate.init (token);
  } catch (...) {
    in.setstate (ios_base::failbit);
  }
  return in;
}

// ================================================================================
