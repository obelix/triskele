////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>

#include "obelixDebug.hpp"
#include "misc.hpp"
#include "obelixGeo.hpp"
#include "Border.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/triskelePriorityQueue.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "Weight.tcpp"

namespace obelix {
  template<>
  ostream &operator << (ostream &out, const vector<DimImg> &v);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunctionBase<PixelT, WeightT, GeoDimT>::reset (const PixelT *pixels, const Size<GeoDimT> &size) {
  DEF_LOG ("WeightFunctionBase::reset",  "size:" << size);
  this->pixels = pixels;
  this->size = size;
  maxPixel = std::numeric_limits<PixelT>::max ();
  // only if unsigned
  halfPixel = maxPixel / 2U;
  if (typeid (PixelT) != typeid (float))
    halfPixel++;
  LOG ("maxPixel:" << maxPixel << " halfPixel:" << halfPixel);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunctionBase<PixelT, WeightT, GeoDimT>::WeightFunctionBase (const PixelT *pixels, const Size<GeoDimT> &size)
  : pixels (nullptr),
    size () {
  reset (pixels, size);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunctionBase<PixelT, WeightT, GeoDimT>::sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isEdgeInf);
}

template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunctionBase<PixelT, WeightT, GeoDimT>::sort (TileEdge<WeightT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isTileEdgeInf);
}

template <typename PixelT, typename WeightT, int GeoDimT>
PixelT
WeightFunctionBase<PixelT, WeightT, GeoDimT>::getMedian (const GraphWalker<GeoDimT> &graphWalker) const {
  DEF_LOG ("WeightFunctionBase::getMedian",  "sizeof:" << sizeof (PixelT));
  if (sizeof (PixelT) < 3) {
    double maxVal (numeric_limits<WeightT>::max ());  // not const to avoid Warning overflow
    double minVal (numeric_limits<WeightT>::min ());  // not const to avoid Warning overflow
    const DimTile dim (maxVal - minVal + 1);
    vector<DimImg> indices (dim+1, 0);
    DimImg * const histogramBase = indices.data ()+1;
    DimImg * const histogramRelative = histogramBase-int (minVal);
    graphWalker.forEachVertexSimple ([this, &histogramRelative, &minVal] (const DimImg &idx) {
				       ++histogramRelative[int (getValue (idx))];
				     });
    partial_sum (histogramBase, histogramBase+dim, histogramBase);
    SMART_LOG_EXPR (cerr << "indices: "; for (size_t i =0; i < dim && i < 256; ++i) cerr << indices[i] << " "; cerr << endl;);

    LOG ("sum:" << indices[dim] << " rank:" << (lower_bound (indices.begin (), indices.end (), indices[dim] >> 1)-indices.begin ()) << " minVal:" << minVal);
    return PixelT (size_t (lower_bound (indices.begin (), indices.end (), indices[dim] >> 1)-indices.begin ()) - minVal-1);
  }
  DimImg pixelsCount (graphWalker.getSize ().getPixelsCount ());
  vector <PixelT> allVal (graphWalker.getSize ().getPixelsCount ());
  graphWalker.forEachVertexSimple ([this, &allVal] (const DimImg &idx) {
				     allVal[idx] = getValue (idx);
				   });
  std::sort (allVal.begin (), allVal.end ());
  LOG ("sort => " << allVal [pixelsCount/2]);
  return PixelT (allVal [pixelsCount/2]);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunctionBase<PixelT, WeightT, GeoDimT>::copyPixelsBound (PixelT *leafAPTree, const DimImg &minVal, const DimImg &maxVal) const {
  for (DimImg i = minVal; i < maxVal; ++i)
    leafAPTree[i] = pixels [i];
}

template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunctionBase<PixelT, WeightT, GeoDimT>::weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
							 const DimParent &minVal, const DimParent &maxVal) const {
  //memcpy (compAPTree+minVal, compWeights+minVal, maxVal-minVal);
  for (DimParent compIdx = minVal; compIdx < maxVal; ++compIdx)
    compAPTree[compIdx] = compWeights[compIdx];
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::WeightFunction ()
  : WB (nullptr, Size<GeoDimT> ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker)
  : WB (pixels, graphWalker.getSize ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MIN> &model, const PixelT *pixels, const Size<GeoDimT> &size)
  : WB (pixels, size) {
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::getEdges (const Rect<GeoDimT> &rect,
					       vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedTileEdges (rect, tileEdges, this);
}

template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MIN>::getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
					       vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedEdges (rect, tileShape, edges, this);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::WeightFunction ()
  : WB (nullptr, Size<GeoDimT> ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker)
  : WB (pixels, graphWalker.getSize ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MAX> &model, const PixelT *pixels, const Size<GeoDimT> &size)
  : WB (pixels, size) {
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isEdgeInf);
}

template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::sort (TileEdge<WeightT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isTileEdgeInf);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::getEdges (const Rect<GeoDimT> &rect,
					       vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedTileEdges (rect, tileEdges, this);
}

template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MAX>::getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
					       vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedEdges (rect, tileShape, edges, this);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::WeightFunction ()
  : WB (nullptr, Size<GeoDimT> ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker)
  : WB (pixels, graphWalker.getSize ()) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, ALPHA> &model, const PixelT *pixels, const Size<GeoDimT> &size)
  : WB (pixels, size) {
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::getEdges (const Rect<GeoDimT> &rect,
						vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedTileEdges (rect, tileEdges, this);
}

template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, ALPHA>::getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
						vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedEdges (rect, tileShape, edges, this);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MED>::WeightFunction ()
  : WB (nullptr, Size<GeoDimT> ()),
    median (0),
    thresholdPixel (0),
    thresholdWeight (0) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MED>::WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker)
  : WB (pixels, graphWalker.getSize ()),
    median (WB::getMedian (graphWalker)),
    thresholdPixel (median < WB::halfPixel ? median * 2U : WB::maxPixel - (WB::maxPixel-median) * 2U),
    thresholdWeight (median < WB::halfPixel ? median * 2U : (WB::maxPixel-median) * 2U) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, MED>::WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, MED> &model, const PixelT *pixels, const Size<GeoDimT> &size)
  : WB (pixels, size),
    median (model.median),
    thresholdPixel (model.thresholdPixel),
    thresholdWeight (model.thresholdWeight) {
}


// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunction<PixelT, WeightT, GeoDimT, MED>::sort (Edge<WeightT, GeoDimT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isEdgeInf);
}

template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunction<PixelT, WeightT, GeoDimT, MED>::sort (TileEdge<WeightT> *edgesP, const DimEdge &edgeCount) {
  std::sort (edgesP, edgesP+edgeCount, isTileEdgeInf);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getEdges (const Rect<GeoDimT> &rect,
						vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const {
  return graphWalker.getSortedTileEdges (rect, tileEdges, this);
}

template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, MED>::getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
						vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const {
  return  graphWalker.getSortedEdges (rect, tileShape, edges, this);
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
void
WeightFunction<PixelT, WeightT, GeoDimT, MED>::weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
						       const DimParent &minVal, const DimParent &maxVal) const {
  for (DimParent compIdx = minVal; compIdx < maxVal; ++compIdx)
    compAPTree[compIdx] = weight2value (compWeights[compIdx]);
}

// ================================================================================
template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::WeightFunction ()
  : WB (nullptr, Size<GeoDimT> ()),
    median (0),
    thresholdPixel (0) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::WeightFunction (const PixelT *pixels, const GraphWalker<GeoDimT> &graphWalker)
  : WB (pixels, graphWalker.getSize ()),
    median (WB::getMedian (graphWalker)),
    thresholdPixel (median < WB::halfPixel ? median * 2U : WB::maxPixel - (WB::maxPixel-median) * 2U) {
}

template <typename PixelT, typename WeightT, int GeoDimT>
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::WeightFunction (const WeightFunction<PixelT, WeightT, GeoDimT, TOS> &model, const PixelT *pixels, const Size<GeoDimT> &size)
  : WB (pixels, size),
    median (model.median),
    thresholdPixel (model.thresholdPixel) {
}

// ----------------------------------------
template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::getEdges (const Rect<GeoDimT> &rect,
						vector <TileEdge<WeightT> > &tileEdges, const GraphWalker<GeoDimT> &graphWalker) const {
  DEF_LOG ("ToSWeight::getEdges",  "rect:" << rect);
  //const PixelT median = WB::getMedian (graphWalker);
  LOG ("median:" << median);
  Border<GeoDimT> dejaVu (graphWalker.border, rect);

  const DimEdge maxEdgeCount (2*graphWalker.edgeMaxCount (rect.size, Volume));
  LOG ("maxEdgeCount:" << maxEdgeCount);
  tileEdges.resize (maxEdgeCount);
  DimEdge edgesCount (0);

  DimTile pixelsCount (rect.size.getPixelsCount ());
  LOG ("pixelsCount:" << pixelsCount);
  PriorityQueue<PixelT> pq (pixelsCount);
  PixelT level (median);
  for (DimTile pInf = 0; pInf < pixelsCount; ++pInf) {
    if (dejaVu.isBorder (pInf) || WB::getValue (pInf) != median)
      // XXX ilot sans median ?
      // XXX trie des index en fonction de la valeur à la médiane
      continue;
    LOG ("pInf:" << pInf);

    for (DimImg idxA = pInf; ; ) {
      if (!dejaVu.isBorder (idxA)) {
	dejaVu.setBorder (idxA);
	LOG ("idxA:" << idxA);
	BorderFlagType borderFlags (graphWalker.getBorderFlag (rect.size, idxA));
	graphWalker.forEachNeighbor
	  (idxA, borderFlags,
	   [this, &dejaVu, &level, &pq, &idxA
#ifdef ENABLE_LOG    
	    , &log
#endif
	    ] (const DimImg &idxB) {
	     if (dejaVu.isBorder (idxB))
	       return;
	     LOG ("idxB:" << idxB);
	     PixelT lower (WB::getValue (idxA));
	     PixelT upper (WB::getValue (idxB));
	     LOG ("lower:" << lower << " upper:" << upper);
	     if (upper < lower)
	       swap (lower, upper);
	     PixelT levelP (level);
	     if (levelP < lower)
	       levelP = lower;
	     else if (upper < levelP)
	       levelP = upper;
	     pq.push (levelP, idxA, idxB);
	   });
      }
      if (pq.empty ())
	break;
      DimTile idxB;
      pq.pop (level, idxB, idxA);
      tileEdges [edgesCount].indexes[0] = idxB;
      tileEdges [edgesCount].indexes[1] = idxA;
      tileEdges [edgesCount].weight = level;
      ++edgesCount;
      BOOST_ASSERT (edgesCount < maxEdgeCount);
    }
    if (dejaVu.borderCount () == pixelsCount)
      break;
    LOG ("still not process (" << (pixelsCount-dejaVu.borderCount ()));
  }
  for (DimImg x (0), y (edgesCount-1); x < y; ++x, --y)
    swapEdge (tileEdges[x], tileEdges[y]);
  
  SMART_LOG_EXPR (for (DimImg x (0); x < edgesCount; ++x) cerr << printTileEdge (tileEdges[x], WB::size, rect) << endl);

  return edgesCount;
}

template <typename PixelT, typename WeightT, int GeoDimT>
DimEdge
WeightFunction<PixelT, WeightT, GeoDimT, TOS>::getEdges (const Rect<GeoDimT> &rect, const TileShape &tileShape,
						vector <Edge<WeightT, GeoDimT> > &edges, const GraphWalker<GeoDimT> &graphWalker) const {
  BOOST_ASSERT (false);
  // XXX
  return 0;
}

// ================================================================================
