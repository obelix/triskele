#include <boost/python.hpp>

#include "test.hpp"

using namespace boost::python;

// ========================================
BOOST_PYTHON_MODULE (libtest) {
  //Py_Initialize ();

  def ("getMsgPy", getMsg);

  class_<Mem> ("MemPy", init<string>())
    .def (init<string, int>())
    .def ("getPy", &Mem::get)
    .def ("setPy", &Mem::set)
    .def_readwrite ("idPy", &Mem::id)
    .add_property ("msgPy", &Mem::get, &Mem::set)
    ;
}

// ========================================
static bool initWrapper () {
  //Py_Initialize ();
  // boost::python::numpy::initialize ();
  return true;
}
static bool isWrapperInit (initWrapper ());

// ========================================
