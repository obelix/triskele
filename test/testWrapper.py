#! /usr/bin/python3

from sys import stderr, argv
import numpy as np
from libtriskeledebug import TriskeleTreeType, TriskeleTree, Triskele

if __name__ == '__main__':
  print ("test: TriskeleTreeType")
  treeType = TriskeleTreeType.MIN
  print (treeType)

  print ("test: getTreeFromArray")
  size = [6, 4]
  pixels = [ 6, 1, 7, 7, 3, 3, 0, 3, 0, 0, 6, 4, 3, 3, 4, 6, 7, 4, 5, 7, 6, 6, 1, 5]
  print (pixels)
  tree = Triskele.getTreeFromArray (treeType, size, pixels);
  count = tree.getNodeCount ()
  parents = tree.getLeafParents ()
  print (count)
  print (parents)
  for i in range (count-1):
    print (parents [i], end=", ")
  print (parents [count-1])

  print ("test: getTreeFromImage")
  fileName = argv[1]
  tree2 = Triskele.getTreeFromImage (treeType, fileName);
  count2 = tree2.getNodeCount ()
  parents2 = tree2.getLeafParents ()
  print (count2)
  print (parents2)
  for i in range (count2-1):
    print (parents2 [i], end=", ")
  print (parents2 [count2-1])
