////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_TreeStats_hpp
#define _Obelix_Triskele_TreeStats_hpp

#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

#include "ArrayTree/triskeleArrayTreeBase.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;
    namespace ba = boost::accumulators;
    typedef ba::accumulator_set<DimImg, ba::stats<ba::tag::count,
						  ba::tag::mean,
						  ba::tag::min,
						  ba::tag::max> > TreeStatsDim;
    typedef ba::accumulator_set<double, ba::stats<ba::tag::sum,
						  ba::tag::count,
						  ba::tag::mean,
						  ba::tag::min,
						  ba::tag::max> > TreeStatsDouble;

    // ================================================================================
    enum TimeType
      {
       borderStats,
       ndviStats,
       pantexStats,
       sobelStats,
       iiStats,
       haarStats,
       statStats,

       buildTreeStats,
       buildSetupStats,
       buildToTilesStats,
       buildEdgesStats,
       buildTileParentsStats,
       buildImgParentsStats,
       buildFromTilesStats,
       buildMergeStats,
       buildForestStats,
       buildIndexStats,
       buildCompressStats,
       buildChildrenStats,

       setAPStats,

       areaStats,
       perimeterStats,
       zLengthStats,
       stsStats,
       compactnessStats,
       complexityStats,
       simplicityStats,
       rectangularityStats,

       minStats,
       maxStats,
       meanStats,

       bbStats,
       centroidStats,
       sdStats,
       sdwStats,
       // sdaStats,
       moiStats,

       filteringStats,
       copyImageStats,
       readStats,
       writeStats,
       hdf5Read,
       hdf5Write,
       allStats,

       learningStats,
       cleanStats,
       checkAccuracyStats,
       predictionStats,

       TimeTypeCard
      };

    // ================================================================================
    class TreeStats {
    public :

      static TreeStats global;
      static const string timeTypeLabels[];

      TreeStats ();

      void reset ();
      inline void addDim (const TreeType &treeType, const DimImg &leafCount, const DimParent &compCount);
      inline void addTime (const TimeType &timeType, const double &duration);
      inline const TreeStatsDouble &getTimeStats (const TimeType &timeType);

      // nice ostream
      struct CPrintDim {
	const TreeStats &treeStats;
	CPrintDim (const TreeStats &treeStats);
	ostream &print (ostream &out) const;
	ostream &print (ostream &out, const vector<TreeStatsDim> stats, const string &msg) const;
      };
      CPrintDim printDim () const;
      friend ostream &operator << (ostream &out, const CPrintDim &cpd) { return cpd.print (out); }

      struct CPrintTime {
	const TreeStats &treeStats;
	CPrintTime (const TreeStats &treeStats);
	ostream &print (ostream &out) const;
      };
      CPrintTime printTime () const;
      friend ostream &operator << (ostream &out, const CPrintTime &cpt) { return cpt.print (out); }

    private :

      vector<TreeStatsDim> leavesStats, compStats;
      vector<TreeStatsDouble> timeStats;
    };

    // ================================================================================
#include "TreeStats.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_TreeStats_hpp

