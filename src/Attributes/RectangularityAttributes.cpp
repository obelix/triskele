////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/RectangularityAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "RectangularityAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
RectangularityAttributes<GeoDimT>::RectangularityAttributes (const Tree<GeoDimT> &tree, const AreaAttributes<GeoDimT> &areaAttributes, const BoundingBoxAttributes<GeoDimT> &boundingBoxAttributes)
  : CompAttribute<double, GeoDimT> (tree) {
  DEF_LOG ("RectangularityAttributes::RectangularityAttributes", "");
  compute (areaAttributes, boundingBoxAttributes);
}

template<int GeoDimT>
RectangularityAttributes<GeoDimT>::~RectangularityAttributes () {
  DEF_LOG ("RectangularityAttributes::~RectangularityAttributes", "");
}

// ================================================================================
template<int GeoDimT>
void
RectangularityAttributes<GeoDimT>::compute (const AreaAttributes<GeoDimT> &areaAttributes, const BoundingBoxAttributes<GeoDimT> &boundingBoxAttributes) {
  DEF_LOG ("RectangularityAttributes::compute", "");
  auto start = high_resolution_clock::now ();

  const DimImgPack *areas = areaAttributes.getValues ();
  const BoundingBox<GeoDimT> *boundingBoxes = boundingBoxAttributes.getValues ();

  CA::tree.forEachComp
    ([this, &areas, &boundingBoxes] (const DimParent &compId) {
       const BoundingBox<GeoDimT> &bb (boundingBoxes[compId]);
       double bbArea (bb.max [0]-bb.min [0]+1);
       for (int i = 1; i < GeoDimT; ++i)
	 bbArea *= (bb.max [i]-bb.min [i]+1);
       CA::values [compId] = double (areas[compId])/bbArea;
     });

  TreeStats::global.addTime (rectangularityStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
