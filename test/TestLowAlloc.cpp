////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <algorithm>		// random_shuffle

#include "obelixDebug.hpp"
#include "obelixLowAlloc.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp>

using namespace std;
using namespace obelix;

static bool show (false); // log or not

// ================================================================================
void
testLowAlloc (const DimImg &maxSize, const DimImg &stepSize) {
  if (show)
    cout << "maxSize:" << maxSize  << " stepSize: " << stepSize << endl;

  LowAlloc<int> heap (stepSize);
  vector<DimImg> used;
  double sum = (0);
  for (DimImg i = 0; i < maxSize; ++i) {
    DimImg idx (heap.get ());
    used.push_back (idx);
    heap [idx] = i;
  }
  for (DimImg idx : used) {
    sum += heap [idx];
    heap.give (idx);
  }

  if (show)
    cout << "capacity:" << heap.getCapacity () << " used:" << heap.getMaxUsed ()  << " available: " << heap.getAvailable ()
	 << " sum:" << sum << endl;

  REQUIRE (heap.getCapacity () == maxSize);
  REQUIRE (heap.getMaxUsed () == maxSize);
  REQUIRE (heap.getAvailable () == maxSize);
  REQUIRE (sum == (maxSize-1) * maxSize / 2 );
}

// ================================================================================
void
test2LowAlloc (const DimImg &maxSize, const DimImg &stepSize) {
  if (show)
    cout << "maxSize:" << maxSize  << " stepSize: " << stepSize << endl;

  LowAlloc<int> heap (stepSize);
  vector<DimImg> used;
  double sum = (0);
  for (DimImg i = 0; i < maxSize; ++i) {
    if (! (i % (stepSize/2))) {
      for (DimImg idx : used) {
	sum += heap [idx];
	heap.give (idx);
      }
      used.clear ();
    }
    DimImg idx (heap.get ());
    used.push_back (idx);
    heap [idx] = i;
  }
  if (show)
    cout << "capacity:" << heap.getCapacity () << " used:" << heap.getMaxUsed ()  << " available: " << heap.getAvailable ()
	 << " sum:" << sum << endl;

  REQUIRE (heap.getAvailable () == 0);
  for (DimImg idx : used) {
    sum += heap [idx];
    heap.give (idx);
  }
  REQUIRE (heap.getCapacity () == stepSize);
  REQUIRE (heap.getMaxUsed () == stepSize/2);
  REQUIRE (sum == (maxSize-1) * maxSize / 2 );
}

// ================================================================================
void
test3LowAlloc (const DimImg &maxSize, const DimImg &stepSize) {
  if (show)
    cout << "maxSize:" << maxSize  << " stepSize: " << stepSize << endl;

  LowAlloc<int> heap (stepSize);
  vector<DimImg> used;
  double sum = (0);
  for (DimImg i = 0; i < maxSize; ++i) {
    if (! (i % (stepSize))) {
      random_shuffle (used.begin (), used.end ());
      for (; used.size () > stepSize/2; ) {
	DimImg idx = used.back ();
	used.pop_back ();
	sum += heap [idx];
	heap.give (idx);
      }
    }
    DimImg idx (heap.get ());
    used.push_back (idx);
    heap [idx] = i;
  }
  if (show)
    cout << "capacity:" << heap.getCapacity () << " used:" << heap.getMaxUsed ()  << " available: " << heap.getAvailable ()
	 << " sum:" << sum << endl;

  REQUIRE (heap.getAvailable () == 0);
  for (DimImg idx : used) {
    sum += heap [idx];
    heap.give (idx);
  }
  REQUIRE (heap.getCapacity () == stepSize*2);
  REQUIRE (heap.getMaxUsed () == stepSize*3/2);
  REQUIRE (sum == (maxSize-1) * maxSize / 2 );
}


// ================================================================================
TEST_CASE ("Testing LowAlloc...") {
  testLowAlloc (1024, 256);
  testLowAlloc (100, 10);

  test2LowAlloc (1024, 256);
  test2LowAlloc (100, 10);

  test3LowAlloc (1024, 256);
  test3LowAlloc (100, 10);

  if (show)
    cout << endl;
}

// ================================================================================

