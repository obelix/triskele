////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>    // std::transform, std::lower_bound, std::upper_bound, std::sort, std::min_element
#include <numeric>      // std::partial_sum
#include <typeinfo>

#include "obelixDebug.hpp"
#include "IImage.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "obelixPantex.hpp"
#include "obelixPantex.tcpp"

using namespace std;

// ================================================================================
static const DimPantexGS PantexMaxGray = 127;
static const DimPantexGS PantexGrayCard = 127+1;

static const int shiftTabSise (10);
static const int shiftTabDx [] = { 1,  1,  2, 1, 2, 0, 1, 2, 0, 1};
static const int shiftTabDy [] = {-2, -1, -1, 0, 0, 1, 1, 1, 2, 2};

static DimPantexSum deltaSquare [PantexGrayCard][PantexGrayCard];

static bool staticInit () {
  for (int i = 0; i < 128; ++i)
    for (int j = 0; j < 128; ++j)
      deltaSquare [i][j] = DimPantexSum ((i-j)*(i-j));
  return true;
}

static const bool init = staticInit ();

// ================================================================================
template<typename PixelT, int GeoDimT>
void
obelix::computePantex (const Raster<PixelT, GeoDimT> &inputRaster, Raster<PixelT, GeoDimT> &pantexRaster, const DimCore &coreCount, const DimPantexOcc &pantexWindowSide) {
  DEF_LOG ("computePantex",  "size:" << inputRaster.getSize ());

  const Size<GeoDimT> &imgSize (inputRaster.getSize ());
  pantexRaster.setSize (imgSize);

  for (DimChannel layer = 0; ; ) {

    const float lbPercent = .15;
    const float ubPercent = .85;
    PixelT lb, ub;
    pantexLbUb (inputRaster, layer, lbPercent, ubPercent, lb, ub);

    Raster<DimPantexGS, 2> inputGSRaster;
    pantexRescale (inputRaster, layer, lb, ub, inputGSRaster);

    Raster<DimPantexIdx, 2> pantexShortRaster;
    computePantexShort (inputGSRaster, pantexShortRaster, coreCount, pantexWindowSide);

    const DimImg pixelsCount = imgSize.getFramePixelsCount ();
    PixelT *pantexPixels = pantexRaster.getFrame (layer);
    const DimPantexIdx *pantexShortPixels = pantexShortRaster.getPixels ();
    if (sizeof (PixelT) < sizeof (DimPantexIdx))
      for (DimImg i = 0; i < pixelsCount; ++i)
	pantexPixels[i] = PixelT (sqrt (pantexShortPixels[i]));
    else
      for (DimImg i = 0; i < pixelsCount; ++i)
	pantexPixels[i] = PixelT (pantexShortPixels[i]);
  
    if (GeoDimT == 2)
      return;
    // BOOST_ASSERT (GeoDimT == 2);
    // XXX 2D only
    if (layer == 0)
      cerr << "*** Warning: Pantex processed only by layer!" << endl;
    if (++layer >= imgSize.side [2])
      break;
  }

}

// ================================================================================
template<typename PixelT, int GeoDimT>
void
obelix::pantexLbUb (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer,
		    const float &lbPercent, const float &ubPercent,
		    PixelT &lb, PixelT &ub) {
  DEF_LOG ("pantexLbUb",  "size:" << inputRaster.getSize () <<
	   " lbPercent:" << lbPercent << " ubPercent:" << ubPercent);
  if (sizeof (PixelT) > 2) {
    pantexQLbUb (inputRaster, layer, lbPercent, ubPercent, lb, ub);
    return;
  }
  double maxVal (numeric_limits<PixelT>::max ());  // not const to avoid Warning overflow
  double minVal (numeric_limits<PixelT>::min ());  // not const to avoid Warning overflow
  const DimImg dim (maxVal - minVal + 1);
  vector<DimImg> indices (dim+1, 0);
  DimImg * const histogramBase = indices.data ()+1;
  DimImg * const histogramRelative = histogramBase-int (minVal);

  const Size<GeoDimT> &imgSize (inputRaster.getSize ());
  const PixelT *inputPixels = inputRaster.getFrame (layer);
  const DimImg framePixelsCount = imgSize.getFramePixelsCount ();

  for (DimImg i = 0; i < framePixelsCount; ++i)
    ++histogramRelative[size_t (int (inputPixels [i]))];
  partial_sum (histogramBase, histogramBase+dim, histogramBase);

  LOG (" dim:" << dim << " pixelsCount:" << framePixelsCount);
  auto floor = lower_bound (indices.begin ()+1, indices.end(), DimImg (framePixelsCount*lbPercent));
  auto ceil = upper_bound (indices.begin ()+1, indices.end(), DimImg (framePixelsCount*ubPercent));

  LOG (" floor:" << floor-indices.begin () << " ceil:" << ceil-indices.begin ());

  lb = PixelT (floor-indices.begin () + int (minVal));
  ub = PixelT (ceil-indices.begin () + int (minVal));

  LOG (" lb:" << lb << " ub:" << ub);
}

template<typename PixelT, int GeoDimT>
void
obelix::pantexQLbUb (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer,
		     const float &lbPercent, const float &ubPercent,
		     PixelT &lb, PixelT &ub) {
  const DimImg framePixelsCount = inputRaster.getSize ().getFramePixelsCount ();
  vector<PixelT> tmp (inputRaster.getFrame (layer),
		      inputRaster.getFrame (layer)+framePixelsCount);
  sort (tmp.begin (), tmp.end ());
  const DimImg lastPixels = framePixelsCount ? framePixelsCount-1 : 0;
  lb = tmp [DimImg (lastPixels*lbPercent)];
  ub = tmp [DimImg (lastPixels*ubPercent)];
}

// ================================================================================
template<typename PixelT, int GeoDimT>
void
obelix::pantexRescale (const Raster<PixelT, GeoDimT> &inputRaster, const DimChannel &layer, const PixelT &lb, const PixelT &ub,
		       Raster<DimPantexGS, 2> &rescaledRaster) {
  DEF_LOG ("pantexRescale",  "size:" << inputRaster.getSize ());
  const Size<GeoDimT> &imgSize (inputRaster.getSize ());
  const Size<2> &frameSize (imgSize.side);
  
  const DimImg pixelsCount = frameSize.getPixelsCount ();
  rescaledRaster.setSize (frameSize);

  const PixelT *inputPixels = inputRaster.getFrame (layer);
  DimPantexGS *rescaledPixels = rescaledRaster.getPixels ();
  double scale = PantexMaxGray / (double (ub)-double (lb));
  for (DimImg i = 0; i < pixelsCount; ++i) {
    const PixelT val = inputPixels [i];
    if (val < lb) {
      rescaledPixels [i] = 0;
      continue;
    }
    if (val > ub) {
      rescaledPixels [i] = PantexMaxGray;
      continue;
    }
    rescaledPixels [i] = DimPantexGS ((val-lb)*scale);
  }
}

// ================================================================================
DimPantexIdx
obelix::pantexOccMat (const Raster<DimPantexGS, 2> &inputByteRaster,
		      const DimSideImg &ox, const DimSideImg &oy,
		      const DimPantexSide &pWidth, const DimPantexSide &pHeight,
		      const DimPantexSide &dx, const DimPantexSide &dy,
		      DimPantexSum &gOcc,
		      const bool &removeOldY, const bool &addNewY) {
  // DEF_LOG ("pantexOccMatInit",  "ox:" << ox << " oy:" << oy << " dx:" << int (dx) << " dy:" << int (dy));
  const DimSideImg startPX = ox;
  const DimSideImg startPY = oy+(dy < 0 ? DimSideImg (-dy) : 0);
  const DimSideImg startQX = ox+dx;
  const DimSideImg startQY = oy+(dy < 0 ? 0 : DimSideImg (dy));

  const DimSideImg stopPX = ox+DimSideImg (pWidth)-DimSideImg (dx);
  const DimSideImg stopPY = oy+DimSideImg (pHeight)-(dy < 0 ? 0 : DimSideImg (dy));

  if (removeOldY || addNewY) {
    if (removeOldY) {
      const DimSideImg pry (startPY-1), qry (startQY-1);
      for (DimSideImg px = startPX, qx = startQX; px < stopPX; ++px, ++qx)
	gOcc -= deltaSquare [inputByteRaster.getValue (Point<2> (px, pry))] [inputByteRaster.getValue (Point<2> (qx, qry))];
    }
    if (addNewY) {
      // const DimSideImg stopQX = ox+pWidth;
      const DimSideImg stopQY = oy+pHeight-(dy < 0 ? DimSideImg (-dy) : 0);
      const DimSideImg pay (stopPY-1), qay (stopQY-1);
      for (DimSideImg px = startPX, qx = startQX; px < stopPX; ++px, ++qx)
	gOcc += deltaSquare [inputByteRaster.getValue (Point<2> (px, pay))] [inputByteRaster.getValue (Point<2> (qx, qay))];
    }
  } else {
    // cerr << ".";
    // LOG ("startPY:" << startPY << " startQY:" << startQY << " stopPY:" << stopPY << " stopPX:" << stopPX);
    gOcc = 0;
    for (DimSideImg py = startPY, qy = startQY; py < stopPY; ++py, ++qy)
      for (DimSideImg px = startPX, qx = startQX; px < stopPX; ++px, ++qx) {
	// int x = inputByteRaster.getValue (Point<2> (px, py));
	// int y = inputByteRaster.getValue (Point<2> (qx, qy));
	// LOG ("x:" << x << " y:" << y);
	gOcc += deltaSquare [inputByteRaster.getValue (Point<2> (px, py))] [inputByteRaster.getValue (Point<2> (qx, qy))];
      }
  }

  // if (pWidth <= abs (dx) || pHeight <= abs (dy)) // if pantexWindowSide < 7
  //     return 0;

  // // LOG ("contrast");
  return gOcc/(DimPantexSum (pWidth - abs (dx)) * DimPantexSum (pHeight - abs (dy)));
}

// ================================================================================
// iWidth, iHeight : image size
// pWidth, pHeight : sliding window size
// ox,oy : origin sliding window
// cx, cy : center sliding window
void
obelix::computePantexShort (const Raster<DimPantexGS, 2> &inputRaster, Raster<DimPantexIdx, 2> &pantexShortRaster, const DimCore &coreCount, const DimPantexOcc &pantexWindowSide) {
  DEF_LOG ("computePantexShort",  "size:" << inputRaster.getSize () << " pantexWindowSide:" << pantexWindowSide);
  const Size<2> &imgSize (inputRaster.getSize ());
  pantexShortRaster.setSize (imgSize);

    dealThread (imgSize.side [0], coreCount,
		[&inputRaster, &pantexShortRaster, &imgSize, &pantexWindowSide] (const DimCore &threadId, const DimSideImg &minVal, const DimSideImg &maxVal) {
		  const DimSideImg iWidth = imgSize.side [0];
		  const DimSideImg iHeight = imgSize.side [1];
		  const DimSideImg pantexWindowHalfSide ((pantexWindowSide-1)/2);

		  for (DimSideImg cx = minVal; cx < maxVal; ++cx) {
		    const DimSideImg ox (cx < pantexWindowHalfSide ? 0 : cx - pantexWindowHalfSide);
		    const DimPantexSide pWidth (cx < pantexWindowHalfSide ? cx+pantexWindowHalfSide+1 :
						cx < iWidth-pantexWindowHalfSide ? pantexWindowSide : (iWidth-cx+pantexWindowHalfSide));
		    // cerr << "cx:" << cx << " ox:" << ox << " pWidth:" << int (pWidth) << endl;
		    // LOG ("cx:" << cx << " ox:" << ox << " pWidth:" << int (pWidth));
		    vector<DimPantexSum> gTabs (shiftTabSise, 0);
		    vector <DimPantexIdx> contrasts (shiftTabSise, 0);
		    for (DimSideImg cy = 0; cy < iHeight; ++cy) {
		      // LOG ("cy:" << cy);
		      const DimSideImg oy (cy < pantexWindowHalfSide ? 0 : cy - pantexWindowHalfSide);
		      const DimPantexSide pHeight (cy < pantexWindowHalfSide ? cy+pantexWindowHalfSide+1 :
						   cy < iHeight-pantexWindowHalfSide ? pantexWindowSide : (iHeight-cy+pantexWindowHalfSide));
		      const bool removeOldY (cy > pantexWindowHalfSide);
		      const bool addNewY (cy != 0 && cy < iHeight-pantexWindowSide);
		      for (int gIdx = 0; gIdx < shiftTabSise; ++gIdx)
			contrasts [gIdx] = pantexOccMat (inputRaster, ox, oy,
							 pWidth, pHeight,
							 shiftTabDx [gIdx], shiftTabDy [gIdx], gTabs [gIdx],
							 removeOldY, addNewY);
		      pantexShortRaster.getValue (Point<2> (cx, cy)) = *min_element (contrasts.begin (), contrasts.end ());
		    }
		}
		});
}

// ================================================================================
