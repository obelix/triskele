////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Centroid_hpp
#define _Obelix_Triskele_Centroid_hpp

#include "obelixGeo.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    template<int GeoDimT>
    struct AverageCoord {
      double coord [GeoDimT];
      inline AverageCoord () { fill (begin (coord), begin (coord)+GeoDimT, 0); }
      inline AverageCoord (const Point<GeoDimT> &p) { for (int i = 0; i < GeoDimT; ++i) coord [i] = p.coord [i]; }
      // only by CPrintMap<T, GeoDimT>::print (ostream &out, const T &val)
      operator DimParent () const {
      	DimParent result (coord [0]);
      	for (int i = 1; i < GeoDimT; ++i)
	  if (result < coord [i])
	    result = coord [i];
      	return result;
      }
    };

    template<int GeoDimT>
    inline ostream &
    operator << (ostream &out, const AverageCoord<GeoDimT> &averageCoord) {
      out << "(" << averageCoord.coord [0];
      for (int i = 1; i < GeoDimT; ++i)
	out << ", " << averageCoord.coord [i];
      return out << ")";
    }
    //template ostream &operator << (ostream &out, const AverageCoord<GeoDimT> &averageCoord);

    // ================================================================================
    template<int GeoDimT>
    class Centroid : public CompAttribute<AverageCoord<GeoDimT>, GeoDimT> {
    public:
      typedef CompAttribute<AverageCoord<GeoDimT>, GeoDimT> CA;

      Centroid (const Tree<GeoDimT> &tree, const AreaAttributes<GeoDimT> &areaAttributes);
      virtual ~Centroid ();

      virtual inline ostream &print (ostream &out) const { CA::print (out, "centroid"); return out; }
      virtual inline const AverageCoord<GeoDimT> getPixelValue (const DimImg &leafId) const {
	return AverageCoord<GeoDimT> (Point<GeoDimT> (CA::tree.getSize (), leafId));
      }
    protected:
      void compute (const AreaAttributes<GeoDimT> &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Centroid_hpp
