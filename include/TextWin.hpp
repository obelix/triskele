////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_TextWin_hpp
#define _Obelix_TextWin_hpp

/**
 * \file TextWin.hpp
 * \brief Apply a pattern on a sliding window to compute a "texture" attribute
 * 				(like Haar feature or statistics feature)
 * \author Francois Merciol, comments by Jerome More
 */

#include "obelixGeo.hpp"

namespace obelix {
  using namespace triskele;

  // ================================================================================
  template<int GeoDimT>
  class TextWin {
  private:
    /*! dimensions of the sliding window
     * _ w  = width of the sliding window
     * _ h  = height of the sliding window
     * _ hw = half width of the sliding window
     * _ hh = half height of the sliding window
     */
    const DimSideImg w, h, hw, hh;
    /*! floors and ceils without adaptation
     * _ xFloor = x floor
     * _ xCeil  = x ceil
     * _ yFloor = y floor 
     * _ yCeil  = y ceil
     */
    const DimSideImg xFloor, xCeil, yFloor, yCeil;
    /*! image size */
    const Size<GeoDimT> size;
    /*! 1D offsets 
     * _ iw  = width of the sliding window
     * _ ih  = width of the sliding window
     * _ ihw = half width of the sliding window
     * _ ihh = half height of the sliding window
     * _ ilt = shift from center to the top-left corner
     */
    const DimImg iw, ih, ihw, ihh, ilt;

    /*! total number of points in the sliding window */
    const double sCount;

    // ========================================
  public:
    /**
     * @brief Constructor
     * @param[in]	size		size of the image
     * @param[in]	sSize		size of the sliding window
     */
    inline TextWin (const Size<GeoDimT> &size, const Size<2> &sSize);

    // ========================================
    inline void resizeSlidingWindow (const DimSideImg &x, const DimSideImg &y, DimSideImg &w2, DimSideImg &h2, DimSideImg &dx, DimSideImg &dy) const;

    /**
     * @brief Compute horizontal Haar feature on the window (using integral image)
     * @param[in]	iiValues		integral image
     * @param[in]	x			x coordinate of the center of the window
     * @param[in]	y			y coordinate of the center of the window
     * @param[in]	o			1D index of the center of the window: o = x + y*width
     * @param[out]	hx			value of horizontal Haar feature on the window
     */
    inline double getHx (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &o) const;

    // ========================================
    /**
     * @brief Compute vertical Haar feature on the window (using integral image)
     * @param[in]	iiValues		integral image
     * @param[in]	x			x coordinate of the center of the window
     * @param[in]	y			y coordinate of the center of the window
     * @param[in]	o			1D index of the center of the window: o = x + y*width
     * @param[out]	hx			value of vertical Haar feature on the window
     */
    inline double getHy (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &o) const;

    // ========================================
    /**
     * @brief Compute statistical features on the window (using integral image)
     * @param[out]	mean			mean value on the window
     * @param[out]	std			standard-deviation on the window
     * @param[out]	ent			entropy on the window
     * @param[in]	iiValues		integral image
     * @param[in]	doubleiiValues		square integral image
     * @param[in]	x			x coordinate of the center of the window
     * @param[in]	y			y coordinate of the center of the window
     * @param[in]	o			1D index of the center of the window: o = x + y*width
     */
    void getMeanStdEnt (double &mean, double &std, double &ent,
			const double* iiValues, const double* doubleiiValues,
			const DimSideImg &x, const DimSideImg &y, const DimImg &o) const;

    // ================================================================================
  };

#include "TextWin.tpp"
} // obelix

#endif // _Obelix_TextWin_hpp
