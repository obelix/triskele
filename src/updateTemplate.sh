#!/bin/bash

############################################################################
## Copyright IRISA 2017							  ##
## 									  ##
## triskele.obelix (at) irisa.fr					  ##
## 									  ##
## This  software  is  a  computer  program whose  purpose  is  to  build ##
## hierarchical representation for remote sensing immages.		  ##
## 									  ##
## This software is governed by the CeCILL-B license under French law and ##
## abiding by  the rules of distribution  of free software. You  can use, ##
## modify  and/or  redistribute  the  software under  the  terms  of  the ##
## CeCILL-B license as circulated by CEA, CNRS and INRIA at the following ##
## URL "http:##www.cecill.info".					  ##
## 									  ##
## As a counterpart to the access to  the source code and rights to copy, ##
## modify and  redistribute granted  by the  license, users  are provided ##
## only with a limited warranty and  the software's author, the holder of ##
## the economic  rights, and the  successive licensors have  only limited ##
## liability.								  ##
## 									  ##
## In this respect, the user's attention is drawn to the risks associated ##
## with loading,  using, modifying  and/or developing or  reproducing the ##
## software by the user in light of its specific status of free software, ##
## that may  mean that  it is  complicated to  manipulate, and  that also ##
## therefore means  that it  is reserved  for developers  and experienced ##
## professionals having in-depth computer  knowledge. Users are therefore ##
## encouraged  to load  and test  the software's  suitability as  regards ##
## their  requirements  in  conditions  enabling the  security  of  their ##
## systems and/or  data to  be ensured  and, more  generally, to  use and ##
## operate it in the same conditions as regards security.		  ##
## 									  ##
## The fact that  you are presently reading this means  that you have had ##
## knowledge of the CeCILL-B license and that you accept its terms.	  ##
############################################################################

cd `dirname $0`

mkdir -p `dirname $2`

UseUint40=""
case "$*" in
    *-DIMAGE_40BITS* | *-DIMAGE_LARGE* )
	UseUint40="uint40_t"
esac

trap "rm -f $2.$$" 0 1 2 3 15

pixelsTypes="uint8_t uint16_t int16_t uint32_t int32_t float double"
scaleAttrTypes="${pixelsTypes} uint64_t ${UseUint40}"
nonScaleAttrTypes="${pixelsTypes} uint64_t ${UseUint40} AverageCoord<GeoDimT> BoundingBox<GeoDimT>"
extraFeatureTypes="double uint32_t ${UseUint40} uint64_t"
treeTypes="MIN MAX MED TOS ALPHA"

(echo "// generated from $1"
 while read line
 do
     # ignore comment
     if test -z "${line}"
     then
	 continue
     fi
     case "${line}" in \#*|//*) continue ;; esac

     # DimImgT class case
     if echo "${line}" | grep -q "DimImgT"
     then
	 # DimSideImg DimImg size_t
	 sizeTypes="uint32_t uint64_t"
	 for size in ${sizeTypes}
	 do
	     echo "${line}" | sed "s/DimImgT/${size}/g"
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     # FeatureT class case
     if echo "${line}" | grep -q "FeatureT"
     then
	 for feature in ${scaleAttrTypes}
	 do
	     echo "${line}" | sed "s/FeatureT/${feature}/g;s/PixelT/${feature}/g"
	     if echo "${line}" | grep -q "PixelT"
	     then
		 for extraFeature in ${extraFeatureTypes}
		 do
		     if test "${feature}" != "${extraFeature}"
		     then
			 echo "${line}" | sed "s/FeatureT/${extraFeature}/g;s/PixelT/${feature}/g"
		     fi
		 done
	     fi
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     # AttrT class case
     if echo "${line}" | grep -q "AttrT"
     then
	 attrTypes=${scaleAttrTypes}
	 if ! echo "${line}" | grep -q "OnlyAScaleType"
	 then
	     attrTypes=${nonScaleAttrTypes}
	 fi

	 for attr in ${attrTypes}
	 do
	     echo "${line}" | sed "s/AttrT/${attr}/g"
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     # WeightT class case
     if echo "${line}" | grep -q "WeightT"
     then
	 # PixelT DimImg double
	 for weight in ${scaleAttrTypes}
	 do
	     echo "${line}" | sed "s/WeightT/${weight}/g;"
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     if echo "${line}" | grep -q "PixelT"
     then
	 # PixelT double
	 for pixel in ${scaleAttrTypes}
	 do
	     echo "${line}" | sed "s/PixelT/${pixel}/g"
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     if echo "${line}" | grep -q "PixelGT"
     then
	 for gt in ${pixelsTypes}
	 do
	     echo "${line}" | sed "s/PixelGT/${gt}/g"
	 done
	 echo
     else
	 echo "${line}"
     fi
 done | while read line
 do
     if echo "${line}" | grep -q "GeoDimT"
     then
	 # GeoDimT class case
	 for geoDim in 2 3
	 do
	     echo "${line}" | sed "s/GeoDimT/${geoDim}/g"
	 done
     else
	 echo "${line}"
     fi
 done | while read line
 do
     if echo "${line}" | grep -q "TreeTypeT"
     then
	 # TreeType class case
	 for treeType in ${treeTypes}
	 do
	     echo "${line}" | sed "s/TreeTypeT/${treeType}/g"
	 done
     else
	 echo "${line}"
     fi
 done
)  < "$1" > "$2.$$"

if ! cmp -s -- "$2.$$" "$2" ; then
    echo
    echo "*** mv $2.$$ $2  ***"
    echo
    cp "$2.$$" "$2"
fi
