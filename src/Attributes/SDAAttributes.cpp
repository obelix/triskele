////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>    // std::fill
#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/SDAAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "SDAAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
SDAAttributes<GeoDimT>::SDAAttributes (const Tree<GeoDimT> &tree, const AreaAttributes<GeoDimT> &areaAttributes)
  : CompAttribute<double, GeoDimT> (tree) {
  DEF_LOG ("SDAAttributes::SDAAttributes", "");
  compute (areaAttributes);
}

template<int GeoDimT>
SDAAttributes<GeoDimT>::~SDAAttributes () {
  DEF_LOG ("SDAAttributes::~SDAAttributes", "");
}

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
void
SDAAttributes<GeoDimT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
			 const vector<double> &thresholds, const PruningStrategy &pruningStrategy) const {
  DEF_LOG ("SDAAttributes::cut", "thresholds:" << thresholds.size ());
  if (thresholds.empty ())
    return;
  double maxValue = CA::getMaxValue ();
  cerr << "sda max value:" << maxValue << endl;
  CA::cut (allBands, attributeProfiles, CA::getScaledThresholds (thresholds, maxValue), pruningStrategy);
}

// ================================================================================
template<int GeoDimT>
void
SDAAttributes<GeoDimT>::compute (const AreaAttributes<GeoDimT> &areaAttributes) {
  DEF_LOG ("SDAAttributes::compute", "");
  // auto start = high_resolution_clock::now ();
  const DimImgPack *areas = areaAttributes.getValues ();

  fill (CA::values.begin(), CA::values.end (), 0.);

  CA::tree.forEachLeaf
    ([this, &areas] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       CA::values [parentId] += 1;
     });

  CA::tree.forEachComp
    ([this, &areas] (const DimParent &compId) {
       double &sdc (CA::values [compId]);
       sdc /= DimImg (areas[compId]);
       double impact = areas[compId];
       impact *= impact;
       sdc -= impact;
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId != DimParent_MAX)
	 CA::values [parentId] += DimImg (areas[compId]) * impact;
       // XXX SD*SD speed-up computation time
       // sdc = sqrt (sdc);
     });

  // TreeStats::global.addTime (sdaStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
