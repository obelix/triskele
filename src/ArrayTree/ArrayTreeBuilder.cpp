////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>

using namespace boost::chrono;

#include "misc.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"


using namespace obelix;
using namespace obelix::triskele;
#include "ArrayTreeBuilder.tcpp"

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeBuilder (const Raster<PixelT, GeoDimT> &raster, const GraphWalker<GeoDimT> &graphWalker,
							  const TreeType &treeType,
							  const bool &autoThreadFlag,
							  const bool &needChildren)
  : ArrayTreeBuilder (raster, graphWalker, treeType, 0, 0, false, autoThreadFlag, needChildren) {
}
template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::ArrayTreeBuilder (const Raster<PixelT, GeoDimT> &raster, const GraphWalker<GeoDimT> &graphWalker,
							  const TreeType &treeType, const DimCore &threadPerImage, const DimCore &tilePerThread,
							  const bool &seqTile, const bool &autoThreadFlag,
							  const bool &needChildren)
  : TreeBuilder<GeoDimT> (),
    coreCount (boost::thread::hardware_concurrency ()),
    threadPerImage (threadPerImage),
    tilePerThread (tilePerThread),
    seqTile (seqTile),
    autoThreadFlag (autoThreadFlag),
    raster (raster),
    graphWalker (graphWalker),
    treeType (treeType),
    needChildren (needChildren),
    childCount (nullptr),
    pCompWeights (nullptr),
    compWeights (nullptr),
    parentCapacity (0) {
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeBuilder",  "");
}

template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::~ArrayTreeBuilder () {
  DEF_LOG ("ArrayTreeBuilder::~ArrayTreeBuilder",  "");
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::unlinkParent (const DimParent &par) {
  SMART_DEF_LOG ("ArrayTreeBuilder::unlinkParent", "par: " << printComp (par));
  if (par == DimParent_MAX)
    return;
  BOOST_ASSERT (DimImg (childCount [par]));
  --childCount [DimParent (par)];
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::updateNewId (DimParentPack *newCompId, const DimParent curComp, DimParent &compCount) {
  SMART_DEF_LOG ("ArrayTreeBuilder::updateNewId", "curComp: " << curComp << " compCount:" << compCount);

  if (newCompId[curComp] != DimParent_MAX)
    // top already set
    return;
  const DimParent &top = findMultiChildrenTop (newCompId, curComp);
  BOOST_ASSERT (top != DimParent_MAX);
  BOOST_ASSERT (childCount[top]);
  if (childCount[top] < 2 && TreeBuilder<GeoDimT>::compParents[top] != DimParent_MAX) {
    // mark lonely nodes
    // XXX todo: mark curComp => top
    newCompId[curComp] = 0;
    return;
  }
  if (curComp != top) {
    BOOST_ASSERT (curComp != DimParent_MAX);
    DimParent newTopIdx = newCompId[top];
    if (newTopIdx == DimParent_MAX)
      newCompId[top] = newTopIdx = compCount++;
    const DimNodePack &newTopChildCount = childCount[top];
    const DimParent &newTopCompParent = TreeBuilder<GeoDimT>::compParents[top];
    //const WeightT &newTopWeight = compWeights[top];
    for (DimParent sibling = curComp; sibling != top; ) {
      DimParent nextSibling = DimParent (TreeBuilder<GeoDimT>::compParents[sibling]);
      newCompId[sibling] = newTopIdx;
      childCount[sibling] = newTopChildCount;
      TreeBuilder<GeoDimT>::compParents[sibling] = newTopCompParent;
      // compWeights[sibling] = newTopWeight;
      sibling = nextSibling;
    }
    return;
  }
  newCompId[curComp] = compCount++;
  SMART_LOG ("newCompId[" << curComp << "]:" << newCompId[curComp]);
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::compress (DimParentPack *newCompId, const DimParent &compTop) {

  SMART_DEF_LOG ("ArrayTreeBuilder::compress", " compTop:" << compTop);
  dealThread (compTop, coreCount, [this, &newCompId] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
				    for (DimParent curComp = minVal; curComp < maxVal; ++curComp) {
				      if (newCompId[curComp] || childCount[curComp] > 1U)
					continue;
				      // reduce lonely nodes (newCompId == 0)
				      const DimParent &top = findNotLonelyTop (curComp);
				      BOOST_ASSERT (top == DimParent_MAX || TreeBuilder<GeoDimT>::compParents[top] == DimParent_MAX || childCount[top] > 1U);
				      BOOST_ASSERT (curComp != top);
				      childCount[curComp] = childCount[top];
				      TreeBuilder<GeoDimT>::compParents[curComp] = TreeBuilder<GeoDimT>::compParents[top]; // XXX pb if //
				      newCompId[curComp] = newCompId[top];
				      compWeights[curComp] = compWeights[top];
				    }
				  });

  dealThread (compTop, coreCount, [this, &newCompId] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
				    for (DimParent curComp = minVal; curComp < maxVal; ++curComp) {
				      DimParent old = TreeBuilder<GeoDimT>::compParents[curComp];
				      if (old != DimParent_MAX)
					TreeBuilder<GeoDimT>::compParents[curComp] = newCompId[old];
				    }
				  });

  dealThread (TreeBuilder<GeoDimT>::leafCount, coreCount, [this, &newCompId] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
							for (DimImg leaf = minVal; leaf < maxVal; ++leaf) {
							  DimParent old = TreeBuilder<GeoDimT>::leafParents[leaf];
							  if (old != DimParent_MAX)
							    TreeBuilder<GeoDimT>::leafParents[leaf] = newCompId[old];
							}
						      });

  // XXX non parallèle
  for (DimParent curComp = 0; curComp < compTop; ) {
    DimParent newIdxComp = newCompId[curComp];
    if (newIdxComp == curComp || newIdxComp == DimParent_MAX || newCompId[newIdxComp] == newIdxComp) {
      ++curComp;
      continue;
    }

    SMART_LOG ("comp curComp:" << curComp << " newIdxComp:" << newIdxComp);

    swap (TreeBuilder<GeoDimT>::compParents[curComp], TreeBuilder<GeoDimT>::compParents[newIdxComp]);
    swap (compWeights[curComp], compWeights[newIdxComp]);
    swap (childCount[curComp], childCount[newIdxComp]);
    swap (newCompId[curComp], newCompId[newIdxComp]);
  }
  // XXX YYY curComp ? compTop ?
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::dynImgAlloc (DimParent currentMax) {
  DEF_LOG ("ArrayTreeBuilder::ArrayTreeTile::dynImgAlloc", "currentMax:" << currentMax);
  currentMax += coreCount;
  DimParent estimatedCount = TreeBuilder<GeoDimT>::leafCount-1U;
  for (DimImg i = 0; i < 10; ++i) {
    DimTile threshold = DimTile (TreeBuilder<GeoDimT>::leafCount / DimImg (10U) * i);
    if (currentMax < threshold) {
      estimatedCount = threshold;
      break;
    }
  }
  TreeBuilder<GeoDimT>::resizeParents (estimatedCount);
  childCount = TreeBuilder<GeoDimT>::childrenStart+1+needChildren;
  parentCapacity = estimatedCount;
  pCompWeights->resize (estimatedCount);
  compWeights = pCompWeights->getValues ();
  LOG ("parentCapacity:" << parentCapacity);
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::createImgComp (DimParent &topParent, const WeightT &weight, DimParentPack &parentChildA, DimParentPack &parentChildB) {
  if (topParent >= parentCapacity) {
    DimNode idxA (&parentChildA - TreeBuilder<GeoDimT>::leafParents);
    DimNode idxB (&parentChildB - TreeBuilder<GeoDimT>::leafParents);
    dynImgAlloc (topParent+1);
    TreeBuilder<GeoDimT>::leafParents[idxA] = TreeBuilder<GeoDimT>::leafParents [idxB] = topParent;
  } else
    parentChildA = parentChildB = topParent;
  childCount [topParent] = 2U;
  compWeights [topParent] = weight;
  return topParent++;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::addChild (const DimParent &parent, DimParentPack &child) {
  ++childCount [parent];
  child = parent;
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::findRoot (DimParent comp) const {
  if (comp == DimParent_MAX)
    return comp;
  for (;;) {
    DimParent p = TreeBuilder<GeoDimT>::compParents [comp];
    if (p == DimParent_MAX)
      return comp;
    comp = p;
  }
  // XXX never execute
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::findNotLonelyTop (DimParent comp) const {
  BOOST_ASSERT (comp != DimParent_MAX);
  for (;;) {
    if (DimImg (childCount[comp]) > 1U)
      return comp;
    const DimParent &parent = TreeBuilder<GeoDimT>::compParents[comp];
    if (parent == DimParent_MAX)
      return comp;
    comp = parent;
  }
  // XXX never execute
}

// ----------------------------------------
template<typename PixelT, typename WeightT, int GeoDimT>
DimParent
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::findMultiChildrenTop (DimParentPack *newCompId, DimParent comp) const {
  SMART_DEF_LOG ("DimParent::findMultiChildrenTop", "comp:" << comp);
  BOOST_ASSERT (comp != DimParent_MAX);
  for (;;) {
    if (DimParent (newCompId [comp]) != DimParent_MAX)
      return comp;
    const DimParent parent = TreeBuilder<GeoDimT>::compParents[comp];
    SMART_LOG ("parent:" << parent);
    if (parent == DimParent_MAX)
      return comp;
    SMART_LOG ("compWeight:" << compWeights[comp] << " parentWeight:" << compWeights[parent]);
    if (compWeights[comp] != compWeights[parent]) {
      BOOST_ASSERT (childCount[comp] > 0);
      return comp;
    }
    // XXX YYY merge children
    BOOST_ASSERT (!childCount[comp]);
    comp = parent;
  }
  // XXX never execute
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
void
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::buildChildren () {
  SMART_DEF_LOG ("ArrayTreeBuilder::buildChildren", "");
  BOOST_ASSERT (needChildren);
  BOOST_ASSERT (TreeBuilder<GeoDimT>::childrenStart[0] == 0);
  BOOST_ASSERT (TreeBuilder<GeoDimT>::childrenStart[1] == 0);

  DimParent compCount = TreeBuilder<GeoDimT>::getCompCount ();
  TreeBuilder<GeoDimT>::bookChildren ();
  childCount = TreeBuilder<GeoDimT>::childrenStart+1+needChildren;

  // #if defined IMAGE_40BITS || defined IMAGE_LARGE  // partial_sum
  partial_sum (childCount, childCount+compCount, childCount);

  // set
  DimImgPack *childGetOrder = childCount-1U;
  DimNode nodeRootId = DimNode (compCount)+TreeBuilder<GeoDimT>::leafCount-1U;
  for (DimNode i = 0; i < nodeRootId; ++i) {
    if (DimParent (TreeBuilder<GeoDimT>::leafParents[i]) == DimParent_MAX)
      continue;
    BOOST_ASSERT (DimParent (TreeBuilder<GeoDimT>::leafParents[i]) < compCount);
    TreeBuilder<GeoDimT>::children[DimNode (childGetOrder[DimParent (TreeBuilder<GeoDimT>::leafParents[i])]++)] = i;
  }
  BOOST_ASSERT (TreeBuilder<GeoDimT>::childrenStart[0] == 0);
}

// ================================================================================
template<typename PixelT, typename WeightT, int GeoDimT>
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::CPrintComp::CPrintComp (const ArrayTreeBuilder &atb, const DimParent &compId)
  : atb (atb), compId (compId) {
}
template<typename PixelT, typename WeightT, int GeoDimT>
ostream &
ArrayTreeBuilder<PixelT, WeightT, GeoDimT>::CPrintComp::print (ostream &out) const {
  if (compId == DimParent_MAX)
    return out << "M(-/-)";
  out << compId << "(" << atb.childCount[compId] << "/";
  printWeight (out, atb.compWeights [compId]);
  return out << ")";
}

// ================================================================================
void
::obelix::triskele::ArrayTreeBuilderMemoryImpact (MemoryEstimation &memoryEstimation) {
  // XXX not 3d computation
  DEF_LOG("ArrayTreeBuilderMemoryImpact", "");

  // Computing tile sizes
  DimNode numberOfParentsPerTile = memoryEstimation.numberOfParents / memoryEstimation.tileNumber;
  LOG ("First step: construction of the Tree.");

  float valuesTabMemSize = memoryEstimation.valuesSize;
  float weightsTabMemSize = memoryEstimation.numberOfParents * memoryEstimation.pixelSize;
  float parentsMemSize = (memoryEstimation.numberOfPixels+memoryEstimation.numberOfParents) * sizeof (DimParentPack);
  float countMemSize = memoryEstimation.numberOfParents * sizeof (DimParentPack);
  // float leadersMemSize = numberOfPixels * sizeof (DimParentPack);
  // float childrenMemSize = (numberOfPixels+numberOfParents) * sizeof (DimParentPack);

  int bits = 8*memoryEstimation.pixelSize;
  float edgesPerTileMemSize = memoryEstimation.numberOfPixelsPerTile * memoryEstimation.neighborsNumber * (memoryEstimation.pixelSize + sizeof (DimParentPack) + 1);
  float frequencyCountPerTileMemSize = (memoryEstimation.pixelSize <= memoryEstimation.countingSortCeil) ? (((DimEdge) 1U) << bits) * sizeof (DimTile) : 0;
  float valuesTabPerTileMemSize = memoryEstimation.numberOfPixelsPerTile * memoryEstimation.pixelSize;
  float weightsTabPerTileMemSize = numberOfParentsPerTile * memoryEstimation.pixelSize;
  float parentsPerTileMemSize = (memoryEstimation.numberOfPixelsPerTile+numberOfParentsPerTile) * sizeof (DimTile);
  float countPerTileMemSize = numberOfParentsPerTile * sizeof (DimTile);
  float leadersPerTileMemSize = memoryEstimation.numberOfPixelsPerTile * sizeof (DimTile);

  memoryEstimation.addTree (valuesTabMemSize+weightsTabMemSize+parentsMemSize+countMemSize,
			    edgesPerTileMemSize+frequencyCountPerTileMemSize+valuesTabPerTileMemSize+weightsTabPerTileMemSize+parentsPerTileMemSize+countPerTileMemSize+leadersPerTileMemSize);
}

// ================================================================================
