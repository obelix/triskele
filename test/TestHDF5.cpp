////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <thread>

#include "HDF5.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;

// ========================================
int
main (int argc, char** argv) {
  cout << "start test HDF5" << endl;

  vector<float> big0 = vector<float> (1000000, 0);

  for (size_t i = 0; i < big0.size (); i += big0.size ()/10) {
    big0[i] = i;
  }

  cout << "test big write with [0..9] compression level" << endl;

  for (int i = 0; i < 10; i++){
    HDF5 h5;
    h5.set_compression_level (i);
    char filename[16];
    sprintf (filename,"big%i.hdf5",i);
    cout << "writing file " << filename << endl;
    h5.open_write (filename);
    h5.write_array (big0, "big", "/");
    h5.write_string ("valeur", "nom", "/");
    h5.close_file ();

    h5.open_read (filename);
    string s;
    h5.read_string (s, "nom", "/");
    h5.close_file ();
    cout << "read :" << s << endl;
  }


  return 0;
}
