////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Geo3D_tpp
#define _Obelix_Geo3D_tpp

// ================================================================================
template<>
inline
Point<3>::Point () {
  coord [0] = coord [1] = coord [2] = 0;
}
template<>
inline
Point<3>::Point (const DimSideImg &x, const DimSideImg &y) {
  BOOST_ASSERT (false);
  coord [0] = x;
  coord [1] = y;
  coord [2] = 0;
}
template<>
inline
Point<3>::Point (const DimSideImg &x, const DimSideImg &y, const DimSideImg &z) {
  coord [0] = x;
  coord [1] = y;
  coord [2] = z;
}
template<>
inline
Point<3>::Point (const DimSideImg _coord[]) {
  coord [0] = _coord [0];
  coord [1] = _coord [1];
  coord [2] = _coord [2];
}

// ================================================================================
template<>
inline const Point<3> &
Point<3>::getNull () {
  return NullPoint3D;
}

template<>
inline bool
obelix::operator == (const Point<3> &a, const Point<3> &b) {
  return
    a.coord [0] == b.coord [0] &&
    a.coord [1] == b.coord [1] &&
    a.coord [2] == b.coord [2];
}

// inline template bool &obelix::operator != <3>(const Point<3> &a, const Point<3> &b);

// ================================================================================
template<>
inline DimImg
obelix::point2idx (const Size<3> &size, const Point<3> &p) {
  return
    DimImg (p.coord [0]) +
    DimImg (size.side [0])*((DimImg (p.coord [1]) +
			     DimImg (size.side [1])*DimImg (p.coord [2])));
}

template<>
inline Point<3>
obelix::idx2point (const Size<3> &size, const DimImg &idx) {
  const DimImg line = idx / size.side [0];
  return Point<3> (idx % size.side [0], line % size.side [1], line / size.side [1]);
}

// ================================================================================
template<>
inline
DeltaPoint<3>::DeltaPoint (const Size<3> &size, const DimImg &idx1, const DimImg &idx2) {
  const DimImg yz1 (idx1 / size.side [0]);
  const DimImg yz2 (idx2 / size.side [0]);

  const DimImg x1 (idx1 % size.side [0]);
  const DimImg x2 (idx2 % size.side [0]);
  BOOST_ASSERT (x1 <= x2 || x1 == x2+1);
  BOOST_ASSERT (x2 <= x1 || x2 == x1+1);

  const DimImg y1 (yz1 % size.side [1]);
  const DimImg y2 (yz2 % size.side [1]);
  BOOST_ASSERT (y1 <= y2 || y1 == y2+1);
  BOOST_ASSERT (y2 <= y1 || y2 == y1+1);

  const DimImg z1 (yz1 / size.side [1]);
  const DimImg z2 (yz2 / size.side [1]);
  BOOST_ASSERT (z1 <= z2 || z1 == z2+1);
  BOOST_ASSERT (z2 <= z1 || z2 == z1+1);

  delta.x = ((x1 > x2) << 1) + (x1 != x2);
  delta.y = ((y1 > y2) << 1) + (y1 != y2);
  delta.z = ((z1 > z2) << 1) + (z1 != z2);
}

template<>
inline DimImg
DeltaPoint<3>::addIdx (const Size<3> &size, const DimImg &idx1) const {
  // BOOST_ASSERT (!(deltaX & 1) || idx1 < size.width-1);
  // BOOST_ASSERT (!(deltaX & 2) || idx1 > 0);
  // ...
  return idx1
    + (delta.x & 1)
    - (delta.x & 2)
    + (delta.y & 1)*size.side [0]
    - (delta.y & 2)*size.side [0]
    + (delta.z & 1)*size.side [0]*size.side [1]
    - (delta.z & 2)*size.side [0]*size.side [1];
}

// ================================================================================
template<>
inline
Size<3>::Size () {
  side [0] = side [1] = side [2] = 0;
}
template<>
inline
Size<3>::Size (const DimSideImg &w, const DimSideImg &h) {
  BOOST_ASSERT (false);
  side [0] = w;
  side [1] = h;
  side [2] = 1;
}
template<>
inline
Size<3>::Size (const DimSideImg &w, const DimSideImg &h, const DimSideImg &l) {
  side [0] = w;
  side [1] = h;
  side [2] = l;
}
template<>
inline
Size<3>::Size (const DimSideImg _side[]) {
  side [0] = _side [0];
  side [1] = _side [1];
  side [2] = _side [2];
}


template<>
inline bool
Size<3>::isNull () const {
  return !side [0] || !side [1] || !side [2];
}

template<>
inline bool
operator== (const Size<3> &a, const Size<3> &b) {
  return
    a.side [0] == b.side [0] &&
    a.side [1] == b.side [1] &&
    a.side [2] == b.side [2];
}

// ================================================================================
template<>
inline DimImg
Rect<3>::relIdx (const DimImg &absIdx, const Size<3> &fullSize) const {
  const DimImg yz (absIdx / fullSize.side [0]);
  return
    DimImg (absIdx % fullSize.side [0] - point.coord [0]) +
    DimImg (size.side [0])*((yz % fullSize.side [1] - point.coord [1]) +
			    DimImg (size.side [1])*DimImg (yz / fullSize.side [1] - point.coord [2]));
}
template<>
inline DimImg
Rect<3>::absIdx (const DimTile &relIdx, const Size<3> &fullSize) const {
  DimImg line = relIdx / size.side [0];
  return
    (relIdx % size.side [0])+point.coord [0] +
    fullSize.side [0]*((line%size.side [1]) + point.coord [1] +
		       fullSize.side [1]*(line/size.side [1] + point.coord [2]));
}
template<>
inline Point<3>
Rect<3>::absPt (const DimTile &relIdx) const {
  DimImg line = relIdx / size.side [0];
  return Point<3> (DimSideImg (relIdx % size.side [0]) + point.coord [0],
		   DimSideImg (line   % size.side [1]) + point.coord [1],
		   DimSideImg (line   / size.side [1]) + point.coord [2]);
}

template <>
inline bool
Rect<3>::isNull () const {
  return size.isNull ();
}

// ================================================================================
// template<typename T>
// inline void
// Rect<3>::cpToTile (const Size<3> &size, const T* src, vector<T> &dst) const;

// template<typename T>
// inline void
// Rect<3>::cpFromTile (const Size<3> &size, const vector<T> &src, T *dst) const;

// template<typename T>
// inline void
// Rect<3>::cpFromTileMove (const Size<3> &size, const vector<T> &src, T *dst, const T &move) const;


// ================================================================================
#endif // _Obelix_Geo3D_tpp
