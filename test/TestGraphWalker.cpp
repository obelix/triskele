////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "obelixDebug.hpp"
#include "misc.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/GraphWalker.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp>

using namespace std;
using namespace obelix;
using namespace obelix::triskele;

typedef uint16_t PixelT;
typedef uint16_t WeightT;
const DimCore coreCount = 4;

static bool show (false); // log or not

PixelT pixelsC1_6x4 [] = {
			  6, 1, 7, 7, 3, 3,
			  0, 3, 0, 0, 6, 4,
			  3, 3, 4, 6, 7, 4,
			  5, 7, 6, 6, 1, 5
};

PixelT pixelsT7_12x8 [] = {
			   1,  0,  3,  1,  3,  1,  0,  1,  7,  5,  6,  1 ,
			   1,  0,  6,  3,  4,  0,  2,  4,  0,  2,  2,  1 ,
			   1,  2,  5,  1,  3,  5,  7,  4,  5,  3,  5,  1 ,
			   4,  6,  2,  4,  3,  1,  5,  4,  1,  3,  0,  5 ,
			   3,  2,  2,  3,  4,  4,  5,  5,  6,  2,  7,  2 ,
			   0,  6,  6,  5,  1,  4,  6,  6,  2,  1,  2,  5 ,
			   2,  7,  2,  3,  2,  2,  0,  5,  4,  2,  0,  0 ,
			   7,  5,  6,  5,  0,  5,  7,  0,  3,  6,  5,  5
};

PixelT pixelsT1_18x12 [] = {
			    5,  0,  0,  5,  0,  7,  4,  5,  2,  6,  0,  2,  2,  6,  5,  2,  1,  5,
			    3,  2,  4,  7,  6,  1,  7,  2,  7,  5,  4,  6,  7,  1,  6,  0,  6,  7,
			    7,  2,  4,  1,  1,  4,  3,  3,  3,  0,  5,  4,  5,  1,  6,  2,  0,  5,
			    3,  7,  7,  2,  5,  3,  0,  4,  4,  7,  4,  2,  6,  3,  4,  2,  5,  5,
			    7,  0,  0,  2,  1,  6,  6,  6,  7,  4,  0,  7,  1,  4,  6,  0,  6,  3,
			    3,  7,  0,  7,  6,  4,  1,  4,  0,  6,  6,  5,  3,  5,  5,  4,  7,  6,
			    2,  5,  5,  1,  2,  5,  0,  3,  1,  6,  4,  0,  2,  7,  7,  2,  7,  5,
			    6,  0,  1,  6,  6,  7,  3,  2,  5,  1,  6,  4,  7,  0,  2,  4,  1,  4,
			    2,  1,  7,  3,  7,  3,  3,  1,  3,  2,  3,  2,  7,  2,  2,  0,  0,  1,
			    0,  4,  3,  5,  5,  1,  1,  4,  1,  3,  1,  2,  7,  3,  3,  7,  6,  2,
			    2,  2,  4,  5,  4,  7,  7,  4,  1,  2,  4,  2,  3,  4,  6,  6,  1,  3,
			    7,  3,  7,  0,  6,  0,  2,  6,  3,  5,  5,  2,  7,  7,  4,  3,  5,  0
};

// ================================================================================
void
testPixels (const Size<2> &size, const PixelT *pixels, const Connectivity &connectivity) {
  static const int GeoDimT (2);
  if (show)
    cout << endl
	 << "TestGraphWalker: " << size << endl
	 << endl;
  const DimImg pixelsCount = size.getPixelsCount ();
  Raster<PixelT, GeoDimT> raster (size);
  for (DimImg i = 0; i < pixelsCount; ++i)
    raster.getPixels ()[i] = pixels[i];
  Border<GeoDimT> border (size, false);
  GraphWalker<GeoDimT> graphWalker (border, connectivity);
  // delta
  if (show)
    cout << "volDelta1:" << graphWalker.volDelta1 << endl
	 << "volDelta2:" << graphWalker.volDelta2 << endl
	 << "horDelta1:" << graphWalker.horDelta1 << endl
	 << "horDelta2:" << graphWalker.horDelta2 << endl
	 << "verDelta1:" << graphWalker.verDelta1 << endl
	 << "verDelta2:" << graphWalker.verDelta2 << endl
	 << "plaDelta :" << graphWalker.plaDelta << endl
	 << endl;
  // outsideRect

  const double sumIdx ((pixelsCount-1)*pixelsCount/2);
  double sumPixels (0);
  for (DimImg i = 0; i < pixelsCount; ++i)
    sumPixels += pixels[i];
  if (show)
    cout << "pixelsCount: " << pixelsCount << " sumIdx: " << sumIdx << endl;

  // ================================================================================
  // setTiles
  vector<Rect<GeoDimT> > tiles, boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (coreCount, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);
  if (show)
    cout << "tiles        : " << tiles << endl
	 << "boundaries   : " << boundaries << endl
	 << "boundaryAxes : " << boundaryAxes << endl
	 << endl;
  REQUIRE (tiles.size () == coreCount);
  REQUIRE (boundaries.size () == coreCount-1);
  REQUIRE (boundaryAxes.size () == coreCount-1);

  // ================================================================================
  // forEachVertexSimple
  { // Frame
    double sumPixelsSize (0);
    graphWalker.forEachVertexSimple
      ([&sumPixelsSize, &raster] (const DimImg &idx) {
	 sumPixelsSize += raster.getValue (idx);
       });
    double sumPixelsRect (0);
    if (show)
      cout << "idx:" << endl;
    graphWalker.forEachVertex (Rect<GeoDimT> (Point<GeoDimT> (), size),
			       [&sumPixelsRect, &raster] (const DimImg &idx, const BorderFlagType &lowBorder) {
				 sumPixelsRect += raster.getValue (idx);
				 if (show)
				   cout << " " << idx;
				 if (show && lowBorder & X2)
				   cout << endl;
			       });
    if (show)
      cout << "sumPixels: " << sumPixels << " sumPixelsSize: " << sumPixelsSize << " sumPixelsRect: " << sumPixelsRect << endl;
    REQUIRE (sumPixels == sumPixelsSize);
    REQUIRE (sumPixels == sumPixelsRect);
  }

  { // Tile
    if (show)
      cout << "sumPixelsTile:";
    double sumPixelsTiles (0);
    for (Rect<GeoDimT> tile : tiles) {
      double sumPixelsTile (0);
      graphWalker.forEachVertex (tile,
				 [&sumPixelsTile, &raster] (const DimImg &idx, const BorderFlagType &lowBorder) {
				   sumPixelsTile += raster.getValue (idx);
				   if (show)
				     cout << " " << idx;
				   if (show && lowBorder & X2)
				     cout << endl;
				 });
      if (show)
	cout << " " << sumPixelsTile;
      sumPixelsTiles += sumPixelsTile;
    }
    if (show)
      cout << endl;
    REQUIRE (sumPixels == sumPixelsTiles);
  }
}

static const int GeoDimT (3);
void
testGraphWalker (Size<3> size, const Connectivity &connectivity) {
  static const int GeoDimT (3);

  Border<GeoDimT> border (size, false);
  GraphWalker<GeoDimT> graphWalker (border, connectivity);

  // ================================================================================
  // forEachVertexIdx
  DimImg neighbors (0);
  DimImg xNoNeighbors (0);
  DimImg yNoNeighbors (0);
  DimImg zNoNeighbors (0);
  if (connectivity & C4) {
    neighbors += 2;
    ++xNoNeighbors;
    ++yNoNeighbors;
  }
  if (connectivity & C6P) {
    ++neighbors;
    ++xNoNeighbors;
    ++yNoNeighbors;
  }
  if (connectivity & C6N) {
    ++neighbors;
    ++xNoNeighbors;
    ++yNoNeighbors;
  }
  if (connectivity & CT) {
    ++neighbors;
    ++zNoNeighbors;
  }
  if (connectivity & CTP) {
    neighbors += 4;
    zNoNeighbors += 4;
  }
  if (connectivity & CTN) {
    neighbors += 4;
    zNoNeighbors += 4;
  }
  if (show)
    cout << " connectivity: " << connectivity << " neighbors: " << neighbors << " xNoNeighbors: " << xNoNeighbors << " yNoNeighbors: " << yNoNeighbors << " zNoNeighbors: " << zNoNeighbors << endl;

  double sumIdxBorder (0);
  for (DimChannel z = 0; z < size.side [2]; ++z)
    for (DimSideImg y = 0; y < size.side [1]; ++y)
      for (DimSideImg x = 0; x < size.side [0]; ++x) {
	DimImg idx = point2idx (size, Point<GeoDimT> (x, y, z));
	if (x == 0)
	  sumIdxBorder += idx*xNoNeighbors;
	if (x == size.side [0]-1)
	  sumIdxBorder += idx*xNoNeighbors;
	if (y == 0)
	  sumIdxBorder += idx*yNoNeighbors;
	if (y == size.side [1]-1)
	  sumIdxBorder += idx*yNoNeighbors;
	if (z == 0)
	  sumIdxBorder += idx*zNoNeighbors;
	if (z == size.side [1]-1)
	  sumIdxBorder += idx*zNoNeighbors;
      }
  const DimImg pixelsCount = size.getPixelsCount ();
  const double sumIdx ((pixelsCount-1)*pixelsCount/2);
  double sumIdxEdges (sumIdx*2*neighbors-sumIdxBorder);
  if (connectivity & C6P)
    for (DimChannel z = 0; z < size.side [2]; ++z)
      sumIdxEdges +=
	point2idx (size, Point<GeoDimT> (0, 0, z))+
	point2idx (size, Point<GeoDimT> (size.side [0]-1, size.side [1]-1, z));
  if (connectivity & C6N)
    for (DimChannel z = 0; z < size.side [2]; ++z)
      sumIdxEdges +=
  	point2idx (size, Point<GeoDimT> (size.side [0]-1, 0, z))+
  	point2idx (size, Point<GeoDimT> (0, size.side [1]-1, z));
  // XXX CT / CTP / CTN
  if (show)
    cout << "sumIdx: " << sumIdx << " sumIdxBorder: " << sumIdxBorder << " sumIdxEdges: " << sumIdxEdges << endl;

  { // Frame
    double sumIdxEdgesSize (0);
    graphWalker.forEachEdgeIdx
      (Rect<GeoDimT> (Point<GeoDimT> (), size), Volume,
       [&sumIdxEdgesSize] (const DimImg &a, const DimImg &b) {
	 // cout << "a: " << a << " b: " << b << endl;
	 sumIdxEdgesSize += a;
	 sumIdxEdgesSize += b;
       });
    if (show)
      cout << "sumIdxEdgesSize: " << sumIdxEdgesSize << endl;
    REQUIRE (sumIdxEdges == sumIdxEdgesSize);
  }
  vector<Rect<GeoDimT> > tiles, boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (coreCount, Rect<GeoDimT> (Point<GeoDimT> (), size), tiles, boundaries, boundaryAxes);
  {
    double sumIdxTiles (0);
    DimImg pixelsCountTiles (0);
    for (Rect<GeoDimT> tile : tiles) {
      double sumIdxTile (0);
      DimImg pixelsCountTile (0);
      for (DimChannel z = 0; z < tile.size.side [2]; ++z)
	for (DimSideImg y = 0; y < tile.size.side [1]; ++y)
	  for (DimSideImg x = 0; x < tile.size.side [0]; ++x) {
	    // if (show)
	    //   cout << "o: " << tile.point  << " size:" << Size<GeoDimT> (x, y, z) << " p: " << Point<GeoDimT> (tile.point, Size<GeoDimT> (x, y, z)) << endl;
	    sumIdxTile += point2idx (size, Point<GeoDimT> (tile.point, Size<GeoDimT> (x, y, z)));
	    ++pixelsCountTile;
	  }
      if (show)
	cout << "pixelsCountTile: " << pixelsCountTile << " sumIdxTile: " << sumIdxTile << endl;
      sumIdxTiles += sumIdxTile;
      pixelsCountTiles += pixelsCountTile;
    }
    REQUIRE (sumIdx == sumIdxTiles);
    REQUIRE (pixelsCount == pixelsCountTiles);
  }

  { // Tile
    // if (show)
    //   cout << "sumIdxEdgesSize:";
    double sumIdxEdgesTiles (0);
    for (Rect<GeoDimT> tile : tiles) {
      double sumIdxEdgesTile (0);
      graphWalker.forEachEdgeIdx (tile, Volume,
				  [&sumIdxEdgesTile] (const DimImg &a, const DimImg &b) {
				    // cout << "a: " << a << " b: " << b << endl;
				    sumIdxEdgesTile += a;
				    sumIdxEdgesTile += b;
				  });
      // cout << endl;
      // if (show)
      // 	cout << " " << sumIdxEdgesTile;
      sumIdxEdgesTiles += sumIdxEdgesTile;
    }
    for (size_t i = 0; i < boundaries.size (); ++i) {
      // cout << "boundaries: " << boundaries[i] << " axe: " << boundaryAxes[i] << endl;
      double sumIdxEdgesBound (0);
      graphWalker.forEachEdgeIdx (boundaries[i], boundaryAxes [i],
				  [&sumIdxEdgesBound] (const DimImg &a, const DimImg &b) {
				    // cout << "a: " << a << " b: " << b << endl;
				    sumIdxEdgesBound += a;
				    sumIdxEdgesBound += b;
				  });
      // cout << endl;
      // if (show)
      // 	cout << " " << sumIdxEdgesBound;
      sumIdxEdgesTiles += sumIdxEdgesBound;
    }

    if (show)
      cout << endl;
    REQUIRE (sumIdxEdges == sumIdxEdgesTiles);
  }
}

// ================================================================================
TEST_CASE ("Testing GraphWalker...") {
  // Log::debug = true;

  // for (Connectivity connectivity : {C4, C4|C6P, C4|C6N, C8, C6P, C6N, C6P|C6N}) {

  //   testPixels (Size<2> (6, 4), pixelsC1_6x4, connectivity);
  //   testGraphWalker (Size<3> (6, 4, 1), connectivity);

  //   testPixels (Size<2> (12, 8), pixelsT7_12x8, connectivity);
  //   testGraphWalker (Size<3> (12, 8, 3), connectivity);

  //   testPixels (Size<2> (18, 12), pixelsT1_18x12, connectivity);
  //   testGraphWalker (Size<3> (18, 12, 5), connectivity);
  // }

  Size<3> size (5, 5, 2);
  Border<3> border (size, false);
  GraphWalker<3> graphWalker (border, C4|CT);
  Raster<PixelT, GeoDimT> raster (size);
  const DimImg pixelsCount = size.getPixelsCount ();
  for (DimImg i = 0; i < pixelsCount; ++i)
    raster.getPixels ()[i] = i;

  Rect<3> rect (NullPoint3D, size);
  graphWalker.forEachEdgeIdx (rect, Volume,
			      [] (const DimImg &a, const DimImg &b) {
				if (show)
				  cout << "a: " << a << " b: " << b << endl;
			      });

  graphWalker.forEachVertex
    (rect,
     [&graphWalker, &rect] (const DimImg &idx, const BorderFlagType &lowBorder) {
       BorderFlagType borderFlags (graphWalker.getBorderFlag (rect.size, idx));
       if (lowBorder != borderFlags)
	 cout << lowBorder << " != " << borderFlags << " ";
       graphWalker.forEachNeighbor
	 (idx, borderFlags,
	  [&idx] (const DimImg &idxB) {
	    if (show)
	      cout << "a: " << idx << " b: " << idxB << endl;
	  });
     });


  
  vector <TileEdge<WeightT> > tileEdges;
  WeightFunction<PixelT, WeightT, 3, MAX> maxWeight (raster.getPixels (), graphWalker);
  graphWalker.getSortedTileEdges (rect, tileEdges, &maxWeight);
  for (TileEdge<WeightT> edge : tileEdges)
    if (show)
      cout << printTileEdge (edge, size, rect) << endl;

  if (show)
    cout << endl;
}
