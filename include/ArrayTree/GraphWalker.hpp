////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_GraphWalker_hpp
#define _Obelix_Triskele_GraphWalker_hpp

#include <boost/assert.hpp>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iostream>

#include "obelixDebug.hpp"
#include "obelixThreads.hpp"
#include "obelixDebug.hpp"
#include "triskeleArrayTreeBase.hpp"
#include "Border.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;
    enum BorderFlagType {NO = 0, X1 = 1, Y1 = 2, Z1 = 4, X2 = 8, Y2 = 16, Z2 = 32, ALL = X1|X2|Y1|Y2|Z1|Z2};
    inline BorderFlagType operator~ (const BorderFlagType &a);
    inline BorderFlagType operator| (const BorderFlagType &a, const BorderFlagType &b);
    inline BorderFlagType operator& (const BorderFlagType &a, const BorderFlagType &b);
    inline BorderFlagType operator^ (const BorderFlagType &a, const BorderFlagType &b);
    inline BorderFlagType& operator|= (BorderFlagType &a, const BorderFlagType &b);
    inline BorderFlagType& operator&= (BorderFlagType &a, const BorderFlagType &b);
    ostream &operator << (ostream &out, const BorderFlagType &a);

    struct Neighbor {
      DimImg off;
      BorderFlagType border;
      Neighbor (const BorderFlagType &border, const DimImg &off) : off (off), border (border) {}
      bool operator< (const Neighbor &rhs) const { return off < rhs.off; }
    };
    ostream &operator << (ostream &out, const Neighbor &a);

    template <typename PixelT, typename WeightT, int GeoDimT, TreeType TreeTypeT>
    struct WeightFunction;

    // ================================================================================
    /** GraphWalker */
    template<int GeoDimT>
    class GraphWalker {
    public:
      const Border<GeoDimT> &border;
      const Connectivity connectivity;
      const DimSortCeil countingSortCeil;
      const vector<Neighbor> volDelta1, volDelta2, horDelta1, horDelta2, verDelta1, verDelta2, plaDelta;
      static const vector<Neighbor> determineDelta (const Size<GeoDimT> &size, const Connectivity &connectivity, const TileShape &tileShape, bool first = true);
      static BorderFlagType getBorderFlag (const Size<GeoDimT> &size, const DimImg &idx);

      inline const Size<GeoDimT> &getSize () const;
      GraphWalker (const Border<GeoDimT> &border, const Connectivity &connectivity = Connectivity::C4, const DimSortCeil &countingSortCeil = 2);

      DimEdge	edgeMaxCount () const;
      DimEdge	edgeMaxCount (const Size<GeoDimT> &tileSize) const;
      DimEdge	edgeMaxCount (const Size<GeoDimT> &size, const TileShape &boundaryAxe) const;

      void setTiles (const DimCore &coreCount, vector<Rect<GeoDimT> > &tiles, vector<Rect<GeoDimT> > &boundaries, vector<TileShape> &boundaryAxes) const;
      void setTiles (const DimCore &coreCount, const Rect<GeoDimT> &rect, vector<Rect<GeoDimT> > &tiles, vector<Rect<GeoDimT> > &boundaries, vector<TileShape> &boundaryAxes) const;
      void setMaxBounds (const vector<Rect<GeoDimT> > &tiles, vector<DimImg> &vertexMaxBounds, vector<DimImg> &edgesMaxBounds) const;

      template<typename Funct>
      inline DimImg forEachVertexSimple (const Funct &lambda /*(const DimImg &idx)*/) const;
      template<typename Funct>
      inline DimImg forEachVertex (const Rect<GeoDimT> &rect, const Funct &lambda /*(const DimImg &idx, const BorderFlagType &borderFlags)*/) const;
      template<typename Funct>
      inline DimImg forEachVertexYX (const Rect<GeoDimT> &rect, const Funct &lambda /*(const DimImg &idx, const BorderFlagType &borderFlags)*/) const;

      template<typename Funct>
      inline DimEdge forEachPrevEdge (const Rect<GeoDimT> &edgeRect, const vector<Neighbor> prevDelta, const BorderFlagType &mask,
				      const Funct &lambda /*(const DimImg &a, const DimImg &b)*/) const;
      template<typename Funct>
      inline DimEdge forEachEdgeIdx (const Rect<GeoDimT> &rect, TileShape tileShape, const Funct &lambda /*(const DimImg &a, const DimImg &b)*/) const;
      template<typename Funct>
      inline void forEachNeighbor (const DimImg &a, const BorderFlagType &borderFlags, const Funct &lambda /*(const DimImg &b)*/) const;

      template<typename PixelT, typename WeightT, TreeType TreeTypeT>
      DimEdge getSortedTileEdges (const Rect<GeoDimT> &rect, vector <TileEdge<WeightT> > &tileEdges, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b)*/) const;
      template<typename PixelT, typename WeightT, TreeType TreeTypeT>
      DimEdge getQSortedTileEdges (const Rect<GeoDimT> &rect, vector <TileEdge<WeightT> > &tileEdges, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b) ef->sort ()*/) const;

      template<typename PixelT, typename WeightT, TreeType TreeTypeT>
      DimEdge getSortedEdges (const Rect<GeoDimT> &rect, TileShape tileShape, vector <Edge<WeightT, GeoDimT> > &edges, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b)*/) const;
      template<typename PixelT, typename WeightT, TreeType TreeTypeT>
      DimEdge getQSortedEdges (const Rect<GeoDimT> &rect, TileShape tileShape, vector <Edge<WeightT, GeoDimT> > &edges, const WeightFunction<PixelT, WeightT, GeoDimT, TreeTypeT> *ef /*ef->getWeight (const DimImg &a, const DimImg &b) ef->sort ()*/) const;
    };


    // ================================================================================
#include "GraphWalker.tpp"

  } // triskele
} // obelix


#endif // _Obelix_Triskele_GraphWalker_hpp
