////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Geo_tpp
#define _Obelix_Geo_tpp

// ================================================================================
template<int GeoDimT>
inline
Point<GeoDimT>::Point () {
  fill (begin (coord), begin (coord)+GeoDimT, 0);
}
template<int GeoDimT>
inline
Point<GeoDimT>::Point (const DimSideImg _coord[]) {
  for (int i = 0; i < GeoDimT; ++i)
    coord [i] = _coord[i];
}

// ================================================================================
template<int GeoDimT>
inline const Point<GeoDimT> &
Point<GeoDimT>::getNull () {
  return Point<GeoDimT> ();
}

// ================================================================================
template<int GeoDimT>
inline bool
obelix::operator == (const Point<GeoDimT> &a, const Point<GeoDimT> &b) {
  for (int i = 0; i < GeoDimT; ++i)
    if (a.coord [i] != b.coord [i])
      return false;
  return true;
}
template<int GeoDimT>
inline bool
operator!= (const Point<GeoDimT> &a, const Point<GeoDimT> &b) {
  return ! (a == b);
}

// ================================================================================
template<int GeoDimT>
inline DimImg
obelix::point2idx (const Size<GeoDimT> &size, const Point<GeoDimT> &p) {
  DimImg result = 0;
  for (int i = GeoDimT; i > 0; ) {
    result *= size.side [i];
    result += p.coord [i];
    --i;
  }
  return result;
}

template<int GeoDimT>
inline Point<GeoDimT>
obelix::idx2point (const Size<GeoDimT> &size, const DimImg &idx) {
  Point<GeoDimT> result;
  const DimImg left = idx;
  for (int i = 0; i < GeoDimT; ++i) {
    result.coord [i] = left % size.side [i];
    left /= size.side [i];
  }
  return result;
}

template<int GeoDimT>
inline
DeltaPoint<GeoDimT>::DeltaPoint ()
  : set (0) {
}
template<int GeoDimT>
inline
DeltaPoint<GeoDimT>::DeltaPoint (const int &deltaX, const int &deltaY) {
  BOOST_ASSERT (GeoDimT == 2);
  delta.x = deltaX;
  delta.y = deltaY;
}
template<int GeoDimT>
inline
DeltaPoint<GeoDimT>::DeltaPoint (const int &deltaX, const int &deltaY, const int &deltaZ) {
  BOOST_ASSERT (GeoDimT == 3);
  delta.x = deltaX;
  delta.y = deltaY;
  delta.z = deltaZ;
}

// TODO : inline	DeltaPoint (const Size<GeoDimT> &size, const DimImg &idx1, const DimImg &idx2);
// TODO : inline DimImg	addIdx (const Size<GeoDimT> &size, const DimImg &idx) const;

// ================================================================================
template<int GeoDimT>
inline
Size<GeoDimT>::Size () {
  fill (begin (side), begin (side)+GeoDimT, 0);
}
template<int GeoDimT>
inline
Size<GeoDimT>::Size (const DimSideImg _side[]) {
  for (int i = 0; i < GeoDimT; ++i)
    side [i] = _side[i];
}
template<int GeoDimT>
inline DimImg
Size<GeoDimT>::getFramePixelsCount () const {
  return side [0]*side [1];
}
template<int GeoDimT>
inline bool
Size<GeoDimT>::isNull () const {
  for (int i = 0; i < GeoDimT; ++i)
    if (!side [i])
      return true;
  return false;
}
template<int GeoDimT>
inline bool
operator== (const Size<2> &a, const Size<2> &b) {
  for (int i = 0; i < GeoDimT; ++i)
    if (a.side [i] != b.side [i])
      return false;
  return true;
}

template<int GeoDimT>
inline bool
operator!= (const Size<GeoDimT> &a, const Size<GeoDimT> &b) {
  return ! (a == b);
}

// ================================================================================
template<int GeoDimT>
inline
Rect<GeoDimT>::Rect ()
  : point (),
    size () {
}

template<int GeoDimT>
inline
Rect<GeoDimT>::Rect (const Point<GeoDimT> &orig, const Size<GeoDimT> &size)
  : point (orig),
    size (size) {
}

// TODO : inline DimImg		relIdx (const DimImg &absIdx, const Size<GeoDimT> &fullSize) const;
// TODO : inline DimImg		absIdx (const DimTile &relIdx, const Size<GeoDimT> &fullSize) const;
// TODO : inline Point<GeoDimT>	absPt (const DimTile &relIdx) const; 

template<int GeoDimT>
template<typename T>
inline void
Rect<GeoDimT>::cpToTile (const Size<GeoDimT> &size, const T* src, vector<T> &dst) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  dst.resize (maxCount);
  for (DimImg idx = 0; idx < maxCount; ++idx) {
    dst [idx] = src [DimImg (absIdx (idx, size))];
  }
}
template<int GeoDimT>
template<typename T>
inline void
Rect<GeoDimT>::cpFromTile (const Size<GeoDimT> &size, const vector<T> &src, T *dst) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  for (DimImg idx = 0; idx < maxCount; ++idx) {
    dst [absIdx (idx, size)] = src [idx];
  }
}

template<int GeoDimT>
template<typename T>
inline void
Rect<GeoDimT>::cpFromTileMove (const Size<GeoDimT> &size, const vector<T> &src, T *dst, const T &move) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  DEF_LOG ("Rect::cpFromTileTrans", "rectSize:" << size << " move:" << move);
  for (DimImg idx = 0; idx < maxCount; ++idx)
    dst [absIdx (idx, size)] = src [idx] + move;
  SMART_LOG ("tile:"<< *this << " src/dst:" << endl
	     << printMap (src.data (), size, 0) << endl
	     << printMap (dst, size, 0));
}

template<int GeoDimT>
inline bool
Rect<GeoDimT>::isNull () const {
  return size.isNull ();
}

// ================================================================================
template <typename T, int GeoDimT>
inline
CPrintMap<T, GeoDimT>::CPrintMap (const T *map, const Size<GeoDimT> &size, const DimNode &maxValues)
  : map (map),
    size (size),
    maxValues (maxValues == 0 ? ((DimNode) size.getPixelsCount ()) : maxValues) {
}

template <typename T, int GeoDimT>
inline ostream &
CPrintMap<T, GeoDimT>::print (ostream &out) const {
  for (int i = 0; i < GeoDimT; ++i)
    if ((i < 2 && size.side [i] > printMapMaxSide) ||
	size.side [i] > 2*printMapMaxSide)
      return out << "/* map too big to print! */";
  const T *curVal (map);
  DimNode countDown (maxValues);
  printRank (out, GeoDimT-1, curVal, countDown);
  return out;
}

template <typename T, int GeoDimT>
inline void
CPrintMap<T, GeoDimT>::printRank (ostream &out, const int &rank, const T *&curVal, DimNode &countDown) const {
  for (DimSideImg i = 0; i < size.side [rank]; ++i) {
    if (rank) {
      printRank (out, rank-1, curVal, countDown);
      out << endl;
    } else {
      if (!countDown)
	return;
      print (out, *curVal);
      ++curVal;
      --countDown;
    }
  }
}

template <typename T, int GeoDimT>
inline ostream &
CPrintMap<T, GeoDimT>::print (ostream &out, const T &val) const {
  if ((DimParent (val)) == DimParent_MAX)
    return out << "  M ";
  return out << setw (3) << val << " ";
}

// ================================================================================
template <>
inline ostream &
CPrintMap<uint8_t, 2>::print (ostream &out, const uint8_t &val) const {
  if ((DimParent (val)) == DimParent_MAX)
    return out << "  M ";
  return out << setw (3) << (int) val << " ";
}
template <>
inline ostream &
CPrintMap<uint8_t, 3>::print (ostream &out, const uint8_t &val) const {
  if ((DimParent (val)) == DimParent_MAX)
    return out << "  M ";
  return out << setw (3) << (int) val << " ";
}
template <typename T, int GeoDimT>
inline CPrintMap<T, GeoDimT>
printMap (const T *map, const Size<GeoDimT> &size, const DimNode &maxValues) {
  return CPrintMap<T, GeoDimT> (map, size, maxValues);
}

// ================================================================================
#endif // _Obelix_Geo_tpp
