////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_CompAttribute_hpp
#define _Obelix_Triskele_CompAttribute_hpp

#include <iostream>
#include <vector>
#include <map>

#include "obelixThreads.hpp"
#include "obelixGeo.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "AttributeProfiles.hpp"
#include "HDF5.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;

    enum AttributeType
      {
       AArea, APerimeter, AZLength, ASTS, ACompactness, AComplexity, ASimplicity, ARectangularity, AWeight, ASD, ASDW, AMoI //, ASDA, Bound
      };
    extern const string attributeLabels[];
    extern const map<string, AttributeType> attributeMap;
    ostream &operator << (ostream &out, const AttributeType &attributeType);
    istream &operator >> (istream &in, AttributeType &attributeType);


    /*! Type d'attribut pour réaliser un filtrage */
    enum FeatureType
      {
       FAP, FArea, FPerimeter, FZLength, FSTS, FCompactness, FComplexity, FSimplicity, FRectangularity, FMin, FMax, FMean, FSD, FSDW, FMoI // FSDA FLevel
      };
    /*! Définit les noms des connectivités (utile pour le debug) */
    extern const string featureTypeLabels[];
    extern const map<string, FeatureType> featureTypeMap;
    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const FeatureType &featureType);
    istream &operator >> (istream &in, FeatureType &featureType);

    /*! Type de stratégie d'élagage de l'arbre */
    enum PruningStrategy {
			  Direct, Min, Max //, Viterbi, Substractive
    };
    extern const string pruningLabels[];
    extern const map<string, PruningStrategy> pruningMap;
    ostream &operator << (ostream &out, const PruningStrategy &pruningStrategy);
    istream &operator >> (istream &in, PruningStrategy &pruningStrategy);

    // ================================================================================
    template<typename AttrT, int GeoDimT>
    class CompAttribute {
    public:
      static vector<AttrT> getScaledThresholds (const vector<double> &thresholds, const AttrT &maxValue);
      static vector<AttrT> getConvertedThresholds (const vector<double> &thresholds);

      CompAttribute (const Tree<GeoDimT> &tree, const bool &decr = false);
      virtual ~CompAttribute ();

      virtual inline void save (HDF5 &hdf5) const {}
      virtual inline void load (HDF5 &hdf5) {}

      void save (HDF5 &hdf5, const string &compLabel) const;
      void load (HDF5 &hdf5, const string &compLabel);

      void updateTranscient ();
      const AttrT *getValues () const;
      virtual const AttrT getPixelValue (const DimImg &leafId) const = 0;
      AttrT *getValues ();
      AttrT getMaxValue () const;
      bool getDecr () const;

      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
		const vector<AttrT> &thresholds, const PruningStrategy &pruningStrategy = Direct) const;

      template<typename PixelT>
      void cutOnPos (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
		     const DimImg &pixelId, const vector<AttrT> &thresholds, const vector<AttrT> &values) const;

      virtual inline ostream &print (ostream &out) const { print (out, ""); return out; }

    protected:
      const Tree<GeoDimT> &tree;
      DimImg leafCount;
      DimParent compCount;
      vector<AttrT> values;
      bool decr;

      void free ();
      void book (const DimParent &compCount);

      template<typename CumpFunctPSE>
      inline void computeSameCompLevel (const CumpFunctPSE &cumpFunctPSE /* cumpFunctPSE (DimParent parentId)*/) const;

      template<typename CumpFunctPSE>
      inline void computeSameCompLevelFromRoot (const CumpFunctPSE &cumpFunctPSE /* cumpFunctPSE (DimParent parentId)*/) const;

      ostream &print (ostream &out, const string &msg) const;
      friend ostream &operator << (ostream &out, const CompAttribute &ca) { return ca.print (out); return out; }

    };

    // ================================================================================

  } // triskele
} // obelix
#include "Attributes/WeightAttributes.hpp"
namespace obelix {
  namespace triskele {
#include "CompAttribute.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_CompAttribute_hpp
