////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <numeric> // partial_sum
#include <vector>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "Border.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "TreeBuilder.hpp"
#include "IImage.hpp"
#include "AttributeProfiles.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/triskeleSort.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/Leader.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;

static const int GeoDimT (2);

const bool seqTile (false), autoThreadFlag (false), needChildren (true);

const unsigned int nbTest = 1; // 1000;
const TreeType treeType = MIN; //MIN; // MAX; // MIN; // ALPHA; // TOS; // MED; // MIN; // MAX;
const DimCore coreCount = 4; //3;

typedef uint16_t PixelT;
typedef uint16_t WeightT;

// ========================================
PixelT pixelsA[] = {
		    4, 0, 2,  8, 7, 2,
		    5, 8, 0,  0, 0, 2,

		    5, 5, 2,  0, 2, 3,
		    1, 1, 0,  0, 3, 3
};
PixelT pixelsB[] = {
		    2, 0, 3,  6, 5, 2,
		    4, 0, 6,  7, 1, 7,

		    1, 5, 2,  3, 0, 6,
		    4, 5, 4,  1, 1, 6
};
PixelT pixelsC [] = {
		     2, 0, 0,  6, 4, 3,
		     6, 7, 0,  7, 2, 6,

		     7, 5, 7,  4, 4, 1,
		     7, 0, 0,  3, 5, 4
};
PixelT pixelsN1 [] = { // pb A)
		      5, 1, 7,  2, 7, 0,
		      2, 6, 0,  5, 2, 5,

		      6, 7, 2,  2, 5, 7,
		      3, 7, 5,  6, 6, 6
};
PixelT pixelsN2 [] = { // B3) parentId:2 nodeStart:4 nodeEnd:5
		      6, 6, 2,  4, 5, 2,
		      0, 2, 0,  1, 5, 5,

		      2, 7, 7,  4, 7, 0,
		      7, 6, 5,  6, 5, 1
};
PixelT pixelsN3 [] = { // A) childIdx:27 parentId:2 parentIdx:26 rootId:5
		      5, 1, 7,  2, 7, 0,
		      2, 6, 0,  5, 2, 5,

		      6, 7, 2,  2, 5, 7,
		      3, 7, 5,  6, 6, 6
};
PixelT pixelsB1 [] = { //A) childIdx:27 parentId:2 parentIdx:26 rootId:8
		      5, 6, 7,  0, 5, 3,
		      2, 2, 1,  3, 2, 1,

		      3, 3, 7,  3, 7, 5,
		      4, 6, 5,  7, 4, 3
};
PixelT pixelsB2 [] = { //B3) parentId:4 nodeStart:17 nodeEnd:18
		      0, 6, 6, 5, 7, 2,
		      7, 6, 0, 3, 7, 5,
		      4, 0, 1, 1, 1, 2,
		      2, 2, 2, 1, 1, 1
};
PixelT pixelsB3 [] = { //) parents[0]:0 parentId:6 level child:0 parent:0 parentOrig:5
		      0, 7, 1, 4, 3, 0,
		      0, 4, 4, 4, 6, 0,
		      5, 1, 7, 2, 4, 1,
		      6, 6, 4, 6, 1, 5
};
PixelT pixelsC1_6x4 [] = { //A) childIdx:26 parentId:1 parentIdx:25 rootId:9
			  6, 1, 7, 7, 3, 3,
			  0, 3, 0, 0, 6, 4,
			  3, 3, 4, 6, 7, 4,
			  5, 7, 6, 6, 1, 5
};

PixelT pixelsC2 [] = { // init leafCount ?
		      1, 3, 0, 0, 5, 3,
		      0, 5, 7, 7, 5, 4,
		      3, 3, 6, 3, 4, 6,
		      0, 0, 0, 1, 5, 7
};

// ========================================
PixelT pixels3x3_A [] = {
			 0,   0,   0,
			 2,   3,   2,
			 0,   3,   3
};
PixelT pixels4x4_B [] = {
			 0,   7,   2,   5,
			 2,   1,   5,   7,
			 1,   3,   5,   0,
			 6,   4,   0,   5
};
PixelT pixels4x4_C [] = {
			 2,  7,  7,  4,
			 5,  6,  1,  1,
			 7,  5,  3,  5,
			 4,  4,  4,  2
};

PixelT pixelsT1_18x12 [] = {
			    5,  0,  0,  5,  0,  7,  4,  5,  2,  6,  0,  2,  2,  6,  5,  2,  1,  5,
			    3,  2,  4,  7,  6,  1,  7,  2,  7,  5,  4,  6,  7,  1,  6,  0,  6,  7,
			    7,  2,  4,  1,  1,  4,  3,  3,  3,  0,  5,  4,  5,  1,  6,  2,  0,  5,
			    3,  7,  7,  2,  5,  3,  0,  4,  4,  7,  4,  2,  6,  3,  4,  2,  5,  5,
			    7,  0,  0,  2,  1,  6,  6,  6,  7,  4,  0,  7,  1,  4,  6,  0,  6,  3,
			    3,  7,  0,  7,  6,  4,  1,  4,  0,  6,  6,  5,  3,  5,  5,  4,  7,  6,
			    2,  5,  5,  1,  2,  5,  0,  3,  1,  6,  4,  0,  2,  7,  7,  2,  7,  5,
			    6,  0,  1,  6,  6,  7,  3,  2,  5,  1,  6,  4,  7,  0,  2,  4,  1,  4,
			    2,  1,  7,  3,  7,  3,  3,  1,  3,  2,  3,  2,  7,  2,  2,  0,  0,  1,
			    0,  4,  3,  5,  5,  1,  1,  4,  1,  3,  1,  2,  7,  3,  3,  7,  6,  2,
			    2,  2,  4,  5,  4,  7,  7,  4,  1,  2,  4,  2,  3,  4,  6,  6,  1,  3,
			    7,  3,  7,  0,  6,  0,  2,  6,  3,  5,  5,  2,  7,  7,  4,  3,  5,  0
};

PixelT pixelsT2_12x8 [] = {
			   0,  5,  4,  6,  4,  2,  5,  1,  7,  6,  7,  4,
			   3,  5,  7,  0,  0,  6,  5,  0,  3,  5,  0,  5,
			   3,  5,  7,  4,  6,  0,  6,  6,  5,  2,  4,  1,
			   4,  2,  3,  3,  0,  2,  7,  4,  7,  6,  4,  0,
			   4,  1,  0,  0,  6,  0,  5,  1,  5,  4,  5,  3,
			   5,  3,  2,  2,  5,  6,  4,  1,  0,  7,  4,  1,
			   1,  3,  5,  0,  1,  1,  0,  6,  2,  0,  6,  0,
			   0,  3,  1,  6,  0,  7,  1,  5,  2,  3,  7,  0
};

PixelT pixelsT3_12x8 [] = {
			   7,  3,  3,  2,  7,  1,  6,  3,  4,  7,  0,  3,
			   0,  5,  5,  5,  3,  7,  3,  1,  5,  0,  1,  2,
			   5,  4,  5,  2,  1,  5,  4,  1,  1,  7,  3,  0,
			   1,  1,  4,  5,  0,  4,  0,  0,  1,  5,  5,  4,
			   4,  1,  5,  1,  1,  6,  3,  6,  2,  0,  0,  4,
			   6,  5,  5,  7,  4,  0,  7,  5,  1,  3,  3,  1,
			   7,  3,  2,  0,  1,  7,  4,  5,  0,  2,  7,  1,
			   0,  2,  7,  3,  3,  0,  7,  1,  5,  4,  0,  1
};
PixelT pixelsT4_12x8 [] = {
			   1,  5,  6,  6,  0,  1,  7,  0,  1,  2,  1,  7 ,
			   3,  1,  0,  0,  1,  0,  2,  4,  0,  2,  5,  2 ,
			   6,  3,  3,  7,  0,  6,  0,  2,  3,  6,  0,  3 ,
			   0,  7,  3,  1,  1,  4,  1,  4,  5,  1,  4,  7 ,
			   1,  7,  3,  1,  1,  1,  3,  0,  4,  7,  7,  4 ,
			   5,  7,  6,  0,  6,  6,  3,  6,  6,  6,  7,  7 ,
			   3,  0,  4,  0,  1,  0,  7,  2,  7,  3,  4,  1 ,
			   4,  7,  1,  0,  6,  0,  4,  3,  7,  3,  3,  5
};

PixelT pixelsT5_12x8 [] = {
			   5,  3,  6,  3,  6,  5,  6,  5,  3,  0,  7,  6 ,
			   2,  7,  2,  2,  5,  4,  5,  7,  6,  2,  7,  0 ,
			   3,  1,  4,  2,  7,  3,  5,  4,  7,  4,  7,  5 ,
			   1,  5,  2,  4,  6,  2,  3,  0,  1,  5,  2,  7 ,
			   1,  7,  6,  7,  2,  5,  7,  5,  6,  3,  7,  6 ,
			   7,  5,  2,  6,  1,  2,  3,  2,  7,  5,  7,  5 ,
			   7,  2,  6,  1,  7,  0,  0,  0,  0,  6,  7,  2 ,
			   4,  6,  7,  2,  2,  7,  0,  1,  4,  3,  7,  5
};

PixelT pixelsT6_12x8 [] = {
			   1,  1,  7,  0,  2,  5,  5,  3,  2,  0,  0,  3 ,
			   3,  1,  5,  3,  2,  4,  6,  6,  0,  3,  2,  5 ,
			   0,  1,  5,  1,  1,  2,  7,  2,  3,  6,  3,  5 ,
			   3,  0,  0,  5,  0,  1,  1,  3,  2,  6,  7,  5 ,
			   2,  5,  3,  2,  0,  6,  7,  0,  7,  5,  1,  0 ,
			   7,  1,  3,  2,  7,  6,  0,  3,  6,  0,  0,  6 ,
			   1,  1,  1,  4,  7,  0,  1,  2,  5,  4,  4,  5 ,
			   2,  4,  5,  2,  1,  7,  2,  0,  0,  5,  2,  7
};

PixelT pixelsT7_12x8 [] = {
			   1,  0,  3,  1,  3,  1,  0,  1,  7,  5,  6,  1 ,
			   1,  0,  6,  3,  4,  0,  2,  4,  0,  2,  2,  1 ,
			   1,  2,  5,  1,  3,  5,  7,  4,  5,  3,  5,  1 ,
			   4,  6,  2,  4,  3,  1,  5,  4,  1,  3,  0,  5 ,
			   3,  2,  2,  3,  4,  4,  5,  5,  6,  2,  7,  2 ,
			   0,  6,  6,  5,  1,  4,  6,  6,  2,  1,  2,  5 ,
			   2,  7,  2,  3,  2,  2,  0,  5,  4,  2,  0,  0 ,
			   7,  5,  6,  5,  0,  5,  7,  0,  3,  6,  5,  5
};

// ========================================
void test () {
  Size<GeoDimT> size (4, 4);
  // Size<GeoDimT> size (6, 4);
  // Size<GeoDimT> size (18, 12);
  // Size<GeoDimT> size (12, 8);
  Border<GeoDimT> border (size, false);
  // border.setBorder (1);
  // border.setBorder (12);
  // border.setBorder (13);
  // border.setBorder (24);
  // border.setBorder (25);

  DimImg leafCount = size.getPixelsCount ();

  // ====================
  PixelT *pixels = pixels4x4_C;
  // PixelT *pixels = pixelsT1_18x12;
  // PixelT *pixels = pixelsT2_12x8;
  // PixelT *pixels = pixelsT3_12x8;
  // PixelT *pixels = pixelsT4_12x8;
  // PixelT *pixels = pixelsT5_12x8;
  // PixelT *pixels = pixelsT6_12x8;
  // PixelT *pixels = pixelsT7_12x8;
  // PixelT *pixels = new PixelT [leafCount];
  // for (int i = 0; i < leafCount; ++i)
  //   pixels[i] = std::rand() % 8;
  // ====================

  Raster<PixelT, GeoDimT> raster (size);
  for (DimImg i = 0; i < leafCount; ++i)
    raster.getPixels ()[i] = pixels[i];

  Size<GeoDimT> doubleSize (size.getDoubleHeight ());
  PixelT id[doubleSize.getPixelsCount ()];
  for (DimImg i = 0; i < doubleSize.getPixelsCount (); ++i)
    id[i] = i;
  cout << endl << "*** coreCount:" << coreCount << endl
       << "index / level / border imageSize: " << size << endl << endl
       << printMap (id, doubleSize, 0) << endl
       << printMap (pixels, size, 0) << endl
       << border << endl;

  GraphWalker<GeoDimT> graphWalker (border);
  Log::debug = true;
  // ArrayTreeBuilder<PixelT, WeightT, GeoDimT> atb (raster, graphWalker, treeType, coreCount, 1);
  ArrayTreeBuilder<PixelT, WeightT, GeoDimT> atb (raster, graphWalker, treeType, coreCount, 1, seqTile, autoThreadFlag, needChildren);
  Tree<GeoDimT> tree (coreCount);
  WeightAttributes<PixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);
  tree.check (border);

  AttributeProfiles<PixelT, GeoDimT> attributeProfiles (tree);
  atb.setAttributProfiles (attributeProfiles);
  cerr << attributeProfiles;
}

// ========================================
void test2 () {
  IImage<GeoDimT> inputImage ("data/test_img_grey.png");
  inputImage.readImage ();
  Size<GeoDimT> size (inputImage.getSize ());
  Raster<uint8_t, GeoDimT> inputRaster;
  inputImage.readBand<uint8_t> (inputRaster, 0);
  Border<GeoDimT> border (size, false);

  Size<GeoDimT> doubleSize (size.getDoubleHeight ());
  PixelT id[doubleSize.getPixelsCount ()];
  for (DimImg i = 0; i < doubleSize.getPixelsCount (); ++i)
    id[i] = i;
  cout << endl << "*** coreCount:" << coreCount << endl
       << "index / level / border imageSize: " << size << endl << endl
       << printMap (id, doubleSize, 0) << endl
       << printMap (inputRaster.getPixels (), size, 0) << endl
       << border << endl;

  GraphWalker<GeoDimT> graphWalker (border);
  Log::debug = true;
  ArrayTreeBuilder<uint8_t, uint8_t, GeoDimT> atb (inputRaster, graphWalker, treeType, coreCount, 1);
  Tree<GeoDimT> tree (coreCount);
  WeightAttributes<uint8_t, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);

  tree.check (border);

  
  AttributeProfiles<uint8_t, GeoDimT> attributeProfiles (tree);
  atb.setAttributProfiles (attributeProfiles);
  cerr << attributeProfiles;
}

// ========================================
int
main (int argc, char **argv, char **envp) {
  srand (time (NULL));
  for (unsigned int i = 0; i < nbTest ; ++i) {
    cout << endl << "========================================" << endl;
    test ();
  }
  return 0;
}

// ========================================
