////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Geo_hpp
#define _Obelix_Geo_hpp

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdint>

#include "obelixDebug.hpp"
#include "obelixExtendInt.hpp"

#ifdef IMAGE_64BITS
# define OBELIX_IMG_TYPE	uint64_t
# define OBELIX_TILE_TYPE	uint32_t
# define OBELIX_PARENT_TYPE	uint64_t
# define OBELIX_NODE_TYPE	uint64_t
# define OBELIX_IMG_STYPE	uint64_t
# define OBELIX_PARENT_STYPE	uint64_t
# define DimImg_MAX		UINT64_MAX
# define DimTile_MAX		UINT32_MAX
# define DimParent_MAX		UINT64_MAX
# define DimNode_MAX		UINT64_MAX
#else
# ifdef IMAGE_40BITS		// pixels < 1024G / parents < 1024G
#  define OBELIX_IMG_TYPE	uint64_t
#  define OBELIX_TILE_TYPE	uint32_t
#  define OBELIX_PARENT_TYPE	uint32_t
#  define OBELIX_NODE_TYPE	uint64_t
#  define OBELIX_IMG_STYPE	uint40_t
#  define OBELIX_PARENT_STYPE	uint32_t
#  define DimImg_MAX		UINT40_MAX
#  define DimTile_MAX		UINT32_MAX
#  define DimParent_MAX		UINT32_MAX
#  define DimNode_MAX		UINT40_MAX
# else
#  ifdef IMAGE_LARGE		// pixels < 20G / parents < 4G
#   define OBELIX_IMG_TYPE	uint64_t
#   define OBELIX_TILE_TYPE	uint32_t
#   define OBELIX_PARENT_TYPE	uint32_t
#   define OBELIX_NODE_TYPE	uint64_t
#   define OBELIX_IMG_STYPE	uint40_t
#   define OBELIX_PARENT_STYPE	uint32_t
#   define DimImg_MAX		UINT40_MAX
#   define DimTile_MAX		UINT32_MAX
#   define DimParent_MAX	UINT32_MAX
#   define DimNode_MAX		UINT40_MAX
#  else				// pixels < 4G / parents < 4G
#   define OBELIX_IMG_TYPE	uint32_t
#   define OBELIX_TILE_TYPE	uint32_t
#   define OBELIX_PARENT_TYPE	uint32_t
#   define OBELIX_NODE_TYPE	uint32_t
#   define OBELIX_IMG_STYPE	uint32_t
#   define OBELIX_PARENT_STYPE	uint32_t
#   define DimImg_MAX		UINT32_MAX
#   define DimTile_MAX		UINT32_MAX
#   define DimParent_MAX	UINT32_MAX
#   define DimNode_MAX		UINT32_MAX
#  endif
# endif
#endif

#define DimSideImg_MAX UINT32_MAX
#define DimChannel_MAX UINT32_MAX

namespace obelix {

  using namespace std;

  // ================================================================================
  /*! sort size type */
  typedef uint8_t DimSortCeil;

  /*! Image size type */
  typedef uint32_t DimSideImg;

  /*! Image band type */
  typedef uint32_t DimChannel; // hyperspectral > 256

  /*! Number of pixels on a tile */
  typedef OBELIX_TILE_TYPE DimTile;

  /*! Number of pixels */
  typedef OBELIX_IMG_STYPE DimImgPack;
  typedef OBELIX_IMG_TYPE DimImg;
  /*! Number of components */
  typedef OBELIX_PARENT_STYPE DimParentPack;
  typedef OBELIX_PARENT_TYPE DimParent;

  /*! Number of nodes (leaves + components) */
  typedef OBELIX_IMG_STYPE DimNodePack;
  typedef OBELIX_NODE_TYPE DimNode;

  // ================================================================================
  template<int GeoDimT>
  struct Size;

  template<int GeoDimT>
  struct Point {
    DimSideImg coord [GeoDimT];
    inline Point ();
    inline Point (const DimSideImg &x, const DimSideImg &y);
    inline Point (const DimSideImg coord[]); //[GeoDimT]);
    inline Point (const DimSideImg &x, const DimSideImg &y, const DimSideImg &z);
    Point (const Size<GeoDimT> &size, const DimImg &idx);
    Point (const Point<GeoDimT> &orig, const Size<GeoDimT> &moveVector);
    inline const Point<GeoDimT> &getNull ();
  };
  template<int GeoDimT>
  ostream &operator << (ostream &out, const Point<GeoDimT> &p);
  template<int GeoDimT>
  inline bool operator == (const Point<GeoDimT> &a, const Point<GeoDimT> &b);
  template<int GeoDimT>
  inline bool operator != (const Point<GeoDimT> &a, const Point<GeoDimT> &b);

  /*! Convertit un point d'un tableau (on peut imaginer une image à 2 dimension) en un index */
  template<int GeoDimT>
  inline DimImg point2idx (const Size<GeoDimT> &size, const Point<GeoDimT> &p);

  /*! Convertit un index d'un tableau (on peut imaginer une image à 2 dimension) en point correspondant à des coordonnées */
  template<int GeoDimT>
  inline Point<GeoDimT> idx2point (const Size<GeoDimT> &size, const DimImg &idx);

  // ================================================================================
  /*! delta-point is like a "point" but having relative coordinates (deltaX,deltaY, deltaZ */
  /* !!! WARNING 1) Works only with values -1, 0 or +1 for each component deltaX, deltaY and deltaZ */
  /* !!! WARNING 2) when adding point and delta-point, the result of each component should never be negative */
  /* If one of the previous rule is not respected, the result could be unpredictable ! */

  template<int GeoDimT>
  union DeltaPoint {
    // for each component deltaK=x,y,z, the possible values are:
    // deltaK = -1 (=0b11 using type int:2)
    // deltaK = +1 (=0b01 using type int:2)
    // deltaK =  0 (=0b00 using type int:2)
    int8_t set;
    struct {
      int	x:2;
      int	y:2;
      int	z:2;
    } delta;

    inline		DeltaPoint ();
    inline		DeltaPoint (const int &deltaX, const int &deltaY);
    inline		DeltaPoint (const int &deltaX, const int &deltaY, const int &deltaZ);
    inline		DeltaPoint (const Size<GeoDimT> &size, const DimImg &idx1, const DimImg &idx2);

    inline DimImg	addIdx (const Size<GeoDimT> &size, const DimImg &idx) const;
  };
  template<int GeoDimT>
  ostream &operator << (ostream &out, const DeltaPoint<GeoDimT> &dp);

  // ================================================================================

  template<int GeoDimT>
  struct Size {
    DimSideImg		side [GeoDimT];

    inline		Size ();
    inline		Size (const DimSideImg &w, const DimSideImg &h);
    inline		Size (const DimSideImg &w, const DimSideImg &h, const DimSideImg &l);
    inline		Size (const DimSideImg side[]); // [GeoDimT]

    DimImg		getPixelsCount () const;
    inline DimImg	getFramePixelsCount () const;
    Size<GeoDimT>	getDoubleHeight () const;
    DimImg		getOffsetFrame (const DimSideImg &frameId) const;
    inline bool		isNull () const;
  };
  template<int GeoDimT>
  inline bool operator == (const Size<GeoDimT> &a, const Size<GeoDimT> &b);
  template<int GeoDimT>
  inline bool operator != (const Size<GeoDimT> &a, const Size<GeoDimT> &b);
  template<int GeoDimT>
  ostream &operator << (ostream &out, const Size<GeoDimT> &s);

  // ================================================================================
  template<int GeoDimT>
  struct Rect {
    Point<GeoDimT>	point;
    Size<GeoDimT>	size;

    inline		Rect ();
    inline		Rect (const Point<GeoDimT> &orig, const Size<GeoDimT> &size);

    // XXX DimTile ?
    inline DimImg	relIdx (const DimImg &absIdx, const Size<GeoDimT> &fullSize) const;
    // inline DimImg relIdx (const Point &absPos) const;
    inline DimImg	absIdx (const DimTile &relIdx, const Size<GeoDimT> &fullSize) const;
    inline Point<GeoDimT>	absPt (const DimTile &relIdx) const; 

    template<typename T>
    inline void		cpToTile (const Size<GeoDimT> &size, const T *src, vector<T> &dst) const;
    template<typename T>
    inline void		cpFromTile (const Size<GeoDimT> &size, const vector<T> &src, T *dst) const;
    template<typename T>
    inline void		cpFromTileMove (const Size<GeoDimT> &size, const vector<T> &src, T *dst, const T &move) const;

    inline bool		isNull () const;
  };
  template<int GeoDimT>
  ostream &operator << (ostream &out, const Rect<GeoDimT> &r);

  // ================================================================================
  static const DimSideImg printMapMaxSide = 30;

  template <typename T, int GeoDimT>
  struct CPrintMap {
    const T		*map;
    const Size<GeoDimT>	&size;
    const DimNode	maxValues;
    inline CPrintMap (const T *map, const Size<GeoDimT> &size, const DimNode &maxValues = 0);
    inline ostream &print (ostream &out) const;
    inline void printRank (ostream &out, const int &rank, const T *&curVal, DimNode &countDown) const;
    inline ostream &print (ostream &out, const T &val) const;
  };

  template <typename T, int GeoDimT>
  inline CPrintMap<T, GeoDimT> printMap (const T *map, const Size<GeoDimT> &size, const DimNode &maxValues);

  template <typename T, int GeoDimT>
  inline ostream &operator << (ostream &out, const CPrintMap<T, GeoDimT> &cpm) { return cpm.print (out); }

  // ================================================================================

#include "obelixGeo.tpp"
} // namespace obelix

#include "obelixGeo2D.hpp"
#include "obelixGeo3D.hpp"

#endif // _Obelix_Geo_hpp
