////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/python.hpp>
//#include <boost/python/numpy.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "Tree.hpp"
#include "obelixThreads.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"

using namespace std;
using namespace boost::python;
using namespace obelix;
using namespace obelix::triskele;

static const DimCore GeoDimT (2);
static const DimCore coreCount = 3;
typedef vector<DimParentPack> parentList;

#include "PythonWrapper.hpp"

// ########################################
PythonTree *
PythonWrapper::getTreeFromArray (TreeType treeType, boost::python::list &pSize, boost::python::list &pPixels) {
  typedef uint16_t PixelT;
  typedef uint16_t WeightT;

  DimSideImg w = extract<DimSideImg> (pSize[0]);
  DimSideImg h = extract<DimSideImg> (pSize[1]);
  Size<GeoDimT> size (w, h);
  Border<GeoDimT> border (size, false);
  DimImg leafCount = size.getPixelsCount ();
  Raster<PixelT, GeoDimT> raster (size);
  for (DimImg i = 0; i < leafCount; ++i)
    raster.getPixels ()[i] = extract<PixelT> (pPixels[i]);
  GraphWalker<GeoDimT> graphWalker (border);
  ArrayTreeBuilder<PixelT, WeightT, GeoDimT> atb (raster, graphWalker, treeType, coreCount, 1);
  PythonTree *treeP = new PythonTree (coreCount);
  WeightAttributes<PixelT, GeoDimT> *weightAttributesP = new WeightAttributes<PixelT, GeoDimT> (*treeP, getDecrFromTreetype (treeType));
  atb.buildTree (*treeP, *weightAttributesP);
  return treeP;
}


template<typename InPixelT>
PythonTree *
getTreeFromImageType (TreeType treeType, IImage<GeoDimT> &inputImage) {
  DimChannel band (0);
  Raster<InPixelT, GeoDimT> raster;
  inputImage.readBand (raster, band);
  Border<GeoDimT> border (inputImage.getSize (), false);
  GraphWalker<GeoDimT> graphWalker (border);
  ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> atb (raster, graphWalker, treeType, coreCount, 1);
  PythonTree *treeP = new PythonTree (coreCount);
  WeightAttributes<InPixelT, GeoDimT> *weightAttributesP = new WeightAttributes<InPixelT, GeoDimT> (*treeP, getDecrFromTreetype (treeType));
  atb.buildTree (*treeP, *weightAttributesP);

  return treeP;
}

PythonTree *
PythonWrapper::getTreeFromImage (TreeType treeType, const string &fileName) {
  cerr << fileName << endl;
  IImage<GeoDimT> inputImage (fileName);
  PythonTree *treeP = nullptr;

  inputImage.readImage ();

  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    treeP = getTreeFromImageType<uint8_t> (treeType, inputImage); break;
  case GDT_UInt16:
    treeP = getTreeFromImageType<uint16_t> (treeType, inputImage); break;
  case GDT_Int16:
    treeP = getTreeFromImageType<int16_t> (treeType, inputImage); break;
  case GDT_UInt32:
    treeP = getTreeFromImageType<uint32_t> (treeType, inputImage); break;
  case GDT_Int32:
    treeP = getTreeFromImageType<int32_t> (treeType, inputImage); break;
  case GDT_Float32:
    treeP = getTreeFromImageType<float> (treeType, inputImage); break;
  case GDT_Float64:
    treeP = getTreeFromImageType<double> (treeType, inputImage); break;

  default :
    cerr << "unknown type!" << endl; break;
  }
  return treeP;
}


// ########################################
BOOST_PYTHON_MODULE(libtriskeledebug) {
#include "PythonWrapper.tcpp"
}


BOOST_PYTHON_MODULE(libtriskele) {
#include "PythonWrapper.tcpp"
}

// ########################################
