////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>

#include "obelixDebug.hpp"
#include "obelixThreads.hpp"
#include "CompAttribute.hpp"

#include "obelixGeo.hpp"
#include "Attributes/Centroid.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "CompAttribute.tcpp"

// ================================================================================
const string obelix::triskele::attributeLabels[] =
  {
   "Area", "Perimeter", "zLength", "STS", "Compactness", "Complexity", "Simplicity", "Rectangularity", "Weight", "SD", "SDW", "MoI" // "SDA"
  };
const map<string, AttributeType>
obelix::triskele::attributeMap = boost::assign::map_list_of
  ("area", AArea)
  ("perimeter", APerimeter)
  ("zlength", AZLength)
  ("sts", ASTS)
  ("compactness", ACompactness)
  ("complexity", AComplexity)
  ("simplicity", ASimplicity)
  ("rectangularity", ARectangularity)
  ("weight", AWeight)
  ("sd", ASD)
  ("sdw", ASDW)
  // ("sda", ASDA)
  ("moi", AMoI)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const AttributeType &attributeType) {
  BOOST_ASSERT (attributeType >= AArea && attributeType <= AMoI);
  return out << attributeLabels[attributeType];
}
istream &
obelix::triskele::operator >> (istream &in, AttributeType &attributeType) {
  string token;
  in >> token;
  auto pos = attributeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == attributeMap.end ())
    in.setstate (ios_base::failbit);
  else
    attributeType = pos->second;
  return in;
}

// ================================================================================
const string obelix::triskele::pruningLabels[] = {
						  "Direct", "Min", "Max"
};
const map<string, PruningStrategy>
obelix::triskele::pruningMap = boost::assign::map_list_of
  ("direct", Direct)
  ("min", Min)
  ("max", Max)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const PruningStrategy &pruningStrategy) {
  BOOST_ASSERT (pruningStrategy >= Direct && pruningStrategy <= Max);
  return out << pruningLabels[pruningStrategy];
}
istream &
obelix::triskele::operator >> (istream &in, PruningStrategy &pruningStrategy) {
  string token;
  in >> token;
  auto pos = pruningMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == pruningMap.end ())
    in.setstate (ios_base::failbit);
  else
    pruningStrategy = pos->second;
  return in;
}

// ================================================================================
const string
obelix::triskele::featureTypeLabels[] =
  {
   "AP", "Area", "Perimeter", "zLength", "STS", "Compactness", "Complexity", "Simplicity", "Rectangularity", "Min", "Max", "Mean", "SD", "SDW", "MoI" // "SDA"
  };
const map<string, FeatureType>
obelix::triskele::featureTypeMap = boost::assign::map_list_of
  ("ap", FAP)
  ("area", FArea)
  ("perimeter", FPerimeter)
  ("zlength", FZLength)
  ("sts", FSTS)
  ("compactness", FCompactness)
  ("complexity", FComplexity)
  ("simplicity", FSimplicity)
  ("rectangularity", FRectangularity)
  ("min", FMin)
  ("max", FMax)
  ("mean", FMean)
  ("sd", FSD)
  ("sdw", FSDW)
  // ("sda", FSDA)
  ("moi", FMoI)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const FeatureType &featureType) {
  BOOST_ASSERT (featureType >= FAP && featureType <= FMoI);
  return out << featureTypeLabels[featureType];
}
istream &
obelix::triskele::operator >> (istream &in, FeatureType &featureType) {
  string token;
  in >> token;
  auto pos = featureTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == featureTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    featureType = pos->second;
  return in;
}

// ========================================
template<typename AttrT, int GeoDimT>
vector<AttrT>
CompAttribute<AttrT, GeoDimT>::getScaledThresholds (const vector<double> &thresholds, const AttrT &maxValue) {
  vector<AttrT> result;
  for (double percent : thresholds)
    result.push_back (percent*double (maxValue));
  return result;
}

template<typename AttrT, int GeoDimT>
vector<AttrT>
CompAttribute<AttrT, GeoDimT>::getConvertedThresholds (const vector<double> &thresholds) {
  vector<AttrT> result;
  for (double value : thresholds)
    result.push_back ((AttrT) value);
  return result;
}

// ========================================
template<typename AttrT, int GeoDimT>
CompAttribute<AttrT, GeoDimT>::CompAttribute (const Tree<GeoDimT> &tree, const bool &decr)
  : tree (tree),
    leafCount (0),
    compCount (0),
    values (0),
    decr (decr)
{
  DEF_LOG ("CompAttribute::CompAttribute", "decr: " << decr);
  updateTranscient ();
}

template<typename AttrT, int GeoDimT>
CompAttribute<AttrT, GeoDimT>::~CompAttribute () {
  DEF_LOG ("CompAttribute::~CompAttribute", "");
}

// ================================================================================
template<typename AttrT, int GeoDimT>
void
CompAttribute<AttrT, GeoDimT>::save (HDF5 &hdf5, const string &compLabel) const {
  DEF_LOG ("CompAttribute::save", "");
  hdf5.write_array (values, compLabel,"/");
}

template<typename AttrT, int GeoDimT>
void
CompAttribute<AttrT, GeoDimT>::load (HDF5 &hdf5, const string &compLabel) {
  DEF_LOG ("CompAttribute::load", "");
  hdf5.read_array (values, compLabel,"/");
}

// ================================================================================
template<typename AttrT, int GeoDimT>
void
CompAttribute<AttrT, GeoDimT>::updateTranscient () {
  DEF_LOG ("CompAttribute::updateTranscient", "");
  leafCount = tree.getLeafCount ();
  book (tree.getCompCount ());
}

template<typename AttrT, int GeoDimT>
const AttrT *
CompAttribute<AttrT, GeoDimT>::getValues () const {
  return values.data ();
}

template<typename AttrT, int GeoDimT>
AttrT *
CompAttribute<AttrT, GeoDimT>::getValues () {
  return values.data ();
}

template<typename AttrT, int GeoDimT>
AttrT
CompAttribute<AttrT, GeoDimT>::getMaxValue () const {
  if (!compCount)
    return 0;
  AttrT max = values[0];
  CompAttribute<AttrT, GeoDimT>::tree.forEachComp ([this, &max] (const DimParent &compId) {
						 if (values[compId] > max)
						   max = values[compId];
					       });
  return max;
}

// ========================================
template<typename AttrT, int GeoDimT>
template<typename PixelT>
void
CompAttribute<AttrT, GeoDimT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, const vector<AttrT> &thresholds, const PruningStrategy &pruningStrategy) const {
  DEF_LOG ("CompAttribute::cut", "coreCount:" << tree.getCoreCount () << " thresholds:" << thresholds.size ());
  auto start = high_resolution_clock::now ();
  const vector<AttrT> *pValues (&values);
  vector<AttrT> modifiedValues;
  switch (pruningStrategy) {
  case Min: {
    LOG ("cutting with min pruning rule");
    modifiedValues = values;
    tree.forEachCompFromRoot ([this, &modifiedValues] (const DimParent &compId) {
				const DimParent parentId (tree.getCompParent (compId));
				if (parentId == DimParent_MAX)
				  return;
				if (modifiedValues [parentId] > modifiedValues [compId])
				  modifiedValues [parentId] = modifiedValues [compId];
			      });
    pValues = &modifiedValues;
    break;
  }
  case Max: {
    LOG ("cutting with max pruning rule");
    modifiedValues = values;
    tree.forEachComp ([this, &modifiedValues] (const DimParent &compId) {
			const DimParent parentId (tree.getCompParent (compId));
			if (parentId == DimParent_MAX)
			  return;
			if (modifiedValues [parentId] < modifiedValues [compId])
			  modifiedValues [parentId] = modifiedValues [compId];
		      });
    pValues = &modifiedValues;
    break;
  }
  case Direct: {
    LOG ("cutting with direct pruning rule");
    break;
  }
  default:
    break;
  }

  dealThread (leafCount, tree.getCoreCount (),
	      [this, &allBands, &attributeProfiles, &thresholds, &pValues] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
		for (DimImg leafId = minVal; leafId < maxVal; ++leafId)
		  cutOnPos (allBands, attributeProfiles, leafId, thresholds, *pValues);
	      });
  // for (DimImg leafId = 0; leafId < leafCount; ++leafId)
  //   cutOnPos (allBands, attributeProfiles, leafId, thresholds);
  TreeStats::global.addTime (filteringStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

template<typename AttrT, int GeoDimT>
template<typename PixelT>
void
CompAttribute<AttrT, GeoDimT>::cutOnPos (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles,
				     const DimImg &leafId, const vector<AttrT> &thresholds, const vector<AttrT> &values) const {
  // no log (to many pixels)
  const PixelT *apValues = attributeProfiles.getValues ();
  const PixelT *compApValues = apValues+leafCount;
  const DimParent rootId = tree.getRootId ();
  const PixelT rootAp =  compApValues [rootId];
  const DimChannel thresholdsSize = thresholds.size ();
  DimParent parentId = tree.getLeafParent (leafId);
  if (parentId == DimParent_MAX) {
    // border case
    for (DimChannel channel = 0; channel < thresholdsSize; ++channel)
      allBands[channel][leafId] = rootAp;
    return;
  }
  bool managePixel = true;
  AttrT pixelAtt = getPixelValue (leafId);
  DimParent curId = parentId;
  AttrT curAtt = values [curId];
  PixelT curAp = compApValues [curId];
  for (DimChannel channel = 0; channel < thresholdsSize; ++channel) {
    AttrT ceil = thresholds[channel];
    if (managePixel) {
      if ((decr && !(ceil < pixelAtt)) ||
	  (!decr && !(pixelAtt < ceil))) {
	allBands[channel][leafId] = apValues[leafId];
	continue;
      }
      managePixel = false;
    }
    for ( ; curId <= rootId; ) {
      if ((decr && !(ceil < curAtt)) ||
	  (!decr && !(curAtt < ceil)))
	break;
      if (parentId == DimParent_MAX || parentId == rootId) {
	// XXX cerr << "CompAttribute::cutOnPos find " << (parentId == DimParent_MAX ? "(DimParent_MAX) " : "") << "sub-root:" << rootId << " rootId:" << rootId << endl;
	// find root
	for (; channel < thresholdsSize; ++channel)
	  allBands[channel][leafId] = rootAp;
	return;
      }
      curId = parentId;
      curAtt = values [curId];
      curAp = compApValues [curId];
      parentId = tree.getCompParent (curId);
    }
    allBands[channel][leafId] = curAp;
  }
}

// ========================================
template<typename AttrT, int GeoDimT>
void
CompAttribute<AttrT, GeoDimT>::free () {
  values = vector<AttrT> ();
}

template<typename AttrT, int GeoDimT>
void
CompAttribute<AttrT, GeoDimT>::book (const DimParent &compCount) {
  this->compCount = compCount;
  values.resize (compCount);
}

// ================================================================================
