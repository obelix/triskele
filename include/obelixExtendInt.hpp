////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_ExtendInt_hpp
#define _Obelix_ExtendInt_hpp

#include <stdint.h>
#include <iostream>
#include <boost/assert.hpp>

#define UINT40_MAX		(__UINT64_C(1099511627775))

namespace obelix {
  // ================================================================================
  using namespace std;

  class uint40_t {
  protected:
    uint8_t bytes[5];
  public:
    uint40_t () { bytes[4] = bytes[3] = bytes[2] = bytes[1] = bytes[0] = 0U; }
    uint40_t (const uint64_t val) { *this = val; }
    uint40_t (const uint40_t &val) { *this = val; }

    friend ostream &operator << (ostream &out, const uint40_t &val) {
      return out << (uint64_t)val;
    }
    friend istream &operator >> (istream &in, uint40_t &val) {
      uint64_t tmp (0);
      in >> tmp;
      val = tmp;
      return in;
    }
    operator double () const {
      return (double) this->operator uint64_t ();
    }
    operator float () const {
      return (float) this->operator uint64_t ();
    }
    operator uint64_t () const {
      return
	(((uint64_t) bytes[0] & 0xFFL) << 0) |
	(((uint64_t) bytes[1] & 0xFFL) << 8) |
	(((uint64_t) bytes[2] & 0xFFL) << 16) |
	(((uint64_t) bytes[3] & 0xFFL) << 24) |
	(((uint64_t) bytes[4] & 0xFFL) << 32);
    }
    operator uint32_t () const {
      BOOST_ASSERT (bytes[4] == 0);
      return
	(((uint64_t) bytes[0] & 0xFFL) << 0) |
	(((uint64_t) bytes[1] & 0xFFL) << 8) |
	(((uint64_t) bytes[2] & 0xFFL) << 16) |
	(((uint64_t) bytes[3] & 0xFFL) << 24);
    }
    operator int32_t () const {
      return (int32_t) this->operator uint64_t ();
    }
    operator uint16_t () const {
      BOOST_ASSERT (bytes[2] == 0 && bytes[3] == 0 && bytes[4] == 0);
      return
	(bytes[0] << 0) |
	(bytes[1] << 8);
    }
    operator int16_t () const {
      return  (int16_t) this->operator uint16_t ();
    }
    operator uint8_t () const {
      BOOST_ASSERT (bytes[1] == 0 && bytes[2] == 0 && bytes[3] == 0 && bytes[4] == 0);
      return
	bytes[1] << 8;
    }

    uint40_t &operator= (const uint40_t &right) {
      bytes[0] = right.bytes[0];
      bytes[1] = right.bytes[1];
      bytes[2] = right.bytes[2];
      bytes[3] = right.bytes[3];
      bytes[4] = right.bytes[4];
      return *this;
    }
    uint40_t &operator= (const uint64_t right) {
      bytes[0] = (uint8_t) right;
      bytes[1] = (uint8_t) (right >> 8);
      bytes[2] = (uint8_t) (right >> 16);
      bytes[3] = (uint8_t) (right >> 24);
      bytes[4] = (uint8_t) (right >> 32);
      return *this;
    }
    uint40_t &operator++ () {
      if (!++bytes[0])
	if (!++bytes[1])
	  if (!++bytes[2])
	    if (!++bytes[3])
	      if (!++bytes[4])
		BOOST_ASSERT (false);
      return *this;
    }
    uint40_t  operator++ (int) {
      uint40_t result (*this);
      ++(*this);
      return result;
    }
    uint40_t &operator-- () {
      if (!bytes[0]--) {
	BOOST_ASSERT (bytes[0] == 0xFF);
	if (!bytes[1]--) {
	  BOOST_ASSERT (bytes[1] == 0xFF);
	  if (!bytes[2]--) {
	    BOOST_ASSERT (bytes[2] == 0xFF);
	    if (!bytes[3]--) {
	      BOOST_ASSERT (bytes[3] == 0xFF);
	      if (!bytes[4]--)
		BOOST_ASSERT (false);
	    }
	  }
	}
      }
      return *this;
    }
    uint40_t  operator-- (int) {
      uint40_t result (*this);
      --(*this);
      return result;
    }

    uint64_t operator+ (const uint64_t val) const {
      BOOST_ASSERT ((uint64_t)*this + val <= UINT40_MAX);
      return (uint64_t)*this + val;
    }

    uint64_t operator- (const uint64_t &val) const {
      BOOST_ASSERT (val <= (uint64_t)*this);
      return (uint64_t)*this - val;
    }
    uint64_t operator- (const uint40_t &val) const {
      BOOST_ASSERT ((uint64_t)val <= (uint64_t)*this);
      return (uint64_t)*this - (uint64_t)val;
    }
    uint64_t operator- (const uint32_t &val) const {
      BOOST_ASSERT (val <= (uint64_t)*this);
      return (uint64_t)*this - val;
    }
    uint64_t operator- (const uint16_t &val) const {
      BOOST_ASSERT (val <= (uint64_t)*this);
      return (uint64_t)*this - val;
    }
    uint64_t operator- (const uint8_t &val) const {
      BOOST_ASSERT (val <= (uint64_t)*this);
      return (uint64_t)*this - val;
    }

    uint64_t operator* (const uint64_t &val) const {
      return (uint64_t)*this * val;
    }
    uint64_t operator* (const uint32_t &val) const {
      return (uint64_t)*this * val;
    }
    uint64_t operator* (const uint16_t &val) const {
      return (uint64_t)*this * val;
    }
    uint64_t operator* (const uint8_t &val) const {
      return (uint64_t)*this * val;
    }

    uint64_t operator/ (const uint64_t val) const {
      return (uint64_t)*this / val;
    }
    uint64_t operator/ (const uint32_t val) const {
      return (uint64_t)*this / val;
    }
    uint64_t operator/ (const uint16_t val) const {
      return (uint64_t)*this / val;
    }
    uint64_t operator/ (const uint8_t val) const {
      return (uint64_t)*this / val;
    }

    uint40_t &operator+= (const uint64_t val) {
      BOOST_ASSERT ((uint64_t)*this + (uint64_t)val <= UINT40_MAX);
      return *this = (uint64_t)*this + val;
    }

    operator bool () const {
      return
	bytes[0] != 0 ||
	bytes[1] != 0 ||
	bytes[2] != 0 ||
	bytes[3] != 0 ||
	bytes[4] != 0;
    }
    bool operator! () const {
      return
	bytes[0] == 0 &&
	bytes[1] == 0 &&
	bytes[2] == 0 &&
	bytes[3] == 0 &&
	bytes[4] == 0;
    }

    bool operator== (const uint40_t &right) const {
      return
	bytes[0] == right.bytes[0] &&
        bytes[1] == right.bytes[1] &&
        bytes[2] == right.bytes[2] &&
        bytes[3] == right.bytes[3] &&
        bytes[4] == right.bytes[4];
    }
    bool operator!= (const uint40_t &right) const {
      return
	bytes[0] != right.bytes[0] ||
        bytes[1] != right.bytes[1] ||
        bytes[2] != right.bytes[2] ||
        bytes[3] != right.bytes[3] ||
        bytes[4] != right.bytes[4];
    }
    bool operator>= (const uint40_t &right) const {
      return
	!(bytes[4] < right.bytes[4] ||
	  bytes[3] < right.bytes[3] ||
	  bytes[2] < right.bytes[2] ||
	  bytes[1] < right.bytes[1] ||
	  bytes[0] < right.bytes[0]);
    }
    bool operator<= (const uint40_t &right) const {
      return
	!(bytes[4] > right.bytes[4] ||
	  bytes[3] > right.bytes[3] ||
	  bytes[2] > right.bytes[2] ||
	  bytes[1] > right.bytes[1] ||
	  bytes[0] > right.bytes[0]);
    }
    bool operator> (const uint40_t &val) const {
      return !(*this <= val);
    }
    bool operator< (const uint40_t &val) const {
      return !(*this >= val);
    }

    bool operator== (const uint64_t val) const {
      return (uint64_t)*this == val;
    }
    bool operator!= (const uint64_t val) const {
      return (uint64_t)*this != val;
    }
    bool operator>= (const uint64_t val) const {
      return (uint64_t)*this >= val;
    }
    bool operator<= (const uint64_t val) const {
      return (uint64_t)*this <= val;
    }
    bool operator> (const uint64_t val) const {
      return ((uint64_t)*this) > val;
    }
    bool operator< (const uint64_t val) const {
      return (uint64_t)*this < val;
    }

    friend bool operator< (const uint32_t &left, const uint40_t &right) {
      return left < (uint64_t) right;
    }
  };

  // ================================================================================
} // obelix

#endif //_Obelix_ExtendInt_hpp
