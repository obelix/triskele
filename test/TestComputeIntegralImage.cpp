////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>

#include "obelixGeo.hpp"
#include "Tree.hpp"
#include "IntegralImage.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace obelix;
using namespace obelix::triskele;

typedef uint16_t PixelT;

static bool show (false); // log or not

PixelT pixelsT5_12x8_ones [] =
  {
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  };


bool test_integralImage()
{
  Size<2> size (12, 8);

  PixelT *pixels_ones = pixelsT5_12x8_ones;
  if (show)
    cout << "==> pixels_ones (image avec seulement des 1)" << endl
	 << printMap (pixels_ones, size, 0) << endl;

  DimImg pixelCount (size.getPixelsCount());
  if (show)
    cout << "==> pixelCount=" << pixelCount << endl;

  vector<double> output(pixelCount,0.);  // initialize with 0.
  if (show)
    cout << "==> output (before calling computeIntegralImage)" << endl
	 << printMap(output.data (), size, 0) << endl;

  // Compute the integral image
  computeIntegralImage (size, pixels_ones, output.data ());

  if (show)
    cout << "==> output (after calling computeIntegralImage)" << endl
	 << printMap(output.data (), size, 0) << endl;

  // Check that for the pixelsT5_12x8_ones input image (with only 1. values),
  // the integral image at (ii,jj) location corresponds to the area (ii+1)*(jj+1)
  if (show)
    cout << "==> Checking the results" << endl;
  int ii(0), jj(0), cpt(0);
  bool testValid(true);
  for (auto iter = output.begin (); iter != output.end (); ++iter) {
    ii = cpt % size.side[0];
    jj = (cpt - ii)/size.side [0];
    testValid = testValid && (output[cpt] == (ii+1)*(jj+1));
    if (show)
      cout << *iter << ":" << "(" << ii << "," << jj << ") -> " << (output[cpt] == (ii+1)*(jj+1)) << ' ';
    cpt++;
    if (show && cpt % size.side [0] == 0)
      cout << endl;
    if (show)
      cout << endl;
  }

  return testValid;
}

//int main () { cout << "Test result : " << test_integralImage() << endl; }

TEST_CASE("Testing computeIntegralImage...") {
  REQUIRE( test_integralImage() == true );
}
