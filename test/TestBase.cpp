////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "obelixDebug.hpp"
#include "misc.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main () */
#include <catch.hpp>

using namespace std;
using namespace obelix;

typedef uint16_t PixelT;
static bool show (false); // log or not

// ================================================================================
TEST_CASE ("Testing point2idx") {
  if (show)
    cout << endl
	 << "TestBase" << endl
	 << endl;

  vector<DimImg> testW = {1, 3, 5, 7};
  vector<DimImg> testH = {1, 3, 5, 7};
  vector<DimChannel> testL = {1, 3, 5, 7};
  for (DimImg w : testW)
    for (DimImg h : testH)
      for (DimChannel l : testL) {
	Size<3> size (w, h, l);
	Raster<PixelT, 3> raster (size);
	const DimImg pixelsCount = size.getPixelsCount ();
	REQUIRE (pixelsCount == w*h*l);
	for (DimImg i = 0; i < pixelsCount; ++i)
	  raster.getPixels ()[i] = i;

	for (DimImg z = 0; z < size.side [2]; ++z)
	  for (DimImg y = 0; y < size.side [1]; ++y)
	    for (DimChannel x = 0; x < size.side [0]; ++x) {
	      Point<3> p (x, y, z);
	      DimImg idx = point2idx (size, p);
	      REQUIRE (idx == raster.getPixels ()[idx]);
	      Point<3> p2 = idx2point (size, idx);
	      REQUIRE (p == p2);
	    }
      }
}

// ================================================================================
TEST_CASE ("Testing Rect...") {
  Size<3> size (19, 17, 5);
  Raster<PixelT, 3> raster (size);
  const DimImg pixelsCount = size.getPixelsCount ();
  REQUIRE (pixelsCount == 19*17*5);
  for (DimImg i = 0; i < pixelsCount; ++i)
    raster.getPixels ()[i] = i;

  vector<DimImg> testW = {1, 3, 5, 7};
  vector<DimImg> testH = {1, 3, 5, 7};
  vector<DimChannel> testL = {1, 3};
  for (Point<3> orig :  { Point<3> (0, 0, 0), Point<3> (1, 1, 1) })
    for (DimImg w : testW)
      for (DimImg h : testH)
	for (DimChannel l : testL) {
	  Size<3> rectSize (w, h, l);
	  Rect<3> rect (orig, rectSize);
	  DimImg relIdx = 0;
	  for (DimImg z = 0; z < rectSize.side [2]; ++z)
	    for (DimImg y = 0; y < rectSize.side [1]; ++y)
	      for (DimChannel x = 0; x < rectSize.side [0]; ++x, ++relIdx) {
		Point<3> p (orig, Size<3> (x, y, z));
		//cout << p << endl;
		DimImg absIdx = point2idx (size, p);
		REQUIRE (absIdx == raster.getPixels ()[absIdx]);
		REQUIRE (rect.relIdx (absIdx, size) == relIdx);
		REQUIRE (rect.absIdx (relIdx, size) == absIdx);
	      }
	}
}

