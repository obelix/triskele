////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_ArrayBase_hpp
#define _Obelix_Triskele_ArrayBase_hpp

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <boost/assert.hpp>
#include <climits>

#include "obelixGeo.hpp"
#include "Tree.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    template<int GeoDimT>
    class GraphWalker;
    using namespace std;

    /*! Nombre de voisins */
    typedef DimImg DimEdge;		// ~4*nbPixels (8-connectivity) only per tile tile >! 1

    /*! Type de connectivité possible pour le voisinage */
    enum Connectivity { C1 = 0, C4 = 1, C6P = 2, C6N = 4, C8 = (C4|C6P|C6N), CT = 8, CTP = 16, CTN = 32};
    inline Connectivity operator& (Connectivity a, Connectivity b) { return (Connectivity)((int)a & (int)b); }
    inline Connectivity operator| (Connectivity a, Connectivity b) { return (Connectivity)((int)a | (int)b); }
    /*! Définit de quel type est la "sélection" */
    enum TileShape { Volume, Horizontal, Vertical, Plane };

    inline size_t getNeighborsNumber (const Connectivity &connectivity);
    inline bool getDecrFromTreetype (const TreeType &treeType);

    /*! Définit les noms des tileShape (utile pour le debug) */
    extern const string tileShapeLabels[];
    extern const map<string, TileShape> tileShapeMap;

    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const Connectivity &c);
    istream &operator >> (istream &in, Connectivity &c);
    /*! Opérateur de flux sur les TileShape */
    ostream &operator << (ostream &out, const TileShape &tileShape);
    istream &operator >> (istream &in, TileShape &tileShape);

    // ================================================================================
    template <typename WeightT, int GeoDimT> struct Edge {
      // point A
      DimImg		idxA;
      // difference between point A and B (instead of point B)
      DeltaPoint<GeoDimT>	deltaAB;
      WeightT		weight;
      /* accessors/mutators */
      inline DimImg	getA () const;
      inline DimImg	getB (const Size<GeoDimT> &size) const;
      inline WeightT	getWeight () const;

      inline void	setAB (const Size<GeoDimT> &size, const DimImg &idxa, const DimImg &idxb);
      inline void	setWeight (const WeightT &w);
    };

    /*! Effectue l'échange entre 2 Edge */
    template <typename WeightT, int GeoDimT>
    inline void swapEdge (Edge<WeightT, GeoDimT> &a, Edge<WeightT, GeoDimT> &b);

    /*! Opérateur de flux sur les voisins */
    template <typename WeightT, int GeoDimT>
    struct CPrintEdge {
      const Edge<WeightT, GeoDimT> &edge;
      const Size<GeoDimT> &size;
      inline CPrintEdge (const Edge<WeightT, GeoDimT> &edge, const Size<GeoDimT> &size);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT, int GeoDimT>
    inline CPrintEdge<WeightT, GeoDimT> printEdge (const Edge<WeightT, GeoDimT> &edge, const Size<GeoDimT> &size);

    template <typename WeightT, int GeoDimT>
    inline ostream &operator << (ostream &out, const CPrintEdge<WeightT, GeoDimT> &cpe) { return cpe.print (out); }

    template <typename WeightT, int GeoDimT>
    struct CPrintEdges {
      const Edge<WeightT, GeoDimT> *edges;
      const Size<GeoDimT> &size;
      const DimEdge edgesCount;
      inline CPrintEdges (const Edge<WeightT, GeoDimT> edges[], const Size<GeoDimT> &size, const DimEdge edgesCount);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT, int GeoDimT>
    inline CPrintEdges<WeightT, GeoDimT> printEdges (const Edge<WeightT, GeoDimT> edges[], const Size<GeoDimT> &size, const DimEdge edgesCount);

    template <typename WeightT, int GeoDimT>
    inline ostream &operator << (ostream &out, const CPrintEdges<WeightT, GeoDimT> &cpe) { return cpe.print (out); }

    // ================================================================================
    template <typename WeightT> struct TileEdge {
      DimTile indexes[2];
      WeightT weight;
    };

    /*! Effectue l'échange entre 2 Edge */
    template <typename WeightT>
    inline void swapEdge (TileEdge<WeightT> &a, TileEdge<WeightT> &b);

    /*! Opérateur de flux sur les voisins */
    template <typename WeightT, int GeoDimT>
    struct CPrintTileEdge {
      const TileEdge<WeightT> &tileEdge;
      const Size<GeoDimT> &imgSize;
      const Rect<GeoDimT> &tile;
      inline CPrintTileEdge (const TileEdge<WeightT> &tileEdge, const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT, int GeoDimT>
    inline CPrintTileEdge<WeightT, GeoDimT> printTileEdge (const TileEdge<WeightT> &tileEdge, const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile);

    template <typename WeightT, int GeoDimT>
    inline ostream &operator << (ostream &out, const CPrintTileEdge<WeightT, GeoDimT> &cpte) { return cpte.print (out); }

    template <typename WeightT, int GeoDimT>
    struct CPrintTileEdges {
      const TileEdge<WeightT> *tileEdges;
      const Size<GeoDimT> &imgSize;
      const Rect<GeoDimT> &tile;
      const DimEdge tileEdgesCount;
      inline CPrintTileEdges (const TileEdge<WeightT> tileEdges[], const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile, const DimEdge tileEdgesCount);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT, int GeoDimT>
    inline CPrintTileEdges<WeightT, GeoDimT> printTileEdges (const TileEdge<WeightT> tileEdges[], const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile, const DimEdge tileEdgesCount);

    template <typename WeightT, int GeoDimT>
    inline ostream &operator << (ostream &out, const CPrintTileEdges<WeightT, GeoDimT> &cpte) { return cpte.print (out); }

    // ================================================================================
#include "triskeleArrayTreeBase.tpp"
  } // triskele
} // obelix


#endif // _Obelix_Triskele_ArrayBase_hpp
