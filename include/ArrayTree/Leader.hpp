////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_Leader_hpp
#define _Obelix_Triskele_Leader_hpp

#include <memory>
#include <vector>
#include <boost/assert.hpp>

#include "obelixGeo.hpp"
#include "triskeleArrayTreeBase.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class Leader {
    private:

      /*! Taille de l'image (donc des tableaux leaderSetSize et leader) */
      DimTile size;

      /*! Tableau des leaders, chaque case contient une référence vers principal leader connu */
      vector<DimTile> leaders;
      /*! speed up operator[] of leaders */
      DimTile *leadersP;

    public:
      Leader (DimTile vertexCount = 0);
      ~Leader ();

      /*! Remet à 0 et redéfinit la taille des tableaux */
      void book (DimTile vertexCount);

      /*! Libère la mémoire allouée par les tableaux et met size à 0 */
      void free ();

      /*! Cherche le leaders du pixel a, Si a n'en a pas, cela retourne a */
      inline DimTile	find (DimTile a) const;

      /*! Rédéfinit les leaders : a et tous les leaders de a ont pour leader r */
      inline void	link (DimTile a, const DimTile &r);

      DimTile *getLeaders ();
    };

    // ================================================================================
#include "Leader.tpp"

  } // triskele
} // obelix


#endif // _Obelix_Triskele_Leader_hpp
