////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "obelixDebug.hpp"
#include "IImage.hpp"

#include <limits> // numeric_limits<>::min () numeric_limits<>::max ()

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "obelixNdvi.hpp"
#include "obelixNdvi.tcpp"

// ================================================================================
template<typename PixelT, int GeoDimT>
void
obelix::computeNdvi (const Raster<PixelT, GeoDimT> &inputRaster, Raster<PixelT, GeoDimT> &ndviRaster, const bool &minChannel, const bool &reverse) {
  DEF_LOG ("obelix::computeNdvi", "minChannel: " << minChannel << " reverse:" << reverse);
  DimImg pixelCount = inputRaster.getSize ().getPixelsCount ();
  LOG ("input: " << inputRaster.getSize () << " ndvi: " << ndviRaster.getSize () << " pixelCount: " << pixelCount);
  BOOST_ASSERT (inputRaster.getSize () == ndviRaster.getSize ());
  const PixelT *inputPixels (inputRaster.getPixels ());
  PixelT *ndviPixels (ndviRaster.getPixels ());

  if (minChannel) {
    LOG ("minChannel");
    for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId, ++inputPixels, ++ndviPixels)
      *ndviPixels = *inputPixels;
    return;
  }
  double halfMaxPT = double (numeric_limits<PixelT>::max ()/2) - double (numeric_limits<PixelT>::min ()/2);
  double minPT = double (numeric_limits<PixelT>::min ());

  LOG ("maxChannel halfMaxPT:" << halfMaxPT << " minPT:" << minPT);
  for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId, ++inputPixels, ++ndviPixels) {
    double q = double (*ndviPixels) + double (*inputPixels);
    if (q == 0.) {
      *ndviPixels = 0;
      continue;
    }
    q = (double (*ndviPixels) - double (*inputPixels)) / q;
    if (reverse)
      q = -q;
    *ndviPixels = PixelT ((q+1.)*halfMaxPT+minPT);
  }
}

// ================================================================================
