////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_ArrayBase_tpp
#define _Obelix_Triskele_ArrayBase_tpp

inline size_t
getNeighborsNumber (const Connectivity &connectivity) {
  size_t result = 0;
  if (connectivity | C4)
    result += 4;
  if (connectivity | C6P)
    result += 2;
  if (connectivity | C6N)
    result += 2;
  if (connectivity | CT)
    result += 2;
  if (connectivity | CTP)
    result += 8;
  if (connectivity | CTN)
    result += 8;
  return result;
}

inline bool
getDecrFromTreetype (const TreeType &treeType) {
  switch (treeType) {
  case MIN: return false;
  default: return true;
  }
}

// ================================================================================
template <typename WeightT, int GeoDimT>
inline void
swapEdge (Edge<WeightT, GeoDimT> &a, Edge<WeightT, GeoDimT> &b) {
  Edge<WeightT, GeoDimT> c = a;
  a = b;
  b = c;
}

// ================================================================================
template <typename WeightT, int GeoDimT>
inline
CPrintEdge<WeightT, GeoDimT>::CPrintEdge (const Edge<WeightT, GeoDimT> &edge, const Size<GeoDimT> &size)
  : edge (edge),
    size (size) {
}

template <typename WeightT, int GeoDimT>
inline ostream &
CPrintEdge<WeightT, GeoDimT>::print (ostream &out) const {
  // XXX if WeightT == uint8_t => print char :-(
  return out << idx2point (size, edge.getA ()) << ":"
	     << idx2point (size, edge.getB (size)) << ":"
	     << edge.getWeight () << ":"
	     << edge.getA () << ":" << edge.getB (size);
}

template <typename WeightT, int GeoDimT>
inline CPrintEdge<WeightT, GeoDimT>
printEdge (const Edge<WeightT, GeoDimT> &edge, const Size<GeoDimT> &size) {
  return CPrintEdge<WeightT, GeoDimT> (edge, size);
}

template <typename WeightT, int GeoDimT>
inline
CPrintEdges<WeightT, GeoDimT>::CPrintEdges (const Edge<WeightT, GeoDimT> edges[], const Size<GeoDimT> &size, const DimEdge edgesCount)
  : edges (edges),
    size (size),
    edgesCount (edgesCount) {
}

template <typename WeightT, int GeoDimT>
inline
ostream &
CPrintEdges<WeightT, GeoDimT>::print (ostream &out) const {
  for (DimEdge i = 0; i < edgesCount; ++i)
    out << printEdge (edges[i], size) << endl;
  return out;
}

template <typename WeightT, int GeoDimT>
inline CPrintEdges<WeightT, GeoDimT>
printEdges (const Edge<WeightT, GeoDimT> edges[], const Size<GeoDimT> &size, const DimEdge edgesCount) {
  return CPrintEdges<WeightT, GeoDimT> (edges, size, edgesCount);
}

// ================================================================================
template <typename WeightT>
inline void
swapEdge (TileEdge<WeightT> &a, TileEdge<WeightT> &b) {
  TileEdge<WeightT> c = a;
  a = b;
  b = c;
}

// ================================================================================
template <typename WeightT, int GeoDimT>
inline
CPrintTileEdge<WeightT, GeoDimT>::CPrintTileEdge (const TileEdge<WeightT> &tileEdge, const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile)
  : tileEdge (tileEdge),
    imgSize (imgSize),
    tile (tile) {
}

template <typename WeightT, int GeoDimT>
inline ostream &
CPrintTileEdge<WeightT, GeoDimT>::print (ostream &out) const {
  // XXX if WeightT == uint8_t => print char :-(
  out << tile.absPt (tileEdge.indexes[0]) << ":"
      << tile.absPt (tileEdge.indexes[1]) << ":";
  printWeight (out, tileEdge.weight);
  out << ":"
      << tile.absIdx (tileEdge.indexes[0], imgSize) << ":"
      << tile.absIdx (tileEdge.indexes[1], imgSize);
  // out << ": :";
  // out << idx2point (tile.size, tileEdge.indexes[0]) << ":"
  //     << idx2point (tile.size, tileEdge.indexes[1]) << ":";
  // printWeight (out, tileEdge.weight);
  // out  << ":"
  //      << tileEdge.indexes[0] << ":"
  //      << tileEdge.indexes[1];
  return out;
}

template <typename WeightT, int GeoDimT>
inline CPrintTileEdge<WeightT, GeoDimT>
printTileEdge (const TileEdge<WeightT> &tileEdge, const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile) {
  return CPrintTileEdge<WeightT, GeoDimT> (tileEdge, imgSize, tile);
}

template <typename WeightT, int GeoDimT>
inline
CPrintTileEdges<WeightT, GeoDimT>::CPrintTileEdges (const TileEdge<WeightT> tileEdges[], const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile, const DimEdge tileEdgesCount)
  : tileEdges (tileEdges),
    imgSize (imgSize),
    tile (tile),
    tileEdgesCount (tileEdgesCount) {
}

template <typename WeightT, int GeoDimT>
inline
ostream &
CPrintTileEdges<WeightT, GeoDimT>::print (ostream &out) const {
  for (DimEdge i = 0; i < tileEdgesCount; ++i)
    out << printTileEdge (tileEdges[i], imgSize, tile) << endl;
  return out;
}

template <typename WeightT, int GeoDimT>
inline CPrintTileEdges<WeightT, GeoDimT>
printTileEdges (const TileEdge<WeightT> tileEdges[], const Size<GeoDimT> &imgSize, const Rect<GeoDimT> &tile, const DimEdge tileEdgesCount) {
  return CPrintTileEdges<WeightT, GeoDimT> (tileEdges, imgSize, tile, tileEdgesCount);
}

// ================================================================================
template <typename WeightT, int GeoDimT>
inline DimImg
Edge<WeightT, GeoDimT>::getA () const {
  return idxA;
}

template <typename WeightT, int GeoDimT>
inline DimImg
Edge<WeightT, GeoDimT>::getB (const Size<GeoDimT> &size) const {
  return deltaAB.addIdx (size, idxA);
}

template <typename WeightT, int GeoDimT>
inline WeightT
Edge<WeightT, GeoDimT>::getWeight () const {
  return weight;
}

// ================================================================================
// template <typename WeightT, int GeoDimT>
// inline void
// Edge<WeightT, GeoDimT>::setPointAB (const Point<GeoDimT> &pa, const Point<GeoDimT> &pb) {
//   pointA = pa;
//   deltaPointAB = DeltaPoint<GeoDimT> (pointA, pb);
// }

template <typename WeightT, int GeoDimT>
inline void
Edge<WeightT, GeoDimT>::setAB (const Size<GeoDimT> &size, const DimImg &idxa, const DimImg &idxb) {
  if (idxa <= idxb) {
    idxA = idxa;
    deltaAB = DeltaPoint<GeoDimT> (size, idxa, idxb);
    return;
  }
  idxA = idxb;
  deltaAB = DeltaPoint<GeoDimT> (size, idxb, idxa);
}

template <typename WeightT, int GeoDimT>
inline void
Edge<WeightT, GeoDimT>::setWeight (const WeightT &w) {
  weight = w;
}

// ================================================================================

#endif // _Obelix_Triskele_ArrayBase_tpp
