////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_MeanAttributes_hpp
#define _Obelix_Triskele_MeanAttributes_hpp

#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    template<int GeoDimT>
    class MeanAttributes : public CompAttribute<double, GeoDimT> {
    public:
      typedef CompAttribute<double, GeoDimT> CA;

      template<typename PixelT>
      MeanAttributes (const Tree<GeoDimT> &tree, const Raster<PixelT, GeoDimT> &raster, const AreaAttributes<GeoDimT> &areaAttributes);
      virtual ~MeanAttributes ();

      virtual inline void save (HDF5 &hdf5) const { CA::save (hdf5, "mean"); }
      virtual inline void load (HDF5 &hdf5) { CA::load (hdf5, "mean"); }

      // template<typename PixelT>
      // void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT, GeoDimT> &attributeProfiles, const vector<double> &thresholds, const PruningStrategy &pruningStrategy = Direct) const;
      virtual inline ostream &print (ostream &out) const { CA::print (out, "mean"); return out; }
      virtual inline const double getPixelValue (const DimImg &leafId) const { return raster.getValue (leafId); }
    protected:
      Raster<double, GeoDimT> raster;
      template<typename PixelT>
      void compute (const Raster<PixelT, GeoDimT> &raster, const AreaAttributes<GeoDimT> &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_MeanAttributes_hpp
