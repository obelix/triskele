////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "Attributes/AttributesCache.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "AttributesCache.tcpp"

// ================================================================================
template<typename PixelT, int GeoDimT>
AttributesCache<PixelT, GeoDimT>::AttributesCache (const Tree<GeoDimT> &tree, const GraphWalker<GeoDimT> &graphWalker, const Raster<PixelT, GeoDimT> &inputRaster, const WeightAttributes<PixelT, GeoDimT> &weightAttributes)
  : tree (tree),
    graphWalker (graphWalker),
    inputRaster (inputRaster),
    weightAttributes (weightAttributes),
    areaAttributesP (nullptr),
    perimeterAttributesP (nullptr),
    boundingBoxAttributesP (nullptr),
    zLengthAttributesP (nullptr),
    stsAttributesP (nullptr),
    compactnessAttributesP (nullptr),
    complexityAttributesP (nullptr),
    simplicityAttributesP (nullptr),
    rectangularityAttributesP (nullptr),
    minAttributesP (nullptr),
    maxAttributesP (nullptr),
    meanAttributesP (nullptr),
    sdAttributesP (nullptr),
    sdwAttributesP (nullptr),
    // sdaAttributesP (nullptr),
    centroidP (nullptr),
    moiAttributesP (nullptr) {
}

// ================================================================================
template<typename PixelT, int GeoDimT>
AttributesCache<PixelT, GeoDimT>::~AttributesCache () {
  if (areaAttributesP != nullptr)
    delete areaAttributesP;
  if (perimeterAttributesP != nullptr)
    delete perimeterAttributesP;
  if (boundingBoxAttributesP != nullptr)
    delete boundingBoxAttributesP;
  if (zLengthAttributesP != nullptr)
    delete zLengthAttributesP;
  if (stsAttributesP != nullptr)
    delete stsAttributesP;
  if (compactnessAttributesP != nullptr)
    delete compactnessAttributesP;
  if (complexityAttributesP != nullptr)
    delete complexityAttributesP;
  if (simplicityAttributesP != nullptr)
    delete simplicityAttributesP;
  if (rectangularityAttributesP != nullptr)
    delete rectangularityAttributesP;
  if (minAttributesP != nullptr)
    delete minAttributesP;
  if (maxAttributesP != nullptr)
    delete maxAttributesP;
  if (meanAttributesP != nullptr)
    delete meanAttributesP;
  if (sdAttributesP != nullptr)
    delete sdAttributesP;
  if (sdwAttributesP != nullptr)
    delete sdwAttributesP;
  // if (sdaAttributesP != nullptr)
  //   delete sdaAttributesP;
  if (centroidP != nullptr)
    delete centroidP;
  if (moiAttributesP != nullptr)
    delete moiAttributesP;
}


// ================================================================================
template<typename PixelT, int GeoDimT>
const Raster<PixelT, GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getRaster () {
  return inputRaster;
}

template<typename PixelT, int GeoDimT>
const WeightAttributes<PixelT, GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getWeight () {
  return weightAttributes;
}

template<typename PixelT, int GeoDimT>
const AreaAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getArea () {
  if (areaAttributesP == nullptr)
    areaAttributesP = new AreaAttributes<GeoDimT> (tree);
  return *areaAttributesP;
}

template<typename PixelT, int GeoDimT>
const PerimeterAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getPerimeter () {
  if (perimeterAttributesP == nullptr)
    perimeterAttributesP = new PerimeterAttributes<GeoDimT> (tree, graphWalker);
  return *perimeterAttributesP;
}

template<typename PixelT, int GeoDimT>
const BoundingBoxAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getBoundingBox () {
  if (boundingBoxAttributesP == nullptr)
    boundingBoxAttributesP = new BoundingBoxAttributes<GeoDimT> (tree);
  return *boundingBoxAttributesP;
}

template<typename PixelT, int GeoDimT>
const ZLengthAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getZLength () {
  if (zLengthAttributesP == nullptr)
    zLengthAttributesP =  new ZLengthAttributes<GeoDimT> (tree, getBoundingBox ());
  return *zLengthAttributesP;
}

template<typename PixelT, int GeoDimT>
const STSAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getSTS () {
  if (stsAttributesP == nullptr)
    stsAttributesP =  new STSAttributes<GeoDimT> (tree);
  return *stsAttributesP;
}

template<typename PixelT, int GeoDimT>
const CompactnessAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getCompactness () {
  if (compactnessAttributesP == nullptr)
    compactnessAttributesP = new CompactnessAttributes<GeoDimT> (tree, getArea (), getPerimeter ());
  return *compactnessAttributesP;
}

template<typename PixelT, int GeoDimT>
const ComplexityAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getComplexity () {
  if (complexityAttributesP == nullptr)
    complexityAttributesP = new ComplexityAttributes<GeoDimT> (tree, getArea (), getPerimeter ());
  return *complexityAttributesP;
}

template<typename PixelT, int GeoDimT>
const SimplicityAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getSimplicity () {
  if (simplicityAttributesP == nullptr)
    simplicityAttributesP = new SimplicityAttributes<GeoDimT> (tree, getArea (), getPerimeter ());
  return *simplicityAttributesP;
}

template<typename PixelT, int GeoDimT>
const RectangularityAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getRectangularity () {
  if (rectangularityAttributesP == nullptr)
    rectangularityAttributesP = new RectangularityAttributes<GeoDimT> (tree, getArea (), getBoundingBox ());
  return *rectangularityAttributesP;
}

template<typename PixelT, int GeoDimT>
const MinAttributes<PixelT, GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getMin () {
  if (minAttributesP == nullptr)
    minAttributesP = new MinAttributes<PixelT, GeoDimT> (tree, inputRaster);
  return *minAttributesP;
}

template<typename PixelT, int GeoDimT>
const MaxAttributes<PixelT, GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getMax () {
  if (maxAttributesP == nullptr)
    maxAttributesP = new MaxAttributes<PixelT, GeoDimT> (tree, inputRaster);
  return *maxAttributesP;
}

template<typename PixelT, int GeoDimT>
const MeanAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getMean () {
  if (meanAttributesP == nullptr)
    meanAttributesP = new MeanAttributes<GeoDimT> (tree, inputRaster, getArea ());
  return *meanAttributesP;
}

template<typename PixelT, int GeoDimT>
const SDAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getSD () {
  if (sdAttributesP == nullptr)
    sdAttributesP = new SDAttributes<GeoDimT> (tree, getMean (), getArea ());
  return *sdAttributesP;
}

template<typename PixelT, int GeoDimT>
const SDWAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getSDW () {
  if (sdwAttributesP == nullptr)
    sdwAttributesP = new SDWAttributes<GeoDimT> (tree, inputRaster, weightAttributes, getArea ());
  return *sdwAttributesP;
}

// template<typename PixelT, int GeoDimT>
// const SDAAttributes<GeoDimT> &
// AttributesCache<PixelT, GeoDimT>::getSDA () {
//   if (sdaAttributesP == nullptr)
//     sdaAttributesP = new SDAAttributes<GeoDimT> (tree, getArea ());
//   return *sdaAttributesP;
// }

template<typename PixelT, int GeoDimT>
const Centroid<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getCentroid () {
  if (centroidP == nullptr)
    centroidP = new Centroid<GeoDimT> (tree, getArea ());
  return *centroidP;
}

template<typename PixelT, int GeoDimT>
const MoIAttributes<GeoDimT> &
AttributesCache<PixelT, GeoDimT>::getMoI () {
  if (moiAttributesP == nullptr)
    moiAttributesP = new MoIAttributes<GeoDimT> (tree, getArea (), getCentroid ());
  return *moiAttributesP;
}

// ================================================================================
