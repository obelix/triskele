////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_AttributesCache_hpp
#define _Obelix_Triskele_AttributesCache_hpp

#include "Tree.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "IImage.hpp"
#include "Attributes/WeightAttributes.hpp"

#include "Attributes/AreaAttributes.hpp"
#include "Attributes/PerimeterAttributes.hpp"
#include "Attributes/BoundingBoxAttributes.hpp"
#include "Attributes/ZLengthAttributes.hpp"
#include "Attributes/STSAttributes.hpp"
#include "Attributes/CompactnessAttributes.hpp"
#include "Attributes/ComplexityAttributes.hpp"
#include "Attributes/SimplicityAttributes.hpp"
#include "Attributes/RectangularityAttributes.hpp"
#include "Attributes/MinAttributes.hpp"
#include "Attributes/MaxAttributes.hpp"
#include "Attributes/MeanAttributes.hpp"
#include "Attributes/SDAttributes.hpp"
#include "Attributes/SDWAttributes.hpp"
// #include "Attributes/SDAAttributes.hpp"
#include "Attributes/Centroid.hpp"
#include "Attributes/MoIAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne, globalement croissante. */
    template<typename PixelT, int GeoDimT>
    class AttributesCache {
    public:
      AttributesCache (const Tree<GeoDimT> &tree, const GraphWalker<GeoDimT> &graphWalker, const Raster<PixelT, GeoDimT> &inputRaster, const WeightAttributes<PixelT, GeoDimT> &weightAttributes);
      ~AttributesCache ();
      const WeightAttributes<PixelT, GeoDimT> &getWeight ();
      const Raster<PixelT, GeoDimT> &getRaster ();

      const AreaAttributes<GeoDimT> &getArea ();
      const PerimeterAttributes<GeoDimT> &getPerimeter ();
      const BoundingBoxAttributes<GeoDimT> &getBoundingBox ();
      const ZLengthAttributes<GeoDimT> &getZLength ();
      const STSAttributes<GeoDimT> &getSTS ();
      const CompactnessAttributes<GeoDimT> &getCompactness ();
      const ComplexityAttributes<GeoDimT> &getComplexity ();
      const SimplicityAttributes<GeoDimT> &getSimplicity ();
      const RectangularityAttributes<GeoDimT> &getRectangularity ();
      const MinAttributes<PixelT, GeoDimT> &getMin ();
      const MaxAttributes<PixelT, GeoDimT> &getMax ();
      const MeanAttributes<GeoDimT> &getMean ();
      const SDAttributes<GeoDimT> &getSD ();
      const SDWAttributes<GeoDimT> &getSDW ();
      // const SDAAttributes<GeoDimT> &getSDA ();
      const Centroid<GeoDimT> &getCentroid ();
      const MoIAttributes<GeoDimT> &getMoI ();

    protected:
      const Tree<GeoDimT> &tree;
      const GraphWalker<GeoDimT> &graphWalker;
      const Raster<PixelT, GeoDimT> &inputRaster;
      const WeightAttributes<PixelT, GeoDimT> &weightAttributes;

      AreaAttributes<GeoDimT> *areaAttributesP;
      PerimeterAttributes<GeoDimT> *perimeterAttributesP;
      BoundingBoxAttributes<GeoDimT> *boundingBoxAttributesP;
      ZLengthAttributes<GeoDimT> *zLengthAttributesP;
      STSAttributes<GeoDimT> *stsAttributesP;
      CompactnessAttributes<GeoDimT> *compactnessAttributesP;
      ComplexityAttributes<GeoDimT> *complexityAttributesP;
      SimplicityAttributes<GeoDimT> *simplicityAttributesP;
      RectangularityAttributes<GeoDimT> *rectangularityAttributesP;
      MinAttributes<PixelT, GeoDimT> *minAttributesP;
      MaxAttributes<PixelT, GeoDimT> *maxAttributesP;
      MeanAttributes<GeoDimT> *meanAttributesP;
      SDAttributes<GeoDimT> *sdAttributesP;
      SDWAttributes<GeoDimT> *sdwAttributesP;
      // SDAAttributes<GeoDimT> *sdaAttributesP;
      Centroid<GeoDimT> *centroidP;
      MoIAttributes<GeoDimT> *moiAttributesP;
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_AttributesCache_hpp
