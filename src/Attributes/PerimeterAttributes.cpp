////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "Attributes/PerimeterAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "PerimeterAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
PerimeterAttributes<GeoDimT>::PerimeterAttributes (const Tree<GeoDimT> &tree, const GraphWalker<GeoDimT> &graphWalker)
  : CompAttribute<DimImgPack, GeoDimT> (tree),
    graphWalker (graphWalker) {
  DEF_LOG ("PerimeterAttributes::PerimeterAttributes", "");
  compute ();
}

template<int GeoDimT>
PerimeterAttributes<GeoDimT>::~PerimeterAttributes () {
  DEF_LOG ("PerimeterAttributes::~PerimeterAttributes", "");
}

static const BorderFlagType frameBorder (X1|X2|Y1|Y2);

// ================================================================================
template<int GeoDimT>
void
PerimeterAttributes<GeoDimT>::compute () {
  DEF_LOG ("PerimeterAttributes::compute", "");
  auto start = high_resolution_clock::now ();

  dealThread (CA::tree.getCompCount (), CA::tree.getCoreCount (),
	      [this] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
		for (DimParent compId = minVal; compId < maxVal; ++compId)
		  //  tree.forEachComp ([this] (const DimParent &compId) {
		  CA::values[compId] = 0;
	      });

  const DimParent rootId = CA::tree.getRootId ();
  const DimParentPack * const leafParents (CA::tree.getLeafParents ());
  const Border<GeoDimT> &border (graphWalker.border);
  const Neighbor *volDelta2 (graphWalker.volDelta2.data ());
  const DimChannel volDeltaCount (graphWalker.volDelta2.size ());
  graphWalker.forEachVertex (Rect<GeoDimT> (Point<GeoDimT> (), CA::tree.getSize ()),
			     [this, &rootId, &leafParents, &border, &volDelta2, &volDeltaCount] (const DimImg &idx, const BorderFlagType &borderFlags) {
			       const DimParent parentId (leafParents [idx]);
			       if (parentId == DimParent_MAX)
				 return;

			       if (borderFlags & frameBorder)  {
				 for (DimParent p = parentId; p < rootId; p = CA::tree.getCompParent (p))
				   ++CA::values[p];
				 return;
			       }

			       DimParent neighbors [27]; // XXX 3^3
			       DimParent max = neighbors [0] = parentId;
			       DimChannel neighborsSize = 0;
			       //for (Neighbor delta : graphWalker.volDelta2) {
			       for (DimChannel i = 0; i < volDeltaCount; ++i) {
				 const DimImg b1 (idx-volDelta2[i].off);
				 //const DimImg b1 (idx-delta.off);
				 if (border.isBorder (b1))
				   max = rootId;
				 else
				   neighbors [++neighborsSize] = leafParents [b1];
				 const DimImg b2 (idx+volDelta2[i].off); // volDelta1 == volDelta2
				 //const DimImg b2 (idx+delta.off);
				 if (border.isBorder (b2))
				   max = rootId;
				 else
				   neighbors [++neighborsSize] = leafParents [b2];
			       }
			       ++neighborsSize;
			       if (max < rootId && neighborsSize) {
				 sort (neighbors, neighbors + neighborsSize);
				 max = neighbors[0];
				 for (DimChannel i = 1; i < neighborsSize; ++i)
				   max = CA::tree.ancestor (max, neighbors [i]);
			       }
			       for (DimParent p = parentId; p < max; p = CA::tree.getCompParent (p))
				 ++CA::values[p];
			     });
  TreeStats::global.addTime (perimeterStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
