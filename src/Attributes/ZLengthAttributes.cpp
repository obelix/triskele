////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/ZLengthAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "ZLengthAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
ZLengthAttributes<GeoDimT>::ZLengthAttributes (const Tree<GeoDimT> &tree, const BoundingBoxAttributes<GeoDimT> &boundingBoxAttributes)
  : CompAttribute<DimChannel, GeoDimT> (tree) {
  DEF_LOG ("ZLengthAttributes::ZLengthAttributes", "");
  BOOST_ASSERT (GeoDimT != 2);
  compute (boundingBoxAttributes);
}

template<int GeoDimT>
ZLengthAttributes<GeoDimT>::~ZLengthAttributes () {
  DEF_LOG ("ZLengthAttributes::~ZLengthAttributes", "");
}

// ================================================================================
template<int GeoDimT>
void
ZLengthAttributes<GeoDimT>::compute (const BoundingBoxAttributes<GeoDimT> &boundingBoxAttributes) {
  DEF_LOG ("ZLengthAttributes::compute", "");
  if (GeoDimT == 2) {
    cerr << "/* ZLengthAttributes not 3 GeoDimT! */" << endl;
    return;
  }
  auto start = high_resolution_clock::now ();

  const BoundingBox<GeoDimT> *boundingBoxes = boundingBoxAttributes.getValues ();

  CA::tree.forEachComp
    ([this, &boundingBoxes] (const DimParent &compId) {
       const BoundingBox<GeoDimT> &bb (boundingBoxes[compId]);
       CA::values [compId] = bb.max [2] - bb.min [2] + 1;
     });

  TreeStats::global.addTime (zLengthStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
