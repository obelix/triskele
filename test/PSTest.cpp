////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"
#include "Border.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
//#include "AttributeProfiles.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "gdal.h"

using namespace obelix;
using namespace obelix::triskele;

inline unsigned int abs (unsigned int x) { return x;}
static const int GeoDimT (2);

template<typename InPixelT>
void
patternSpectra (IImage<GeoDimT> &inputImage) {

  TreeType treeType = MAX;

  Raster<InPixelT, GeoDimT> raster;
  inputImage.readBand (raster, 0);

  Border<GeoDimT> border (inputImage.getSize (), false);
  GraphWalker<GeoDimT> graphWalker (border);
  ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> atb (raster, graphWalker, treeType, 2);
  const DimCore coreCount (boost::thread::hardware_concurrency ());
  Tree<GeoDimT> tree (coreCount);
  WeightAttributes<InPixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);
  //AttributeProfiles<OutPixelT> attributeProfiles (tree);
  AreaAttributes<GeoDimT> areaAttributes (tree);
  DimImg pixelCount = inputImage.getSize ().getPixelsCount ();
  int binCount = 40;
  vector<InPixelT> psTab (binCount, 0);
  for (DimParent nodeId = 0; nodeId < tree.getRootId (); nodeId++){
    int i1 = int (DimImg (areaAttributes.getValues ()[nodeId]) * binCount / pixelCount);
    uint32_t v = abs (weightAttributes.getValues ()[nodeId]-
		      weightAttributes.getValues ()[tree.getCompParent (nodeId)])
      *DimImg (areaAttributes.getValues ()[nodeId]);
    psTab [i1] += v;
    cout << "node " << nodeId << " : bins[" << i1 << "] += " << v << endl;
  }
  for (int binId = 0; binId < binCount; binId++)
    cout << " " << psTab[binId];
  cout << endl;

  // XXX write result
}


int
main (int argc, char** argv, char** envp) {
  IImage<GeoDimT> inputImage ("data/arles.tif");

  inputImage.readImage ();

  switch (inputImage.getDataType ()) {
  case GDT_Byte:
    patternSpectra<uint8_t> (inputImage); break;
  case GDT_UInt16:
    patternSpectra<uint16_t> (inputImage); break;
  case GDT_Int16:
    patternSpectra<int16_t> (inputImage); break;
  case GDT_UInt32:
    patternSpectra<uint32_t> (inputImage); break;
  case GDT_Int32:
    patternSpectra<int32_t> (inputImage); break;
  case GDT_Float32:
    patternSpectra<float> (inputImage); break;
  case GDT_Float64:
    patternSpectra<double> (inputImage); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;

}
