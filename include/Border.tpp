////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Border_tpp
#define _Obelix_Border_tpp

// ================================================================================
template<int GeoDimT>
inline const Size<GeoDimT> &
Border<GeoDimT>::getSize () const {
  return size;
}

// ================================================================================
template<>
inline bool
Border<2>::isBorder (const DimImg &idx) const {
  BOOST_ASSERT (idx < pixelsCount);
  return map.size () ? map[idx/BorderBlockBits] & (1 << idx%BorderBlockBits) : defaultVal;
}

template<int GeoDimT>
inline bool
Border<GeoDimT>::isBorder (const DimImg &idx) const {
  BOOST_ASSERT (idx < pixelsCount);
  const DimImg idx2 (idx % framePixelsCount);
  return map.size () ? map[idx2/BorderBlockBits] & (1 << idx2%BorderBlockBits) : defaultVal;
}

template<int GeoDimT>
inline bool
Border<GeoDimT>::isBorder (const Point<GeoDimT> &p) const {
  BOOST_ASSERT (p.coord [0] < size.side [0] && p.coord [1] < size.side [1] && (GeoDimT < 3 || p.coord [2] < size.side [2]));
  return isBorder (point2idx (size, p));
}

// ================================================================================
#endif // _Obelix_Border_tpp
