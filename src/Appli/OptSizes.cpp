////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <string>
#include "Appli/OptSizes.hpp"

using namespace std;
using namespace obelix;
using namespace boost;


// ================================================================================
OptSizes::OptSizes (const string &option) {
  reset ();
  init (option);
}

OptSizes::OptSizes (const OptSizes &optSizes)
  : OptSizes () {
  sizes = optSizes.sizes;
}

// ================================================================================
void
OptSizes::reset () {
  sizes.clear ();
}

void
OptSizes::init (const string &option) {
  reset ();
  if (option.empty ())
    return;
  vector <string> fields;
  vector <string> intervals;
  try {
    split (fields, option, is_any_of (","));
    for (auto &field : fields) {
      intervals.clear();
      split (intervals, field, is_any_of ("x"));
      switch (intervals.size ()) {
      case 0:
	continue;
      case 1:
	throw invalid_argument ("Bad range");
	continue;
      case 2:
	sizes.push_back (Size<2> (stol ((string&) intervals[0]),
				  stol ((string&) intervals[1])));
	continue;
      case 3:
	sizes.push_back (Size<2> (stol ((string&) intervals[0]),
				  stol ((string&) intervals[1])));
	continue;
      default:
	throw invalid_argument ("Bad range");
	return;
	;
      }
    }
  } catch (...) {
    throw invalid_argument ("Bad size option: "+option);
  }
}

ostream &
obelix::operator << (ostream &out, const OptSizes &optSizes) {
  out << "{";
  string sep = "";
  for (auto &i : optSizes.getSizes ()) {
    out << sep << i;
    sep = ", ";
  }
  return out << "}";
}

istream &
obelix::operator >> (istream &in, OptSizes &optSizes) {
  string token;
  in >> token;
  optSizes.init (token);
  return in;
}

// ================================================================================
