////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#define LAST_VERSION "2.5 2020-11-11 ("+getOsName ()+")"

#include <iostream>
#include <string.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "misc.hpp"
#include "obelixPantex.hpp"
#include "IImage.hpp"
#include "Appli/OptFilter.hpp"
#include "Appli/OptChannelParser.hpp"
#include "Appli/OptCropParser.hpp"

// #ifndef TRISKELE_ERROR
// #define TRISKELE_ERROR(expr) std::cerr << expr << std::endl << std::flush, std::exit (1)
// #endif

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
OptFilter::OptFilter ()
  : debugFlag (false),
    timeFlag (false),
    countFlag (false),
    countingSortCeil (2),
    oneBandFlag (false),
    pantexWindowSide (35),
    includeNoDataFlag (false),
    noDataValue (NAN),
    connectivity (Connectivity (Connectivity::C4|Connectivity::CT)),
    coreCount (boost::thread::hardware_concurrency ()),
    threadPerImage (max (DimCore (1), coreCount/4)),
    tilePerThread (max (DimCore (1), threadPerImage*2)),
    seqTileFlag (false),
    autoThreadFlag (false),
    memImpactFlag (false),
    inputDataType (GDT_Unknown) {
}

OptFilter::OptFilter (int argc, char** argv)
  : OptFilter () {
  parse (argc, argv);
}

// ================================================================================
static OptCropParser optCropParser;
static OptChannelParser optChannelParser;
static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
OptFilter::usage (string msg, bool hidden) const {
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [-i] inputFilename [-o] outputFilename [options]" << endl << endl
       << endl << mainDescription
       << endl << optCropParser.description
       << endl << optChannelParser.description
       << endl;
  if (hidden)
    cout << hide << endl;
  if (msg.size ())
    cout << msg << endl;
}

void
OptFilter::version () const {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  GDAL  : read and write image (http://www.gdal.org/)" << endl
       << "  cct   : inspired by jirihavel library to build trees (https://github.com/jirihavel/libcct)" << endl
       << "  Boost : C++ libraries (http://www.boost.org/)" << endl
       << endl << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}
// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptFilter::parse (int argc, char** argv) {
  prog = argv [0];
  unsigned int countingSortCeilLocal (countingSortCeil);
  bool helpFlag = false, versionFlag = false, useTheForceLuke = false;
  try {
    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("version", po::bool_switch (&versionFlag), "display version information")
      ("debug", po::bool_switch (&debugFlag), "debug mode")

      ("timeFlag", po::bool_switch (&timeFlag), "give execution time")
      ("countFlag", po::bool_switch (&countFlag), "give pixels and nodes count")
      ("countingSortCeil", po::value<unsigned int> (&countingSortCeilLocal)->default_value (countingSortCeilLocal), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")
      ("showConfig", po::value<string> (&showConfig), "show actual configuration on a file")

      ("includeNoDataFlag", po::bool_switch (&includeNoDataFlag), "build tree with all pixels (included no-data)")
      ("noDataValue", po::value<double> (&noDataValue)->default_value (noDataValue), "value use as nodata in all bands")

      ("input,i", po::value<string> (&inputFilename), "input file name image")
      ("output,o", po::value<string> (&outputFilename), "output file name for hyperbands image contains attributs profiles (or classification image with -g options)")
      ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
      ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used for parallel process")
      ("threadPerImage", po::value<DimCore> (&threadPerImage)->default_value (threadPerImage), "thread per Image to build tree")
      ("tilePerThread", po::value<DimCore> (&tilePerThread)->default_value (tilePerThread), "tiles per threads")
      ("seqTileFlag", po::bool_switch (&seqTileFlag), "force Tile sequential computation")
      ("autoThreadFlag", po::bool_switch (&autoThreadFlag), "auto tune tilePerThread count")
      ("memImpactFlag", po::bool_switch (&memImpactFlag), "estimate memory impact")
      ;
    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("noWriteFlag", po::bool_switch (&noWriteFlag), "don't produce image")
      ("oneBandFlag", po::bool_switch (&oneBandFlag), "split all bands, one band per image")
      ("pantexWindowSide", po::value<DimPantexOcc> (&pantexWindowSide)->default_value (pantexWindowSide), "side length of the sliding window to compute Pantex index (min 5, max 127)")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optCropParser.description).add (optChannelParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    countingSortCeil = countingSortCeilLocal;
    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (versionFlag)
      version ();
    if (useTheForceLuke) {
      usage ("", true);
      exit (0);
    }
    if (helpFlag) {
      usage ();
      exit (0);
    }
    int required = 2;
    if (vm.count ("input"))
      required--;
    if (vm.count ("output"))
      required--;

    int nbArgs = 0;
    if (vm.count (inputFileC))
      nbArgs = vm[inputFileC].as<vector<string> > ().size ();
    if (required-nbArgs != 0) {
      usage ("Error: need one input and one output");
      exit (1);
    }
    if (required) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      if (outputFilename.empty ())
	outputFilename = var [--required];
      if (inputFilename.empty ())
	inputFilename = var [--required];
    }


    IImage<2> inputImage (inputFilename);
    inputImage.readImage ();
    inputImage.close ();
    inputDataType = inputImage.getDataType ();
    DimChannel inputBandsCount = inputImage.getBandCount ();
    Size<2> imgSize = inputImage.getSize ();
    optCropParser.parse (optCrop, imgSize, parsed);
    optChannelParser.parse (optChannel, inputFilename, parsed);

    cout
      << "Input:" << inputFilename << " (" << inputBandsCount << " channels of " << GDALGetDataTypeName (inputDataType) << ")" << endl
      << "Crop:" << optCrop << endl
      << "Output:" << outputFilename << endl
      << "core count:" << coreCount << "(" << threadPerImage << "*" << tilePerThread << ")" << endl
      << "seqTileFlag:" << seqTileFlag << endl
      << "autoThreadFlag:" << autoThreadFlag << endl
      << "dim:" << optChannel.dim << endl;

    // if (optCrop.topLeft.coord [0] || optCrop.topLeft.coord [1] ||
    // 	optCrop.size.side [0] != imgSize.side [0] || optCrop.size.side [1] != imgSize.side [1])
    //   cout << Log::getLocalTimeStr () << " crop:" << optCrop << endl;

    if (!optChannel.getOutputCount ())
      throw invalid_argument ("No output band selected");

    if (pantexWindowSide < 5 || pantexWindowSide > 127)
      throw invalid_argument ("invalid pantexWindowSide");

    if (memImpactFlag) {
      MemoryEstimation memoryEstimation (optCrop.size, inputImage.getDataType (), connectivity, countingSortCeil, coreCount, .40, true);
      optChannel.memoryImpact (memoryEstimation);
      printMem (cerr);
      cerr << memoryEstimation;
    }
  } catch (std::exception &e) {
    usage ("Bad options");
    cerr << "Error: " << e.what () << endl;
    exit (1);
  }

  if (showConfig.size ()) {
    out.open (showConfig, ios_base::out);
    out
      << endl
      << Log::getLocalTimeStr () << " ChannelFilter " << LAST_VERSION << " config:" << endl << endl
      << *this << endl;
  }
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const OptFilter &optFilter) {
  unsigned int leftMarging = 25;
  return out
    SHOW_OPTION_FLAG ("--debugFlag", optFilter.debugFlag)
    SHOW_OPTION_FLAG ("--timeFlag", optFilter.timeFlag)
    SHOW_OPTION_FLAG ("--countFlag", optFilter.countFlag)
    SHOW_OPTION ("--countingSortCeil", (int) optFilter.countingSortCeil)
    SHOW_OPTION ("--showConfig", optFilter.showConfig)
    SHOW_OPTION_FLAG ("--noWriteFlag", optFilter.noWriteFlag)
    SHOW_OPTION_FLAG ("--oneBandFlag", optFilter.oneBandFlag)
    SHOW_OPTION_FLAG ("--pantexWindowSide", optFilter.pantexWindowSide)
    SHOW_OPTION_FLAG ("--includeNoDataFlag", optFilter.includeNoDataFlag)
    SHOW_OPTION ("--noDataValue", optFilter.noDataValue)
    SHOW_OPTION ("--connectivity", optFilter.connectivity << " (" << int (optFilter.connectivity) << ")")
    SHOW_OPTION ("--coreCount", optFilter.coreCount)
    SHOW_OPTION ("--coreThreadPerImage", optFilter.threadPerImage)
    SHOW_OPTION ("--coreTilePerThread", optFilter.tilePerThread)
    SHOW_OPTION_FLAG ("--seqTileFlag", optFilter.seqTileFlag)
    SHOW_OPTION_FLAG ("--autoThreadFlag", optFilter.autoThreadFlag)

    SHOW_OPTION ("--input", optFilter.inputFilename)
    SHOW_OPTION ("--output", optFilter.outputFilename)

    << endl
    << "OptCrop: " << endl << optFilter.optCrop << endl
    << endl
    << "OptChannel: " << endl << optFilter.optChannel << endl
    << flush;
}

// ================================================================================
