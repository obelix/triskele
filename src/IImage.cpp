///////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string.hpp>
#include <boost/assert.hpp>
#include <boost/filesystem.hpp>
#include <limits> // infinity
#include <algorithm>    // std::fill

#include "triskeleGdalGetType.hpp"
#include "IImage.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "IImage.tcpp"

// ================================================================================
template<>
size_t
IImage<2>::gdalCount = 0;
template<>
size_t
IImage<3>::gdalCount = 0;

// ================================================================================
#if defined IMAGE_40BITS || defined IMAGE_LARGE
namespace obelix {
  template<int GeoDimT>
  template<>
  void
  IImage<GeoDimT>::writeBand<DimImgPack> (const DimImgPack *pixels, DimChannel band) const {
    // XXX todo
    BOOST_ASSERT (false);
  }
  template<int GeoDimT>
  template<>
  void
  IImage<GeoDimT>::writeFrame<DimImgPack> (const DimImgPack *pixels, DimChannel band) const {
    DimSideImg nbChannel (GeoDimT < 3 ? 1 : size.side [2]);
    for (DimChannel z = 0; z < nbChannel; ++z)
      writeBand (pixels+size.getOffsetFrame (z), band*nbChannel+z);
  }
} // obelix
#endif // defined IMAGE_40BITS || defined IMAGE_LARGE


// ================================================================================
template<typename PixelT, int GeoDimT>
void
Raster<PixelT, GeoDimT>::setSize (const Size<GeoDimT> &size) {
  DEF_LOG ("Raster::setSize",  "size: " << size);
  if (this->size == size)
    return;
  pixels.resize (size.getPixelsCount ());
  this->size = size;
}

// ================================================================================
template<typename PixelT, int GeoDimT>
Raster<PixelT, GeoDimT>::Raster (const Size<GeoDimT> &size)
  : size (size),
    pixels (size.getPixelsCount ()) {
  DEF_LOG ("Raster::Raster", "size: " << size);
}

template<typename PixelT, int GeoDimT>
Raster<PixelT, GeoDimT>::Raster (const Size<GeoDimT> &size, const PixelT &initValue)
  : size (size),
    pixels (size.getPixelsCount (), initValue) {
  DEF_LOG ("Raster::Raster", "size: " << size << " initValue (" << initValue << ")");
}

template<typename PixelT, int GeoDimT>
Raster<PixelT, GeoDimT>::Raster (const Raster<PixelT, GeoDimT> &raster, const Rect<GeoDimT> &tile)
  : size (tile.size),
    pixels () {
  DEF_LOG ("Raster::Raster", "tile: " << tile);
  tile.cpToTile (raster.size, raster.pixels.data (), pixels);
}

template<typename PixelT, int GeoDimT>
Raster<PixelT, GeoDimT>::~Raster () {
  DEF_LOG ("Raster::~Raster", "");
}

// ================================================================================
template<int GeoDimT>
void
IImage<GeoDimT>::setFileName (string fileName) {
  DEF_LOG ("IImage::setFileName", "fileName: " << fileName);
  if (this->fileName == fileName)
    return;
  this->fileName = fileName;
}

/*
  https://en.wikipedia.org/wiki/World_file
*/
template<int GeoDimT>
void
IImage<GeoDimT>::getGeo (string &projectionRef, vector<double> &geoTransform, const Point<2> &topLeft) const {
  projectionRef = this->projectionRef;
  moveGeo (this->geoTransform, geoTransform, topLeft);
}

template<int GeoDimT>
void
IImage<GeoDimT>::setGeo (const string &projectionRef, const vector<double> &geoTransform, const Point<2> &topLeft) {
  DEF_LOG ("IImage::setGeo", "projectionRef:" << projectionRef);
  if (geoTransform[0] == 0. &&
      geoTransform[1] == 0. &&
      geoTransform[2] == 0. &&
      geoTransform[3] == 0. &&
      geoTransform[4] == 0. &&
      geoTransform[5] == 0.)
    return;
  vector<double> moved (6);
  moveGeo (geoTransform, moved, topLeft);
  CPLErr err = gdalOutputDataset->SetGeoTransform (moved.data ());
  if (err != CE_None)
    cerr << "IImage::setGeo: can't set geoTransform for " << fileName << endl;
  err = gdalOutputDataset->SetProjection (projectionRef.c_str ());
  if (err != CE_None)
    cerr << "IImage::setGeo: can't set projection for " << fileName << endl;
}

template<int GeoDimT>
void
IImage<GeoDimT>::moveGeo (const vector<double> &src, vector<double> &dst, const Point<2> &topLeft) const {
  // move TopLeft cf WordFile
  dst = src;
  double
    x (dst[0]),
    xScale (dst[1]),
    xSkew (dst[2]),
    y (dst[3]),
    ySkew (dst[4]),
    yScale (dst[5]);
  double
    xTrans = x + topLeft.coord [0]*xScale + topLeft.coord [1]*xSkew,
    yTrans = y + topLeft.coord [0]*ySkew + topLeft.coord [1]*yScale;
  dst[0] = xTrans;
  dst[3] = yTrans;
}

// ================================================================================
template<int GeoDimT>
IImage<GeoDimT>::IImage (const string &imageFileName)
  : projectionRef (""),
    geoTransform (6, 0),
    fileName (imageFileName),
    size (),
    bandCount (0),
    mixedBandFlag (false),
    dataType (GDT_Unknown),
    gdalInputDataset (nullptr),
    gdalOutputDataset (nullptr),
    read (false) {
  DEF_LOG ("IImage::IImage", "imageFileName: " << imageFileName);
}

template<int GeoDimT>
IImage<GeoDimT>::~IImage () {
  close ();
}

// ================================================================================
template<>
void
IImage<2>::init (const DimChannel &spectralDepth, const bool mixedBandFlag) {
  bandCount = gdalInputDataset->GetRasterCount ();
  this->mixedBandFlag = false;
  BOOST_ASSERT (!spectralDepth || bandCount%spectralDepth == 0);
  size = Size<2> (gdalInputDataset->GetRasterXSize (),
		  gdalInputDataset->GetRasterYSize ());
}
template<>
void
IImage<3>::init (const DimChannel &spectralDepth, const bool mixedBandFlag) {
  bandCount = gdalInputDataset->GetRasterCount ();
  this->mixedBandFlag = mixedBandFlag;
  BOOST_ASSERT (!spectralDepth || bandCount%spectralDepth == 0);
  size = Size<3> (gdalInputDataset->GetRasterXSize (),
		  gdalInputDataset->GetRasterYSize (),
		  spectralDepth ? bandCount/spectralDepth : 1);
  if (spectralDepth)
    bandCount = spectralDepth;
}

template<int GeoDimT>
void
IImage<GeoDimT>::readImage (const DimChannel &spectralDepth, const bool mixedBandFlag) {
  DEF_LOG ("IImage::readImage", "fileName: " << fileName << " spectralDepth: " << spectralDepth << " mixedBandFlag: " << mixedBandFlag);
  BOOST_ASSERT (gdalInputDataset == nullptr);
  BOOST_ASSERT (gdalOutputDataset == nullptr);
  if (fileName.empty ())
    return;
  if (!gdalCount++)
    GDALAllRegister ();
  dataType = GDT_Unknown;
  gdalInputDataset = (GDALDataset *) GDALOpen (fileName.c_str (), GA_ReadOnly);
  if (!gdalInputDataset) {
    cerr << "GDALError: can't define dataset" << endl;
    return;
  }
  init (spectralDepth, mixedBandFlag);
  LOG ("size: " << size << " x " << bandCount);
  read = true;
  projectionRef = gdalInputDataset->GetProjectionRef ();
  CPLErr err = gdalInputDataset->GetGeoTransform (geoTransform.data ());
  if (err != CE_None) {
    fill (begin (geoTransform), begin (geoTransform)+6, 0.);
    cerr << "IImage::readImage: can't read geoTransform from " << fileName << endl;
  }
  else
    LOG ("geoTransform: " << geoTransform [0] << " " << geoTransform [1] << " " <<  geoTransform [2] << " " <<  geoTransform [3] << " " <<  geoTransform [4] << " " <<  geoTransform [5]);

  for (DimChannel band = 0; band < bandCount; ++band) {
    GDALRasterBand &poBand = *gdalInputDataset->GetRasterBand (band+1);
    GDALDataType bandType = poBand.GetRasterDataType ();
    LOG ("band " << band << " " << GDALGetDataTypeName (bandType));
    if (dataType == GDT_Unknown) {
      dataType = bandType;
      continue;
    }
    if (dataType != bandType) {
      dataType = GDT_Unknown;
      LOG ("Can't parse inconsistant bands");
      // exit (1);
      return;
    }
  }
  LOG ("gdalCount: " << gdalCount);
}

// ================================================================================
template<int GeoDimT>
void
IImage<GeoDimT>::createImage (const Size<GeoDimT> &size, const GDALDataType &dataType, const DimChannel &nbBands) {
  DEF_LOG ("IImage::createImage", "fileName: " << fileName << " size: " << size
	   << " dataType: " << GDALGetDataTypeName (dataType) << " nbBands: " << nbBands);
  BOOST_ASSERT (gdalInputDataset == nullptr);
  BOOST_ASSERT (gdalOutputDataset == nullptr);
  this->size = size;
  this->dataType = dataType; //this->dataType = GDT_Float64;
  if (!gdalCount++)
    GDALAllRegister ();
  string fileExtension  = boost::filesystem::extension (fileName);
  boost::algorithm::to_lower (fileExtension);
  if (!boost::iequals (fileExtension, ".tif") &&
      !boost::iequals (fileExtension, ".tiff")) {
    cerr << "!!! Warning !!!" << endl
	 << "Output image not a TIF file <" << fileName << ">" << endl;
    BOOST_ASSERT (false);
  }
  GDALDriver *driverTiff = GetGDALDriverManager ()->GetDriverByName ("GTiff");
  remove (fileName.c_str ());
  bandCount = nbBands;
  DimSideImg imageNbBands (GeoDimT < 3 ? nbBands : size.side [2]*nbBands);
  gdalOutputDataset = driverTiff->Create (fileName.c_str (), size.side [0], size.side [1], imageNbBands, this->dataType, NULL);
  LOG("gdalCount: " << gdalCount);
}

template<int GeoDimT>
void
IImage<GeoDimT>::createImage (const Size<GeoDimT> &size, const GDALDataType &dataType, const DimChannel &nbBands,
			  const IImage<GeoDimT> &inputImage, const Point<2> &topLeft)
{
  createImage (size, dataType, nbBands);
  string projectionRef;
  vector<double> geoTransform;
  inputImage.getGeo (projectionRef, geoTransform);
  setGeo (projectionRef, geoTransform, topLeft);
}

// ================================================================================
template<int GeoDimT>
void
IImage<GeoDimT>::close () {
  DEF_LOG ("IImage::close", "fileName: " << fileName);
  if (gdalOutputDataset) {
    GDALClose (gdalOutputDataset);
    gdalOutputDataset = nullptr;
    if (!--gdalCount)
      GDALDestroyDriverManager ();
  }
  if (gdalInputDataset) {
    GDALClose (gdalInputDataset);
    gdalInputDataset = nullptr;
    if (!--gdalCount)
      GDALDestroyDriverManager ();
  }
  BOOST_ASSERT (gdalCount >= 0);
  LOG ("gdalCount:" << gdalCount);
  read = false;
}

// ================================================================================
template<int GeoDimT>
ostream &
obelix::operator << (ostream &out, const IImage<GeoDimT> &iImage) {
  out << iImage.getFileName ();
  if (!iImage.isRead ())
    return out;
  return out << ": " << iImage.getSize ()
	     << " (" << iImage.getBandCount () << " channel(s) of " << GDALGetDataTypeName (iImage.getDataType ())  << ")";
}

// ================================================================================
template<int GeoDimT>
double
IImage<GeoDimT>::getNoDataValue (const DimChannel &band) const {
  DEF_LOG ("IImage::getNoDataValue", "band: " << band);
  GDALRasterBand &poBand = *gdalInputDataset->GetRasterBand (band+1);
  return poBand.GetNoDataValue ();
}

template<int GeoDimT>
template<typename PixelT>
void
IImage<GeoDimT>::readBand (Raster<PixelT, GeoDimT> &raster, const DimChannel &band) const {
  DEF_LOG ("IImage::readBand", "band: " << band);
  readBand (raster, band, Point<2> (), size);
}

template<int GeoDimT>
template<typename PixelT>
void
IImage<GeoDimT>::readBand (Raster<PixelT, GeoDimT> &raster, DimChannel band, const Point<2> &cropOrig, const Size<GeoDimT> &cropSize) const {
  DEF_LOG ("IImage::readBand",  "band: " << band << " crop: " << cropOrig << " - " << cropSize);
  raster.setSize (cropSize);
  DimSideImg nbChannel (GeoDimT < 3 ? 1 : size.side [2]);
  for (DimChannel z = 0; z < nbChannel; ++z)
    readFrame (raster.getFrame (z), mixedBandFlag ? (band+z*nbChannel) : (band*nbChannel+z),
	       cropOrig, Size<2> (cropSize.side [0], cropSize.side [1]));
}

template<int GeoDimT>
template<typename PixelT>
void
IImage<GeoDimT>::readFrame (PixelT *pixels, DimChannel band, const Point<2> &cropOrig, const Size<2> &cropSize) const {
  DEF_LOG ("IImage::readBand",  "band: " << band << " crop: " << cropOrig << " - " << cropSize);
  BOOST_ASSERT (gdalInputDataset);
  band++; // !!! GDAL bands start at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalInputDataset->GetRasterBand (band);
  GDALDataType bandType = poBand.GetRasterDataType ();
  CPLErr err = poBand.RasterIO (GF_Read, cropOrig.coord [0], cropOrig.coord [1], cropSize.side [0], cropSize.side [1],
				pixels, cropSize.side [0], cropSize.side [1], bandType, 0, 0);
  if (err != CE_None)
    cerr << "IImage::readBand: can't acces " << fileName << endl;
}

// ================================================================================
template<int GeoDimT>
template<typename PixelT>
void
IImage<GeoDimT>::writeBand (const PixelT *pixels, DimChannel band) const {
  DEF_LOG ("IImage::writeBand", "band: " << band << " size:" << size << " / " << getTriskeleType (pixels[0]));
  DimSideImg nbChannel (GeoDimT < 3 ? 1 : size.side [2]);
  for (DimChannel z = 0; z < nbChannel; ++z)
    writeFrame (pixels+size.getOffsetFrame (z), band*nbChannel+z);
}

template<int GeoDimT>
template<typename PixelT>
void
IImage<GeoDimT>::writeFrame (const PixelT *pixels, DimChannel band) const {
  DEF_LOG ("IImage::writeFrame", "band: " << band << " size:" << size << " / " << getTriskeleType (pixels[0]));
  BOOST_ASSERT (gdalOutputDataset);
  band++; // !!! GDAL layers starts at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalOutputDataset->GetRasterBand (band);
  CPLErr err = poBand.RasterIO (GF_Write, 0, 0, size.side [0], size.side [1],
				(PixelT *) pixels, size.side [0], size.side [1], getGDALType (pixels[0]), 0, 0);
  if (err != CE_None)
    cerr << "IImage::writeFrame: can't acces " << fileName << endl;
  // XXX log histogram
  // const DimImg pixelsCount = size.getPixelsCount ();
  // double maxV (0.);
  // for (DimImg i = 0; i < pixelsCount; ++i)
  //   if (pixels[i] != std::numeric_limits<double>::infinity ())
  //     maxV = max (maxV, double (pixels[i]));
  // double decil (maxV/10);
  // cout << "coucou :" << maxV << " d:" << decil << " p:" << pixelsCount << endl;
  // DimImg h[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  // for (DimImg i = 0; i < pixelsCount; ++i)
  //   if (pixels[i] != std::numeric_limits<double>::infinity ())
  //     ++h[int(pixels[i]/decil)];
  // for (DimImg i = 0; i < 11; ++i)
  //   cout << "h[" << i << "] :" << h[i] << endl;
}

// template<int GeoDimT>
// void
// IImage::writeDoubleBand (double *pixels, DimChannel band) const {
//   DEF_LOG ("IImage::writeDoubleBand", "band: " << band << " size:" << size << " / " << getTriskeleType (pixels[0]));
//   writeBand (pixels, band);
// }

// void
// IImage::writeDoubleFrame (double *pixels, DimChannel band) const {
//   DEF_LOG ("IImage::writeDoubleFrame", "band: " << band << " size:" << size << " / " << getTriskeleType (pixels[0]));
//   writeFrame (pixels, band);
// }

// void
// IImage::writeDimImgBand (DimImgPack *pixels, DimChannel band) const {
//   writeBand (pixels, band);
// }

// void
// IImage::writeDimImgFrame (DimImgPack *pixels, DimChannel band) const {
//   writeFrame (pixels, band);
// }

// ================================================================================
