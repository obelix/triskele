////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <math.h>
#include <sstream>

#if !defined(_WIN32) && !defined(_WIN64)
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#else
#include <windows.h>
#include <psapi.h>
#endif

#include "misc.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/GraphWalker.hpp"

using namespace obelix;
using namespace obelix::triskele;
using namespace std;
using namespace boost::filesystem;
#include "misc.tcpp"
template ostream &obelix::operator << <TreeType>(ostream &out, const vector<TreeType> &v);
template ostream &obelix::operator << <TileShape>(ostream &out, const vector<TileShape> &v);
template ostream &obelix::operator << <Neighbor>(ostream &out, const vector<Neighbor> &v);
//template ostream &obelix::operator << <DimImgPack>(ostream &out, const vector<DimImgPack> &v);
//template ostream &obelix::operator << <double>(ostream &out, const vector<double> &v);
//template ostream &obelix::operator << <DimImg>(ostream &out, const vector<DimImg> &v);

// ================================================================================
string
obelix::getOsName () {
#ifdef __linux__
  return "Linux";
#elif __FreeBSD__
  return "FreeBSD";
#elif __unix
  return "Unix";
#elif __unix__
  return "Unix";
#elif __APPLE__
  return "Mac OSX";
#elif __MACH__
  return "Mac OSX";
#elif _WIN32
  return "Windows 32-bit";
#elif _WIN64
  return "Windows 64-bit";
#else
  return "Unknowned OS";
#endif
}

// ================================================================================
uint16_t
obelix::getCols () {
#if defined (_WIN32) || defined (_WIN64)
  return 80;
#else
  struct winsize w;
  ioctl (0, TIOCGWINSZ, &w);
  return w.ws_col;
#endif
}

// ================================================================================
long
obelix::getPageSize () {
  return sysconf (_SC_PAGESIZE);
}

void
obelix::getMemSize (vector<double> &memMonitor) {
  memMonitor.resize (11);
#if defined (_WIN32) || defined (_WIN64)
  MEMORYSTATUSEX memInfo;
  memInfo.dwLength = sizeof (MEMORYSTATUSEX);
  GlobalMemoryStatusEx (&memInfo);
  DWORDLONG totalPhysMem = memInfo.ullTotalPhys;
  DWORDLONG freePhysMem = memInfo.ullAvailPhys;
  DWORDLONG totalVirtualMem = memInfo.ullTotalPageFile;
  DWORDLONG freeVirtualMem = memInfo.ullAvailPageFile;

  SIZE_T physMemUsedByMe = pmc.WorkingSetSize;

  memMonitor [0] = totalPhysMem-freePhysMem;
  memMonitor [1] = freePhysMem;
  memMonitor [2] = totalPhysMem;

  memMonitor [3] = (totalVirtualMem-totalPhysMem)-(freeVirtualMem-freePhysMem);
  memMonitor [4] = freeVirtualMem-freePhysMem;
  memMonitor [5] = totalVirtualMem-totalPhysMem;

  memMonitor [6] = totalVirtualMem-freeVirtualMem;
  memMonitor [7] = freeVirtualMem;
  memMonitor [8] = totalVirtualMem;

  memMonitor [9] = physMemUsedByMe;
  memMonitor [9] = memMonitor [0]; // XXX
#else
  struct sysinfo memInfo;
  sysinfo (&memInfo);
  memMonitor [0] = memInfo.totalram-memInfo.freeram;
  memMonitor [1] = memInfo.freeram;
  memMonitor [2] = memInfo.totalram;

  memMonitor [3] = memInfo.totalswap-memInfo.freeswap;
  memMonitor [4] = memInfo.freeswap;
  memMonitor [5] = memInfo.totalswap;

  memMonitor [6] = (memInfo.totalram-memInfo.freeram) + (memInfo.totalswap-memInfo.freeswap);
  memMonitor [7] = memInfo.freeram + memInfo.freeswap;
  memMonitor [8] = memInfo.totalram + memInfo.totalswap;

  for (int i = 0; i < 9; ++i)
    memMonitor[i] *= memInfo.mem_unit;

  char line [128];
  memMonitor [9] = 0;
  //FILE* file = fopen ("/proc/self/statm", "r");
  //fgets (line, 128, file);
  //memMonitor [9] = strtol (line, NULL, 10)*1024.;
  FILE* file = fopen ("/proc/self/status", "r");
  while (fgets (line, 128, file) != NULL)
    if (strncmp (line, "VmRSS:", 6) == 0) {
      memMonitor [9] = strtol (line+6, NULL, 10)*1024.;
      break;
    }
  fclose (file);

  memMonitor [10] = 0;
  file = fopen ("/proc/meminfo", "r");
  while (fgets (line, 128, file) != NULL)
    if (strncmp (line, "Active:", 7) == 0) {
      memMonitor [10] = strtol (line+7, NULL, 10)*1024.;
      break;
    }
  fclose (file);

#endif
}

ostream &
obelix::printMem (ostream &out) {
  vector<double> memMonitor;
  getMemSize (memMonitor);
  out << "phys: " << readableBytes (memMonitor[0]) << " " << readableBytes (memMonitor[1]) << " " << readableBytes (memMonitor[2]) << endl;
  // out << "swap: " << readableBytes (memMonitor[3]) << " " << readableBytes (memMonitor[4]) << " " << readableBytes (memMonitor[5]) << endl;
  out << "virt: " << readableBytes (memMonitor[6]) << " " << readableBytes (memMonitor[7]) << " " << readableBytes (memMonitor[8]) << endl;
  //out << "proc: " << readableBytes (memMonitor[9]) << endl;
  return out;
}

// ================================================================================
template <typename T>
ostream&
obelix::operator << (ostream &out, const vector<T> &v) {
  if (!v.empty ()) {
    out << "{";
    copy (v.begin (), v.end (), ostream_iterator<T> (out, ", "));
    out << "\b\b}";
  }
  return out;
}

// ================================================================================
int
obelix::bitCount [] = {
		       0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
		       1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
		       1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
		       2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
		       1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
		       2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
		       2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
		       3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

// ================================================================================
string
obelix::getCurrentTime (const string &format) {
  time_t rawtime = time (nullptr);

#if defined(_WIN32) || defined(_WIN64)
  struct tm timeinfo, *timeinfoP (&timeinfo);
  localtime_s (&timeinfo, &rawtime);
#else
  struct tm  *timeinfoP = localtime (&rawtime);
#endif

  ostringstream oss;
  oss << put_time (timeinfoP, format.c_str ());
  return oss.str ();
}

// ================================================================================
string
obelix::ns2string (const double &delta) {
  using namespace std::chrono;

  ostringstream oss;
  duration<double> ns (delta);
  oss.fill ('0');
  // typedef duration<int, ratio<86400> > days;
  // auto d = duration_cast<days>(ns);
  // ns -= d;
  auto h = duration_cast<hours> (ns);
  ns -= h;
  auto m = duration_cast<minutes> (ns);
  ns -= m;
  oss << setw (2) << h.count () << ":"
      << setw (2) << m.count () << ":"
      << setw (9) << fixed << setprecision (6) << ns.count ();
  return oss.str ();
}

// ================================================================================
string
obelix::readableBytes (double bytes) {
  if (!bytes)
    return "0 MB";
  bool neg = (bytes < 0);
  if (neg)
    bytes = -bytes;
  static string sizes[] = {"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
  int nbBytes = (int) floor (log (bytes) / log (1024));
  return (neg ? "-" : "")+boost::str (boost::format("%.2f ") % (bytes / pow (1024, nbBytes))) + sizes [nbBytes];
}

// ================================================================================
string
obelix::addExtensionIfNone (const string &fileName, const string &ext) {
  path p (fileName);
  if (!p.has_extension ())
    return fileName+ext;
  return fileName;
}
string
obelix::replaceExtension (const string &fileName, const string &ext) {
  path p (fileName);
  return p.replace_extension (ext).string ();
}

// ================================================================================
