////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_TextWin_tpp
#define _Obelix_TextWin_tpp

/**
 * \file TextWin.tpp
 * \brief Apply a pattern on a moving window to compute a "texture" attribute
 * 				(like Haar feature or statistics feature)
 * \author Francois Merciol, comments by Jerome More
 */

template<int GeoDimT>
inline
TextWin<GeoDimT>::TextWin (const Size<GeoDimT> &size, const Size<2> &sSize)
  /*! below ">>1" means "/2" using INTEGER division */
  /*! note: if sSize.side [0] is an even number, then wr=wl, and if sSize.side [0] is an odd number, then wr=wl+1 */
  /*! note: if sSize.side [1] is an even number, then ht=hb, and if sSize.side [1] is an odd number, then ht=hb+1 */
  : w (sSize.side [0]),
    h (sSize.side [1]),
    hw (w >> 1),
    hh (h>>1),
    xFloor (sSize.side [0]>>1),
    xCeil (size.side [0]-1-w+xFloor),
    yFloor (sSize.side [1]>>1),
    yCeil (size.side [1]-1-h+yFloor),
    size (size),
    iw (w),
    ih (h*size.side [0]),
    ihw (hw),
    ihh (hh*size.side [0]),
    ilt (ihh+hw),
    sCount (sSize.getFramePixelsCount ()) {
  DEF_LOG ("Haar::Haar", "size:" << size <<
  	   " xFloor:" << xFloor << " xCeil:" << xCeil << " yFloor:" << yFloor << " yCeil:" << yCeil <<
  	   " iw:" << iw << " ih:" << ih << " ihw:" << ihw << " ihh:" << ihh <<
  	   " sCount:" << sCount << " ilt:" << ilt);
  BOOST_ASSERT (sSize.side [0] < size.side [0]);
  BOOST_ASSERT (sSize.side [1] < size.side [1]);
  BOOST_ASSERT (GeoDimT == 2);

}

template<int GeoDimT>
inline void
TextWin<GeoDimT>::resizeSlidingWindow (const DimSideImg &x, const DimSideImg &y, DimImg &w2, DimImg &h2, DimImg &dx, DimImg &dy) const {
  if (x < xFloor) {
    dx = x;
    w2 -= xFloor-x;
    if (w2%2 != w%2)
      ++w2;
  }
  if (x > xCeil) {
    w2 -= x-xCeil;
    if (w2%2 != w%2)
      --w2;
  }
  if (y < yFloor) {
    dy = y;
    h2 -= yFloor-y;
    if (h2%2 != h%2)
      ++h2;
  }
  if (y >= yCeil) {
    h2 -= y-yCeil;
    if (h2%2 != h%2)
      --h2;
  }
}

// ========================================
template<int GeoDimT>
inline double
TextWin<GeoDimT>::getHx (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &o) const {
  double hx = 0;
  /*! Hx = f + 2b + d − a − c − 2e <br>
   * _ a = Center - LeftTop
   * _ b = Center - Top
   * _ c = Center - RightTop
   * _ d = Center + LeftBottom
   * _ e = Center + Bottom
   * _ f = Center + RightBottom
   */
  if (x < xFloor || x >= xCeil ||
      y < yFloor || y >= yCeil) {
    DimSideImg
      w2 (w),
      h2 (h),
      dx (hw),
      dy (hh);
    resizeSlidingWindow (x, y, w2, h2, dx, dy);
    const DimImg
      lt2 (dx+dy*size.side [0]),
      ihw2 (w2>>1),
      iw2 (w2),
      ih2 (h2*size.side [0]);
    const DimImg a (o-lt2);
    hx += iiValues [a];			// + a
    hx -= 2 * iiValues [a+ihw2];	// -2b
    hx += iiValues [a+iw2];		// + c
    hx -= iiValues [a+ih2];		// - d
    hx += 2 * iiValues [a+ih2+ihw2];	// +2e
    hx -= iiValues [a+ih2+iw2];		// - f
    return hx/(w2*h2);
  }
  const DimImg a (o - ilt);
  hx += iiValues [a];			// + a
  hx -= 2 * iiValues [a+ihw];		// -2b
  hx += iiValues [a+iw];		// + c
  hx -= iiValues [a+ih];		// - d
  hx += 2 * iiValues [a+ih+ihw];	// +2e
  hx -= iiValues [a+ih+iw];		// - f
  return hx/sCount;
}

// ========================================
template<int GeoDimT>
inline double
TextWin<GeoDimT>::getHy (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &o) const {
  double hy = 0;
  if (x < xFloor || x >= xCeil ||
      y < yFloor || y >= yCeil) {
    DimSideImg
      w2 (w),
      h2 (h),
      dx (hw),
      dy (hh);
    resizeSlidingWindow (x, y, w2, h2, dx, dy);
    const DimImg
      lt2 (dx+dy*size.side [0]),
      iw2 (w2),
      ihh2 ((h2>>1)*size.side [0]),
      ih2 (h2*size.side [0]);
    const DimImg a (o-lt2);
    hy -= iiValues [a];			// - a
    hy += 2 * iiValues [a+ihh2];	// +2b
    hy -= iiValues [a+ih2];		// - c
    hy += iiValues [a+iw2];		// + d
    hy -= 2 * iiValues [a+iw2+ihh2];	// -2e
    hy += iiValues [a+iw2+ih2];		// + f
    return hy/(w2*h2);
  }
  const DimImg a (o - ilt);
  hy -= iiValues [a];			// - a
  hy += 2 * iiValues [a+ihh];		// +2b
  hy -= iiValues [a+ih];		// - c
  hy += iiValues [a+iw];		// + d
  hy -= 2 * iiValues [a+w+ihh];		// -2e
  hy += iiValues [a+iw+ih];		// + f
  return hy/sCount;
}

// ========================================
template<int GeoDimT>
inline void
TextWin<GeoDimT>::getMeanStdEnt (double &mean, double &std, double &ent,
				 const double* iiValues, const double* doubleiiValues,
				 const DimSideImg &x, const DimSideImg &y, const DimImg &o) const {
  double area = 0, doubleArea = 0;
  DimImg
    iw2 (iw),
    ih2 (ih),
    ilt2 (ilt),
    sCount2 (sCount);
  if (x < xFloor || x > xCeil ||
      y < yFloor || y > yCeil) {
    DimImg
      w2 (w),
      h2 (h),
      dx (hw),
      dy (hh);
    if (x < xFloor) {
      dx = x;
      w2 -= xFloor-x;
    }
    if (x > xCeil)
      w2 -= x-xCeil;
    if (y < yFloor) {
      dy = y;
      h2 -= yFloor-y;
    }
    if (y >= yCeil)
      h2 -= y-yCeil;
    iw2 = w2;
    ih2 = h2*size.side [0];
    ilt2 = dx+dy*size.side [0];
    sCount2 = w2*h2;
  }
  const DimImg a (o - ilt2);

  area += iiValues [a+iw2+ih2];			// bottom-right
  doubleArea += doubleiiValues [a+iw2+ih2];

  area += iiValues [a];				// top-left
  doubleArea += doubleiiValues [a];

  area -= iiValues [a+iw2];			// top-right
  doubleArea -= doubleiiValues [a+iw2];

  area -= iiValues [a+ih2];			// bottom-left
  doubleArea -= doubleiiValues [a+ih2];

  mean = area/sCount2;
  std = doubleArea/sCount2 - mean*mean;
  ent = area ? 1 - doubleArea/(area*area) : 1;
}

// ================================================================================
#endif // _Obelix_TextWin_tpp
