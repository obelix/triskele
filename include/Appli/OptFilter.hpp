////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_OptFilter_hpp
#define _Obelix_Triskele_OptFilter_hpp

#include <iostream>
#include <fstream>
#include <vector>

#include "obelixGeo.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "obelixPantex.hpp"

#include "Appli/OptCrop.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptChannel.hpp"
#include "CompAttribute.hpp"

namespace obelix {
  namespace triskele {
    using namespace std;

    // ================================================================================
    class OptFilter {
    public:
      bool		debugFlag;
      bool		timeFlag;
      bool		countFlag;
      DimSortCeil	countingSortCeil;
      string		showConfig;
      bool		noWriteFlag;
      bool		oneBandFlag;
      DimPantexOcc	pantexWindowSide;
      bool		includeNoDataFlag;
      double		noDataValue;
      string		inputFilename;
      string		outputFilename;
      Connectivity	connectivity;
      DimCore		coreCount;
      DimCore		threadPerImage;
      DimCore		tilePerThread;
      bool		seqTileFlag;
      bool		autoThreadFlag;
      bool		memImpactFlag;
      GDALDataType	inputDataType;

      OptCrop		optCrop;
      OptChannel	optChannel;

      OptFilter ();
      OptFilter (int argc, char** argv);

      ofstream out;

      void usage (string msg = "", bool hidden = false) const;
      void version () const;
      void parse (int argc, char** argv);
    };

    // ================================================================================
    ostream &operator << (ostream &out, const OptFilter &optFilter);

    template <typename T>
    ostream &operator << (ostream &out, const vector<T> &v);

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptGenerator_hpp
