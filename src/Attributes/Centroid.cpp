////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/Centroid.hpp"
using namespace obelix::triskele;
#include "Centroid.tcpp"

// ================================================================================
typedef CompAttribute<AverageCoord<2>, 2> CA_2_2;

template<>
void
Centroid<2>::compute (const AreaAttributes<2> &areaAttributes) {
  DEF_LOG ("Centroid::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CA_2_2::tree.getSize ().side [0];

  CA_2_2::tree.forEachComp
    ([this] (const DimParent &compId) {
       AverageCoord<2> &avc (CA_2_2::values[compId]);
       avc.coord [0] = avc.coord [1] = 0;
     });

  CA_2_2::tree.forEachLeaf
    ([this, &width] (const DimImg &leafId) {
       const DimParent parentId (CA_2_2::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimSideImg y = leafId / width;
       AverageCoord<2> &avp (CA_2_2::values[parentId]);
       avp.coord [0] += x;
       avp.coord [1] += y;
     });

  const DimImgPack *areas = areaAttributes.getValues ();
  CA_2_2::tree.forEachComp
    ([this, &areas] (const DimParent &compId) {
       AverageCoord<2> &avc (CA_2_2::values[compId]);
       const DimImg area (areas[compId]);
       avc.coord [0] /= area;
       avc.coord [1] /= area;
       const DimParent parentId (CA_2_2::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       AverageCoord<2> &avp (CA_2_2::values[parentId]);
       avp.coord [0] += avc.coord [0] * area;
       avp.coord [1] += avc.coord [1] * area;
     });
  TreeStats::global.addTime (centroidStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<int GeoDimT>
Centroid<GeoDimT>::Centroid (const Tree<GeoDimT> &tree, const AreaAttributes<GeoDimT> &areaAttributes)
  : CompAttribute<AverageCoord<GeoDimT>, GeoDimT> (tree) {
  DEF_LOG ("Centroid::Centroid", "");
  compute (areaAttributes);
}

template<int GeoDimT>
Centroid<GeoDimT>::~Centroid () {
  DEF_LOG ("Centroid::~Centroid", "");
}

// ================================================================================
template<int GeoDimT>
void
Centroid<GeoDimT>::compute (const AreaAttributes<GeoDimT> &areaAttributes) {
  DEF_LOG ("Centroid::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CA::tree.getSize ().side [0];
  DimImg const height = CA::tree.getSize ().side [1];

  CA::tree.forEachComp
    ([this] (const DimParent &compId) {
       AverageCoord<GeoDimT> &avc (CA::values[compId]);
       avc.coord [0] = avc.coord [1] = avc.coord [2] = 0;
     });

  CA::tree.forEachLeaf
    ([this, &width, &height] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
	 return;
       const DimSideImg x = leafId % width;
       const DimImg yz = leafId / width;
       const DimSideImg y = yz % height;
       const DimChannel z = yz / height;
       AverageCoord<GeoDimT> &avp (CA::values[parentId]);
       avp.coord [0] += x;
       avp.coord [1] += y;
       avp.coord [2] += z;
     });

  const DimImgPack *areas = areaAttributes.getValues ();
  CA::tree.forEachComp
    ([this, &areas] (const DimParent &compId) {
       AverageCoord<GeoDimT> &avc (CA::values[compId]);
       const DimImg area (areas[compId]);
       for (int i = 0; i < GeoDimT; ++i)
	 avc.coord [i] /= area;
       const DimParent parentId (CA::tree.getCompParent (compId));
       if (parentId == DimParent_MAX)
	 return;
       AverageCoord<GeoDimT> &avp (CA::values[parentId]);
       for (int i = 0; i < GeoDimT; ++i)
	 avp.coord [i] += avc.coord [i] * area;
     });
  TreeStats::global.addTime (centroidStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
