////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "misc.hpp" // operator<< (vector)
#include "obelixGeo.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace std;
using namespace obelix;
using namespace obelix::triskele;

static bool show (false); // log or not

static const int GeoDimT (3);

// ================================================================================
void
testDelta (const Size<GeoDimT> &size, const vector<int> &deltaVx) {

  // ####################
  {
    Point<GeoDimT> a (11, 22, 33);
    Point<GeoDimT> b (a.coord [0]+deltaVx[0], a.coord [1]+deltaVx[1], a.coord [2]+deltaVx[2]);
    DimImg idxA (point2idx (size, a));
    DimImg idxB (point2idx (size, b));
    DeltaPoint<GeoDimT> delta (size, idxA, idxB);
    DeltaPoint<GeoDimT> deltaInv (size, idxB, idxA);
    if (show)
      cout << "size     : " << size << endl
	   << "a        : " << a << "   ( <=> " << idxA << ")" << endl
	   << "b        : " << b << "   ( <=> " << idxB << ")" << endl
	   << "delta    : " << delta << endl
	   << "deltaInv : " << deltaInv << endl
	   << endl;
    REQUIRE (delta.delta.x == deltaVx[0]);
    REQUIRE (delta.delta.y == deltaVx[1]);
    REQUIRE (delta.delta.z == deltaVx[2]);

    REQUIRE (deltaInv.delta.x == -deltaVx[0]);
    REQUIRE (deltaInv.delta.y == -deltaVx[1]);
    REQUIRE (deltaInv.delta.z == -deltaVx[2]);
  }

  // ####################
  {
    Point<GeoDimT> a (11, 22, 33);
    DimImg idxA (point2idx (size, a));
    DeltaPoint<GeoDimT> delta (deltaVx[0], deltaVx[1], deltaVx[2]);
    DimImg idxB (delta.addIdx (size, idxA));
    Point<GeoDimT> b (idx2point (size, idxB));
    if (show)
      cout << "size     : " << size << endl
	   << "a        : " << a << "   ( <=> " << idxA << ")" << endl
	   << "delta    : " << delta << endl
	   << "b        : " << b << "   ( <=> " << idxB << ")" << endl
	   << endl;
    REQUIRE (b.coord [0] == a.coord [0]+deltaVx[0]);
    REQUIRE (b.coord [1] == a.coord [1]+deltaVx[1]);
    REQUIRE (b.coord [2] == a.coord [2]+deltaVx[2]);

  }


  // ####################
  int weight (100);
  {
    Point<GeoDimT> a (11, 22, 33);
    Point<GeoDimT> b (a.coord [0]+deltaVx[0], a.coord [1]+deltaVx[1], a.coord [2]+deltaVx[2]);
    DimImg idxA (point2idx (size, a));
    DimImg idxB (point2idx (size, b));
    Edge<uint16_t, GeoDimT> edge, edgeInv;
    edge.setAB (size, idxA, idxB);
    edge.setWeight (weight);
    edgeInv.setAB (size, idxB, idxA);
    edgeInv.setWeight (weight);
    if (show)
      cout << "size     : " << size << endl
	   << "a        : " << a << "   ( <=> " << idxA << ")" << endl
	   << "b        : " << b << "   ( <=> " << idxB << ")" << endl
	   << "deltaVx  : " << deltaVx << endl
	   << "edge     : " << printEdge (edge, size) << endl
	   << "edgeInv  : " << printEdge (edgeInv, size) << endl
	   << endl;
    DimImg eA (edge.getA ());
    DimImg eB (edge.getB (size));
    DimImg eInvA (edgeInv.getA ());
    DimImg eInvB (edgeInv.getB (size));

    REQUIRE (eA == eInvA);
    REQUIRE (eB == eInvB);
    if (idxA <= idxB) {
      REQUIRE (eA == idxA);
      REQUIRE (eB == idxB);
    } else {
      REQUIRE (eA == idxB);
      REQUIRE (eB == idxA);
    }
  }
  cout << endl;
}

// ================================================================================
TEST_CASE ("Testing Delta...") {
  Size<GeoDimT> size (100, 80, 1);

  vector<vector<int> > deltaValues =
    {{ 1,  0, -1},
     {-1,  1,  0},
     { 0, -1,  1}};

  for (vector<int> deltaVx : deltaValues)
    testDelta (size, deltaVx);

  // vector<vector<int> > delta2Values =
  //   {{ 2,  0, -2},
  //    {-2,  2,  0},
  //    { 0, -2,  2}};

  // for (vector<int> deltaVx : delta2Values)
  //   //REQUIRE_THROWS_AS (testDelta (size, deltaVx), std::invalid_argument);
  //   testDelta (size, deltaVx);


}

// ================================================================================
