////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Triskele_CompAttribute_tpp
#define _Obelix_Triskele_CompAttribute_tpp

// ================================================================================
template<typename AttrT, int GeoDimT>
inline ostream &
CompAttribute<AttrT, GeoDimT>::print (ostream &out, const string &msg) const {
  cout << "values: " << msg << endl;
  cout << printMap (values.data (), tree.getSize ().getDoubleHeight (), tree.getCompCount ()) << endl << endl;
  return out;
}

// ================================================================================
template<typename AttrT, int GeoDimT>
template<typename CumpFunctPSE>
inline void
CompAttribute<AttrT, GeoDimT>::computeSameCompLevel (const CumpFunctPSE &cumpFunctPSE) const {
  // CompAttribute<AttrT, GeoDimT>::tree.forEachComp (cumpFunctPSE);

  const vector<DimParentPack> &weightBounds (CompAttribute<AttrT, GeoDimT>::tree.getWeightBounds ());
  DimCore coreCount = CompAttribute<AttrT, GeoDimT>::tree.getCoreCount ();
  DEF_LOG ("CompAttribute::computeSameCompLevel", "coreCount:" << coreCount);
  if (weightBounds.empty () || CompAttribute<AttrT, GeoDimT>::tree.getCompCount ()/weightBounds.size () < coreCount) {
    LOG ("CompAttribute::computeSameCompLevel: no thread");
    CompAttribute<AttrT, GeoDimT>::tree.forEachComp (cumpFunctPSE);
    return;
  }
  DimParent first = weightBounds [0];
  for (DimParent curBound = 1; curBound < weightBounds.size (); ++curBound) {
    DimParent next = weightBounds [curBound];
    dealThread (next-first, coreCount, [&first, &cumpFunctPSE] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
					 const DimParent end (maxVal+first);
					 for (DimParent parentId = minVal+first; parentId < end; ++parentId)
					   cumpFunctPSE (parentId);
				       });
    first = next;
  }
}

// ================================================================================
template<typename AttrT, int GeoDimT>
template<typename CumpFunctPSE>
inline void
CompAttribute<AttrT, GeoDimT>::computeSameCompLevelFromRoot (const CumpFunctPSE &cumpFunctPSE) const {
  // CompAttribute<AttrT, GeoDimT>::tree.forEachCompFromRoot (cumpFunctPSE);

  const vector<DimParentPack> &weightBounds (CompAttribute<AttrT, GeoDimT>::tree.getWeightBounds ());
  DimCore coreCount = CompAttribute<AttrT, GeoDimT>::tree.getCoreCount ();
  DEF_LOG ("CompAttribute::computeSameCompLevelFromRoot", "coreCount:" << coreCount);
  if (weightBounds.empty () || CompAttribute<AttrT, GeoDimT>::tree.getCompCount ()/weightBounds.size () < coreCount) {
    LOG ("CompAttribute::computeSameCompLevel: no thread");
    CompAttribute<AttrT, GeoDimT>::tree.forEachCompFromRoot (cumpFunctPSE);
    return;
  }
  DimParent last = weightBounds [weightBounds.size ()-1];
  for (DimParent curBound = weightBounds.size ()-1; ; --curBound) {
    DimParent prev = weightBounds [curBound];
    dealThread (prev-last, coreCount, [&prev, &cumpFunctPSE] (const DimCore &threadId, const DimParent &minVal, const DimParent &maxVal) {
					const DimParent end (maxVal+prev);
					for (DimParent parentId = minVal+prev; parentId < end; ++parentId)
					  cumpFunctPSE (parentId);
				      });
    last = prev;
    if (!curBound)
      break;
  }
}

// ================================================================================
#endif // _Obelix_Triskele_CompAttribute_tpp
