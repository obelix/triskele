////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <boost/chrono.hpp>
#include <algorithm>    // std::fill
using namespace boost::chrono;

#include "obelixGeo.hpp"
#include "Attributes/STSAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "STSAttributes.tcpp"

// ================================================================================
template<int GeoDimT>
STSAttributes<GeoDimT>::STSAttributes (const Tree<GeoDimT> &tree)
  : CompAttribute<double, GeoDimT> (tree) {
  DEF_LOG ("STSAttributes::STSAttributes", "");
  compute ();
}

template<int GeoDimT>
STSAttributes<GeoDimT>::~STSAttributes () {
  DEF_LOG ("STSAttributes::~STSAttributes", "");
}

// ================================================================================
template<int GeoDimT>
void
STSAttributes<GeoDimT>::compute () {
  DEF_LOG ("STSAttributes::compute", "");
  BOOST_ASSERT (GeoDimT > 2);
  auto start = high_resolution_clock::now ();


  const Size<GeoDimT> size (CA::tree.getSize ());
  const DimSideImg depth (size.side [2]);
  if (depth < 2) {
    // BOOST_ASSERT (depth > 1); // XXX
    fill (CA::values.begin (), CA::values.end (), 0.);
    return;
  }
  const DimImg framePixelsCount (size.getFramePixelsCount ());
  const DimParent compCount (CA::tree.getCompCount ());

  vector <vector<DimImgPack> > compAreaTab (compCount, vector <DimImgPack> (depth, 0));
  // fill (&compAreaTab [0][0], &compAreaTab [compCount-1][depth-1], 0);

  CA::tree.forEachLeaf
    ([this, &compAreaTab, &framePixelsCount] (const DimImg &leafId) {
       const DimParent parentId (CA::tree.getLeafParent (leafId));
       if (parentId == DimParent_MAX)
  	 return;
       ++compAreaTab [parentId][leafId / framePixelsCount];
     });

  CA::tree.forEachComp
    ([this, &depth, &compAreaTab] (const DimParent &compId) {
       const DimParent parentId (CA::tree.getCompParent (compId));
       DimImg * const compTab (&compAreaTab [compId][0]);

       double sum (0.);
       for (DimChannel z = 1; z < depth; ++z) {
	 DimImg a (compTab [z-1]);
	 DimImg b (compTab [z]);
	   if (a > b)
	     swap (a, b);
	   if (b)
	     sum += a/b;
       }
       CA::values [compId] = sum / (depth-1);

       if (parentId != DimParent_MAX) {
  	 return;
	 DimImg * const parTab (&compAreaTab [parentId][0]);
	 for (DimChannel z = 0; z < depth; ++z)
	   parTab [z] += compTab [z];
       }
     });

  TreeStats::global.addTime (areaStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
