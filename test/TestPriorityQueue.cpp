////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <stdlib.h>     /* srand, rand */

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "ArrayTree/triskelePriorityQueue.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp>

using namespace std;
using namespace obelix;
using namespace obelix::triskele;

static bool show (true); // log or not
typedef uint16_t PixelT;

// ================================================================================
void
testPriorityQueue (const DimImg &maxSize, const PixelT &startLevel) {
  if (show)
    cout << "maxSize:" << maxSize << endl;

  PriorityQueue<PixelT> pq (maxSize);
  PixelT level ((maxSize % numeric_limits<PixelT>::max ())/2);
  
  for (DimImg i = 0; i < maxSize; ++i) {
    PixelT lower = rand () % maxSize % numeric_limits<PixelT>::max ();
    PixelT upper = rand () % maxSize % numeric_limits<PixelT>::max ();
    if (lower > upper)
      swap (lower, upper);

    PixelT levelP (level);
    if (levelP < lower)
      levelP = lower;
    else if (upper < levelP)
      levelP = upper;
    pq.push (levelP, lower, upper);
  }
  DimImg idxa (0), idxb (0);
  for (PixelT lastLevel (level = startLevel); ! pq.empty (); lastLevel = level) {
    pq.pop (level, idxa, idxb);
    if (show)
      cout << " " << level << " (" << idxa << "-" << idxb << ")";
    if (!startLevel)
      REQUIRE (level >= lastLevel);
    else if (startLevel == maxSize)
      REQUIRE (level <= lastLevel);
    else
      ;
  }
  if (show)
    cout << endl;
}

// ================================================================================
//static unsigned int seed (badValue); // XXX if catch bad test
static unsigned int seed (time (NULL));

TEST_CASE ("Testing PriorityQueue...") {
  cout << "Used seed: " << seed << endl;
  srand (seed);
  // Log::debug = true;

  // testPriorityQueue (4, 0);

  testPriorityQueue (32, 0);
  testPriorityQueue (32, 32);
  testPriorityQueue (32, 16);

  if (show)
    cout << endl;
}
