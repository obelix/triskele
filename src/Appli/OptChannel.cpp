////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <boost/assign.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <set>
#include <queue>
#include <algorithm>
#include <tinyxml.h>

#include "misc.hpp"
#include "obelixDebug.hpp"
#include "triskeleGdalGetType.hpp"

#include "Appli/OptChannel.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
const string
OptChannel::chExt (".ch");

// ================================================================================
const string
obelix::triskele::apOrigPosLabels[] =
  {
   "apOrigNoPos", "apOrigPosBegin", "apOrigPosEnd", "apOrigPosBoth", "apOrigPosEverywhere"
  };
const map<string, ApOrigPos>
obelix::triskele::ApOrigPosMap = boost::assign::map_list_of
  ("aporignopos", apOrigNoPos)
  ("aporigposbegin", apOrigPosBegin)
  ("aporigposend", apOrigPosEnd)
  ("aporigposboth", apOrigPosBoth)
  ("aporigposeverywhere", apOrigPosEverywhere)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const ApOrigPos &pos) {
  BOOST_ASSERT (pos >= apOrigNoPos && pos <= apOrigPosEverywhere);
  return out << apOrigPosLabels[pos];
}

istream &
obelix::triskele::operator >> (istream &in, ApOrigPos &apOrigPos) {
  string token;
  in >> token;
  auto pos = ApOrigPosMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == ApOrigPosMap.end ())
    in.setstate (ios_base::failbit);
  else
    apOrigPos = pos->second;
  return in;
}

// ================================================================================
const string
obelix::triskele::channelTypeLabels[] =
  {
   "Original", "Ndvi", "Pantex", "Sobel", "AP", "DAP", "Haar", "Stat"
  };

const map<string, ChannelType>
obelix::triskele::channelTypeMap = boost::assign::map_list_of
  ("original", Original)
  ("ndvi", Ndvi)
  ("pantex", Pantex)
  ("sobel", Sobel)
  ("ap", AP)
  ("dap", DAP)
  ("haar", Haar)
  ("stat", Stat)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const ChannelType &channelType) {
  if (channelType >= Original && channelType <= Stat)
    return out << channelTypeLabels[channelType];
  BOOST_ASSERT (false);
  return out << (int) channelType;
}

istream &
obelix::triskele::operator >> (istream &in, ChannelType &channelType) {
  string token;
  in >> token;
  auto pos = channelTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == channelTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    channelType = pos->second;
  return in;
}

const string
obelix::triskele::featureOutputTypeLabels[] =
  {
   "PixelFeature", "DimFeature", "ChannelFeature", "DoubleFeature", "FeatureOutputTypeCount"
  };

ostream &
obelix::triskele::operator << (ostream &out, const FeatureOutputType &feature) {
  BOOST_ASSERT (feature >= PixelFeature && feature <= FeatureOutputTypeCount);
  return out << featureOutputTypeLabels[feature];
}

// ================================================================================
InputChannel::InputChannel (const ChannelType &channelType, const DimChannel &inputId, const bool &doWrite)
  : channelType (channelType),
    inputId (inputId),
    treeType (MIN),
    featureType (FAP),
    attributeType (AArea),
    dapWeight (false),
    apOrigPos (apOrigPosBegin),
    doWrite (doWrite) {
  resetTransient ();
  DEF_LOG ("InputChannel::InputChannel", "inputId: " << inputId << " this: " << *this);
}
InputChannel::InputChannel (const ChannelType &channelType, const DimChannel &inputId, const bool &doWrite, const vector<InputChannel *> &generateBy)
  : InputChannel (channelType, inputId, doWrite) {
  this->generateBy = generateBy;
}

void
InputChannel::resetTransient () {
  prodWrite = prodNewImage = prodInetgralImage = prodDoubleInetgralImage = prodDap = false;
  copyRank = typeRank = origDapRank = integralImageRank = doubleIntegralImageRank = outputRank = 0;
}

bool
InputChannel::updateProdWrite () {
  return prodWrite = prodWrite || doWrite || [this] {
					       for (InputChannel* child : produce)
						 if (child->updateProdWrite ())
						   return true;
					       return false;
					     } ();
}

// ================================================================================
vector<DimChannel>
InputChannel::getRelIdx () const {
  BOOST_ASSERT (channelType == DAP);
  DimChannel nbApSrc (generateBy[0]->thresholds.size ());
  vector<DimChannel> result;
  DimChannel nbDapIdx (nbApSrc);
  switch (apOrigPos) {
  case apOrigNoPos:		--nbDapIdx; break;
  case apOrigPosBoth:		++nbDapIdx; break;
  default:
    ;//cout << "This apOrigPos value is not handled\n"; exit(1);
  }
  nbDapIdx *= 2;
  result.resize (nbDapIdx, DimChannel (0));
  if (!nbDapIdx)
    return result;
  if (apOrigPos == apOrigPosEverywhere) {
    for (DimChannel i = 0; i < nbApSrc; ++i)
      result [2*i+1] = i+1;
    return result;
  }

  DimChannel start = !(apOrigPos == apOrigPosBegin ||
		       apOrigPos == apOrigPosBoth);
  for (DimChannel i = 0; i < nbDapIdx; ) {
    result [i++] = start;
    result [i++] = ++start;
  }
  if (apOrigPos == apOrigPosEnd ||
      apOrigPos == apOrigPosBoth)
    result [nbDapIdx-1] = 0;
  return result;
}

// ================================================================================
void
InputChannel::writeXml (TiXmlElement *triskeleNode) const {
  DEF_LOG ("InputChannel::writeXml", *this);
  if (channelType == Original && !doWrite)
    return;
  TiXmlElement *elem (new TiXmlElement (channelTypeLabels [channelType]));
  elem->SetAttribute ("id", inputId);
  if (doWrite)
    elem->SetAttribute ("doWrite", "true");
  if (generateBy.size ())
    elem->SetAttribute ("from", generateBy[0]->inputId);
  if (generateBy.size () > 1)
    elem->SetAttribute ("from2", generateBy[1]->inputId);
  for (Size<2> size : winSizes) {
    TiXmlElement *elemSize (new TiXmlElement ("Size"));
    elemSize->SetAttribute ("width", size.side [0]);
    elemSize->SetAttribute ("height", size.side [1]);
    elem->LinkEndChild (elemSize);
  }
  if (channelType == AP) {
    elem->SetAttribute ("treeType", treeTypeLabels[treeType]);
    elem->SetAttribute ("featureType", featureTypeLabels[featureType]);
    elem->SetAttribute ("attributeType", attributeLabels[attributeType]);
  }
  for (double threshold : thresholds) {
    TiXmlElement *elemThreshold (new TiXmlElement ("Cut"));
    elemThreshold->SetAttribute ("threshold", threshold);
    elem->LinkEndChild (elemThreshold);
  }
  if (channelType == DAP) {
    if (dapWeight)
      elem->SetAttribute ("dapWeight", "true");
    elem->SetAttribute ("apOrigPos", apOrigPosLabels[apOrigPos]);
  }
  for (string comment : localComments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    elem->LinkEndChild (child);
  }
  triskeleNode->LinkEndChild (elem);
}

ostream &
obelix::triskele::operator << (ostream &out, const InputChannel &inputChannel) {
  out << "[" << inputChannel.channelType << " " << inputChannel.inputId;
  if (inputChannel.generateBy.size ()) {
    out << "(" << inputChannel.generateBy[0]->inputId;
    if (inputChannel.generateBy.size () > 1)
      out << "," << inputChannel.generateBy[1]->inputId;
    out << ")";
  }
  if (inputChannel.doWrite)
    out << " > " << inputChannel.outputRank << " C:" << inputChannel.copyRank;
  if (inputChannel.prodWrite)
    out << " R:" << inputChannel.typeRank;
  if (inputChannel.prodInetgralImage)
    out << " II:" << inputChannel.integralImageRank;
  if (inputChannel.prodDoubleInetgralImage)
    out << " dII:" << inputChannel.doubleIntegralImageRank;
  if (inputChannel.prodNewImage)
    out << " prodNewImage";
  if (inputChannel.prodDap)
    out << " prodDap";
  if (inputChannel.winSizes.size ())
    out << " " << inputChannel.winSizes;
  if (inputChannel.thresholds.size ())
    obelix::operator<< (out << " " << inputChannel.treeType << " " << inputChannel.featureType << " " << inputChannel.attributeType << " ", inputChannel.thresholds);
  if (inputChannel.channelType == DAP)
    out << (inputChannel.dapWeight ? " dapWeight" : "") << " " << inputChannel.apOrigPos;
  return out << "]";
}

// ================================================================================
void
OptChannel::resetTransient () {
  nextChannelUniqId = copyCount = originalUsedCount = origDapUsedCount = ndviUsedCount = pantexUsedCount = sobelUsedCount = integralImageCount = doubleIntegralImageCount = apCount = dapCount = haarCount = statCount = outputCount = 0;
}

DimChannel
OptChannel::getSize (const ChannelType &channelType) const {
  return channels [channelType].size ();
}

OptChannel::OptChannel ()
  : created (getCurrentTime ()),
    inputType (GDT_Unknown),
    channels (ChannelType::Stat+1, vector <InputChannel *> (0)),
    selectedTreeType (MIN),
    selectedFeatureType (FAP),
    selectedAttributeType (AArea),
    spectralDepth (0),
    mixedBandFlag (false),
    dim (2) {
  resetTransient ();
}

OptChannel::~OptChannel () {
}

bool
OptChannel::operator == (const OptChannel &rhs) const {
  try {
    if (inputType != rhs.inputType ||
	channels[Original].size () != rhs.channels[Original].size () ||
	outputOrder.size () != rhs.outputOrder.size ())
      return false;
    // XXX verif apOrder, dapOrder, haarOrder, statOrder
    for (map<DimChannel, InputChannel *>::const_iterator it = outputOrder.cbegin ();
	 it != outputOrder.cend (); ++it) {
      DimChannel rank = it->first;
      const InputChannel *left (outputOrder.at (rank)), *right (rhs.outputOrder.at (rank));
      if (left->channelType != right->channelType ||
	  left->typeRank != right->typeRank)
	return false;
      for (DimChannel parentId = 0; parentId < left->generateBy.size (); ++parentId)
	if (left->generateBy [parentId]->channelType != right->generateBy [parentId]->channelType ||
	    left->generateBy [parentId]->typeRank != right->generateBy [parentId]->typeRank)
	  return false;
      switch (left->channelType) {
      case Original:
      case Ndvi:
      case Pantex:
      case Sobel:
	break;
      case AP:
	if (left->treeType != right->treeType ||
	    left->featureType != right->featureType ||
	    left->attributeType != right->attributeType ||
	    left->thresholds.size () != right->thresholds.size ())
	  return false;
	for (DimChannel idx = 0; idx < left->thresholds.size (); ++idx)
	  if (left->thresholds[idx] != right->thresholds[idx])
	    return false;
	break;
      case DAP:
	if (left->dapWeight != right->dapWeight ||
	    left->apOrigPos != right->apOrigPos)
	  return false;
      case Haar:
      case Stat:
	if (left->winSizes.size () != right->winSizes.size ())
	  return false;
	for (DimChannel idx = 0; idx < left->winSizes.size (); ++idx)
	  if (left->winSizes[idx] != right->winSizes[idx])
	    return false;
	break;
      }
    }
    return true;
  } catch (...) {
    return false;
  }
}

bool
OptChannel::isEmpty () const {
  return inputType == GDT_Unknown || !channels[Original].size ();
}

bool
OptChannel::inputMatches (const GDALDataType &inputType, const DimChannel &inputBandsCount) const {
  return this->inputType == inputType && channels[Original].size () == inputBandsCount;
}

void
OptChannel::setType (const GDALDataType &inputType, const DimChannel &inputBandsCount) {
  this->inputType = inputType;
  remove (channels[Original]);
  BOOST_ASSERT (channels[Original].empty ());
  for (nextChannelUniqId = 0; nextChannelUniqId < inputBandsCount; ++nextChannelUniqId)
    channels [Original].push_back (new InputChannel (Original, nextChannelUniqId));
}

void
OptChannel::setName (const string &name) {
  this->name = name;
}

// ================================================================================
void
OptChannel::loadFile (const string &fileName) {
  static const set<string> inputChannelSet = {
					      channelTypeLabels [Original], channelTypeLabels [Ndvi], channelTypeLabels [Pantex], channelTypeLabels [Sobel], channelTypeLabels [AP],
					      channelTypeLabels [DAP], channelTypeLabels [Haar], channelTypeLabels [Stat]
  };

  DEF_LOG ("OptChannel::loadFile", "fileName: " << fileName);
  TiXmlDocument doc (fileName.c_str ());
  if (!doc.LoadFile ()) {
    LOG ("Can't load");
    throw invalid_argument (string ("Can't load channel config file ") + fileName +" (" + current_path ().string () + ")!");
  }
  string typeValue;
  int inputBandsCount (0);
  const TiXmlNode *triskeleRoot (doc.FirstChild ("Triskele"));
  if (!triskeleRoot) {
    LOG ("Not a channel config file!");
    throw invalid_argument ("Not a channel config file!");
  }
  const TiXmlElement *triskeleElem = doc.FirstChild ("Triskele")->ToElement ();
  if (triskeleElem->QueryStringAttribute ("type", &typeValue) != TIXML_SUCCESS ||
      triskeleElem->QueryIntAttribute ("bandCount", &inputBandsCount) != TIXML_SUCCESS)
    return;
  outComments.clear ();
  comments.clear ();
  for (const TiXmlNode *child = doc.FirstChild (); child; child = child->NextSibling ())
    if (child->Type () == TiXmlNode::TINYXML_COMMENT)
      outComments.push_back (child->Value ());

  int sp (0);
  bool mb (false);
  setType (GDALGetDataTypeByName (typeValue.c_str ()), inputBandsCount);
  triskeleElem->QueryIntAttribute ("spectralDepth", &sp);
  triskeleElem->QueryBoolAttribute ("mixedBand", &mb);
  mixedBandFlag = mb;
  spectralDepth = sp;
  triskeleElem->QueryStringAttribute ("name", &name);
  triskeleElem->QueryStringAttribute ("created", &created);
  triskeleElem->QueryStringAttribute ("modified", &modified);

  for (const TiXmlNode *child = triskeleElem->FirstChild (); child; child = child->NextSibling ())
    try {
      if (child->Type () == TiXmlNode::TINYXML_COMMENT) {
	comments.push_back (child->Value ());
	continue;
      }
      string token (child->Value ());
      if (inputChannelSet.find (token) == inputChannelSet.end ()) {
	// XXX trace
	continue;
      }
      ChannelType channelType = channelTypeMap.find (boost::algorithm::to_lower_copy (token))->second;

      const TiXmlElement *elem = child->ToElement ();
      bool doWrite (false);
      int inputId (0), from (0), from2 (0);
      vector<Size<2> > winSizes;
      int width (0), height (0);
      vector<double> thresholds;
      double threshold (0);
      string treeType, featureType (featureTypeLabels[FAP]), attributeType;
      bool dapWeight (false);
      string dapPos;
      vector<string> localComments;

      if (elem->QueryIntAttribute ("id", &inputId) != TIXML_SUCCESS)
	continue;
      LOG ("channelType: " << channelType << " id: " << inputId);
      elem->QueryBoolAttribute ("doWrite", &doWrite);
      if (channelType != Original &&
	  elem->QueryIntAttribute ("from", &from) != TIXML_SUCCESS)
	continue;
      if (channelType == Ndvi &&
	  elem->QueryIntAttribute ("from2", &from2) != TIXML_SUCCESS)
	continue;
      if (channelType == AP &&
	  (elem->QueryStringAttribute ("treeType", &treeType) != TIXML_SUCCESS ||
	   elem->QueryStringAttribute ("attributeType", &attributeType) != TIXML_SUCCESS))
	continue;
      elem->QueryStringAttribute ("featureType", &featureType);
      elem->QueryBoolAttribute ("dapWeight", &dapWeight);
      if (channelType == DAP &&
	  elem->QueryStringAttribute ("apOrigPos", &dapPos) != TIXML_SUCCESS)
	continue;

      switch (channelType) {
      case Original:
	channels [Original][inputId]->doWrite = doWrite;
	break;
      case Ndvi:
	addNdvi (inputId, from, from2, doWrite);
	break;
      case Pantex:
	addPantex (inputId, from, doWrite);
	break;
      case Sobel:
	addSobel (inputId, from, doWrite);
	break;
      case AP:
	for (const TiXmlNode *child2 = child->FirstChild ("Cut"); child2; child2 = child2->NextSibling ("Cut")) {
	  if (child2->Type () == TiXmlNode::TINYXML_COMMENT) {
	    localComments.push_back (child->Value ());
	    continue;
	  }
	  if (child2->ToElement ()->QueryDoubleAttribute ("threshold", &threshold) != TIXML_SUCCESS)
	    continue;
	  thresholds.push_back (threshold);
	}
	addThresholds (inputId, from, doWrite, localComments,
		       treeTypeMap.find (boost::algorithm::to_lower_copy (treeType))->second,
		       featureTypeMap.find (boost::algorithm::to_lower_copy (featureType))->second,
		       attributeMap.find (boost::algorithm::to_lower_copy (attributeType))->second,
		       thresholds);
	break;
      case DAP:
	addDap (inputId, from, doWrite, dapWeight,
		ApOrigPosMap.find (boost::algorithm::to_lower_copy (dapPos))->second);
	break;
      case Haar:
      case Stat:
	for (const TiXmlNode *child2 = child->FirstChild ("Size"); child2; child2 = child2->NextSibling ("Size")) {
	  if (child2->Type () == TiXmlNode::TINYXML_COMMENT) {
	    localComments.push_back (child->Value ());
	    continue;
	  }
	  if (child2->ToElement ()->QueryIntAttribute ("width", &width) != TIXML_SUCCESS ||
	      child2->ToElement ()->QueryIntAttribute ("height", &height) != TIXML_SUCCESS)
	    continue;
	  winSizes.push_back (Size<2> (width, height));
	}
	addTexture (channelType, inputId, from, doWrite, localComments, winSizes);
	break;
      }
    } catch (std::exception &e) {
      cerr << "error: " << e.what() << ". Loading file continue..." << endl;
    }
  updateChannelUniqId ();
}

void
OptChannel::saveFile (const string &fileName) const {
  DEF_LOG ("OptChannel::saveFile", "fileName: " << fileName);
  // XXX update (); //

  TiXmlDeclaration *decl = new TiXmlDeclaration ( "1.0", "", "" );
  TiXmlDocument doc;
  doc.LinkEndChild (decl);

  for (string comment : outComments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    doc.LinkEndChild (child);
  }
  TiXmlElement *triskeleElem = new TiXmlElement ("Triskele");
  doc.LinkEndChild (triskeleElem);
  triskeleElem->SetAttribute ("type", GDALGetDataTypeName (inputType));
  triskeleElem->SetAttribute ("bandCount", channels [Original].size ());
  if (spectralDepth)
    triskeleElem->SetAttribute ("spectralDepth", spectralDepth);
  if (mixedBandFlag)
    triskeleElem->SetAttribute ("mixedBand", "true");
  if (name.size ())
    triskeleElem->SetAttribute ("name", name);
  if (created.size ())
    triskeleElem->SetAttribute ("created", created);
  triskeleElem->SetAttribute ("modified", getCurrentTime ());

  vector<InputChannel *> circularDefinition;
  for (InputChannel *inputChannel : printableOrder (circularDefinition))
    inputChannel->writeXml (triskeleElem);
  for (string comment : comments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    triskeleElem->LinkEndChild (child);
  }
  doc.SaveFile (fileName.c_str ());
}

// ================================================================================
InputChannel *
OptChannel::findChannel (const DimChannel &channelId) {
  DEF_LOG ("InputChannel::findChannel", "channelId: " << channelId);
  for (DimChannel channelType = 0; channelType < channels.size (); ++channelType) {
    vector <InputChannel *> &dedicatedChannel (channels[channelType]);
    for (DimChannel i = 0; i < dedicatedChannel.size (); ++i) {
      LOG ("test: " << *dedicatedChannel[i]);
      if (dedicatedChannel[i]->inputId == channelId)
	return dedicatedChannel[i];
    }
  }
  LOG ("not found!");
  return nullptr;
}

void
OptChannel::updateChannelUniqId () {
  DimChannel newValue (0);
  for (DimChannel channelType = Original; channelType <= Stat; ++channelType) {
    vector <InputChannel *> &dedicatedChannel (channels[channelType]);
    for (DimChannel i = 0; i < dedicatedChannel.size (); ++i)
      newValue = DimChannel (max (newValue, 1+dedicatedChannel[i]->inputId));
  }
  nextChannelUniqId = newValue;
}

void
OptChannel::updateChannels () {
  DEF_LOG ("InputChannel::updateChannels", "");
  // XXX trier AP par input/tree/featureType/attributeType => ne pas recalculer l'arbre
  vector <InputChannel *> circularDefinition;
  vector <InputChannel *> orderedInputChannels (printableOrder (circularDefinition));
  remove (circularDefinition);
  resetTransient ();
  for (InputChannel *inputChannel : orderedInputChannels) {
    inputChannel->resetTransient ();
    inputChannel->inputId = nextChannelUniqId++;
    inputChannel->produce.clear ();
    for (InputChannel *parent : inputChannel->generateBy)
      parent->produce.push_back (inputChannel);
  }
  outputOrder.clear ();
  apOrder.clear ();
  dapOrder.clear ();
  haarOrder.clear ();
  statOrder.clear ();
  for (InputChannel *inputChannel : orderedInputChannels) {
    inputChannel->updateProdWrite ();
    for (InputChannel* child : inputChannel->produce) {
      if (!child->updateProdWrite ())
	continue;
      switch (child->channelType) {
      case Pantex: inputChannel->prodNewImage = true; break;
      case Sobel: inputChannel->prodNewImage = true; break;
      case AP:
	for (InputChannel* grandChild : child->produce) {
	  if (!grandChild->updateProdWrite ())
	    continue;
	  if (grandChild->channelType == DAP)
	    inputChannel->prodDap = true;
	  break;
	}
	break;
      case Haar: inputChannel->prodInetgralImage = true; break;
      case Stat: inputChannel->prodInetgralImage = inputChannel->prodDoubleInetgralImage = true; break;
      default:
	; //cout << "This channelType value is not handled\n"; exit(1);
      }
    }
    if (inputChannel->prodInetgralImage)
      inputChannel->integralImageRank = integralImageCount++;
    if (inputChannel->prodDoubleInetgralImage)
      inputChannel->doubleIntegralImageRank = doubleIntegralImageCount++;
    if (inputChannel->updateProdWrite ())
      switch (inputChannel->channelType) {
      case Original:
	if (inputChannel->prodDap && !inputChannel->doWrite)
	  inputChannel->origDapRank = origDapUsedCount++;
	inputChannel->typeRank = originalUsedCount++;
	break;
      case Ndvi: inputChannel->typeRank = ndviUsedCount++; break;
      case Pantex: inputChannel->typeRank = pantexUsedCount++; break;
      case Sobel: inputChannel->typeRank = sobelUsedCount++; break;
      case AP:
	apOrder[inputChannel->typeRank = apCount++] = inputChannel;
	break;
      case DAP:
	dapOrder[inputChannel->typeRank = dapCount++] = inputChannel;
	break;
      case Haar:
	haarOrder[inputChannel->typeRank = haarCount++] = inputChannel;
	break;
      case Stat:
	statOrder[inputChannel->typeRank = statCount++] = inputChannel;
	break;
      }
    if (inputChannel->doWrite) {
      inputChannel->outputRank = outputCount;
      outputOrder[outputCount] = inputChannel;
      switch (inputChannel->channelType) {
      case Original:
      case Ndvi:
      case Pantex:
      case Sobel:
	inputChannel->copyRank = copyCount++;
	++outputCount;
	break;
      case AP:
	outputCount += inputChannel->thresholds.size ();
	break;
      case DAP:
	outputCount += inputChannel->generateBy[0]->thresholds.size ();
	switch (inputChannel->apOrigPos) {
	case apOrigNoPos:	--outputCount; break;
	case apOrigPosBoth:	++outputCount; break;
	default:
	  ;//cout << "This apOrigPos value is not handled\n"; exit(1);
	}
	break;
      case Haar:
	outputCount += 4;
	break;
      case Stat:
	outputCount += 3;
	break;
      }
    }
  }
}

// ================================================================================
vector<InputChannel*>
OptChannel::printableOrder (vector<InputChannel*> &circularDefinition) const {
  DEF_LOG ("InputChannel::printableOrder", "original.size: " << channels [Original].size ());
  vector<InputChannel*> result (channels [Original]);
  queue<InputChannel*> toPrint;
  for (int channelType = Ndvi; channelType <= Stat; ++channelType)
    for (DimChannel i = 0; i < channels [channelType].size (); ++i)
      toPrint.push (channels [channelType][i]);
  result.reserve (result.size ()+toPrint.size ());

  queue<InputChannel*> nextStage;
  for (DimChannel toPrintSize = 0;;) {
    toPrintSize = toPrint.size ();
    if (!toPrintSize)
      return result;

    while (toPrint.size ())
      [&] {
	InputChannel* inputChannel = toPrint.front ();
	toPrint.pop ();
	for (InputChannel* parent : inputChannel->generateBy) {
	  if (find (result.begin (), result.end (), parent) != result.end ())
	    continue;
	  nextStage.push (inputChannel);
	  return;
	}
	result.push_back (inputChannel);
      } ();

    nextStage.swap (toPrint);
    if (toPrintSize == toPrint.size ()) {
      LOG ("channels loop or orphans");
      circularDefinition.clear ();
      for ( ; toPrint.size (); toPrint.pop ())
	circularDefinition.push_back (toPrint.front ());
      return result;
    }
  }
  // XXX never execute
}

vector<InputChannel*>
OptChannel::getActiveOrder () const {
  vector<InputChannel *> circularDefinition;
  vector <InputChannel *> orderedInputChannels (printableOrder (circularDefinition));
  vector<InputChannel*> result;
  result.reserve (orderedInputChannels.size ());
  for (InputChannel *inputChannel : orderedInputChannels)
    if (inputChannel->prodWrite)
      result.push_back (inputChannel);
  return result;
}

FeatureOutputType
OptChannel::getFeatureOutputType (const InputChannel &inputChannel) const {
  if (inputChannel.channelType == Stat)
    return DoubleFeature;
  switch (inputChannel.featureType) {
  case FArea:
  case FPerimeter:
    return DimFeature;
  case FZLength:
    return ChannelFeature;
  case FSTS:
  case FCompactness:
  case FComplexity:
  case FSimplicity:
  case FRectangularity:
  case FSD:
  case FSDW:
    // case FSDA:
  case FMoI:
    return DoubleFeature;
  case FMin:
  case FMax:
  case FMean:
  case FAP:
    break;
  }
  return PixelFeature;
}

set<FeatureOutputType>
OptChannel::getFeatureOutputType () const {
  set<FeatureOutputType> result;
  for (const InputChannel *inputChannelP : getActiveOrder ()) {
    const InputChannel &inputChannel (*inputChannelP);
    if (!inputChannel.doWrite)
      continue;
    result.insert (getFeatureOutputType (inputChannel));
  }
  return result;
}

GDALDataType
OptChannel::getOutputImageType (const GDALDataType &inputType) const {
  set<FeatureOutputType> featureSet = getFeatureOutputType ();
  for (set<FeatureOutputType>::iterator it=featureSet.begin(); it!=featureSet.end(); ++it)
    cout << ' ' << *it;
  cout << endl << endl;
  
  if (featureSet.size () != 1)
    return getGDALType (double (0));

  switch (*featureSet.begin ()) {
  case DimFeature:
    return getGDALType (DimImg (0));
  case ChannelFeature:
    return getGDALType (DimChannel (0));
  case DoubleFeature:
    return getGDALType (double (0));
  default:
    return inputType;
  }
}

// ================================================================================
void
OptChannel::selectInputs (const vector<int> &channelsId) {
  DEF_LOG ("OptChannel::selectInputs", "channelsId: " << channelsId.size ());
  selectedSet.clear ();
  selectedSet.reserve (channelsId.size ());
  for (int id : channelsId) {
    InputChannel *inputChannel = findChannel (id);
    if (inputChannel && inputChannel->channelType <= Pantex)
      // XXX vérifier double
      selectedSet.push_back (findChannel (id));
  }
}
void
OptChannel::selectTreeType (const TreeType &treeType) {
  selectedTreeType = treeType;
}
void
OptChannel::selectFeatureType (const FeatureType &featureType) {
  selectedFeatureType = featureType;
}
void
OptChannel::selectAttributeType (const AttributeType &attributeType) {
  this->selectedAttributeType = attributeType;
}
vector <InputChannel *>
OptChannel::selectedAP () {
  vector <InputChannel *> result;
  vector <InputChannel *> &dedicatedChannel (channels[AP]);
  for (InputChannel *inputChannel : selectedSet)
    for (InputChannel *apChannel : dedicatedChannel) {
      BOOST_ASSERT (apChannel->generateBy.size () == 1);
      if (apChannel->generateBy[0] == inputChannel &&
	  apChannel->treeType == selectedTreeType &&
	  apChannel->featureType == selectedFeatureType &&
	  apChannel->attributeType == selectedAttributeType)
	result.push_back (apChannel);
    }
  return result;
}

// ================================================================================
void
OptChannel::remove (const vector<InputChannel *> &toRemove) {
  DEF_LOG ("OptChannel::remove", "toRemove.size: " << toRemove.size ());
  vector<InputChannel *> immutableRemove (toRemove);
  for (InputChannel *inputChannel : immutableRemove) {
    vector<InputChannel *> &vector (channels[inputChannel->channelType]);
    vector.erase (find (vector.begin (), vector.end (), inputChannel));
  }
}
void
OptChannel::remove (const ChannelType &channelType) {
  remove (channels [channelType]);
  BOOST_ASSERT (channels [channelType].empty ());
}
void
OptChannel::removeCopy () {
  for (ChannelType channelType : {Original, Ndvi, Pantex, Sobel})
    for (DimChannel i = 0; i < channels [channelType].size (); ++i)
      channels [channelType][i]->doWrite = false;
}
void
OptChannel::removeThresholds () {
  DEF_LOG ("OptChannel::removeThresholds", "selectedTreeType: " << selectedTreeType << "selectedFeatureType: " << selectedFeatureType << " selectedAttributeType: " << selectedAttributeType);
  remove (selectedAP ());
}

// ================================================================================
void
OptChannel::addNdvi (const DimChannel &first, const DimChannel &last) {
  addNdvi (nextChannelUniqId++, first, last, false);
}
void
OptChannel::addNdvi (const DimChannel &id, const DimChannel &first, const DimChannel &last, const bool &doWrite) {
  DEF_LOG ("OptChannel::addNdvi", "id:" << id << " first: " << first << " last: " << last);
  if (first == last)
    throw invalid_argument ("ndvi input must be different");
  InputChannel *firstChannel (findChannel (first)), *lastChannel (findChannel (last));
  if (!firstChannel || !lastChannel)
    throw invalid_argument ("ndvi out of input");
  for (const InputChannel *registeredNdvi : channels [Ndvi])
    if (registeredNdvi->generateBy[0] == firstChannel && registeredNdvi->generateBy[1] == lastChannel)
      return;
  if (findChannel (id))
    throw invalid_argument ("ndvi id already used");
  LOG ("id:" << id << " first: " << *firstChannel << " last: " << *lastChannel);
  channels [Ndvi].push_back (new InputChannel (Ndvi, id, doWrite, boost::assign::list_of (firstChannel) (lastChannel)));
}

void
OptChannel::addPantex (const DimChannel &channel) {
  addPantex (nextChannelUniqId++, channel, false);
}
void
OptChannel::addPantex (const DimChannel &id, const DimChannel &channel, const bool &doWrite) {
  DEF_LOG ("OptChannel::addPantex", channel);
  InputChannel *originalChannel (findChannel (channel));
  if (!originalChannel)
    throw invalid_argument ("pantex out of input");
  for (const InputChannel *registeredPantex : channels [Pantex])
    if (registeredPantex->generateBy[0] == originalChannel)
      return;
  if (findChannel (id))
    throw invalid_argument ("pantex id already used");
  channels [Pantex].push_back (new InputChannel (Pantex, id, doWrite, boost::assign::list_of (originalChannel)));
}

void
OptChannel::addSobel (const DimChannel &channel) {
  addSobel (nextChannelUniqId++, channel, false);
}
void
OptChannel::addSobel (const DimChannel &id, const DimChannel &channel, const bool &doWrite) {
  DEF_LOG ("OptChannel::addSobel", channel);
  InputChannel *originalChannel (findChannel (channel));
  if (!originalChannel)
    throw invalid_argument ("sobel out of input");
  for (const InputChannel *registeredSobel : channels [Sobel])
    if (registeredSobel->generateBy[0] == originalChannel)
      return;
  if (findChannel (id))
    throw invalid_argument ("sobel id already used");
  channels [Sobel].push_back (new InputChannel (Sobel, id, doWrite, boost::assign::list_of (originalChannel)));
}

void
OptChannel::addCopy (const DimChannel &channel) {
  DEF_LOG ("OptChannel::addCopy", channel);
  InputChannel *inputChannel (findChannel (channel));
  if (!inputChannel)
    throw invalid_argument ("copy out of input");
  inputChannel->doWrite = true;
}

// ================================================================================
void
OptChannel::addThresholds (const string &thresholds) {
  DEF_LOG ("OptChannel::addThresholds", "thresholds: " << thresholds);
  if (thresholds.empty ()) {
    removeThresholds ();
    return;
  }
  vector <string> values;
  split (values, thresholds, is_any_of (","));
  vector <double> doubles (values.size ());
  transform (values.begin (), values.end (), doubles.begin (), [] (const string &val) { return stod (val); });
  addThresholds (doubles);
}

void
OptChannel::addThresholds (const vector<double> &thresholds) {
  DEF_LOG ("OptChannel::addThresholds", "thresholds.size: " << thresholds.size ());
  for (InputChannel *inputChannel : selectedSet)
    addThresholds (nextChannelUniqId++, true, selectedTreeType, selectedFeatureType, selectedAttributeType, thresholds, inputChannel, vector<string> ());
}
void
OptChannel::addThresholds (const DimChannel &id, const DimChannel &from, const bool &doWrite, const vector<string> &localComments,
			   const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds) {
  // XXX test from exist
  addThresholds (id, doWrite, treeType, featureType, attributeType, thresholds, findChannel (from), localComments);
}
void
OptChannel::addThresholds (const DimChannel &id, const bool &doWrite,
			   const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds,
			   InputChannel *inputChannel, const vector<string> &localComments) {

  DEF_LOG ("OptChannel::addThresholds", "id:" << id << " inputChannel:" << *inputChannel << " treeType: " << treeType << " featureType: " << featureType << " attributeType: " << attributeType);
  vector <InputChannel *> &dedicatedChannel (channels[AP]);
  for (InputChannel *apChannel : dedicatedChannel) {
    BOOST_ASSERT (apChannel->generateBy.size () == 1);
    if (apChannel->generateBy[0] == inputChannel &&
	apChannel->treeType == treeType &&
	apChannel->featureType == featureType &&
	apChannel->attributeType == attributeType) {
      apChannel->doWrite = doWrite;
      apChannel->thresholds = thresholds;
      apChannel->localComments = localComments;
      return;
    }
  }
  InputChannel *apChannel = new InputChannel (AP, id, doWrite, boost::assign::list_of (inputChannel));
  apChannel->treeType = treeType;
  apChannel->featureType = featureType;
  apChannel->attributeType = attributeType;
  apChannel->thresholds = thresholds;
  apChannel->localComments = localComments;
  dedicatedChannel.push_back (apChannel);
}

// ================================================================================
void
OptChannel::addDap (const bool &dapWeight, const ApOrigPos &apOrigPos) {
  for (InputChannel *inputChannel : selectedAP ()) {
    addDap (nextChannelUniqId++, true, dapWeight, apOrigPos, inputChannel);
    inputChannel->doWrite = false;
  }
}
void
OptChannel::addDap (const DimChannel &id, const DimChannel &from, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos) {
  DEF_LOG ("OptChannel::addDap", "id: " << id << " from: " << from << " dapWeight: " << dapWeight << " apOrigPos: " << apOrigPos);
  addDap (id, doWrite, dapWeight, apOrigPos, findChannel (from));
}
void
OptChannel::addDap (const DimChannel &id, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos, InputChannel *inputChannel) {
  DEF_LOG ("OptChannel::addDap", "inputChannel: " << *inputChannel);
  vector <InputChannel *> &dedicatedChannel (channels[DAP]);
  for (InputChannel *dapChannel : dedicatedChannel) {
    BOOST_ASSERT (dapChannel->generateBy.size () == 1);
    if (dapChannel->generateBy[0] == inputChannel) {
      dapChannel->doWrite = doWrite;
      dapChannel->dapWeight = dapWeight;
      dapChannel->apOrigPos = apOrigPos;
      return;
    }
  }
  InputChannel *dapChannel = new InputChannel (DAP, id, doWrite, boost::assign::list_of (inputChannel));
  dapChannel->dapWeight = dapWeight;
  dapChannel->apOrigPos = apOrigPos;
  dedicatedChannel.push_back (dapChannel);
  LOG (*dapChannel);
}

// ================================================================================
void
OptChannel::addHaar (const vector<Size<2> > &winSizes) {
  addTexture (Haar, winSizes);
}
void
OptChannel::addStat (const vector<Size<2> > &winSizes) {
  addTexture (Stat, winSizes);
}
void
OptChannel::addTexture (const ChannelType &channelType, const vector<Size<2> > &winSizes) {
  for (InputChannel *inputChannel : selectedSet)
    addTexture (channelType, nextChannelUniqId++, true, winSizes, inputChannel, vector<string> ());
}

void
OptChannel::addTexture (const ChannelType &channelType, const DimChannel &id, const DimChannel &from, const bool &doWrite, const vector<string> &localComments,
			const vector<Size<2> > &winSizes) {
  // XXX test from exist
  addTexture (channelType, id, doWrite, winSizes, findChannel (from), localComments);
}

void
OptChannel::addTexture (const ChannelType &channelType, const DimChannel &id, const bool &doWrite, const vector<Size<2> > &winSizes,
			InputChannel *inputChannel, const vector<string> &localComments) {
  DEF_LOG ("OptChannel::addTexture", channelType << " id:" << id << " inputChannel:" << *inputChannel);
  vector <InputChannel *> &dedicatedChannel (channels[channelType]);
  for (InputChannel *textChannel : dedicatedChannel) {
    BOOST_ASSERT (textChannel->generateBy.size () == 1);
    if (textChannel->generateBy[0] == inputChannel) {
      textChannel->doWrite = doWrite;
      textChannel->winSizes = winSizes;
      textChannel->localComments = localComments;
      return;
    }
  }
  InputChannel *textChannel = new InputChannel (channelType, id, doWrite, boost::assign::list_of (inputChannel));
  textChannel->winSizes = winSizes;
  textChannel->localComments = localComments;
  dedicatedChannel.push_back (textChannel);
}

// ================================================================================
void
OptChannel::memoryImpact (MemoryEstimation &memoryEstimation) {
  size_t mem (0);
  for (map<DimChannel, InputChannel *>::const_iterator it = outputOrder.cbegin ();
       it != outputOrder.cend (); ++it) {
    InputChannel &inputChannel (*it->second);
    if (!inputChannel.prodWrite)
      continue;
    switch (inputChannel.channelType) {
    case Original:
    case Ndvi:
    case Pantex:
    case Sobel:
      if (memoryEstimation.onFly)
	mem = max (mem, memoryEstimation.valuesSize);
      else
	mem += memoryEstimation.valuesSize;
      break;
    case AP:
      ArrayTreeBuilderMemoryImpact (memoryEstimation);
    case DAP:
      if (memoryEstimation.onFly)
	mem = max (mem, inputChannel.thresholds.size ()*memoryEstimation.valuesSize);
      else
	mem += inputChannel.thresholds.size ()*memoryEstimation.valuesSize;
      break;
    case Haar:
      if (memoryEstimation.onFly)
	mem = max (mem, 4*memoryEstimation.valuesSize);
      else
	mem += 4*memoryEstimation.valuesSize;
      break;
    case Stat:
      if (memoryEstimation.onFly)
	mem = max (mem, 3*memoryEstimation.valuesSize);
      else
	mem += 3*memoryEstimation.valuesSize;
      break;
    }
    if (pantexUsedCount > 0)
      mem += sizeof (double)*memoryEstimation.numberOfPixels;
    if (sobelUsedCount > 0)
      mem += sizeof (double)*memoryEstimation.numberOfPixels;
    mem +=
      inputChannel.prodInetgralImage * sizeof (double)*memoryEstimation.numberOfPixels+
      inputChannel.prodDoubleInetgralImage * sizeof (double)*memoryEstimation.numberOfPixels;
    memoryEstimation.addChanel (mem);
  }
}


// ================================================================================
ostream &
OptChannel::printOutput (ostream &out) const {
  out
    << "    name: " << name << endl
    << " created: " << created << endl
    << "modified: " << modified << endl
    << "   image: " << channels[Original].size () << " * " << GDALGetDataTypeName (inputType) << endl << endl;
  // XXX format nombre
  for (map<DimChannel, InputChannel *>::const_iterator it = outputOrder.cbegin ();
       it != outputOrder.cend (); ++it) {
    InputChannel &inputChannel (*it->second);
    DimChannel outputRank (inputChannel.outputRank);
    if (!inputChannel.doWrite)
      continue;
    switch (inputChannel.channelType) {
    case Original:
    case Ndvi:
    case Pantex:
    case Sobel:
      out << setw (4) << right << outputRank << ": " << inputChannel.channelType;
      if (inputChannel.generateBy.size ()) {
	out << " (";
	string sep = "";
	for (InputChannel *parent : inputChannel.generateBy) {
	  out << sep << parent->channelType << ":" << parent->typeRank;
	  sep = ", ";
	}
	out << ")";
      }
      out << endl;
      break;
    case AP:
      for (DimChannel channel = 0; channel < inputChannel.thresholds.size (); ++channel)
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << inputChannel.treeType << "/" << inputChannel.featureType << "/" << inputChannel.attributeType << " " << inputChannel.thresholds[channel] << endl;
      break;
    case DAP: {
      vector<DimChannel> dapIdx = inputChannel.getRelIdx ();
      InputChannel &parent (*inputChannel.generateBy[0]);
      vector<string> srcIdx (parent.thresholds.size ()+1);
      srcIdx [0] = channelTypeLabels[parent.channelType]+":"+boost::lexical_cast<string> (parent.typeRank);
      for (DimChannel channel = 0; channel < parent.thresholds.size (); ++channel)
	srcIdx [channel+1] = treeTypeLabels[parent.treeType] + "/" + featureTypeLabels[parent.featureType] + "/" + attributeLabels[parent.attributeType] + " " + boost::lexical_cast<string> (parent.thresholds[channel]);

      for (DimChannel i = 0; i < dapIdx.size ()/2; ++i) {
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << "/" << inputChannel.apOrigPos << " ";
	if (inputChannel.dapWeight)
	  out << srcIdx [0] << " * ";
	out << "|" << srcIdx [dapIdx[i*2]] << " - " << srcIdx [dapIdx[i*2+1]] << "|";
	out << endl;
      }
    }
      break;
    case Haar:
      for (Size<2> size : inputChannel.winSizes) {
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " hx" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " hx+hy" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " hy" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " hy-hx" << endl;
      }
      break;
    case Stat:
      for (Size<2> size : inputChannel.winSizes) {
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " mean" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " std" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChannel.channelType << " " << size.side [0] << "x" << size.side [1] << " ent" << endl;
      }
      break;
    }
  }
  return out;
}

// ================================================================================
ostream &
OptChannel::printState (ostream &out) const {
  if (modified.size () || name.size ())
    out << modified << " " << name;
  if (spectralDepth)
    out << " spectralDepth: " << spectralDepth;
  out << (mixedBandFlag ? " mixedBand" : "") << endl
      << " (c:" << copyCount << " o:" << originalUsedCount << " d" << origDapUsedCount
      << " n:" << ndviUsedCount << " p:" << pantexUsedCount << " s:" << sobelUsedCount
      << " a:" << apCount << " d:" << dapCount
      << " II:" << integralImageCount << " dII:" << doubleIntegralImageCount
      << " h:" << haarCount << " st:" << statCount
      << " O:" << outputCount << ")"
      << endl;
  for (DimChannel channelType = 0; channelType < channels.size (); ++channelType) {
    const vector <InputChannel *> &dedicatedChannel (channels[channelType]);
    for (DimChannel i = 0; i < dedicatedChannel.size (); ++i)
      out << *dedicatedChannel[i] << endl;
  }
  return out;
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const OptChannel &optChannel) {
  return optChannel.printState (out);
  // XXX affichage des options
}

// ================================================================================
