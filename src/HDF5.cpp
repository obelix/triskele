////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

/*
 * PatternSpectra.cpp
 *
 *  Created on: 14 nov. 2019
 *      Author: raimbaul
 */

#include <iostream>
#include <algorithm>    // std::min
#include <boost/lexical_cast.hpp>
#include <boost/chrono.hpp>

#include "H5Lpublic.h"

#include "TreeStats.hpp"
#include "HDF5.hpp"

#ifndef H5_NO_NAMESPACE
using namespace H5;
#endif

#if defined IMAGE_40BITS || defined IMAGE_LARGE
namespace obelix {
  namespace triskele {
    template<>
    int
    HDF5::write_array<DimImgPack> (const vector<DimImgPack> &arrayvect, const string &arrayname, const string &arraygroup) {
      // XXX todo
      BOOST_ASSERT (false);
    }
    template<>
    int
    HDF5::read_array<DimImgPack> (vector<DimImgPack> &arrayvect, const string &arrayname, const string &arraygroup) const {
      // XXX todo
      BOOST_ASSERT (false);
    }
  } // triskele
} // obelix
#endif // defined IMAGE_40BITS || defined IMAGE_LARGE

using namespace std;
using namespace boost::chrono;
using namespace obelix;
using namespace obelix::triskele;

template<typename T> struct get_hdf5_data_type {
  static H5::PredType type () {
    //static_assert(false, "Unknown HDF5 data type");
    return H5::PredType::NATIVE_DOUBLE;
  }
};

template<> struct get_hdf5_data_type<char>			{ H5::IntType	type { H5::PredType::NATIVE_CHAR }; };
template<> struct get_hdf5_data_type<int8_t>			{ H5::IntType	type { H5::PredType::NATIVE_INT8 }; };
template<> struct get_hdf5_data_type<uint8_t>			{ H5::IntType	type { H5::PredType::NATIVE_UINT8 }; };
template<> struct get_hdf5_data_type<int16_t>			{ H5::IntType	type { H5::PredType::NATIVE_INT16 }; };
template<> struct get_hdf5_data_type<uint16_t>			{ H5::IntType	type { H5::PredType::NATIVE_UINT16 }; };
template<> struct get_hdf5_data_type<int32_t>			{ H5::IntType	type { H5::PredType::NATIVE_INT32 }; };
template<> struct get_hdf5_data_type<uint32_t>			{ H5::IntType	type { H5::PredType::NATIVE_UINT32 }; };
template<> struct get_hdf5_data_type<int64_t>			{ H5::IntType	type { H5::PredType::NATIVE_INT64 }; };
template<> struct get_hdf5_data_type<uint64_t>			{ H5::IntType	type { H5::PredType::NATIVE_UINT64 }; };
template<> struct get_hdf5_data_type<float>			{ H5::FloatType	type { H5::PredType::NATIVE_FLOAT }; };
template<> struct get_hdf5_data_type<double>			{ H5::FloatType	type { H5::PredType::NATIVE_DOUBLE }; };
template<> struct get_hdf5_data_type<long double>		{ H5::FloatType	type { H5::PredType::NATIVE_LDOUBLE }; };
template<> struct get_hdf5_data_type<long long>			{ H5::IntType	type { H5::PredType::NATIVE_LLONG }; };
template<> struct get_hdf5_data_type<unsigned long long>	{ H5::IntType	type { H5::PredType::NATIVE_ULLONG }; };

#include "HDF5.tcpp"


// ========================================
int
HDF5::open_write (string filename) {
  try {
    Exception::dontPrint (); // Turn off the auto-printing when failure occurs
    h5_file = H5File (filename, H5F_ACC_TRUNC);
  } catch (FileIException & error) {
    error.printErrorStack ();
    return 1;
  }
  return 0;
}

int
HDF5::open_read_write (string filename) {
  try {
    Exception::dontPrint (); // Turn off the auto-printing when failure occurs
    h5_file = H5File (filename, H5F_ACC_RDWR);
  } catch (FileIException & error) {
    error.printErrorStack ();
    return 1;
  }
  return 0;
}

int
HDF5::open_read (string filename) {
  if (! H5File::isHdf5 (filename))
    return 2;
  try {
    Exception::dontPrint (); // Turn off the auto-printing when failure occurs
    h5_file = H5File (filename, H5F_ACC_RDONLY);
  } catch (FileIException & error) {
    error.printErrorStack ();
    return 1;
  }
  return 0;
}

int
HDF5::close_file () {
  try {
    // Turn off the auto-printing when failure occurs
    h5_file.close ();
  } catch (FileIException & error) {
    error.printErrorStack ();
    return 1;
  }
  return 0;
}

int
HDF5::set_compression_level (unsigned int level){
  if (level > 9)
    return 1;
  h5_compression_level = level;
  return 0;
}

int
HDF5::set_cache_size (size_t size){
  if (!size)
    return 1;
  h5_cache_size = size;
  return 0;
}


// ========================================
static herr_t
file_info (hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata) {
  hid_t group;
  auto group_names = reinterpret_cast<vector<string>*>(opdata);
  group = H5Gopen2 (loc_id, name, H5P_DEFAULT);
  group_names->push_back (name);
  H5Gclose (group);
  return 0;
}

vector<string>
HDF5::list () const {
  vector<string> result;
  //herr_t idx =
  H5Literate (h5_file.getId (), H5_INDEX_NAME, H5_ITER_INC, NULL, file_info, &result);
  return result;
}

// ========================================
template<typename T>
int
HDF5::write_array (const vector <T> &arrayvect, const string &arrayname, const string &arraygroup) {
  auto start = high_resolution_clock::now ();
  get_hdf5_data_type<T> hdf_data_type;
  //hdf_data_type.type.setOrder (H5T_ORDER_BE);
  const DataType &h5_datatype (hdf_data_type.type);
  Exception::dontPrint ();// Turn off the auto-printing when failure occurs
  Group h5_group;
  bool use_group (false);
  DataSet h5_dataset;
  DSetCreatPropList h5_prop;
  h5_prop.setDeflate (h5_compression_level);

  try {
    const hsize_t h5_dataset_dimensions[h5_rank] = { arrayvect.size () };
    const hsize_t h5_chunk_dimensions[h5_rank] = { min (h5_cache_size, arrayvect.size ()) };
    DataSpace h5_dataspace (h5_rank, h5_dataset_dimensions);
    h5_prop.setChunk (h5_rank, h5_chunk_dimensions);

    if (arraygroup.size () && arraygroup.compare ("/") != 0) {// pas de groupe
      try { // the group may already exist
	h5_group = h5_file.openGroup (arraygroup);
      } catch (FileIException & e) { // group dont exist
	h5_group = h5_file.createGroup (arraygroup);
      }
      use_group = true;
      h5_dataset = h5_group.createDataSet (arrayname, h5_datatype, h5_dataspace, h5_prop);
    } else
      h5_dataset = h5_file.createDataSet (arrayname, h5_datatype, h5_dataspace, h5_prop);

    h5_dataset.write (arrayvect.data (), h5_datatype);
    h5_dataset.close ();

    if (use_group)
      h5_group.close ();
  } catch (FileIException & error) { // catch failure caused by the H5File operations
    error.printErrorStack ();
    return 3;
  } catch (DataSetIException & error) { // catch failure caused by the DataSet operations
    error.printErrorStack ();
    return 4;
  } catch (DataSpaceIException & error) { // catch failure caused by the DataSpace operations
    error.printErrorStack ();
    return 5;
  } catch (ReferenceException & error) { // catch failure caused by Group operations
    error.printErrorStack ();
    return 6;
  }
  TreeStats::global.addTime (hdf5Write, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  return 0;
}

template<typename T>
int
HDF5::read_array (vector<T> &arrayvect, const string &arrayname, const string &arraygroup) const {
  auto start = high_resolution_clock::now ();
  get_hdf5_data_type<T> hdf_data_type;
  //hdf_data_type.type.setOrder (H5T_ORDER_BE);
  Exception::dontPrint (); // Turn off the auto-printing when failure occurs
  DataSet h5_dataset;
  Group h5_group;
  bool use_group (false);

  try {
    if (arraygroup.size () && arraygroup.compare ("/") != 0) {// pas de groupe
      h5_group = Group (h5_file.openGroup (arraygroup));
      use_group = true;
      h5_dataset = h5_group.openDataSet (arrayname);
    } else
      h5_dataset = h5_file.openDataSet (arrayname);

    DataType h5_datatype2 = h5_dataset.getDataType ();
    if (! (h5_datatype2 == hdf_data_type.type))
      return 1;
    DataSpace h5_dataspace = h5_dataset.getSpace ();
    if (h5_dataspace.getSimpleExtentNdims () != h5_rank)
      return 2;
    hsize_t h5_dataset_dimensions[h5_rank];
    h5_dataspace.getSimpleExtentDims (h5_dataset_dimensions);
    arrayvect.resize (h5_dataset_dimensions[0]);
    DataSpace mem_dataspace (h5_rank, h5_dataset_dimensions);
    h5_dataset.read (arrayvect.data (), hdf_data_type.type, mem_dataspace, h5_dataspace);
    h5_dataset.close ();
    h5_dataspace.close ();

    if (use_group)
      h5_group.close ();
  } catch (FileIException & error) { // catch failure caused by the H5File operations
    error.printErrorStack ();
    return 3;
  } catch (DataSetIException & error) { // catch failure caused by the DataSet operations
    error.printErrorStack ();
    return 4;
  } catch (DataSpaceIException & error) { // catch failure caused by the DataSpace operations
    error.printErrorStack ();
    return 5;
  } catch (ReferenceException & error) { // catch failure caused by Group operations
    error.printErrorStack ();
    return 6;
  }
  TreeStats::global.addTime (hdf5Read, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  return 0;
}

// ========================================

int
HDF5::write_string (const string &msg, const string &msgname, const string &msggroup) {
  auto start = high_resolution_clock::now ();
  Exception::dontPrint ();// Turn off the auto-printing when failure occurs
  Group h5_group;
  bool use_group (false);
  DataSet h5_dataset;
  DSetCreatPropList h5_prop;
  h5_prop.setDeflate (h5_compression_level);

  try {

    StrType h5_datatype (PredType::C_S1, msg.length () + 1); // + 1 for trailing zero
    DataSpace h5_dataspace (H5S_SCALAR);

    if (msggroup.size () && msggroup.compare ("/") != 0) {// pas de groupe
      h5_group = Group (h5_file.openGroup (msggroup));
      use_group = true;
      h5_dataset = h5_group.createDataSet (msgname, h5_datatype, h5_dataspace);
    } else
      h5_dataset = h5_file.createDataSet (msgname, h5_datatype, h5_dataspace);

    h5_dataset.write (msg, h5_datatype);
    h5_dataset.close ();

    if (use_group)
      h5_group.close ();
  } catch (FileIException & error) { // catch failure caused by the H5File operations
    error.printErrorStack ();
    return 3;
  } catch (DataSetIException & error) { // catch failure caused by the DataSet operations
    error.printErrorStack ();
    return 4;
  } catch (DataSpaceIException & error) { // catch failure caused by the DataSpace operations
    error.printErrorStack ();
    return 5;
  } catch (ReferenceException & error) { // catch failure caused by Group operations
    error.printErrorStack ();
    return 6;
  }
  TreeStats::global.addTime (hdf5Write, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  return 0;
}

int
HDF5::read_string (string &msg, const string &msgname, const string &msggroup) const {
  auto start = high_resolution_clock::now ();
  Exception::dontPrint (); // Turn off the auto-printing when failure occurs
  DataSet h5_dataset;
  Group h5_group;
  bool use_group (false);

  try {
    if (msggroup.size () && msggroup.compare ("/") != 0) {// pas de groupe
      h5_group = Group (h5_file.openGroup (msggroup));
      use_group = true;
      h5_dataset = h5_group.openDataSet (msgname);
    } else
      h5_dataset = h5_file.openDataSet (msgname);

    H5std_string h5_str;
    DataType h5_datatype = h5_dataset.getDataType ();
    DataSpace h5_dataspace = h5_dataset.getSpace ();
    h5_dataset.read (h5_str, h5_datatype, h5_dataspace);
    h5_dataset.close ();
    h5_dataspace.close ();

    msg = h5_str;
    if (use_group)
      h5_group.close ();
  } catch (FileIException & error) { // catch failure caused by the H5File operations
    error.printErrorStack ();
    return 3;
  } catch (DataSetIException & error) { // catch failure caused by the DataSet operations
    error.printErrorStack ();
    return 4;
  } catch (DataSpaceIException & error) { // catch failure caused by the DataSpace operations
    error.printErrorStack ();
    return 5;
  } catch (ReferenceException & error) { // catch failure caused by Group operations
    error.printErrorStack ();
    return 6;
  } catch (...) {
  }
  TreeStats::global.addTime (hdf5Read, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  return 0;
}

// ========================================
