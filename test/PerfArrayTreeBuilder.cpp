////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>    // std::sort

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "Border.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "TreeBuilder.hpp"
#include "IImage.hpp"
#include "AttributeProfiles.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/triskeleSort.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/Leader.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;

typedef uint16_t PixelT;
typedef uint16_t WeightT;

static const int GeoDimT (2);

// ========================================
void
perf (const Raster<PixelT, GeoDimT> &raster, const GraphWalker<GeoDimT> &graphWalker, const TreeType &treeType,
      const DimCore &coreCount, const DimCore &threadPerImage, const DimCore &tilePerThread) {
  ArrayTreeBuilder<PixelT, WeightT, GeoDimT> atb (raster, graphWalker, treeType, true, true);
  Tree<GeoDimT> tree (coreCount);
  WeightAttributes<PixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);
  tree.check (graphWalker.border);
}

// ========================================
int
main (int argc, char **argv, char **envp) {
  if (argc != 5) {
    cerr << "Usage: " << argv[0] << ": {MIN|MAX|MED|ALPHA|TOS} coreCount nbTest imageSize" << endl;
    exit (1);
  }
  string argType (argv[1]);
  TreeType treeType = MIN;
  if (argType == "MIN")
    treeType = MIN;
  else if (argType == "MAX")
    treeType = MAX;
  else if (argType == "MED")
    treeType = MED;
  else if (argType == "ALPHA")
    treeType = ALPHA;
  else if (argType == "TOS")
    treeType = TOS;
  else {
    cerr << "unknown type: " << argType << endl;
    exit (1);
  }
  const DimCore maxCoreCount (atol (argv[2])+1);
  const size_t nbTest (atol (argv[3]));
  const size_t maxImageSize (atol (argv[4]));

  const size_t forbiden = size_t (nbTest*.1);
  const size_t stepImg = max (size_t (1), size_t (maxImageSize*.1));

  srand (time (NULL));

  for (size_t card = stepImg; card <= maxImageSize; card += stepImg) {
    const size_t h = size_t (sqrt (card*1.1)*2/30)*10;
    const size_t w = card/h;
    Size<GeoDimT> size (w, h);
    Border<GeoDimT> border (size, false);
    GraphWalker<GeoDimT> graphWalker (border);
    int leafCount = size.getPixelsCount ();
    Raster<PixelT, GeoDimT> raster (size);

    cout << endl
	 << "card:" << card << "(" << w << "x" << h << "=" << (w*h) << ") argType:" << argType << " coreCount:" << (maxCoreCount-1) << " nbTest:" << nbTest << "(-" << (2*forbiden) << ")"<< endl
	 << "========================================" << flush;
    vector<vector<double>> times (maxCoreCount);
    for (DimCore coreCount = 1; coreCount < maxCoreCount ; ++coreCount)
      times[coreCount].reserve (nbTest);

    for (unsigned int test = 0; test < nbTest ; ++test) {
      if (!(test%50))
	cout << endl;
      for (int p = 0; p < leafCount; ++p)
	raster.getPixels ()[p] = PixelT (std::rand ());
      cout << "." << flush;
      for (DimCore coreCount = 1; coreCount < maxCoreCount ; ++coreCount) {
	TreeStats::global.reset ();
	perf (raster, graphWalker, treeType, coreCount, coreCount, 1);
	times[coreCount].push_back (ba::mean (TreeStats::global.getTimeStats (buildTreeStats)));
      }
    }
    cout << endl;
    for (DimCore coreCount = 1; coreCount < maxCoreCount ; ++coreCount) {
      vector<double> &tab (times[coreCount]);
      sort (tab.begin (), tab.end ());
      double average = accumulate (tab.begin ()+forbiden, tab.end ()-forbiden, .0) / nbTest;
      cout << "card:" << card << " treeType:" << argType << " coreCount:" << coreCount << " average: " << average << " throughput:" << size_t (card/average) << endl;
    }
  }
  return 0;
}

// ========================================
