////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "obelixGeo.hpp"
#include "Border.hpp"
#include "Appli/OptGenerator.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "TreeBuilder.hpp"
#include "QuadTree/QuadTreeBuilder.hpp"
//#include "XMLTree/XMLTreeBuilder.hpp"
#include "IImage.hpp"

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/triskeleSort.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/Leader.hpp"
#include "ArrayTree/Weight.hpp"

#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "Attributes/AttributesCache.hpp"

using namespace obelix;
using namespace obelix::triskele;

static const int GeoDimT (2);

// ================================================================================
template<typename OutPixelT>
void
writeBand (OptGenerator &optGenerator, const GDALDataType &outDataType, OutPixelT *pixels, DimChannel band) {
  if (!optGenerator.oneBandFlag) {
    // XXX bands
    optGenerator.outputImage.writeBand (pixels, band);
    return;
  }
  string outputBaseName  = boost::filesystem::path (optGenerator.outputImage.getFileName ()).replace_extension ("").string ();
  string outputExtension  = boost::filesystem::extension (optGenerator.outputImage.getFileName ());

  ostringstream fileNameStream;
  fileNameStream << outputBaseName << "-" << std::setfill ('0') << std::setw (3) << (band) << outputExtension;
  IImage<GeoDimT> outputImage (fileNameStream.str ());
  outputImage.createImage (Size<GeoDimT> (optGenerator.optCrop.size.side [0], optGenerator.optCrop.size.side [1], 1),
			   outDataType, 1, optGenerator.inputImage, optGenerator.optCrop.topLeft);

  outputImage.writeFrame (pixels, 0);
}


// ================================================================================
template<typename InPixelT, typename OutPixelT, typename APFunct>
void
apGenerator (OptGenerator &optGenerator, const GDALDataType &outDataType, const APFunct &setAP) {

  vector<TreeType> treeTypes;
  if (optGenerator.minTreeFlag)
    treeTypes.push_back (MIN);
  if (optGenerator.maxTreeFlag)
    treeTypes.push_back (MAX);
  if (optGenerator.medTreeFlag)
    treeTypes.push_back (MED);
  if (optGenerator.alphaTreeFlag)
    treeTypes.push_back (ALPHA);
  if (optGenerator.alphaTreeFlag)
    treeTypes.push_back (TOS);
  DimChannel treeTypesCard = treeTypes.size ();
  if (!treeTypesCard)
    cerr << "*** no tree type ! => copy mode" << endl;

  Border<GeoDimT> border (Size<GeoDimT> (optGenerator.optCrop.size.side [0], optGenerator.optCrop.size.side [1]), optGenerator.border);
  GraphWalker<GeoDimT> graphWalker (border, optGenerator.connectivity, optGenerator.countingSortCeil);
  DimImg leafCount = optGenerator.optCrop.size.getPixelsCount ();
  DimChannel maxThresholds = max (max (max (max (optGenerator.areaThresholds.size (), // max (
						 optGenerator.levelThresholds.size ()),
					    optGenerator.sdThresholds.size ()),
				       optGenerator.sdwThresholds.size ()),
				  // optGenerator.sdaThresholds.size ()),
				  optGenerator.moiThresholds.size ());
  vector <vector <OutPixelT> > allBands (maxThresholds, vector<OutPixelT> (leafCount, 0));

  DimChannel outputBandsCard = optGenerator.selectedBand.getSet ().size ()*
    (1+treeTypesCard*(optGenerator.areaThresholds.size ()+
		      optGenerator.levelThresholds.size ()+
		      optGenerator.sdThresholds.size ()+
		      optGenerator.sdwThresholds.size ()+
		      // optGenerator.sdaThresholds.size ()+
		      optGenerator.moiThresholds.size ()));
  if (!optGenerator.oneBandFlag) {
    Size<GeoDimT> size;
    // XXX dans frameCount dans size ?
    optGenerator.outputImage.createImage (Size<GeoDimT> (optGenerator.optCrop.size.side [0], optGenerator.optCrop.size.side [1]), outDataType, outputBandsCard, optGenerator.inputImage, optGenerator.optCrop.topLeft);
  }

  Raster<InPixelT, GeoDimT> raster;
  if (optGenerator.border) {
    DimChannel bandCount (optGenerator.inputImage.getBandCount ()); // -1); // XXX sans NDVI
    for (DimChannel band = 0; band < bandCount; ++band) {
      optGenerator.inputImage.readBand (raster, band, optGenerator.optCrop.topLeft, optGenerator.optCrop.size);
      for (DimImg idx = 0; idx < leafCount; ++idx)
	if (raster.getValue (idx))
	  border.clearBorder (idx);
    }
    //cerr << "XXX border: " << border.borderCount () << endl;
  }

  DimChannel channel = 0;
  for (DimChannel band : optGenerator.selectedBand.getSet ()) {
    optGenerator.inputImage.readBand (raster, band, optGenerator.optCrop.topLeft, optGenerator.optCrop.size);
    writeBand (optGenerator, outDataType, raster.getPixels (), channel++);

    for (TreeType treeType : treeTypes) {
      ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> atb (raster, graphWalker, treeType, true, false, false);
      Tree<GeoDimT> tree (optGenerator.coreCount);
      WeightAttributes<InPixelT, GeoDimT> weightAttributes (tree, getDecrFromTreetype (treeType));
      atb.buildTree (tree, weightAttributes);
      AttributesCache<InPixelT, GeoDimT> attributesCache (tree, graphWalker, raster, weightAttributes);


      AttributeProfiles<OutPixelT, GeoDimT> attributeProfiles (tree);

      setAP (attributeProfiles, atb, attributesCache);

      if (optGenerator.levelThresholds.size ()) {
	vector<InPixelT> thresholds (attributesCache.getWeight ().getConvertedThresholds (optGenerator.levelThresholds));
	attributesCache.getWeight ().cut (allBands, attributeProfiles, thresholds);
      	for (DimChannel c = 0; c < optGenerator.levelThresholds.size (); ++c, ++channel)
      	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      }
      if (optGenerator.areaThresholds.size ()) {
	attributesCache.getArea ().cut (allBands, attributeProfiles, optGenerator.areaThresholds);
	for (DimChannel c = 0; c < optGenerator.areaThresholds.size (); ++c, ++channel)
	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      }
      if (optGenerator.sdThresholds.size ()) {
      	attributesCache.getSD ().cut (allBands, attributeProfiles, optGenerator.sdThresholds);
      	for (DimChannel c = 0; c < optGenerator.sdThresholds.size (); ++c, ++channel)
      	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      }
      if (optGenerator.sdwThresholds.size ()) {
	attributesCache.getSDW ().cut (allBands, attributeProfiles, optGenerator.sdwThresholds);
	for (DimChannel c = 0; c < optGenerator.sdwThresholds.size (); ++c, ++channel)
	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      }
      // if (optGenerator.sdaThresholds.size ()) {
      // 	attributesCache.getSDA ().cut (allBands, attributeProfiles, optGenerator.sdaThresholds);
      // 	for (DimChannel c = 0; c < optGenerator.sdaThresholds.size (); ++c, ++channel)
      // 	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      // }
      if (optGenerator.moiThresholds.size ()) {
	attributesCache.getMoI ().cut (allBands, attributeProfiles, optGenerator.moiThresholds);
	for (DimChannel c = 0; c < optGenerator.moiThresholds.size (); ++c, ++channel)
	  writeBand (optGenerator, outDataType, allBands[c].data (), channel);
      }
    }
  }

  cerr << endl << "*** apGenerator done!" << endl
       << TreeStats::global.printDim () << endl
       << TreeStats::global.printTime ();
}

// ================================================================================
template<typename InPixelT>
void
outTypeSelection (OptGenerator &optGenerator) {
  switch (optGenerator.featureType) {
  case FMean:
    apGenerator<InPixelT, InPixelT> (optGenerator, optGenerator.inputImage.getDataType (),
				     [] (AttributeProfiles<InPixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
				       attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMean ().getValues ());
				     });
    break;
  case FArea:
    apGenerator<InPixelT, DimImgPack> (optGenerator, GDT_UInt32,
				       [] (AttributeProfiles<DimImgPack, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
					 attributeProfiles.setValues (1, attributesCache.getArea ().getValues ());
				       });
    break;
  case FSD:
    apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
  				   [] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
				     attributeProfiles.setValues (0., attributesCache.getSD ().getValues ());
  				   });
    break;
  case FSDW:
    apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
  				   [] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
				     attributeProfiles.setValues (0., attributesCache.getSDW ().getValues ());
  				   });
    break;
    // case FSDA:
    //   apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
    // 				   [] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
    // 				     attributeProfiles.setValues (0., attributesCache.getSDA ().getValues ());
    // 				   });
    //   break;
  case FMoI:
    apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
  				   [] (AttributeProfiles<double, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
				     attributeProfiles.setValues (0., attributesCache.getMoI ().getValues ());
  				   });
    break;
  default:
    apGenerator<InPixelT, InPixelT> (optGenerator, optGenerator.inputImage.getDataType (),
				     [] (AttributeProfiles<InPixelT, GeoDimT> &attributeProfiles, ArrayTreeBuilder<InPixelT, InPixelT, GeoDimT> &atb, AttributesCache<InPixelT, GeoDimT> attributesCache) {
				       atb.setAttributProfiles (attributeProfiles);
				     });
  }
}

// ================================================================================
int
main (int argc, char** argv, char** envp) {
  OptGenerator optGenerator (argc, argv);
  DEF_LOG ("main", "");

  switch (optGenerator.inputImage.getDataType ()) {
  case GDT_Byte:
    outTypeSelection<uint8_t> (optGenerator); break;
  case GDT_UInt16:
    outTypeSelection<uint16_t> (optGenerator); break;
  case GDT_Int16:
    outTypeSelection<int16_t> (optGenerator); break;
  case GDT_UInt32:
    outTypeSelection<uint32_t> (optGenerator); break;
  case GDT_Int32:
    outTypeSelection<int32_t> (optGenerator); break;
  case GDT_Float32:
    outTypeSelection<float> (optGenerator); break;
  case GDT_Float64:
    outTypeSelection<double> (optGenerator); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}

// ================================================================================
