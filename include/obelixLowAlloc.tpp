////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_LowAlloc_tpp
#define _Obelix_LowAlloc_tpp

// ================================================================================
template<typename Type>
inline
LowAlloc<Type>::LowAlloc (const DimImg &blockSize)
  : LowAlloc<Type> (sizeof (Type), blockSize) {
}

template<typename Type>
inline
LowAlloc<Type>::LowAlloc (const size_t sizeType, const DimImg &blockSize)
  : sizeType (sizeType),
    blockSize (blockSize > 0x400 ? (blockSize + DimImg (0x200)) & ~DimImg (0x3FF) : blockSize),
    capacity (0),
    maxUsed (0),
#ifdef DEBUG
    peaskIndex (0),
#endif // DEBUG
    available (),
    garbage () {
  // DEF_LOG ("LowAlloc::LowAlloc", "blockSize:" << blockSize);
}

template<typename Type>
inline DimImg
LowAlloc<Type>::get () {
  // DEF_LOG ("LowAlloc::get", "available:" << available << " maxUsed:" << maxUsed << " capacity:" << capacity);
  if (available) {
    // LOG ("garbage:" << garbage[available-1]);
    return garbage[--available];
  }
  if (maxUsed == capacity) {
    capacity += blockSize;
    // LOG ("capacity:" << capacity);
    container.resize (capacity*sizeType);
    garbage.resize (capacity);
  }
  // LOG ("maxUsed:" << maxUsed);
#ifdef DEBUG
  if (maxUsed > peaskIndex)
    peaskIndex = maxUsed;
#endif // DEBUG
  return maxUsed++;
}

template<typename Type>
inline void
LowAlloc<Type>::give (const DimImg &idx) {
  // DEF_LOG ("LowAlloc::give", "idx:" << idx << " available:" << available);
  garbage[available++] = idx;
}

template<typename Type>
inline Type &
LowAlloc<Type>::operator[] (const DimImg &idx) {
  return * (Type *) &container[idx*sizeType];
}

template<typename Type>
inline const Type &
LowAlloc<Type>::operator[] (const DimImg &idx) const {
  return * (const Type *) &container[idx*sizeType];
}

// ================================================================================
template<typename Type>
inline DimImg
LowAlloc<Type>::getCapacity () const {
  return capacity;
}

template<typename Type>
inline DimImg
LowAlloc<Type>::getMaxUsed () const {
  return maxUsed;
}

template<typename Type>
inline DimImg
LowAlloc<Type>::getAvailable () const {
  return available;
}

template<typename Type>
inline DimImg
LowAlloc<Type>::getPeakUse () const {
#ifdef DEBUG
  return peaskIndex+1;
#else // DEBUG
  // XXX no log
  return 0;
#endif // DEBUG
}

// ================================================================================
#endif // _Obelix_LowAlloc_tpp
