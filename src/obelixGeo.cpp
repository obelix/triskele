////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software  is  a  computer  program whose  purpose  is  to  build //
// hierarchical representation for remote sensing immages.		  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "obelixGeo.hpp"
using namespace obelix;
#include "obelixGeo.tcpp"

// ================================================================================
const Point<2> obelix::NullPoint2D = Point<2> ();
const Size<2> obelix::NullSize2D = Size<2> ();
const Rect<2> obelix::NullRect2D = Rect<2> ();

// ================================================================================
const Point<3> obelix::NullPoint3D = Point<3> ();
const Size<3> obelix::NullSize3D = Size<3> ();
const Rect<3> obelix::NullRect3D = Rect<3> ();

// ================================================================================
template<>
Point<2>::Point (const Size<2> &size, const DimImg &idx) {
  coord [0] = idx % size.side [0];
  coord [1] = idx / size.side [0];
}
template<>
Point<3>::Point (const Size<3> &size, const DimImg &idx) {
  coord [0] = idx % size.side [0];
  DimImg yz (idx / size.side [0]);
  coord [1] = yz % size.side [1];
  coord [2] = yz / size.side [1];
}

template<>
Point<2>::Point (const Point<2> &orig, const Size<2> &moveVector) {
  coord [0] = orig.coord [0] + moveVector.side [0];
  coord [1] = orig.coord [1] + moveVector.side [1];
}
template<>
Point<3>::Point (const Point<3> &orig, const Size<3> &moveVector) {
  coord [0] = orig.coord [0] + moveVector.side [0];
  coord [1] = orig.coord [1] + moveVector.side [1];
  coord [2] = orig.coord [2] + moveVector.side [2];
}

// ================================================================================
template<int GeoDimT>
ostream &
obelix::operator << (ostream &out, const Point<GeoDimT> &p) {
  out << "(" << p.coord [0];
  for (int i = 1; i < GeoDimT; ++i)
    out << "," << p.coord [i];
  return out << ")";
}

// ================================================================================
template<>
ostream &
obelix::operator << (ostream &out, const DeltaPoint<2> &dp) {
  return out << "(" << dp.delta.x << "," << dp.delta.y << ")";
}
template<>
ostream &
obelix::operator << (ostream &out, const DeltaPoint<3> &dp) {
  return out << "(" << dp.delta.x << "," << dp.delta.y << "," << dp.delta.z << ")";
}

// ================================================================================
template<int GeoDimT>
DimImg
Size<GeoDimT>::getPixelsCount () const {
  DimImg result = side [0];
  for (int i = 1; i < GeoDimT; ++i)
    result *= side [i];
  return result;
}
template<>
DimImg
Size<2>::getPixelsCount () const {
  return getFramePixelsCount ();
}
template<>
DimImg
Size<3>::getPixelsCount () const {
  return getFramePixelsCount ()*side[2];
}

template<int GeoDimT>
Size<GeoDimT>
Size<GeoDimT>::getDoubleHeight () const {
  Size<GeoDimT> result;
  for (int i = 0; i < GeoDimT; ++i)
    result.side [i] = side [i];
  result.side [2] <<= 1;
  return result;
}
template<>
Size<2>
Size<2>::getDoubleHeight () const {
  return Size<2> (side [0], side [1] << 1);
}

template<int GeoDimT>
DimImg
Size<GeoDimT>::getOffsetFrame (const DimSideImg &frameId) const {
  return getFramePixelsCount () * frameId;
}
template<>
DimImg
Size<2>::getOffsetFrame (const DimSideImg &frameId) const {
  return 0;
}

template<int GeoDimT>
ostream &
obelix::operator << (ostream &out, const Size<GeoDimT> &s) {
  out << "[" << s.side [0];
  for (int i = 1; i < GeoDimT; ++i)
    out << "," << s.side [i];
  return out << "]";
}

// ================================================================================
template<int GeoDimT>
ostream &
obelix::operator << (ostream &out, const Rect<GeoDimT> &r) {
  out << "[" << r.point.coord [0];
  for (int i = 1; i < GeoDimT; ++i)
    out << "," << r.point.coord [i];
  out << " / " << r.size.side [0];
  for (int i = 1; i < GeoDimT; ++i)
    out << "x" << r.size.side [i];
  return out << "]";
}

// ================================================================================

